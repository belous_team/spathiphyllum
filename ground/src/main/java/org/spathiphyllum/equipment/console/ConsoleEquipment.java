/*
 * Copyright (C) 2018-2018 The Project Spathiphyllum Authors.
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
package org.spathiphyllum.equipment.console;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

import org.spathiphyllum.DNA;
import org.spathiphyllum.DescriptionDNA;
import org.spathiphyllum.EnumTypes;
import org.spathiphyllum.Environment;
import org.spathiphyllum.Flower;
import org.spathiphyllum.Equipment;
import org.spathiphyllum.InitializeException;
import org.spathiphyllum.Petal;
import org.spathiphyllum.Seed;
import org.spathiphyllum.SeedDomain;

/**
 * @author obelous
 *
 */
public class ConsoleEquipment implements Equipment {
    private static final Set<DescriptionDNA> INITIALIZE_DNA = new HashSet<>();
    private static final Set<DescriptionDNA> INPUT_DNA = new HashSet<>();
    private static final Set<DescriptionDNA> OUTPUT_DNA = new HashSet<>();
    private static SeedDomain domain;
    /**
     * Here is stored request mapping.
     * More than one Flower can run with this Equipment,
     * so this mapping allow the Equipment to recognize which Flower will obtain a request
     */
    private static final Map<String, Petal> requestMapping = new HashMap<String, Petal>();
    /**
     * 
     */
    public ConsoleEquipment() {
        INITIALIZE_DNA.add(new DescriptionDNA("welcome", "This is a string that will be displayed on registration", EnumTypes.STRING, true));
        INITIALIZE_DNA.add(new DescriptionDNA("command_prompt", "This is a string that used in a command-line interface to indicate readiness to accept commands", EnumTypes.STRING, false));
        INPUT_DNA.add(new DescriptionDNA("console_out", "This is a string that is displayed to user", EnumTypes.STRING, true));
        OUTPUT_DNA.add(new DescriptionDNA("console_in", "This is a string that is typed by user and is transferred to flow", EnumTypes.STRING, true));
    }

    /* (non-Javadoc)
     * @see org.spathiphyllum.ground.Measurement#measure(java.lang.String, java.util.Map)
     */
    @Override
    public void measure(String prefix, Map<String, Object> measure) {
        // TODO Auto-generated method stub

    }

    /* (non-Javadoc)
     * @see org.spathiphyllum.ground.GeneticEngineering#init(org.spathiphyllum.ground.Environment)
     */
    @Override
    public void init(Environment environment) throws InitializeException {
        // TODO Auto-generated method stub

    }

    /* (non-Javadoc)
     * @see org.spathiphyllum.ground.GeneticEngineering#initializeDNA()
     */
    @Override
    public Set<DescriptionDNA> initializeDNA() {
        return INITIALIZE_DNA;
    }

    /* (non-Javadoc)
     * @see org.spathiphyllum.ground.GeneticEngineering#inputDNA()
     */
    @Override
    public Set<DescriptionDNA> inputDNA() {
        return INITIALIZE_DNA;
    }

    /* (non-Javadoc)
     * @see org.spathiphyllum.ground.GeneticEngineering#outputDNA()
     */
    @Override
    public Set<DescriptionDNA> outputDNA() {
        return OUTPUT_DNA;
    }

    /* (non-Javadoc)
     * @see org.spathiphyllum.ground.GeneticEngineering#clone(org.spathiphyllum.ground.SeedDomain)
     */
    @Override
    public Equipment clone(SeedDomain domain) throws InitializeException {
        //TODO need security check 
        if(domain == null){
            this.domain = domain;
            return this;
        }
        throw new InitializeException("This equipment can be used one domain only");
    }

    /* (non-Javadoc)
     * @see org.spathiphyllum.ground.GeneticEngineering#isThreadSafe()
     */
    @Override
    public boolean isThreadSafe() {
        return false;
    }

    /* (non-Javadoc)
     * @see org.spathiphyllum.ground.GeneticEngineering#shutdown()
     */
    @Override
    public void shutdown() {

    }

    @Override
    public void registration(Flower flower) {
        // TODO Auto-generated method stub
        
    }

}
