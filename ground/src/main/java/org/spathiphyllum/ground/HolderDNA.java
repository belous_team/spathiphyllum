/*
 * Copyright (C) 2018-2018 The Project Spathiphyllum Authors.
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
package org.spathiphyllum.ground;

import org.spathiphyllum.DNA;
import org.spathiphyllum.SpathiphyllumRuntimeException;

/**
 * The mutable holder of DNA
 * 
 * @author obelous
 *
 */
class HolderDNA {
    private DNA dna;
    final String dnaPath;
    HolderDNA(DNA dna) {
        this.dnaPath = dna.pathDNA();
    }
    DNA getDna() {
        return dna;
    }

    void setDna(DNA dna) {
        if(!dnaPath.equals(dna.pathDNA())){
            throw new SpathiphyllumRuntimeException("Incorrect DNA");
        }
        this.dna = dna;
    }
    @Override
    public int hashCode() {
        return 31 + ((dnaPath == null) ? 0 : dnaPath.hashCode());
    }
    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;
        HolderDNA other = (HolderDNA) obj;
        if (dnaPath == null) {
            if (other.dnaPath != null)
                return false;
        } else if (!dnaPath.equals(other.dnaPath))
            return false;
        return true;
    }
}
