/*
 * Copyright (C) 2018-2018 The Project Spathiphyllum Authors.
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
package org.spathiphyllum.ground.tools;

import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;
import java.net.URI;
import java.net.URL;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardOpenOption;
import java.text.NumberFormat;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Properties;
import java.util.Set;
import java.util.zip.ZipEntry;
import java.util.zip.ZipInputStream;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.xpath.XPath;
import javax.xml.xpath.XPathConstants;
import javax.xml.xpath.XPathExpressionException;
import javax.xml.xpath.XPathFactory;

import org.spathiphyllum.LoadResourceException;
import org.spathiphyllum.SpathiphyllumRuntimeException;
import org.spathiphyllum.el.tools.HelperStringParser;
import org.w3c.dom.Document;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.EntityResolver;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;
import org.xml.sax.ext.EntityResolver2;
import org.yaml.snakeyaml.Yaml;

/**
 * @author obelous
 *
 */
public class PropertiesLoader {
    public static final String[] FILE_EXT_YAML = {".yaml",".yml"};
    public static final String[] FILE_EXT_JSON = {".json"};
    public static final String[] FILE_EXT_XML = {".xml"};
    public static final String[] FILE_EXT_PROPERTY = {".properties",".property"};
    private static final String CLASS_PATH = "classpath:";
    
    public static final InputStream EMPTY_INPUT_STREAM = new InputStream() {
        @Override
        public int read() {
            return -1;  // end of stream
        }
    };
    /**
     * Loads a file, parses it and returns as a map<br>
     * 
     * @param path
     * @param charset
     * @return
     * @throws LoadResourceException
     */
    public static Map<String,Object> loadProperty(String path, Charset charset) throws LoadResourceException {
        try(Reader reader = openReader(path, charset)){
            if(isYamlResource(path)){
                return loadPropertiesAsYaml(reader);
            }else if(isJsonResource(path)){
                return loadPropertiesAsJson(reader);
            }else if(isPropertyResource(path)){
                return loadPropertiesDefault(reader);
            }else{
                throw new LoadResourceException("Unknown resource");
            }
        } catch (IOException e) {
            throw new LoadResourceException("Cannot load properties: "+path, e);
        }
    }
    public static boolean isYamlResource(String path){
        if(!HelperStringParser.isStringEmptySkipSpace(path)){
            for(String s: FILE_EXT_YAML){
                if(path.endsWith(s))
                    return true;
            }
        }
        return false;
    }
    public static boolean isJsonResource(String path){
        if(!HelperStringParser.isStringEmptySkipSpace(path)){
            for(String s: FILE_EXT_JSON){
                if(path.endsWith(s))
                    return true;
            }
        }
        return false;
    }
    public static boolean isXmlResource(String path){
        if(!HelperStringParser.isStringEmptySkipSpace(path)){
            for(String s: FILE_EXT_XML){
                if(path.endsWith(s))
                    return true;
            }
        }
        return false;
    }
    public static boolean isPropertyResource(String path){
        if(!HelperStringParser.isStringEmptySkipSpace(path)){
            for(String s: FILE_EXT_PROPERTY){
                if(path.endsWith(s))
                    return true;
            }
        }
        return false;
    }
    /**
     * Parses a Reader of properties file and returns a map
     * @param reader
     * @return
     * @throws IOException
     */
    public static Map<String,Object> loadPropertiesDefault(Reader reader) throws IOException {
        Properties properties = new Properties();
        properties.load(reader);
        return convertToMap(properties);
    }
    /**
     * Parses a Reader of YAML  and returns a map
     * @param reader
     * @return
     */
    public static Map<String,Object> loadPropertiesAsYaml(Reader reader){
        Yaml yaml = new Yaml();
        Map<String,Object> result = yaml.load(reader);
        return result != null ? result : Collections.emptyMap();
    }
    /**
     * Parses a Reader of JSON and returns a map
     * @param reader
     * @return
     * @throws IOException
     */
    @SuppressWarnings("unchecked")
    public static Map<String,Object> loadPropertiesAsJson(Reader reader) throws IOException {
        return JsonHelper.objectMapper().readValue(reader, Map.class);
    }
    /**
     * Parse a Reader of XML and returns a Document
     * @param reader
     * @return
     * @throws IOException
     * @throws SAXException
     * @throws ParserConfigurationException
     */
    public static Document loadXmlDocument(Reader reader) throws IOException, SAXException, ParserConfigurationException {
        DocumentBuilder builder = DocumentBuilderFactory.newInstance().newDocumentBuilder();
        builder.setEntityResolver(ENTITY_RESOLVER);
        return builder.parse(new InputSource(reader));
    }
    /**
     * Evaluate an XPath expression in the specified context and return the String
     * @param document
     * @param xpath
     * @return
     * @throws XPathExpressionException
     */
    public static String xPathStrig(Document document, String xpath) throws XPathExpressionException {
        XPathFactory xpfactory = XPathFactory.newInstance();
        XPath xPath = xpfactory.newXPath();
        return (String) xPath.evaluate(xpath, document, XPathConstants.STRING);
    }
    /**
     * Evaluate an XPath expression in the specified context and return the Number
     * @param document
     * @param xpath
     * @return
     * @throws XPathExpressionException
     */
    public static Number xPathNumber(Document document, String xpath) throws XPathExpressionException {
        XPathFactory xpfactory = XPathFactory.newInstance();
        XPath xPath = xpfactory.newXPath();
        return (Number) xPath.evaluate(xpath, document, XPathConstants.NUMBER);
    }
    /**
     * Evaluate an XPath expression in the specified context and return the Boolean
     * @param document
     * @param xpath
     * @return
     * @throws XPathExpressionException
     */
    public static Boolean xPathBoolean(Document document, String xpath) throws XPathExpressionException {
        XPathFactory xpfactory = XPathFactory.newInstance();
        XPath xPath = xpfactory.newXPath();
        return (Boolean) xPath.evaluate(xpath, document, XPathConstants.BOOLEAN);
    }
    /**
     * Evaluate an XPath expression in the specified context and return the Node
     * @param document
     * @param xpath
     * @return
     * @throws XPathExpressionException
     */
    public static Node xPathNode(Document document, String xpath) throws XPathExpressionException {
        XPathFactory xpfactory = XPathFactory.newInstance();
        XPath xPath = xpfactory.newXPath();
        return (Node) xPath.evaluate(xpath, document, XPathConstants.NODE);
    }
    /**
     * Evaluate an XPath expression in the specified context and return the NodeList
     * @param document
     * @param xpath
     * @return
     * @throws XPathExpressionException
     */
    public static NodeList xPathNodeList(Document document, String xpath) throws XPathExpressionException {
        XPathFactory xpfactory = XPathFactory.newInstance();
        XPath xPath = xpfactory.newXPath();
        return (NodeList) xPath.evaluate(xpath, document, XPathConstants.NODESET);
    }
    @SuppressWarnings("unchecked")
    private static Map<String, Object> convertToMap(Properties properties) {
        Map<String,Object> map = new HashMap<>();
        if(!properties.isEmpty()){
            Set<String> keys = properties.stringPropertyNames();
            for(String key : keys){
                String value = properties.getProperty(key);
                if(value != null){
                    //skip all not string value
                    String[] s = key.split("\\.");
                    Map<String,Object> last = map;
                    for(int i=0; i < s.length-1; i++){
                        if(last.get(s[i]) == null){
                            map.put(s[i], new HashMap<>());
                            last = (Map<String, Object>) map.get(s[i]);
                        }else{
                            last = (Map<String, Object>) map.get(s[i]);
                        }
                    }
                    last.put(s[s.length -1], value);
                }
            }
        }
        return map;
    }
    @SuppressWarnings("unchecked")
    public static Object findProperty(String key, Map<String,Object> map){
        if(key == null || map == null){
            throw new SpathiphyllumRuntimeException("Parameter is null");
        }
        Object result = map.get(key);
        if(result == null){
            String[] s = key.split("\\.");
            Map<String, Object> last = map;
            for(int i = 0; i < s.length; i++){
                result = last.get(s[i]);
                if(result instanceof Map){
                    last = (Map<String, Object>) result;
                }else{
                    break;
                }
            }
        }
        return result;
    }
    @SuppressWarnings("unchecked")
    public static Set<String> allKeys(String prefix, Map<String,Object> map){
        Set<String> result = new HashSet<>();
        for(Entry<String, Object> e : map.entrySet()){
            String key = (prefix == null || prefix.length() == 0) ? e.getKey()
                    : new StringBuilder(prefix).append('.').append(e.getKey()).toString();
            if(e.getValue() instanceof Map){
                result.addAll(allKeys(key, (Map<String, Object>) e.getValue()));
            }else{
                result.add(key);
            }
        }
        return result;
    }
    public static boolean isDirectory(String resourcePath){
        String path = resourcePath;
        if(path.startsWith(CLASS_PATH)){
            path = resourcePath.substring(CLASS_PATH.length());
            if(path.startsWith("/")){
                path = path.substring(1);
            }
            if(path.endsWith("/")){
                path = path.substring(0, path.length()-1);
            }
            URL url = ClassLoader.getSystemResource(path);
            if(url == null){
                return false;
            }
            String filePath = url.getPath();
            File file = Paths.get(filePath).toFile();
            if(!file.exists()){
                //this is jar file
                path = path + "/";
                url = ClassLoader.getSystemResource(path);
                if(url == null){
                    return false;
                }else{
                    return true;
                }
            }else{
                return file.isDirectory();
            }
        }
        File file = Paths.get(path).toFile();
        return file.isDirectory();
    }
    public static String[] readDirectory(String resourcePath) throws IOException {
        if(isDirectory(resourcePath)){
            String path = resourcePath;
            if(!resourcePath.endsWith("/")){
                resourcePath = resourcePath + "/";
            }
            if(path.startsWith(CLASS_PATH)){
                path = resourcePath.substring(CLASS_PATH.length());
                if(path.startsWith("/")){
                    path = path.substring(1);
                }
                if(path.endsWith("/")){
                    path = path.substring(0, path.length()-1);
                }
                Enumeration<URL> urls = ClassLoader.getSystemResources(path);
                if(urls != null){
                    Set<String> files = new HashSet<>();
                    while(urls.hasMoreElements()){
                        URL url = urls.nextElement();
                        String pathRes = url.getPath();
                        if(url.toString().startsWith("jar")){
                            String[] split = pathRes.split("!");
                            try(ZipInputStream zis = new ZipInputStream(Files.newInputStream(Paths.get(URI.create(split[0])),StandardOpenOption.READ))){
                                ZipEntry ze = zis.getNextEntry();
                                while(ze != null){
                                    String name = ze.getName();
                                    if(!ze.isDirectory() && name.startsWith(path) && (name.indexOf('/', path.length()+1) == -1)){
                                        files.add(resourcePath + name.substring(path.length()+1));
                                    }
                                    ze = zis.getNextEntry();
                                }
                            }
                        }else{
                            File file = Paths.get(pathRes).toFile();
                            if(file.isDirectory()){
                                String[] list = file.list();
                                for(String s: list){
                                    File f = new File(file, s);
                                    if(!f.isDirectory())
                                        files.add(resourcePath + s);
                                }
                            }
                        }
                    }
                    return files.toArray(new String[files.size()]);
                }
            }else{
                File file = Paths.get(path).toFile();
                if(file.isDirectory()){
                    List<String> files = new ArrayList<>();
                    for(String s : file.list()){
                        File f = new File(file, s);
                        if(!f.isDirectory())
                            files.add(resourcePath + s);
                    }
                    return files.toArray(new String[files.size()]);
                }
            }
        }
        return HelperStringParser.EMPTY_STRINGS;
    }
    private static Reader openReader(String resourcePath, Charset charset) throws IOException {
        if(resourcePath.startsWith(CLASS_PATH)){
            String path = resourcePath.substring(CLASS_PATH.length());
            URL url = ClassLoader.getSystemResource(path);
            if(url == null && !path.startsWith("/")){
                path = "/"+path;
                url = ClassLoader.getSystemResource(path);
            }
            if(url != null){
                return new BufferedReader(new InputStreamReader(url.openStream(), charset));
            }else{
                throw new IOException("Invalid path: "+resourcePath);
            }
        }else{
            Path path = Paths.get(resourcePath);
            if(path.toFile().exists() && !path.toFile().isDirectory()){
                return Files.newBufferedReader(path);
            }else{
                throw new IOException("Invalid path: "+resourcePath);
            }
        }
    }
    public static String getPropertyStringExact(String name, Map<String,Object> map) {
        Object obj = findProperty(name, map);
        if(obj instanceof String){
            return (String) obj;
        }else{
            return null;
        }
    }
    @SuppressWarnings("unchecked")
    public static Map<String, Object> getPropertyMapExact(String name, Map<String,Object> map) {
        Object obj = findProperty(name, map);
        if(obj instanceof Map){
            return (Map<String, Object>) obj;
        }else{
            return null;
        }
    }
    @SuppressWarnings("unchecked")
    public static List<Object> getPropertyListExact(String name, Map<String,Object> map) {
        Object obj = findProperty(name, map);
        if(obj instanceof List){
            return (List<Object>) obj;
        }else{
            return null;
        }
    }
    public static Number getPropertyNumberExact(String name, Map<String,Object> map) {
        Object obj = findProperty(name, map);
        if(obj instanceof Number){
            return (Number) obj;
        }else{
            if(obj instanceof String){ 
                 try {
                    return NumberFormat.getInstance().parse((String) obj);
                } catch (ParseException e) {
                }
            }
            return null;
        }
    }
    public static Boolean getPropertyBooleanExact(String name, Map<String,Object> map) {
        Object obj = findProperty(name, map);
        if(obj instanceof Boolean){
            return (Boolean) obj;
        }else{
            if(obj instanceof String){ 
                if(HelperStringParser.BOOLEAN_TRUE.equals(obj)){
                    return true;
                }else if(HelperStringParser.BOOLEAN_FALSE.equals(obj)){
                    return false;
                }
            }
            return null;
        }
    }
    
    private PropertiesLoader() {}
    private static final EntityResolver2 ENTITY_RESOLVER2 = new EntityResolver2(){
        @Override
        public InputSource resolveEntity(String publicId, String systemId) throws SAXException, IOException {
            return new InputSource(EMPTY_INPUT_STREAM);
        }
        @Override
        public InputSource getExternalSubset(String name, String baseURI) throws SAXException, IOException {
            return new InputSource(EMPTY_INPUT_STREAM);
        }
        @Override
        public InputSource resolveEntity(String name, String publicId, String baseURI, String systemId){
            return new InputSource(EMPTY_INPUT_STREAM);
        }
    };
    private static final EntityResolver ENTITY_RESOLVER = new EntityResolver(){
        @Override
        public InputSource resolveEntity(String publicId, String systemId) throws SAXException, IOException {
            return new InputSource(EMPTY_INPUT_STREAM);
        }
    };
}
