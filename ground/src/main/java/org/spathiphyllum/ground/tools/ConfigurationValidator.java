/*
 * Copyright (C) 2018-2018 The Project Spathiphyllum Authors.
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
package org.spathiphyllum.ground.tools;

import java.util.Map;
import java.util.Set;

import org.spathiphyllum.DescriptionDNA;
import org.spathiphyllum.Equipment;
import org.spathiphyllum.el.ParseELException;
import org.spathiphyllum.ground.GardenShedDefault;

/**
 * This class contain methods to validate common configurations
 * @author obelous
 *
 */
public class ConfigurationValidator {
    private static final String ERR_CLASS_NAME_ABSENT = "There is not any '%s' in map for key '%s'";
    private static final String ERR_CLASS_ABSENT = "This key '%s' must containe class name or map";
    private static final String ERR_PROTOTYPE_ABSENT = "For this key '%s' prototype is absent";
    private static final String ERR_EQUIPMENT_ABSENT = "For this key '%s' equipment is not found  for prototype '%s'";
    private static final String ERR_REQUIRED_INIT = "This prototype '%s' required to initialize '%s'. Key '%s'";
    @SuppressWarnings("unchecked")
    public static void validateEquipmentConfiguration(Map<String,Object> configuration) throws ParseELException {
        if(!configuration.isEmpty()){
            for(Map.Entry<String, Object> entry : configuration.entrySet()){
                if(entry.getValue() instanceof String){
                    continue;
                }else if(entry.getValue() instanceof Map){
                    Map<String,Object> conf = (Map<String, Object>) entry.getValue();
                    if(!(conf.get(GardenShedDefault.CLASS_NAME_KEY) instanceof String)){
                        throw new ParseELException(0, String.format(ERR_CLASS_NAME_ABSENT, GardenShedDefault.CLASS_NAME_KEY, entry.getKey()));
                    }
                }else{
                    throw new ParseELException(0, String.format(ERR_CLASS_ABSENT, entry.getKey()));
                }
            }
        }
    }
    @SuppressWarnings("unchecked")
    public static void validateEquipmentSeed(Map<String,Object> configuration, Map<String, Equipment> equipments) throws ParseELException {
        if(!configuration.isEmpty()){
            for(Map.Entry<String, Object> entry : configuration.entrySet()){
                if(entry.getValue() instanceof Map){
                    Map<String,Object> conf = (Map<String, Object>) entry.getValue();
                    Object o = conf.get(GardenShedDefault.PROTOTYPE_NAME);
                    String prototype = null;
                    
                    if(o instanceof String){
                        prototype = (String) o;
                    }else{
                        throw new ParseELException(0, String.format(ERR_PROTOTYPE_ABSENT, entry.getKey()));
                    }
                    Equipment equipment = null;
                    if(equipments != null){
                        equipment = equipments.get(prototype);
                    }
                    if(equipment == null){
                        throw new ParseELException(0, String.format(ERR_EQUIPMENT_ABSENT, entry.getKey(), prototype));
                    }
                    Set<DescriptionDNA> initializes = equipment.initializeDNA();
                    if(initializes != null){
                        for(DescriptionDNA desc : initializes){
                            if(desc.isRequired() && conf.containsKey(desc.getName())){
                                throw new ParseELException(0, String.format(ERR_REQUIRED_INIT, prototype, desc.getName(), entry.getKey()));
                            }
                        }
                    }
                }else{
                    throw new ParseELException(0, new StringBuilder("This key ").append(entry.getKey()).append(" must containe class name or map").toString());
                }
            }
        }
    }
    private ConfigurationValidator() {}

}
