/*
 * Copyright (C) 2018-2018 The Project Spathiphyllum Authors.
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
package org.spathiphyllum.ground.environment;

import java.util.Map;
import java.util.Set;

import org.spathiphyllum.Environment;
import org.spathiphyllum.ground.tools.PropertiesLoader;

/**
 * @author obelous
 *
 */
public class DefaultEnvironment extends AbstractEnvironment {
    private final Environment parent;
    private final Map<String,Object> properties;
    DefaultEnvironment(String environmentName, Environment parent, Map<String,Object> properties) {
        super(environmentName);
        this.parent = parent;
        this.properties = properties;
    }
    @Override
    protected Environment getParent() {
        return parent;
    }
    @Override
    protected Object findProperties(String name) {
        return PropertiesLoader.findProperty(name, properties);
    }
    @Override
    protected void printContent(StringBuilder sb) {
        sb.append(properties.toString());
    }
    @Override
    protected Set<String> allKeys() {
        return PropertiesLoader.allKeys("", properties);
    }
}
