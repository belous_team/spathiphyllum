/*
 * Copyright (C) 2018-2018 The Project Spathiphyllum Authors.
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
package org.spathiphyllum.ground.environment;

import java.net.URL;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.spathiphyllum.EnumEnviroment;
import org.spathiphyllum.Environment;
import org.spathiphyllum.ground.tools.PropertiesLoader;

/**
 * @author obelous
 *
 */
public class RootEnvironment extends AbstractEnvironment {
    private static final Logger LOGGER = LoggerFactory.getLogger(RootEnvironment.class);
    
    private static final RootEnvironment ROOT_ENVIRONMENT = new RootEnvironment();
    
    private Map<String,Object>  properties = new HashMap<>();
    
    private RootEnvironment(){
        super("ROOT_ENVIRONMENT");
        System.getProperties().forEach((key, value) -> {properties.put(key.toString(), value);});
        for(EnumEnviroment def : EnumEnviroment.values()){
            properties.put(def.propertyName(), def.value());
        }
        defineCurrentJarPath();
    }
    public static Environment environment(){
        return ROOT_ENVIRONMENT;
    }
    @Override
    protected Environment getParent() {
        return null;
    }
    @Override
    protected Object findProperties(String name) {
        return properties.get(name);
    }
    @Override
    protected void printContent(StringBuilder sb) {
        sb.append(properties.toString());
    }
    private void defineCurrentJarPath(){
        URL url = Environment.class.getResource("");
        if(url != null){
            int skip = Environment.class.getPackage().getName().split("\\.").length;
            Path path = Paths.get(url.getPath());
            for(; skip >= 0; skip--){
                path = path.getParent();
            }
            properties.put(EnumEnviroment.DEFAULT_LIB_PATH.propertyName(), path.toString());
            String confPath = EnumEnviroment.DEFAULT_CONF_PATH.value();
            if(EnumEnviroment.DEFAULT_CONF_PATH.isDefaultValue(confPath)){
                properties.put(EnumEnviroment.DEFAULT_CONF_PATH.propertyName(), path.resolveSibling(confPath).normalize());
            }
        }else{
            LOGGER.error("Cannot get URL to the lib folder");
        }
    }
    @Override
    protected Set<String> allKeys() {
        return PropertiesLoader.allKeys("", properties);
    }
}
