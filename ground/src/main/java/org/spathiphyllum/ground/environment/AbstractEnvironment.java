/*
 * Copyright (C) 2018-2018 The Project Spathiphyllum Authors.
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
package org.spathiphyllum.ground.environment;

import java.text.NumberFormat;
import java.text.ParseException;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.spathiphyllum.Environment;
import org.spathiphyllum.el.tools.HelperStringParser;

/**
 * @author obelous
 *
 */
abstract class AbstractEnvironment implements Environment {
    private static final Logger LOGGER = LoggerFactory.getLogger(Environment.class);
    private final String environmentName;

    protected abstract Environment getParent();
    protected abstract Object findProperties(String name);
    protected abstract Set<String> allKeys();
    
    AbstractEnvironment(String environmentName){
        this.environmentName = environmentName;
    }
    @Override
    public Set<String> keySet(){
        Set<String> result = new HashSet<>(allKeys());
        if(getParent() != null){
            result.addAll(getParent().keySet());
        }
        return result;
    }
    @Override
    public Object getProperty(String name) {
        Object result = findProperties(name);
        if(result == null && getParent() != null){
            return getParent().getProperty(name);
        }
        return result;
    }
    @Override
    public String getPropertyStringExact(String name) {
        Object obj = getProperty(name);
        if(obj instanceof String){
            return (String) obj;
        }else{
            return null;
        }
    }
    @SuppressWarnings("unchecked")
    @Override
    public Map<String, Object> getPropertyMapExact(String name) {
        Object obj = getProperty(name);
        if(obj instanceof Map){
            return (Map<String, Object>) obj;
        }else{
            return null;
        }
    }
    @SuppressWarnings("unchecked")
    @Override
    public List<Object> getPropertyListExact(String name) {
        Object obj = getProperty(name);
        if(obj instanceof List){
            return (List<Object>) obj;
        }else{
            return null;
        }
    }
    @Override
    public Number getPropertyNumberExact(String name) {
        Object obj = getProperty(name);
        if(obj instanceof Number){
            return (Number) obj;
        }else{
            if(obj instanceof String){ 
                 try {
                    return NumberFormat.getInstance().parse((String) obj);
                } catch (ParseException e) {
                    LOGGER.error("Cannot read as number");
                }
            }
            return null;
        }
    }
    @Override
    public Boolean getPropertyBooleanExact(String name) {
        Object obj = getProperty(name);
        if(obj instanceof Boolean){
            return (Boolean) obj;
        }else{
            if(obj instanceof String){ 
                if(HelperStringParser.BOOLEAN_TRUE.equals(obj)){
                    return true;
                }else if(HelperStringParser.BOOLEAN_FALSE.equals(obj)){
                    return false;
                }
            }
            return null;
        }
    }
    @Override
    public Environment createChild(String environmentName, Map<String,Object> properties) {
        LOGGER.debug("Create child. Parent:{}", this);
        return new DefaultEnvironment(environmentName, this, properties);
    }
    protected abstract void printContent(StringBuilder sb);
    @Override
    public String toString(){
        if(LOGGER.isTraceEnabled()){
            StringBuilder sb = new StringBuilder(environmentName).append(":[\n");
            printContent(sb);
            sb.append("]\n");
            if(getParent() != null){
                sb.append(", parent[").append(getParent().toString()).append("]");
            }
            return sb.toString();
        }else{
            return environmentName;
        }
    }
}
