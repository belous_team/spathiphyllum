/*
 * Copyright (C) 2018-2018 The Project Spathiphyllum Authors.
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
package org.spathiphyllum.ground.source;

import java.io.IOException;
import java.net.URISyntaxException;
import java.net.URL;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Enumeration;

/**
 * @author obelous
 *
 */
public class SourceDNA {
    static final Enumeration<URL> COMPONENTS_URL = initComponents();
    static Enumeration<URL> initComponents() {
        try {
            return SourceDNA.class.getClassLoader().getResources("/services/components.properties");
        } catch (IOException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        return null;
    }
    public static void main(String... args){
        while (COMPONENTS_URL.hasMoreElements()) {
            URL url = (URL) COMPONENTS_URL.nextElement();
            System.out.println(url);
        }
        System.out.println(SourceDNA.class.getClassLoader().getResource("/services/components.properties"));
        System.out.println(SourceDNA.class.getClassLoader().getSystemResource("/services/components.properties"));
        try {
            Enumeration<URL> r = SourceDNA.class.getClassLoader().getSystemResources("/services/components.properties");
            while (r.hasMoreElements()) {
                URL url = (URL) r.nextElement();
                System.out.println(url);
            }
        } catch (IOException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        System.out.println(SourceDNA.class.getResource("/services/components.properties"));
        System.out.println(SourceDNA.class.getResource("/services/"));
        URL url = SourceDNA.class.getResource("/services/");
        try {
            Path path = Paths.get(url.toURI());
//            path.
        } catch (URISyntaxException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        } 
    }
}
