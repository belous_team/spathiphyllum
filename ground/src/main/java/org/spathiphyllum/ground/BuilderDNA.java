/*
 * Copyright (C) 2018-2018 The Project Spathiphyllum Authors.
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
package org.spathiphyllum.ground;

import java.util.HashMap;
import java.util.Map;

import org.spathiphyllum.DNA;
import org.spathiphyllum.EnumTypes;
import org.spathiphyllum.GranaryException;
import org.spathiphyllum.ground.domain.FactoryDefaultMeaning;
import org.spathiphyllum.el.EnumEL;

/**
 * @author obelous
 *
 */
public class BuilderDNA {
    private EnumModificator modificator = EnumModificator.CREATE;
    private String seedPath;
    private final String name;
    private boolean constant;
    private boolean inheritance = true;
    private boolean secured;
    private EnumTypes type;
    private EnumEL defaultValueType;
    private String defaultValue;
    private Map<String, String> additionalValue;

    BuilderDNA(String dnaName){
        this.name = dnaName;
    }
    BuilderDNA inheritDNA(DNA dna) throws GranaryException {
        if(dna == null){
            throw new GranaryException("DNA not found");
        }
        copyFromDNA(this, dna);
        return this;
    }
    BuilderDNA modifyDNA(DNA dna) throws GranaryException {
        if(dna == null){
            throw new GranaryException("DNA not found");
        }
        modificator = EnumModificator.UPDATE;
        copyFromDNA(this, dna);
        return this;
    }
    BuilderDNA deleteDNA(DNA dna) throws GranaryException {
        if(dna == null){
            throw new GranaryException("DNA not found");
        }
        modificator = EnumModificator.DELETE;
        copyFromDNA(this, dna);
        return this;
    }
    private static void copyFromDNA(BuilderDNA builder, DNA dna){
        builder.constant = dna.isConstant();
        builder.inheritance = dna.isInheritance();
        builder.secured = dna.isSecured();
        builder.type = dna.type();
        builder.additionalValue = new HashMap<>(dna.defaultMeaning().additionalValue());
        builder.defaultValue = dna.defaultMeaning().getValue();
        if(!builder.type.isSimple()){
            builder.defaultValueType = dna.defaultMeaning().getDataType();
        }
    }
    public String getSeedPath() {
        return seedPath;
    }
    public BuilderDNA setSeedPath(String seedPath) {
        this.seedPath = seedPath;
        return this;
    }

    public String getName() {
        return name;
    }

    public boolean isConstant() {
        return constant;
    }
    public BuilderDNA setConstant(boolean constant) {
        this.constant = constant;
        return this;
    }

    public boolean isInheritance() {
        return inheritance;
    }
    public BuilderDNA setInheritance(boolean inheritance) {
        this.inheritance = inheritance;
        return this;
    }

    public EnumTypes getType() {
        return type;
    }
    public BuilderDNA setType(EnumTypes type) {
        this.type = type;
        return this;
    }

    public boolean isSecured() {
        return secured;
    }
    public BuilderDNA setSecured(boolean secured) {
        this.secured = secured;
        return this;
    }

    public EnumEL getDefaultValueType() {
        return defaultValueType;
    }
    public BuilderDNA setDefaultValueType(EnumEL defaultValueType) {
        this.defaultValueType = defaultValueType;
        return this;
    }

    public String getDefaultValue() {
        return defaultValue;
    }
    public BuilderDNA setDefaultValue(String value) {
        this.defaultValue = value;
        return this;
    }
    public BuilderDNA setaAditionalValue(Map<String,String> additionalValue){
        this.additionalValue = additionalValue;
        return this;
    }
    public EnumModificator getModificator(){
        return modificator;
    }
    
    public DNA build() throws GranaryException{
        AbstractDNA dna = new AbstractDNA();
        dna.constant = constant;
        dna.defaultMeaning = FactoryDefaultMeaning.factory(type, defaultValueType, defaultValue, additionalValue);
        dna.inheritance = inheritance;
        dna.name = name;
        dna.secured = secured;
        dna.type = type;
        return dna;
    }
    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((name == null) ? 0 : name.hashCode());
        return result;
    }
    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;
        BuilderDNA other = (BuilderDNA) obj;
        if (name == null) {
            if (other.name != null)
                return false;
        } else if (!name.equals(other.name))
            return false;
        return true;
    }
    public Map<String, String> getAdditionalValue() {
        return new HashMap<>(additionalValue);
    }
}
