/*
 * Copyright (C) 2018-2018 The Project Spathiphyllum Authors.
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
package org.spathiphyllum.ground;

import java.nio.charset.Charset;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.spathiphyllum.DNA;
import org.spathiphyllum.EnumEnviroment;
import org.spathiphyllum.Environment;
import org.spathiphyllum.Equipment;
import org.spathiphyllum.GardenShed;
import org.spathiphyllum.InitializeException;
import org.spathiphyllum.LoadResourceException;
import org.spathiphyllum.SeedDomain;
import org.spathiphyllum.ground.tools.PropertiesLoader;

/**
 * @author obelous
 *
 */
public class GardenShedDefault implements GardenShed {
    private static final Logger LOGGER = LoggerFactory.getLogger(GardenShedDefault.class);
    private static final GardenShed LABORATORY_EQUIPMENT = new GardenShedDefault();
    public static final String CLASS_NAME_KEY = "class-name";
    public static final String PROTOTYPE_NAME = "prototype-name";
    private static final String MEASUREMENT_NAME = "factory-equipment";
    private final Map<String, Equipment> equipments = new HashMap<>();
    private final Map<String, Map<String,Equipment>> mappingToSeed = new HashMap<>();
    private Status status = Status.UNKNOWN;
    private Environment environment;
    private Map<String,Object> configuration;
    
    enum Status {
        UNKNOWN, INITIALIZE, ACTIVE, DOWN
    }
    
    private GardenShedDefault() {}
    public static GardenShed instance(){
        return LABORATORY_EQUIPMENT;
    }
    
    /* (non-Javadoc)
     * @see org.spathiphyllum.ground.FactoryEquipment#init(org.spathiphyllum.ground.Environment)
     */
    @Override
    @SuppressWarnings("unchecked")
    public void init(Environment baseEnviroment) {
        synchronized (LABORATORY_EQUIPMENT) {
            if(status == Status.UNKNOWN){
                status = Status.INITIALIZE;
            }else{
                LOGGER.warn("Cannot initialize with status {}", status);
            }
            LOGGER.info("Initialize LaboratoryEquipment");
            this.environment = baseEnviroment;
            Charset charset = Charset.forName(environment.getPropertyStringExact(EnumEnviroment.DEFAULT_CHARSET.propertyName()));
            Path confPath = Paths.get(environment.getPropertyStringExact(EnumEnviroment.DEFAULT_CONF_PATH.propertyName()));
            confPath = confPath.resolve(environment.getPropertyStringExact(EnumEnviroment.EQUIPMENT_CONFIGURATION.propertyName())).normalize();
            try {
                configuration = PropertiesLoader.loadProperty(confPath.toString(), charset);
                if(!configuration.isEmpty()){
                    for(Map.Entry<String, Object> entry : configuration.entrySet()){
                        if(entry.getValue() instanceof String){
                            // must be class name
                            Class<? extends Equipment> clazz = loadClass((String) entry.getValue());
                            if(clazz != null){
                                try {
                                    addEquipment(entry.getKey(), clazz);
                                } catch (InitializeException e) {
                                    LOGGER.error("Cannot add equipment", e);
                                }
                            }
                        }else if(entry.getValue() instanceof Map){
                            // must be configuration 
                            Map<String, Object> conf = (Map<String, Object>) entry.getValue();
                            if(conf.get(CLASS_NAME_KEY) instanceof String){
                                String className = (String) conf.get(CLASS_NAME_KEY);
                                if(className != null){
                                    Class<? extends Equipment> clazz = loadClass(className);
                                    if(clazz != null){
                                        try {
                                            addEquipment(entry.getKey(), clazz, environment.createChild(entry.getKey(), conf));
                                        } catch (InitializeException e) {
                                            LOGGER.error("Cannot add equipment", e);
                                        }
                                    }
                                }
                            }
                        }else{
                            LOGGER.warn("Invalid equipment configuration for: {}", entry.getKey());
                        }
                    }
                }
            } catch (LoadResourceException e) {
                LOGGER.error("Cannot load equipment configuration", e);
            }
            LOGGER.info("Initialized {} equipments", equipmentSize());
            if(!equipments.isEmpty()){
                status = Status.ACTIVE;
            }
        }
    }
    @SuppressWarnings("unchecked")
    private Class<? extends Equipment> loadClass(String className){
        try{
            Class<?> clazz = Thread.currentThread().getContextClassLoader().loadClass(className);
            if(Equipment.class.isAssignableFrom(clazz)){
                return (Class<? extends Equipment>) clazz;
            }else{
                return null;
            }
        }catch(ClassNotFoundException e){
            LOGGER.error("Cannot initialize Equipment, class [{}] not found", className);
            return null;
        }
    }
    private void internalAddEquipment(String name, Class<? extends Equipment> clazz, Environment environment) throws InitializeException {
        try {
            Equipment engineering = clazz.newInstance();
            if(environment != null){
                engineering.init(environment);
            }
            equipments.put(name, engineering);
        } catch (InstantiationException | IllegalAccessException e) {
            throw new InitializeException("Cannot create instance of: " + clazz.getName(), e);
        }
    }
    /* (non-Javadoc)
     * @see org.spathiphyllum.ground.FactoryEquipment#addEquipment(java.lang.String, java.lang.Class, org.spathiphyllum.ground.Environment)
     */
    @Override
    public void addEquipment(String name, Class<? extends Equipment> clazz, Environment environment) throws InitializeException {
        synchronized (LABORATORY_EQUIPMENT) {
            if(status == Status.INITIALIZE){
                internalAddEquipment(name, clazz, environment);
            }else{
                throw new InitializeException("Cannot add Euipment when status is " + status);
            }
            if(!equipments.isEmpty()){
                status = Status.ACTIVE;
            }
        }
    }
    /* (non-Javadoc)
     * @see org.spathiphyllum.ground.FactoryEquipment#addEquipment(java.lang.String, java.lang.Class)
     */
    @Override
    public void addEquipment(String name, Class<? extends Equipment> clazz) throws InitializeException {
        addEquipment(name, clazz, null);
    }
    /* (non-Javadoc)
     * @see org.spathiphyllum.ground.FactoryEquipment#equipmentSize()
     */
    @Override
    public int equipmentSize(){
        return equipments.size();
    }
    /* (non-Javadoc)
     * @see org.spathiphyllum.ground.FactoryEquipment#product(java.lang.String, org.spathiphyllum.ground.Environment)
     */
    @Override
    public Equipment product(String prototypeName, SeedDomain domain, DNA dna) throws InitializeException {
        if(status != Status.ACTIVE){
            Equipment engineering = equipments.get(prototypeName);
            if(engineering == null){
                throw new InitializeException("Prototype not found: " + prototypeName);
            }
            Equipment mappedToSeed = engineering.clone(domain);
            Map<String,Equipment> map = new HashMap<>();
            map = mappingToSeed.putIfAbsent(domain.seedPath(), new HashMap<>());
            if(map == null){
                map = mappingToSeed.get(domain.seedPath());
            }
            map.put(dna.name(), mappedToSeed);
            return mappedToSeed;
        }else{
            throw new InitializeException("Cannot get Euipment when status is not " + Status.ACTIVE);
        }
    }
    /* (non-Javadoc)
     * @see org.spathiphyllum.ground.FactoryEquipment#shutdown()
     */
    @Override
    public void shutdown(){
        synchronized (LABORATORY_EQUIPMENT) {
            if(status == Status.ACTIVE){
                status = Status.DOWN;
                Iterator<Equipment> iter = equipments.values().iterator();
                while (iter.hasNext()) {
                    iter.next().shutdown();
                }
                status = Status.UNKNOWN;
            }
        }
    }
    @Override
    public void measure(String prefix, Map<String, Object> measure) {
        if(measure != null){
            prefix = MEASUREMENT_NAME + ".";
            measure.put(prefix + "size", equipments);
            measure.put(prefix + "status", status.name());
            Iterator<Equipment> iter = equipments.values().iterator();
            while (iter.hasNext()) {
                iter.next().measure(prefix+"equipment.", measure);
            }
        }
    }
}
