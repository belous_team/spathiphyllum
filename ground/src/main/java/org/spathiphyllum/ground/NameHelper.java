/*
 * Copyright (C) 2018-2018 The Project Spathiphyllum Authors.
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
package org.spathiphyllum.ground;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * @author obelous
 *
 */
public class NameHelper {
    private static Pattern patterName = Pattern.compile("[0-9A-Za-z_]*");
    private NameHelper(){}
    /**
     * calculate parent path
     * index defines number of parent
     * for index = 0 you will get seedPath for current Seed
     * @param path
     * @param index
     * @return
     */
    public static String calculateParentPath(String[] path, int index){
        if(index >= path.length){
            throw new IllegalArgumentException("index is out of bounds");
        }
        StringBuilder sb = new StringBuilder();
        for(int i = index; i < path.length; i++){
            sb.append("/").append(path[i]);
        }
        return sb.toString();
    }
    /**
     * parse path to array of names of Seeds. 
     * First element is name of this Seed next element name of his parent and ... 
     * @param seedPath
     * @return
     */
    public static String[] parseSeedPath(String seedPath){
        String[] result = seedPath.split("/");
        for(String s : result){
            validateName(s);
        }
        return result;
    }
    private static void validateName(String s) {
        if(s == null || s.isEmpty()){
            //TODO change exception
            throw new IllegalArgumentException("Name cannot be empty");
        }
        Matcher m = patterName.matcher(s);
        if(!m.matches()){
            //TODO change exception
            throw new IllegalArgumentException("Name contains a forbidden symbol");
        }
    }
    public static void validateNameDNA(String s){
        if(s == null || s.isEmpty()){
            //TODO change exception
            throw new IllegalArgumentException("Name cannot be empty");
        }
        if(s.charAt(0) == '#'){
            //Reserved DNA name
            for(String rn : ReservedDNA.RESERVED_DNA_NAMES){
                if(rn.equals(s)){
                    return;
                }
            }
        }else{
            validateName(s);
        }
    }
}
