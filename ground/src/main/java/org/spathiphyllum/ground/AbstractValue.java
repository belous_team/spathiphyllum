/*
 * Copyright (C) 2018-2018 The Project Spathiphyllum Authors.
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
package org.spathiphyllum.ground;

import org.spathiphyllum.EnumTypes;
import org.spathiphyllum.Petal;

/**
 * @author obelous
 * @deprecated
 */
public interface AbstractValue {
    Petal getPetal();
    
    default EnumTypes geTypePetal() {
        return getPetal().getPetalType();
    }
    /**
     * Validation block
     */
    /**
     * Return true, if this value is valid
     * @return
     */
    boolean isValid();
    /**
     * Return true, if this value can be casted to typePetal
     * @param typePetal
     * @return
     */
    boolean canCastTo(EnumTypes typePetal);
    /**
     * Get and set value
     */
    /**
     * Return true, if this value if null
     */
    boolean isNull();
    /**
     * set value as null
     */
    void setNull();
    /**
     * Return this value as boolean.
     * You will get ClassCastException if canCastTo(Boolean) returned false
     * You will get false if isNull() return true
     * @return
     */
    boolean asBoolean();
    /**
     * Set boolean value
     * You will get ClassCastException if canCastTo(Boolean) returned false
     * @param value
     */
    void setBoolean(boolean value);
    /**
     * Return this value as byte.
     * You will get ClassCastException if canCastTo(Byte) returned false
     * You will get 0 if isNull() return true
     * @return
     */
    byte asByte();
    /**
     * Set byte value
     * You will get ClassCastException if canCastTo(Boolean) returned false
     * @param value
     */
    void setByte(byte value);
    /**
     * Return this value as char.
     * You will get ClassCastException if canCastTo(Byte) returned false
     * You will get 0 if isNull() return true
     * @return
     */
    char asChar();
    /**
     * Set char value
     * You will get ClassCastException if canCastTo(Boolean) returned false
     * @param value
     */
    void setChar(char value);
    /**
     * Return this value as short.
     * You will get ClassCastException if canCastTo(Byte) returned false
     * You will get 0 if isNull() return true
     * @return
     */
    short asShort();
    /**
     * Set short value
     * You will get ClassCastException if canCastTo(Boolean) returned false
     * @param value
     */
    void setShort(short value);
    /**
     * Return this value as int.
     * You will get ClassCastException if canCastTo(Byte) returned false
     * You will get 0 if isNull() return true
     * @return
     */
    int asInt();
    /**
     * Set integer value
     * You will get ClassCastException if canCastTo(Boolean) returned false
     * @param value
     */
    void setInt(int value);
    /**
     * Return this value as long.
     * You will get ClassCastException if canCastTo(Byte) returned false
     * You will get 0 if isNull() return true
     * @return
     */
    long asLong();
    /**
     * Set long value
     * You will get ClassCastException if canCastTo(Boolean) returned false
     * @param value
     */
    void setLong(long value);
    /**
     * Return this value as float.
     * You will get ClassCastException if canCastTo(Byte) returned false
     * You will get 0 if isNull() return true
     * @return
     */
    float asFloat();
    /**
     * Set float value
     * You will get ClassCastException if canCastTo(Boolean) returned false
     * @param value
     */
    void setFloat(float value);
    /**
     * Return this value as double.
     * You will get ClassCastException if canCastTo(Byte) returned false
     * You will get 0 if isNull() return true
     * @return
     */
    double asDouble();
    /**
     * Set double value
     * You will get ClassCastException if canCastTo(Boolean) returned false
     * @param value
     */
    void setDouble(double value);
    /**
     * Return this value as String.
     * You will get ClassCastException if canCastTo(Byte) returned false
     * You will get 0 if isNull() return true
     * @return
     */
    String asString();
    /**
     * Return this value as String.
     * For different type need different a format
     * You will get ClassCastException if canCastTo(Byte) returned false
     * You will get 0 if isNull() return true
     * @param format
     * @return
     */
    String asString(String format);
    /**
     * Parse this string and set value 
     * @param strValue
     */
    void parseString(String strValue);
    /**
     * Parse this string and set value 
     * @param strValue
     */
    void parseString(String strValue, String pattern);
}
