/*
 * Copyright (C) 2018-2018 The Project Spathiphyllum Authors.
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
package org.spathiphyllum.ground;

import java.nio.charset.Charset;
import java.util.Collections;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.spathiphyllum.EnumEnviroment;
import org.spathiphyllum.Environment;
import org.spathiphyllum.LoadResourceException;
import org.spathiphyllum.ground.environment.RootEnvironment;
import org.spathiphyllum.ground.tools.PropertiesLoader;

/**
 * 
 * @author obelous
 *
 */
public class Main {
    private static final Logger LOGGER = LoggerFactory.getLogger(Main.class);
    
    public static void main(String[] args) {
        Main m = new Main();
        m.init();
    }
    private void init(){
        Map<String, Object> properties;
        try {
            properties = PropertiesLoader.loadProperty(EnumEnviroment.DEFAULT_LIB_PATH.value(), Charset.forName(EnumEnviroment.DEFAULT_CHARSET.value()));
        } catch (LoadResourceException e) {
            LOGGER.error("Cannot load the base enviroment", e);
            properties = Collections.emptyMap();
        }
        Environment baseEnviroment = RootEnvironment.environment().createChild("spathiphyllum", properties);
        GardenShedDefault.instance().init(baseEnviroment);
    }
}
