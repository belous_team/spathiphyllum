/*
 * Copyright (C) 2018-2018 The Project Spathiphyllum Authors.
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
package org.spathiphyllum.ground;

import java.util.HashMap;
import java.util.Map;
import java.util.Set;
import java.util.TreeSet;
import java.util.concurrent.ConcurrentHashMap;

import org.spathiphyllum.DNA;
import org.spathiphyllum.EnumSeedStatus;
import org.spathiphyllum.EnumSeedType;
import org.spathiphyllum.GranaryException;
import org.spathiphyllum.Seed;
import org.spathiphyllum.SeedNotFoundException;
import org.spathiphyllum.el.tools.HelperStringParser;

/**
 * Seeds Builder
 * 
 * @author obelous
 *
 */
public class BuilderSeed {
    private final GranaryDefault granaryDefault;
    
    private String seedName;
    private String domainPath;
    private String parentPath;
    /**
     * The list of DNS without parent DNA
     */
    private EnumSeedStatus status = EnumSeedStatus.UNSAVED;
    private EnumSeedType type = EnumSeedType.SEED;
    /**
     * The collection of DNA that is not persisted
     */
    private Map<String, BuilderDNA> modifiedDNA = new ConcurrentHashMap<>();
    
    BuilderSeed(GranaryDefault granary){
        granaryDefault = granary;
    }
    public String getSeedName() {
        return seedName;
    }
    public String seedPath(){
        if(HelperStringParser.isStringEmpty(parentPath)){
            return seedName;
        }else{
            StringBuilder sb = new StringBuilder(parentPath);
            return sb.append('/').append(seedName).toString();
        }
    }
    public BuilderSeed seedName(String name){
        this.seedName = name;
        return this;
    }
    public String getDomainPath() {
        return domainPath;
    }
    public BuilderSeed setDomainPath(String domainPath) {
        this.domainPath = domainPath;
        return this;
    }
    public String getParentPath() {
        return parentPath;
    }
    public BuilderSeed setParentPath(String parentPath) {
        this.parentPath = parentPath;
        return this;
    }
    public EnumSeedStatus getStatus() {
        return status;
    }
    public BuilderSeed setStatus(EnumSeedStatus status) {
        this.status = status;
        return this;
    }
    public EnumSeedType getType() {
        return type;
    }
    public BuilderSeed setType(EnumSeedType type) {
        this.type = type;
        return this;
    }
    Set<StatusDNA> dnaSet(){
        Set<StatusDNA> statusDNA = new TreeSet<>();
        for(BuilderDNA m : modifiedDNA.values()){
            statusDNA.add(new StatusDNA(m.getName(), m.getSeedPath(), m.getModificator()));
        }
        String thisPath = seedPath();
        try {
            Seed seed = granaryDefault.getSeed(thisPath);
            Map<String, String> map = seed.namesDNA();
            map.forEach((dnaName,seedPath)-> 
                statusDNA.add(new StatusDNA(dnaName, seedPath, EnumModificator.ORIGINAL))
            );
        } catch (GranaryException e) {
            //nothing. this is new Seed
        }
        return statusDNA;
    }
    BuilderSeed modifySeed(HolderSeed holderSeed) throws GranaryException {
        SeedDefault seed = holderSeed.getSeed();
        if(seed == null){
            throw new SeedNotFoundException();
        }
        seedName = seed.seedName();
        parentPath = (seed.getParent()!=null)? seed.getParent().seedPath() : null;
        domainPath = seed.seedDomain().seedPath();
        status = seed.seedStatus();
        type = seed.seedType();
        return this;
    }

    BuilderSeed inheritSeed(String name, HolderSeed parent) throws GranaryException {
        seedName = name;
        SeedDefault seed = parent.getSeed();
        if(seed == null){
            throw new SeedNotFoundException();
        }else{
            parentPath = seed.seedPath();
            domainPath = seed.seedDomain().seedPath();
        }
        return this;
    }

    public BuilderSeed newSeed(String name) throws GranaryException {
        seedName = name;
        parentPath = null;
        domainPath = "/"+name;
        type = EnumSeedType.DOMAIN_SEED;
        try{
            granaryDefault.getSeed("/"+name);
        }catch(GranaryException e){
            throw new DuplicateNameException("Seed already present");
        }
        return this;
    }

    /**
     * Return set of DNA for edit
     * @return the modifiedDNA
     */
    Map<String,BuilderDNA> getModifiedDNA() {
        return new HashMap<>(modifiedDNA);
    }
    /**
     * create DNA
     * @param dna
     * @return
     * @throws GranaryException 
     */
    public BuilderDNA createDNA(String dnaName) throws GranaryException {
        StatusDNA statusDNA = dnaSet().stream().filter(s -> dnaName.equals(s.dnaName))
                .findFirst().orElse(null);
        BuilderDNA builder;
        if(statusDNA != null){
            if(statusDNA.dnaState == EnumModificator.ORIGINAL || statusDNA.dnaState == EnumModificator.DELETE){
                if(statusDNA.dnaState == EnumModificator.DELETE){
                    modifiedDNA.remove(dnaName);
                }
                HolderSeed holderSeed = granaryDefault.findHolderSeed(statusDNA.seedPath);
                if(holderSeed != null){
                    DNA dna = holderSeed.getSeed().dna(dnaName);
                    builder = new BuilderDNA(dnaName).inheritDNA(dna).setSeedPath(seedPath());
                }else{
                    throw new GranaryException("Cannot find DNA");
                }
            }else{
                builder = modifiedDNA.get(dnaName);
                if(builder == null){
                    builder = new BuilderDNA(dnaName).setSeedPath(seedPath());
                }
            }
        }else{
            builder = new BuilderDNA(dnaName).setSeedPath(seedPath());
        }
        BuilderDNA tmp = modifiedDNA.putIfAbsent(dnaName, builder);
        if(tmp != null){
            builder = tmp;
        }
        return builder;
    }
    /**
     * Delete DNA
     * @param dnaName
     * @throws GranaryException 
     */
    public void deleteDNA(String dnaName) throws GranaryException{
        if(modifiedDNA.remove(dnaName) == null){
            HolderSeed holderSeed = granaryDefault.findHolderSeed(seedPath());
            if(holderSeed != null){
                DNA dna = holderSeed.getSeed().dna(dnaName);
                modifiedDNA.put(dnaName, new BuilderDNA(dnaName).deleteDNA(dna));
            }else{
                throw new GranaryException("Cannot find DNA");
            }
        }
    }
    /**
     * Create Environment for SeedDmain
     * @return
     */
    public BuilderDNA createEnvironment() throws GranaryException {
        //TODO we must hide body if security do not allow to read
        if(type == EnumSeedType.DOMAIN_SEED){
            return createDNA(ReservedDNA.DOMAIN_ENVIRMENT);
        }else{
            throw new GranaryException("This Seed is not Domain");
        }
    }
    /**
     * Create Environment for SeedDmain
     * @return
     */
    public BuilderDNA createSecurityContext() throws GranaryException {
        //TODO we must hide body if security do not allow to read
        if(type == EnumSeedType.DOMAIN_SEED){
            return createDNA(ReservedDNA.DOMAIN_SECURITY);
        }else{
            throw new GranaryException("This Seed is not Domain");
        }
    }
    /**
     * cancel all modifications
     */
    public void cancel(){
        granaryDefault.deleteBuildeSeed(this);
    }
    /**
     * cancel DNA changes
     */
    public void cancelDNA(){
        modifiedDNA = new ConcurrentHashMap<>();
    }
    public static class StatusDNA implements Comparable<StatusDNA> {
        String dnaName;
        String seedPath;
        EnumModificator dnaState;
        
        public StatusDNA(String dnaName, String seedPath, EnumModificator dnaState) {
            super();
            this.dnaName = dnaName;
            this.seedPath = seedPath;
            this.dnaState = dnaState;
        }
        public String getDnaName() {
            return dnaName;
        }
        public String getSeedPath() {
            return seedPath;
        }
        public EnumModificator getDnaState() {
            return dnaState;
        }
        @Override
        public int hashCode() {
            return 31 + ((dnaName == null) ? 0 : dnaName.hashCode());
        }
        @Override
        public boolean equals(Object obj) {
            if (this == obj)
                return true;
            if (obj == null)
                return false;
            if (getClass() != obj.getClass())
                return false;
            StatusDNA other = (StatusDNA) obj;
            if (dnaName == null) {
                if (other.dnaName != null)
                    return false;
            } else if (!dnaName.equals(other.dnaName))
                return false;
            return true;
        }
        @Override
        public int compareTo(StatusDNA o) {
            return dnaName.compareTo(o.dnaName);
        }
    }
}
