/*
 * Copyright (C) 2018-2018 The Project Spathiphyllum Authors.
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
package org.spathiphyllum.ground;

import org.spathiphyllum.SpathiphyllumRuntimeException;

/**
 * The mutable holder for Seed
 * 
 * @author obelous
 *
 */
class HolderSeed {
    private SeedDefault seed;
    final String seedPath;
    HolderSeed(SeedDefault seed){
        this.seedPath = seed.seedPath();
    }
    SeedDefault getSeed() {
        return seed;
    }
    void setSeed(SeedDefault seed) {
        if(!seedPath.equals(seed.seedPath())){
            throw new SpathiphyllumRuntimeException("Incorrect Seed");
        }
        this.seed = seed;
    }
    @Override
    public int hashCode() {
        return 31 + ((seedPath == null) ? 0 : seedPath.hashCode());
    }
    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;
        HolderSeed other = (HolderSeed) obj;
        if (seedPath == null) {
            if (other.seedPath != null)
                return false;
        } else if (!seedPath.equals(other.seedPath))
            return false;
        return true;
    }
    
}
