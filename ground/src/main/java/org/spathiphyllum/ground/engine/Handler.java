/*
 * Copyright (C) 2018-2018 The Project Spathiphyllum Authors.
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
package org.spathiphyllum.ground.engine;

import org.spathiphyllum.EnumTypes;
import org.spathiphyllum.Flower;
import org.spathiphyllum.Petal;
import org.spathiphyllum.PetalNotFoundException;

/**
 * @author obelous
 *
 */
public class Handler {
    private final Flower flower;
    private final String petalName;
    private final Petal petal;
    private final EnumTypes petalType;
    private Invoker invoker;
    
    public Handler(Flower flower, String petalName) throws PetalNotFoundException {
        this.flower = flower;
        this.petalName = petalName;
        this.petal = flower.find(petalName);
        petalType = petal.getPetalType();
        if(petalType.isSimple()){
            //this petal do nothing so this petal cannot be handler
            throw new PetalNotFoundException("Simple petal cannot be handler");
        }
       this.invoker = compile();
    }
    private Invoker compile() {
        // TODO Auto-generated method stub
        return null;
    }
    public void invoke(){
        invoker.invoke();
    }
}
