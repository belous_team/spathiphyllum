/**
 * 
 */
package org.spathiphyllum.ground.engine;

import java.util.concurrent.Callable;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.FutureTask;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * @startuml
 * (*) -->[Petal run\nPetal next] Start 
 * --> ===B1===
 * -->[Petal run] Create Future
 * --> Run
 * --> ===B2===
 * ===B1=== --> if "Petal next" then
 * -> Create Future task
 * -> Wait
 * endif
 * --> ===B2===
 * @enduml
 * @author obelous
 *
 */
public abstract class Engine {
    private static final Logger LOGGER = LoggerFactory.getLogger(Engine.class);
    /**
     * here stored task that wait as next
     */
    private ConcurrentHashMap<String, InternalFutureTask> waitTaskCollection = new ConcurrentHashMap<>();
    /**
     * here stored task that wait as next
     */
    private ConcurrentHashMap<String, InternalFutureTask> waitOnException = new ConcurrentHashMap<>();
    //TODO this must be configured
    private ExecutorService executorService = Executors.newFixedThreadPool(10);
    private ExecutorService executorServiceForException = Executors.newFixedThreadPool(10);
    
    private class InternalFutureTask extends FutureTask<Void> {
        private final String flowerId;
        private final String thisTaskId;
        private final String nextTaskId;
        private long start;
        private boolean isException = false;
        public InternalFutureTask(Callable<Void> callable, String flowerId, String thisTaskId, String nextTaskId) {
            super(callable);
            this.flowerId = flowerId;
            this.thisTaskId = thisTaskId;
            this.nextTaskId = nextTaskId;
        }

        public InternalFutureTask(Runnable runnable, Void result, String flowerId, String thisTaskId, String nextTaskId) {
            super(runnable, result);
            this.flowerId = flowerId;
            this.thisTaskId = thisTaskId;
            this.nextTaskId = nextTaskId;
        }
        
        @Override
        protected void done() {
            super.done();
            if(LOGGER.isTraceEnabled()){
                String msg = new StringBuilder().append('[').append(flowerId).append(']')
                        .append("Task '").append(thisTaskId).append("' finished, took[").append(took()).append(" ns]")
                        .toString();
                LOGGER.trace(msg);
            }
            if(isCancelled()){
                takeCareOfCancelExeption(flowerId, thisTaskId, nextTaskId);
            } else if(isException){
                takeCareOfExecutionExeption(flowerId, thisTaskId, nextTaskId);
            } else if(nextTaskId != null){
                if(LOGGER.isTraceEnabled()){
                    String msg = new StringBuilder().append('[').append(flowerId).append(']')
                            .append("Task '").append(thisTaskId).append(" is tried to activate next task '").append(nextTaskId).append("'")
                            .toString();
                    LOGGER.trace(msg);
                }
                activateTask(flowerId, nextTaskId);
            }
        }
        @Override
        protected void setException(Throwable t) {
            super.setException(t);
            isException = true;
            if(LOGGER.isErrorEnabled()){
                String msg = new StringBuilder().append('[').append(flowerId).append(']')
                        .append("Task '").append(thisTaskId).append("' throws exception, took[").append(took()).append(" ns]")
                        .toString();
                LOGGER.error(msg, t);
            }
        }

        @Override
        public boolean cancel(boolean mayInterruptIfRunning) {
            String msg = new StringBuilder().append('[').append(flowerId).append(']')
                    .append("Task '").append(thisTaskId).append("' is canceled, took[").append(took()).append(" ns]")
                    .toString();
            LOGGER.warn(msg);
            return super.cancel(mayInterruptIfRunning);
        }

        @Override
        public void run() {
            if(LOGGER.isTraceEnabled()){
                String msg = new StringBuilder().append('[').append(flowerId).append(']')
                        .append("Task '").append(thisTaskId).append("' started").toString();
                LOGGER.trace(msg);
            }
            start = System.nanoTime();
            super.run();
        }
        private long took(){
            return start != 0?System.nanoTime()-start:0;
        }
    }
    
    private void takeCareOfCancelExeption(String flowerId, String thisTaskId, String nextTaskId) {
        // TODO : find out what to do
        deleteAllWaitTaskForFlower(flowerId);
    }

    private void deleteAllWaitTaskForFlower(String flowerId) {
        String prefixKey = flowerId+":";
        waitTaskCollection.entrySet().removeIf(entry -> entry.getKey().startsWith(prefixKey));
    }

    private void takeCareOfExecutionExeption(String flowerId, String thisTaskId, String nextTaskId) {
        // TODO Auto-generated method stub
    }

    private void activateTask(String flowerId, String nextTaskId) {
        InternalFutureTask task = waitTaskCollection.remove(getFullTaskId(flowerId, nextTaskId));
        if(task != null){
            executorService.submit(task);
        }else{
            LOGGER.error("[{}] Cannot find this task '{}' in waitQueue", flowerId, nextTaskId);
        }
    }
    
    private static String getFullTaskId(String flowerId, String taskId){
        return new StringBuilder(flowerId).append(':').append(taskId).toString();
    }
}
