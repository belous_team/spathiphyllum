/*
 * Copyright (C) 2018-2018 The Project Spathiphyllum Authors.
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
package org.spathiphyllum.ground.domain;

import java.math.BigDecimal;
import java.math.BigInteger;

import org.spathiphyllum.ExchangeOfMeaning;
import org.spathiphyllum.InvokeELException;
import org.spathiphyllum.Petal;
import org.spathiphyllum.PetalMeaning;
import org.spathiphyllum.el.ParseELException;
import org.spathiphyllum.el.tools.HelperStringParser;

/**
 * @author obelous
 *
 */
public class BigIntegerMeaning extends AbstractPetalMeaning {
    private BigInteger integer;
    /**
     * @param petal
     */
    public BigIntegerMeaning(Petal petal) {
        super(petal);
    }
    @Override
    public void nourishPetal(Petal petal) {
        PetalMeaning meaning = petal.getPatalMeaning();
        switch (petal.getPetalType()) {
        case BIG_DECIMAL:
            meaning.nourishBigDecimal(new BigDecimal(integer));
            break;
        case BIG_INTEGER:
            meaning.nourishBigInteger(integer);
            break;
        case BYTE:
            meaning.nourishByte(integer.byteValue());
            break;
        case CHAR:
            meaning.nourishChar((char) integer.intValue());
            break;
        case DOUBLE:
            meaning.nourishDouble(integer.doubleValue());
            break;
        case FLOAT:
            meaning.nourishFloat(integer.floatValue());
            break;
        case INT:
            meaning.nourishInt(integer.intValue());
            break;
        case LONG:
            meaning.nourishLong(integer.longValue());
            break;
        case SHORT:
            meaning.nourishShort(integer.shortValue());
            break;
        case STRING:
            try {
                meaning.nourishString(HelperStringParser.toString(integer));
            } catch (ParseELException e) {
                throw new InvokeELException(WTF_IMPOSSIBLE);
            }
            break;
        default:
            throw new InvokeELException(ERR_UNSUPPORT_TYPE);
        }
    }
    @Override
    public void saveToExchage(ExchangeOfMeaning exchange) {
        exchange.setValue(integer);
    }
    @Override
    public int length() {
        return integer.toString().length();
    }
    @Override
    public void nourishBigDecimal(BigDecimal value) {
        integer = value.toBigInteger();
        setDirty();
    }
    @Override
    public void nourishBigInteger(BigInteger value) {
        integer = value;
        setDirty();
    }
    @Override
    public void nourishByte(byte value) {
        integer = BigInteger.valueOf(value);
        setDirty();
    }
    @Override
    public void nourishChar(char value) {
        integer = BigInteger.valueOf(value);
        setDirty();
    }
    @Override
    public void nourishDouble(double value) {
        integer = BigInteger.valueOf((long)value);
        setDirty();
    }
    @Override
    public void nourishFloat(float value) {
        integer = BigInteger.valueOf((long)value);
        setDirty();
    }
    @Override
    public void nourishInt(int value) {
        integer = BigInteger.valueOf(value);
        setDirty();
    }
    @Override
    public void nourishLong(long value) {
        integer = BigInteger.valueOf(value);
        setDirty();
    }
    @Override
    public void nourishShort(short value) {
        integer = BigInteger.valueOf(value);
        setDirty();
    }
    @Override
    public void nourishString(String value) throws ParseELException {
        integer = HelperStringParser.parseBigInteger(value);
        setDirty();
    }
}
