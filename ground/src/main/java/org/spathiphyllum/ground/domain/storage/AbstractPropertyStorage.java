/*
 * Copyright (C) 2018-2018 The Project Spathiphyllum Authors.
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
package org.spathiphyllum.ground.domain.storage;

import java.nio.charset.Charset;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.spathiphyllum.EnumEnviroment;
import org.spathiphyllum.EnumSeedType;
import org.spathiphyllum.EnumTypes;
import org.spathiphyllum.Environment;
import org.spathiphyllum.GranaryException;
import org.spathiphyllum.LoadResourceException;
import org.spathiphyllum.el.EnumEL;
import org.spathiphyllum.el.tools.HelperStringParser;
import org.spathiphyllum.ground.BuilderDNA;
import org.spathiphyllum.ground.BuilderSeed;
import org.spathiphyllum.ground.FlowerDefault;
import org.spathiphyllum.ground.GranaryDefault;
import org.spathiphyllum.ground.GranaryStorageProvider;
import org.spathiphyllum.ground.SeedDefault;
import org.spathiphyllum.ground.tools.PropertiesLoader;

/**
 * @author obelous
 *
 */
abstract class AbstractPropertyStorage implements GranaryStorageProvider {
    /**
     * if this storega readonly
     */
    public static final String STORAGE_READONLY= "storage.readonly";
    /**
     * path to this storage
     */
    public static final String STORAGE_PATH= "storage.path";
    /**
     * prefix of resources
     */
    public static final String STORAGE_PREFIX= "storage.prefix";
    /**
     * method of store (in one resource or multi resource)
     */
    public static final String STORAGE_MULTIFILE= "storage.multifile";
    // properties' names
    /**
     * define Seed
     */
    public static final String FIELD_SEED = "storage.seed";
    /**
     * define seed's name
     */
    public static final String FIELD_SEED_NAME= "storage.seed.name";
    /**
     * define seed's type
     */
    public static final String FIELD_SEED_TYPE= "storage.seed.type";
    /**
     * define seed's domain
     */
    public static final String FIELD_SEED_DOMAIN= "storage.seed.domain";
    /**
     * define seed's parent
     */
    public static final String FIELD_SEED_PARENT= "storage.seed.parent";
    /**
     * define seed's DNA
     */
    public static final String FIELD_SEED_DNA= "storage.seed.dna";
    /**
     * define DNA's name
     */
    public static final String FIELD_SEED_DNA_NAME= "storage.seed.dna.name";
    /**
     * define DNA's constant
     */
    public static final String FIELD_SEED_DNA_CONSTANT= "storage.seed.dna.constant";
    /**
     * define DNA's inheritance
     */
    public static final String FIELD_SEED_DNA_INHERITANCE= "storage.seed.dna.inheritance";
    /**
     * define DNA's secured
     */
    public static final String FIELD_SEED_DNA_SECURED= "storage.seed.dna.secured";
    /**
     * define DNA's type
     */
    public static final String FIELD_SEED_DNA_TYPE= "storage.seed.dna.type";
    /**
     * define DNA's default value
     */
    public static final String FIELD_SEED_DNA_VALUE= "storage.seed.dna.value";
    /**
     * define DNA's data type
     */
    public static final String FIELD_SEED_DNA_DATA_TYPE= "storage.seed.dna.value.type";
    /**
     * define DNA's value extra
     */
    public static final String FIELD_SEED_DNA_VALUE_EXTRA= "storage.seed.dna.value.extra";
    /**
     * define DNA's value extra name
     */
    public static final String FIELD_SEED_DNA_VALUE_EXTRA_NAME= "storage.seed.dna.value.extra.name";
    /**
     * define DNA's value extra value
     */
    public static final String FIELD_SEED_DNA_VALUE_EXTRA_VALUE= "storage.seed.dna.value.extra.value";

    protected Environment environment;
    protected GranaryDefault granary;
    protected boolean READ_ONLY = false;
    /* (non-Javadoc)
     * @see org.spathiphyllum.ground.GranaryStorageProvider#initializeGranary(org.spathiphyllum.ground.GranaryDefault)
     */
    @Override
    public void initializeGranary(GranaryDefault granary, Environment environment) throws GranaryException {
        if(granary == null){
            throw new GranaryException("Granary must be defined");
        }
        if(environment == null){
            throw new GranaryException("Environment must be defined");
        }
        this.granary = granary;
        this.environment = environment;
        Boolean readOnly = environment.getPropertyBooleanExact(STORAGE_READONLY);
        if(readOnly != null){
            READ_ONLY = readOnly;
        }
        String[] resources = defineResources();
        Charset charset = Charset.forName(environment.getPropertyStringExact(EnumEnviroment.DEFAULT_CHARSET.propertyName()));
        if(resources != null){
            for(String resource : resources){
                try {
                    Map<String, Object> map = PropertiesLoader.loadProperty(resource, charset);
                    initResource(map);
                } catch (LoadResourceException e) {
                    getLogger().error(String.format("Cannot load resource: %s", resource), e);
                    throw new GranaryException("Cannot load resource", e);
                }
            }
        }
        granary.validateAllSeeds();
    }
    @SuppressWarnings("unchecked")
    protected void initResource(Map<String, Object> map) throws LoadResourceException, GranaryException{
        Object o = PropertiesLoader.findProperty(FIELD_SEED, map);
        if(o instanceof List){
            //here is more than one seed
            List<Object> list = (List<Object>) o;
            for(Object element : list){
                if(element instanceof Map){
                    loadSeed((Map<String, Object>) element);
                }else{
                    throw new LoadResourceException("Expected Seed");
                }
            }
        }
    }
    protected abstract String[] defineResources() throws GranaryException;
    protected abstract Logger getLogger();
    
    @SuppressWarnings("unchecked")
    private void loadSeed(Map<String,Object> map) throws LoadResourceException, GranaryException{
        String seedName = PropertiesLoader.getPropertyStringExact(FIELD_SEED_NAME, map);
        if(HelperStringParser.isStringEmptySkipSpace(seedName)){
            throw new LoadResourceException("Seed's name must be defined");
        }
        EnumSeedType seedType;
        String seedTypeName = PropertiesLoader.getPropertyStringExact(FIELD_SEED_TYPE, map);
        try{
            seedType = EnumSeedType.valueOf(seedTypeName);
        }catch(Exception e){
            throw new LoadResourceException("Unknown Seed's type: "+seedTypeName);
        }
        String seedDomain = PropertiesLoader.getPropertyStringExact(FIELD_SEED_DOMAIN, map);
        String seedParent = PropertiesLoader.getPropertyStringExact(FIELD_SEED_PARENT, map);
        if(HelperStringParser.isStringEmptySkipSpace(seedDomain)){ 
            if(seedType != EnumSeedType.DOMAIN_SEED){
                seedDomain = (seedParent!=null)?seedParent:"" + "/" + seedName;
            }else
                throw new LoadResourceException("Seed's domain must be defined");
        }
        BuilderSeed builderSeed = granary.builderSeed();
        builderSeed.seedName(seedName);
        builderSeed.setDomainPath(seedDomain);
        if(seedParent != null){
            builderSeed.setParentPath(seedParent);
        }
        builderSeed.setType(seedType);
        Map<String, Object> dnas = PropertiesLoader.getPropertyMapExact(FIELD_SEED_DNA, map);
        if(dnas != null){
            for(Map.Entry<String, Object> entry : dnas.entrySet()){
                String nameDNA = entry.getKey();
                if(entry.getValue() instanceof Map){
                    Map<String,Object> dna = (Map<String, Object>) entry.getValue();
                    loadDNA(builderSeed, nameDNA, dna);
                }else{
                    throw new LoadResourceException("DNA ["+nameDNA+"] must be presented as Map");
                }
            }
        }
    }
    private void loadDNA(BuilderSeed builderSeed, String nameDNA, Map<String, Object> dna) throws LoadResourceException, GranaryException {
        Boolean constant = PropertiesLoader.getPropertyBooleanExact(FIELD_SEED_DNA_CONSTANT, dna);
        Boolean inheritance = PropertiesLoader.getPropertyBooleanExact(FIELD_SEED_DNA_INHERITANCE, dna);
        Boolean secured = PropertiesLoader.getPropertyBooleanExact(FIELD_SEED_DNA_SECURED, dna);
        String typeName = PropertiesLoader.getPropertyStringExact(FIELD_SEED_DNA_TYPE, dna);
        EnumTypes dnaType;
        try{
            dnaType = EnumTypes.valueOf(typeName);
        }catch(Exception e){
            throw new LoadResourceException(String.format("Unknow DNA's type. DNA:[%s], type:[%s]",nameDNA, typeName));
        }
        String value = PropertiesLoader.getPropertyStringExact(FIELD_SEED_DNA_VALUE, dna);
        typeName = PropertiesLoader.getPropertyStringExact(FIELD_SEED_DNA_DATA_TYPE, dna);
        EnumEL dnaDataType = null;
        if(typeName != null){
            try{
                dnaDataType = EnumEL.valueOf(typeName);
            }catch(Exception e){
                throw new LoadResourceException(String.format("Unknow DNA's data type. DNA:[%s], type:[%s]",nameDNA, typeName));
            }
        }
        Map<String, Object> map = PropertiesLoader.getPropertyMapExact(FIELD_SEED_DNA_VALUE_EXTRA, dna);
        Map<String,String> additinalValue = null;
        if(map != null){
            additinalValue = new HashMap<>();
            for(Map.Entry<String,Object> entry : map.entrySet())
                additinalValue.put(entry.getKey(), entry.getValue().toString());
        }
        BuilderDNA builder = builderSeed.createDNA(nameDNA);
        if(constant != null)
            builder.setConstant(constant);
        if(inheritance != null)
            builder.setInheritance(inheritance);
        if(secured != null)
            builder.setSecured(secured);
        builder.setType(dnaType);
        builder.setDefaultValue(value);
        if(dnaDataType != null)
            builder.setDefaultValueType(dnaDataType);
        if(additinalValue != null)
            builder.setaAditionalValue(additinalValue);
    }
    /* (non-Javadoc)
     * @see org.spathiphyllum.ground.GranaryStorageProvider#saveSeed(org.spathiphyllum.ground.SeedDefault)
     */
    @Override
    public void saveSeed(SeedDefault seed) throws GranaryException {
        if(READ_ONLY){
            throw new GranaryException("This storage READ_ONLY");
        }
    }

    /* (non-Javadoc)
     * @see org.spathiphyllum.ground.GranaryStorageProvider#loadFlower(org.spathiphyllum.ground.GranaryDefault)
     */
    @Override
    public void loadFlower(GranaryDefault granary) throws GranaryException {
        if(READ_ONLY){
            throw new GranaryException("This storage READ_ONLY");
        }
    }

    /* (non-Javadoc)
     * @see org.spathiphyllum.ground.GranaryStorageProvider#saveFlower(org.spathiphyllum.ground.FlowerDefault)
     */
    @Override
    public void saveFlower(FlowerDefault flower) throws GranaryException {
        if(READ_ONLY){
            throw new GranaryException("This storage READ_ONLY");
        }
    }

}
