/*
 * Copyright (C) 2018-2018 The Project Spathiphyllum Authors.
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
package org.spathiphyllum.ground.domain;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.Reader;
import java.nio.charset.Charset;
import java.util.Map;

import javax.xml.parsers.ParserConfigurationException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.spathiphyllum.EnumEnviroment;
import org.spathiphyllum.EnumSupportedDocument;
import org.spathiphyllum.EnumTypes;
import org.spathiphyllum.Petal;
import org.spathiphyllum.Seed;
import org.spathiphyllum.el.ParseELException;
import org.spathiphyllum.ground.tools.PropertiesLoader;
import org.w3c.dom.Document;
import org.xml.sax.SAXException;

/**
 * @author obelous
 *
 */
public class DocumentDefautMeaning extends AbstractDefautMeaning {
    private static final Logger LOGGER = LoggerFactory.getLogger(DocumentDefautMeaning.class);
    
    private EnumSupportedDocument documentType;
    private Charset charset = Charset.forName(EnumEnviroment.DEFAULT_CHARSET.value());
    private Document document;
    private Map<String,Object> documentMap;
    
    /**
     * Type of document
     * @return
     */
    public EnumSupportedDocument getDocumentType() {
        return documentType;
    }
    /**
     * Type of document
     * @param documentType
     */
    public void setDocumentType(EnumSupportedDocument documentType) {
        this.documentType = documentType;
    }
    /**
     * Charset
     * @return
     */
    public Charset getCharset() {
        return charset;
    }
    /**
     * Charset
     * @param charset
     */
    public void setCharset(Charset charset) {
        this.charset = charset;
    }
    @Override
    public boolean validate(Seed seed) throws ParseELException {
        if(documentType == null){
            throw new ParseELException(0, "Need to define type of document");
        }
        if(!isValueEmpty()){
            try(Reader reader = new InputStreamReader(new ByteArrayInputStream(getValue().getBytes(charset)),charset)){
                switch (documentType) {
                case JSON:
                    documentMap = PropertiesLoader.loadPropertiesAsJson(reader);
                    break;
                case PROPERTIES:
                    documentMap = PropertiesLoader.loadPropertiesDefault(reader);
                    break;
                case XML:
                    document = PropertiesLoader.loadXmlDocument(reader);
                    break;
                case YAML:
                    documentMap = PropertiesLoader.loadPropertiesAsYaml(reader);
                    break;
                default:
                    throw new ParseELException(0, "Unknown type of document");
                }
            } catch (IOException | SAXException | ParserConfigurationException e) {
                LOGGER.error("validate document", e);
                throw new ParseELException("Cannot parse document", 0, e.getMessage());
            };
        }
        return true;
    }
    @Override
    public void growPetal(Petal petal) {
        //TODO
    }
    @Override
    public EnumTypes getType() {
        return EnumTypes.DOCUMENT;
    }
}
