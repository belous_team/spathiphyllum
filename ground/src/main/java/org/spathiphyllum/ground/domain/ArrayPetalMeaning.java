/*
 * Copyright (C) 2018-2018 The Project Spathiphyllum Authors.
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
package org.spathiphyllum.ground.domain;

import java.math.BigDecimal;
import java.math.BigInteger;

import org.apache.commons.text.StringEscapeUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.spathiphyllum.EnumTypes;
import org.spathiphyllum.ExchangeOfMeaning;
import org.spathiphyllum.InvokeELException;
import org.spathiphyllum.Petal;
import org.spathiphyllum.PetalMeaning;
import org.spathiphyllum.el.ParseELException;
import org.spathiphyllum.el.tools.HelperStringParser;

/**
 * We do not enable to modify array in runtime
 * Only initialize new array through parsing new string value
 * 
 * @author obelous
 *
 */
public class ArrayPetalMeaning extends AbstractPetalMeaning {
    private static final Logger LOGGER = LoggerFactory.getLogger(ArrayPetalMeaning.class);
    
    private final EnumTypes type;
    private byte[] bytes = HelperStringParser.EMPTY_BYTES;
    private boolean[] booleans = HelperStringParser.EMPTY_BOOLEANS;
    private short[] shorts = HelperStringParser.EMPTY_SHORTS;
    private char[] chars = HelperStringParser.EMPTY_CHARS;
    private int[] ints = HelperStringParser.EMPTY_INTS;
    private long[] longs = HelperStringParser.EMPTY_LONGS;
    private float[] floats = HelperStringParser.EMPTY_FLOATS;
    private double[] doubles = HelperStringParser.EMPTY_DOUBLES;
    private BigDecimal[] decimals = HelperStringParser.EMPTY_BIG_DECIMALS;
    private BigInteger[] integers = HelperStringParser.EMPTY_BIG_INTEGERS;
    private String[] strings = HelperStringParser.EMPTY_STRINGS;
    /**
     * @param petal
     */
    public ArrayPetalMeaning(Petal petal, EnumTypes types) {
        super(petal);
        this.type = types;
    }
    
    /* (non-Javadoc)
     * @see org.spathiphyllum.ground.PetalMeaning#nourishPetal(org.spathiphyllum.ground.Petal)
     */
    @Override
    public void nourishPetal(Petal petal) {
        if(petal.getPetalType() == EnumTypes.ARRAY && petal.getPatalMeaning().getType() == type){
            ArrayPetalMeaning array = (ArrayPetalMeaning) petal.getPatalMeaning();
            switch (type) {
            case BIG_DECIMAL:
                array.decimals =  decimals;
                break;
            case BIG_INTEGER:
                array.integers = integers;
                break;
            case BOOLEAN:
                array.booleans = booleans;
                break;
            case BYTE:
                array.bytes = bytes;
                break;
            case CHAR:
                array.chars = chars;
                break;
            case DOUBLE:
                array.doubles = doubles;
                break;
            case FLOAT:
                array.floats = floats;
                break;
            case INT:
                array.ints = ints;
                break;
            case LONG:
                array.longs = longs;
                break;
            case SHORT:
                array.shorts = shorts;
                break;
            case STRING:
                array.strings = strings;
                break;
            default:
                LOGGER.error("Invoke nourish from {} to {}", this, array);
                throw new InvokeELException("Invalide type of Petal ARRAY");
            }
        }else{
            throw new InvokeELException(ERR_UNSUPPORT_TYPE);
        }
    }
    /* (non-Javadoc)
     * @see org.spathiphyllum.ground.PetalMeaning#nourishPetal(org.spathiphyllum.ground.Petal, int)
     */
    @Override
    public void nourishPetal(Petal petal, int index) {
        if(index >= length()){
            throw new InvokeELException("Index out of bounds");
        }
        if(petal.getPetalType().isSimple()){
            PetalMeaning meaning = petal.getPatalMeaning();
            switch (petal.getPetalType()) {
            case BIG_DECIMAL:
                meaning.nourishBigDecimal(asBigDecimal(index));
                break;
            case BIG_INTEGER:
                meaning.nourishBigInteger(asBigInteger(index));
                break;
            case BOOLEAN:
                meaning.nourishBoolean(asBoolean(index));
                break;
            case BYTE:
                meaning.nourishByte(asByte(index));
                break;
            case CHAR:
                meaning.nourishChar(asChar(index));
                break;
            case DOUBLE:
                meaning.nourishDouble(asDouble(index));
                break;
            case FLOAT:
                meaning.nourishFloat(asFloat(index));
                break;
            case INT:
                meaning.nourishInt(asInt(index));
                break;
            case LONG:
                meaning.nourishLong(asLong(index));
                break;
            case SHORT:
                meaning.nourishShort(asShort(index));
                break;
            case STRING:
                String s = StringEscapeUtils.escapeJava(asString(index));
                try {
                    meaning.nourishString(s);
                } catch (ParseELException e) {
                    throw new InvokeELException(WTF_IMPOSSIBLE);
                }
                break;
            default:
                LOGGER.error("Invoke nourish from {} to {}", this, meaning);
                throw new InvokeELException("Invalide type of Petal ARRAY");
            }
        }
    }
    private String asString(int index) {
        switch (type) {
        case BIG_DECIMAL:
            return HelperStringParser.toString(decimals[index]);
        case BIG_INTEGER:
            return HelperStringParser.toString(integers[index]);
        case BOOLEAN:
            return HelperStringParser.toString(booleans[index]);
        case BYTE:
            return HelperStringParser.toString(bytes[index]);
        case CHAR:
            return HelperStringParser.toString(chars[index]);
        case DOUBLE:
            return HelperStringParser.toString(doubles[index]);
        case FLOAT:
            return HelperStringParser.toString(floats[index]);
        case INT:
            return HelperStringParser.toString(ints[index]);
        case LONG:
            return HelperStringParser.toString(longs[index]);
        case SHORT:
            return HelperStringParser.toString(shorts[index]);
        case STRING:
            return strings[index];
        default:
            LOGGER.error(WTF_IMPOSSIBLE,type);
            throw new InvokeELException(ERR_UNSUPPORT_TYPE);
        }
    }

    private short asShort(int index) {
        switch (type) {
        case BIG_DECIMAL:
            return decimals[index].shortValue();
        case BIG_INTEGER:
            return integers[index].shortValue();
        case BOOLEAN:
            throw new InvokeELException("Cannot convert from boolean to short");
        case BYTE:
            return bytes[index];
        case CHAR:
            return (short) chars[index];
        case DOUBLE:
            return (short) doubles[index];
        case FLOAT:
            return (short) floats[index];
        case INT:
            return (short) ints[index];
        case LONG:
            return (short) longs[index];
        case SHORT:
            return shorts[index];
        case STRING:
            throw new InvokeELException("Cannot convert from string to short");
        default:
            LOGGER.error(WTF_IMPOSSIBLE,type);
            throw new InvokeELException(ERR_UNSUPPORT_TYPE);
        }
    }

    private long asLong(int index) {
        switch (type) {
        case BIG_DECIMAL:
            return decimals[index].longValue();
        case BIG_INTEGER:
            return integers[index].longValue();
        case BOOLEAN:
            throw new InvokeELException("Cannot convert from boolean to long");
        case BYTE:
            return bytes[index];
        case CHAR:
            return chars[index];
        case DOUBLE:
            return (long) doubles[index];
        case FLOAT:
            return (long) floats[index];
        case INT:
            return ints[index];
        case LONG:
            return longs[index];
        case SHORT:
            return shorts[index];
        case STRING:
            throw new InvokeELException("Cannot convert from string to long");
        default:
            LOGGER.error(WTF_IMPOSSIBLE,type);
            throw new InvokeELException(ERR_UNSUPPORT_TYPE);
        }
    }

    private int asInt(int index) {
        switch (type) {
        case BIG_DECIMAL:
            return decimals[index].intValue();
        case BIG_INTEGER:
            return integers[index].intValue();
        case BOOLEAN:
            throw new InvokeELException("Cannot convert from boolean to int");
        case BYTE:
            return bytes[index];
        case CHAR:
            return chars[index];
        case DOUBLE:
            return (int) doubles[index];
        case FLOAT:
            return (int) floats[index];
        case INT:
            return ints[index];
        case LONG:
            return (int) longs[index];
        case SHORT:
            return shorts[index];
        case STRING:
            throw new InvokeELException("Cannot convert from string to int");
        default:
            LOGGER.error(WTF_IMPOSSIBLE,type);
            throw new InvokeELException(ERR_UNSUPPORT_TYPE);
        }
    }

    private float asFloat(int index) {
        switch (type) {
        case BIG_DECIMAL:
            return decimals[index].floatValue();
        case BIG_INTEGER:
            return integers[index].floatValue();
        case BOOLEAN:
            throw new InvokeELException("Cannot convert from boolean to float");
        case BYTE:
            return bytes[index];
        case CHAR:
            return chars[index];
        case DOUBLE:
            return (float) doubles[index];
        case FLOAT:
            return floats[index];
        case INT:
            return ints[index];
        case LONG:
            return longs[index];
        case SHORT:
            return shorts[index];
        case STRING:
            throw new InvokeELException("Cannot convert from string to float");
        default:
            LOGGER.error(WTF_IMPOSSIBLE,type);
            throw new InvokeELException(ERR_UNSUPPORT_TYPE);
        }
    }

    private double asDouble(int index) {
        switch (type) {
        case BIG_DECIMAL:
            return decimals[index].doubleValue();
        case BIG_INTEGER:
            return integers[index].doubleValue();
        case BOOLEAN:
            throw new InvokeELException("Cannot convert from boolean to double");
        case BYTE:
            return bytes[index];
        case CHAR:
            return chars[index];
        case DOUBLE:
            return doubles[index];
        case FLOAT:
            return floats[index];
        case INT:
            return ints[index];
        case LONG:
            return longs[index];
        case SHORT:
            return shorts[index];
        case STRING:
            throw new InvokeELException("Cannot convert from string to double");
        default:
            LOGGER.error(WTF_IMPOSSIBLE,type);
            throw new InvokeELException(ERR_UNSUPPORT_TYPE);
        }
    }

    private BigDecimal asBigDecimal(int index){
        switch (type) {
        case BIG_DECIMAL:
            return decimals[index];
        case BIG_INTEGER:
            return new BigDecimal(integers[index]);
        case BOOLEAN:
            throw new InvokeELException("Cannot convert from boolean to BigDecimal");
        case BYTE:
            return BigDecimal.valueOf(bytes[index]);
        case CHAR:
            return BigDecimal.valueOf(chars[index]);
        case DOUBLE:
            return BigDecimal.valueOf(doubles[index]);
        case FLOAT:
            return BigDecimal.valueOf(floats[index]);
        case INT:
            return BigDecimal.valueOf(ints[index]);
        case LONG:
            return BigDecimal.valueOf(longs[index]);
        case SHORT:
            return BigDecimal.valueOf(shorts[index]);
        case STRING:
            throw new InvokeELException("Cannot convert from string to BigDecimal");
        default:
            LOGGER.error(WTF_IMPOSSIBLE,type);
            throw new InvokeELException(ERR_UNSUPPORT_TYPE);
        }
    }
    private BigInteger asBigInteger(int index){
        switch (type) {
        case BIG_DECIMAL:
            return decimals[index].toBigInteger();
        case BIG_INTEGER:
            return integers[index];
        case BOOLEAN:
            throw new InvokeELException("Cannot convert from boolean to BigInteger");
        case BYTE:
            return BigInteger.valueOf(bytes[index]);
        case CHAR:
            return BigInteger.valueOf(chars[index]);
        case DOUBLE:
            return BigInteger.valueOf((long)doubles[index]);
        case FLOAT:
            return BigInteger.valueOf((long)floats[index]);
        case INT:
            return BigInteger.valueOf(ints[index]);
        case LONG:
            return BigInteger.valueOf(longs[index]);
        case SHORT:
            return BigInteger.valueOf(shorts[index]);
        case STRING:
            throw new InvokeELException("Cannot convert from string to BigInteger");
        default:
            LOGGER.error(WTF_IMPOSSIBLE,type);
            throw new InvokeELException(ERR_UNSUPPORT_TYPE);
        }
    }
    private boolean asBoolean(int index){
        switch (type) {
        case BIG_DECIMAL:
            throw new InvokeELException("Cannot convert from BigDecimal to boolean");
        case BIG_INTEGER:
            throw new InvokeELException("Cannot convert from BigInteger to boolean");
        case BOOLEAN:
            return booleans[index];
        case BYTE:
            throw new InvokeELException("Cannot convert from byte to boolean");
        case CHAR:
            throw new InvokeELException("Cannot convert from char to boolean");
        case DOUBLE:
            throw new InvokeELException("Cannot convert from double to boolean");
        case FLOAT:
            throw new InvokeELException("Cannot convert from float to boolean");
        case INT:
            throw new InvokeELException("Cannot convert from int to boolean");
        case LONG:
            throw new InvokeELException("Cannot convert from long to boolean");
        case SHORT:
            throw new InvokeELException("Cannot convert from short to boolean");
        case STRING:
            throw new InvokeELException("Cannot convert from string to boolean");
        default:
            LOGGER.error(WTF_IMPOSSIBLE,type);
            throw new InvokeELException(ERR_UNSUPPORT_TYPE);
        }
    }
    private byte asByte(int index){
        switch (type) {
        case BIG_DECIMAL:
            return decimals[index].byteValue();
        case BIG_INTEGER:
            return integers[index].byteValue();
        case BOOLEAN:
            throw new InvokeELException("Cannot convert from boolean to byte");
        case BYTE:
            return bytes[index];
        case CHAR:
            return (byte) chars[index];
        case DOUBLE:
            return (byte) doubles[index];
        case FLOAT:
            return (byte) floats[index];
        case INT:
            return (byte) ints[index];
        case LONG:
            return (byte) longs[index];
        case SHORT:
            return (byte) shorts[index];
        case STRING:
            throw new InvokeELException("Cannot convert from string to byte");
        default:
            LOGGER.error(WTF_IMPOSSIBLE,type);
            throw new InvokeELException(ERR_UNSUPPORT_TYPE);
        }
    }
    private char asChar(int index){
        switch (type) {
        case BIG_DECIMAL:
            return (char) decimals[index].intValue();
        case BIG_INTEGER:
            return (char) integers[index].intValue();
        case BOOLEAN:
            throw new InvokeELException("Cannot convert from boolean to char");
        case BYTE:
            return (char) bytes[index];
        case CHAR:
            return chars[index];
        case DOUBLE:
            return (char) doubles[index];
        case FLOAT:
            return (char) floats[index];
        case INT:
            return (char) ints[index];
        case LONG:
            return (char) longs[index];
        case SHORT:
            return (char) shorts[index];
        case STRING:
            throw new InvokeELException("Cannot convert from string to char");
        default:
            LOGGER.error(WTF_IMPOSSIBLE,type);
            throw new InvokeELException(ERR_UNSUPPORT_TYPE);
        }
    }

    /* (non-Javadoc)
     * @see org.spathiphyllum.ground.PetalMeaning#nourishString(java.lang.String)
     */
    @Override
    public void nourishString(String value) throws ParseELException {
        switch (type) {
        case BIG_DECIMAL:
            decimals = HelperStringParser.parseBigDecimalArray(value);
            break;
        case BIG_INTEGER:
            integers = HelperStringParser.parseBigIntegerArray(value);
            break;
        case BOOLEAN:
            booleans = HelperStringParser.parseBooleanArray(value);
            break;
        case BYTE:
            bytes = HelperStringParser.parseByteArray(value);
            break;
        case CHAR:
            chars = HelperStringParser.parseCharArray(value);
            break;
        case DOUBLE:
            doubles = HelperStringParser.parseDoubleArray(value);
            break;
        case FLOAT:
            floats = HelperStringParser.parseFloatArray(value);
            break;
        case INT:
            ints = HelperStringParser.parseIntArray(value);
            break;
        case LONG:
            longs = HelperStringParser.parseLongArray(value);
            break;
        case SHORT:
            shorts = HelperStringParser.parseShortArray(value);
            break;
        case STRING:
            strings = HelperStringParser.parseStringArray(value);
            break;
        default:
            LOGGER.error(WTF_IMPOSSIBLE,type);
            throw new InvokeELException(ERR_UNSUPPORT_TYPE);
        }
        setDirty();
    }
    /* (non-Javadoc)
     * @see org.spathiphyllum.ground.PetalMeaning#saveToExchage(org.spathiphyllum.ground.ExchangeOfMeaning)
     */
    @Override
    public void saveToExchage(ExchangeOfMeaning exchange) {
        switch (type) {
        case BIG_DECIMAL:
            exchange.setValue(HelperStringParser.arrayToString(decimals));
            break;
        case BIG_INTEGER:
            exchange.setValue(HelperStringParser.arrayToString(integers));
            break;
        case BOOLEAN:
            exchange.setValue(HelperStringParser.arrayToString(booleans));
            break;
        case BYTE:
            exchange.setValue(HelperStringParser.arrayToString(bytes));
            break;
        case CHAR:
            exchange.setValue(HelperStringParser.arrayToString(chars));
            break;
        case DOUBLE:
            exchange.setValue(HelperStringParser.arrayToString(doubles));
            break;
        case FLOAT:
            exchange.setValue(HelperStringParser.arrayToString(floats));
            break;
        case INT:
            exchange.setValue(HelperStringParser.arrayToString(ints));
            break;
        case LONG:
            exchange.setValue(HelperStringParser.arrayToString(longs));
            break;
        case SHORT:
            exchange.setValue(HelperStringParser.arrayToString(shorts));
            break;
        case STRING:
            exchange.setValue(HelperStringParser.arrayToString(strings));
            break;
        default:
            LOGGER.error(WTF_IMPOSSIBLE,type);
            throw new InvokeELException(ERR_UNSUPPORT_TYPE);
        }
    }
    @Override
    public void saveToExchage(ExchangeOfMeaning exchange, int index) {
        if(index >= length()){
            throw new InvokeELException("Index out of bounds");
        }
        switch (type) {
        case BIG_DECIMAL:
            exchange.setValue(decimals[index]);
            break;
        case BIG_INTEGER:
            exchange.setValue(integers[index]);
            break;
        case BOOLEAN:
            exchange.setValue(booleans[index]);
            break;
        case BYTE:
            exchange.setValue(bytes[index]);
            break;
        case CHAR:
            exchange.setValue(chars[index]);
            break;
        case DOUBLE:
            exchange.setValue(doubles[index]);
            break;
        case FLOAT:
            exchange.setValue(floats[index]);
            break;
        case INT:
            exchange.setValue(ints[index]);
            break;
        case LONG:
            exchange.setValue(longs[index]);
            break;
        case SHORT:
            exchange.setValue(shorts[index]);
            break;
        case STRING:
            exchange.setValue(strings[index]);
            break;
        default:
            LOGGER.error(WTF_IMPOSSIBLE,type);
            throw new InvokeELException(ERR_UNSUPPORT_TYPE);
        }
    }
    @Override
    public int length() {
        switch (type) {
        case BIG_DECIMAL:
            return decimals.length;
        case BIG_INTEGER:
            return integers.length;
        case BOOLEAN:
            return booleans.length;
        case BYTE:
            return bytes.length;
        case CHAR:
            return chars.length;
        case DOUBLE:
            return doubles.length;
        case FLOAT:
            return floats.length;
        case INT:
            return ints.length;
        case LONG:
            return longs.length;
        case SHORT:
            return shorts.length;
        case STRING:
            return strings.length;
        default:
            LOGGER.error(WTF_IMPOSSIBLE,type);
            throw new InvokeELException(ERR_UNSUPPORT_TYPE);
        }
    }
}
