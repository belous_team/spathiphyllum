/*
 * Copyright (C) 2018-2018 The Project Spathiphyllum Authors.
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
package org.spathiphyllum.ground.domain;

import org.spathiphyllum.EnumTypes;
import org.spathiphyllum.ExchangeOfMeaning;
import org.spathiphyllum.InvokeELException;
import org.spathiphyllum.Petal;
import org.spathiphyllum.PetalMeaning;
import org.spathiphyllum.el.ParseELException;
import org.spathiphyllum.el.tools.HelperStringParser;

/**
 * @author obelous
 *
 */
public class StringMeaning extends AbstractPetalMeaning {
    private String v;
    /**
     * @param petal
     */
    public StringMeaning(Petal petal) {
        super(petal);
    }
    @Override
    public void nourishPetal(Petal petal) {
        PetalMeaning meaning = petal.getPatalMeaning();
        if(petal.getPetalType() == EnumTypes.STRING) {
            try {
                meaning.nourishString(v);
            } catch (ParseELException e) {
                throw new InvokeELException(WTF_IMPOSSIBLE);
            }
        }else{
            throw new InvokeELException(ERR_UNSUPPORT_TYPE);
        }
    }
    @Override
    public void saveToExchage(ExchangeOfMeaning exchange) {
        exchange.setValue(HelperStringParser.toString(v));
    }
    @Override
    public int length() {
        return v.length();
    }
    @Override
    public void nourishString(String value) throws ParseELException {
        v = value;
        setDirty();
    }
    @Override
    public void parseString(String value) throws ParseELException {
        v = HelperStringParser.parseString(value);
        setDirty();
    }
}
