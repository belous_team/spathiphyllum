/*
 * Copyright (C) 2018-2018 The Project Spathiphyllum Authors.
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
package org.spathiphyllum.ground.domain;

import java.math.BigDecimal;
import java.math.BigInteger;

import org.spathiphyllum.ExchangeOfMeaning;
import org.spathiphyllum.InvokeELException;
import org.spathiphyllum.Petal;
import org.spathiphyllum.PetalMeaning;
import org.spathiphyllum.el.ParseELException;
import org.spathiphyllum.el.tools.HelperStringParser;

/**
 * @author obelous
 *
 */
public class ShortMeaning extends AbstractPetalMeaning {
    private short v;
    /**
     * @param petal
     */
    public ShortMeaning(Petal petal) {
        super(petal);
    }
    @Override
    public void nourishPetal(Petal petal) {
        PetalMeaning meaning = petal.getPatalMeaning();
        switch (petal.getPetalType()) {
        case BIG_DECIMAL:
            meaning.nourishBigDecimal(BigDecimal.valueOf(v));
            break;
        case BIG_INTEGER:
            meaning.nourishBigInteger(BigInteger.valueOf(v));
            break;
        case BYTE:
            meaning.nourishByte((byte) v);
            break;
        case CHAR:
            meaning.nourishChar((char) v);
            break;
        case DOUBLE:
            meaning.nourishDouble(v);
            break;
        case FLOAT:
            meaning.nourishFloat(v);
            break;
        case INT:
            meaning.nourishInt(v);
            break;
        case LONG:
            meaning.nourishLong(v);
            break;
        case SHORT:
            meaning.nourishShort(v);
            break;
        case STRING:
            try {
                meaning.nourishString(HelperStringParser.toString(v));
            } catch (ParseELException e) {
                throw new InvokeELException(WTF_IMPOSSIBLE);
            }
            break;
        default:
            throw new InvokeELException(ERR_UNSUPPORT_TYPE);
        }
    }
    @Override
    public void saveToExchage(ExchangeOfMeaning exchange) {
        exchange.setValue(v);
    }
    @Override
    public int length() {
        return 1;
    }
    @Override
    public void nourishBigDecimal(BigDecimal decimal) {
        v = decimal.shortValue();
        setDirty();
    }
    @Override
    public void nourishBigInteger(BigInteger integer) {
        v = integer.shortValue();
        setDirty();
    }
    @Override
    public void nourishByte(byte value) {
        v = value;
        setDirty();
    }
    @Override
    public void nourishChar(char value) {
        v = (short) value;
        setDirty();
    }
    @Override
    public void nourishDouble(double value) {
        v = (short) value;
        setDirty();
    }
    @Override
    public void nourishFloat(float value) {
        v = (short) value;
        setDirty();
    }
    @Override
    public void nourishInt(int value) {
        v = (short) value;
        setDirty();
    }
    @Override
    public void nourishLong(long value) {
        v = (short) value;
        setDirty();
    }
    @Override
    public void nourishShort(short value) {
        v = value;
        setDirty();
    }
    @Override
    public void nourishString(String value) throws ParseELException {
        v = HelperStringParser.parseShort(value);
        setDirty();
    }
}
