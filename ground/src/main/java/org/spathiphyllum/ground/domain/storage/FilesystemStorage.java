/*
 * Copyright (C) 2018-2018 The Project Spathiphyllum Authors.
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
package org.spathiphyllum.ground.domain.storage;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.spathiphyllum.GranaryException;
import org.spathiphyllum.el.tools.HelperStringParser;
import org.spathiphyllum.ground.tools.PropertiesLoader;

/**
 * @author obelous
 *
 */
public abstract class FilesystemStorage extends AbstractPropertyStorage {

    @Override
    protected String[] defineResources() throws GranaryException {
        String storagePath = environment.getPropertyStringExact(STORAGE_PATH);
        if(HelperStringParser.isStringEmptySkipSpace(storagePath)){
            throw new GranaryException(STORAGE_PATH+" is not difined");
        }
        if(PropertiesLoader.isDirectory(storagePath)){
            Boolean multiFile = environment.getPropertyBooleanExact(STORAGE_MULTIFILE);
            if(multiFile == null || !multiFile){
                throw new GranaryException(STORAGE_PATH+" is directory but "+STORAGE_MULTIFILE + " disabled");
            }
            try {
                String[] paths = PropertiesLoader.readDirectory(storagePath);
                if(paths != null && paths.length > 0){
                    List<String> files = new ArrayList<>();
                    for(String s: paths){
                        if(isMyResource(s)){
                            files.add(s);
                        }
                    }
                    return files.toArray(new String[files.size()]);
                }else{
                    return HelperStringParser.EMPTY_STRINGS;
                }
            } catch (IOException e) {
                throw new GranaryException("Cannot read directory: "+storagePath, e);
            }
        }else{
            return new String[]{storagePath};
        }
    }
    protected abstract boolean isMyResource(String path);

}
