/*
 * Copyright (C) 2018-2018 The Project Spathiphyllum Authors.
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
package org.spathiphyllum.ground.domain;

import java.util.Map;

import org.spathiphyllum.DefaultMeaning;
import org.spathiphyllum.Petal;
import org.spathiphyllum.el.tools.HelperStringParser;
import org.spathiphyllum.el.EnumEL;

/**
 * @author obelous
 *
 */
abstract class AbstractDefautMeaning implements DefaultMeaning {
    private String value;
    private Map<String, String> extra;
    
    @Override
    public String getValue() {
        return value;
    }

    @Override
    public void setValue(String defaultValue) {
        this.value = defaultValue;
    }

    @Override
    public void growPetal(Petal petal) {
        // TODO Auto-generated method stub
        
    }
    protected boolean isValueEmpty(){
        return HelperStringParser.isStringEmpty(value);
    }
    @Override
    public Map<String, String> additionalValue() {
        return null;
    }
    @Override
    public void setAdditionalValue(Map<String, String> extra) {
        this.extra = extra;
    }
    public void setDataType(EnumEL types){}
}
