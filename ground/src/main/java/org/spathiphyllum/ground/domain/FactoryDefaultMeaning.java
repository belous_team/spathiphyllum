/*
 * Copyright (C) 2018-2018 The Project Spathiphyllum Authors.
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
package org.spathiphyllum.ground.domain;

import java.util.Map;

import org.spathiphyllum.DefaultMeaning;
import org.spathiphyllum.EnumTypes;
import org.spathiphyllum.GranaryException;
import org.spathiphyllum.el.EnumEL;

/**
 * @author obelous
 *
 */
public class FactoryDefaultMeaning {
    
    public static DefaultMeaning factory(EnumTypes typeMeaning, EnumEL typeData, String value, Map<String, String> additional) throws GranaryException {
        AbstractDefautMeaning result = null;
        switch (typeMeaning) {
        case ARRAY:
            result = new ArrayDefautMeaning();
            break;
        case BIG_DECIMAL:
            result = new BigDecimalDefautMeaning();
            break;
        case BIG_INTEGER:
            result = new BigIntegerDefautMeaning();
            break;
        case BOOLEAN:
            result = new BooleanDefautMeaning();
            break;
        case BYTE:
            result = new ByteDefautMeaning();
            break;
        case CHAR:
            result = new CharDefautMeaning();
            break;
        case COMPLEX:
            result = new ComplexDefautMeaning();
            break;
        case EL_LIST:
            result = new ExprLangListDefautMeaning(false);
            break;
        case EL_LIST_ASYNC:
            result = new ExprLangListDefautMeaning(true);
            break;
        case EL_MAP:
            result = new ExprLangMapDefautMeaning();
            break;
        case DOCUMENT:
            result = new DocumentDefautMeaning();
            break;
        case DOUBLE:
            result = new DoubleDefautMeaning();
            break;
        case EL:
            result = new ExprLangDefautMeaning();
            break;
        case FLOAT:
            result = new FloatDefautMeaning();
            break;
        case INT:
            result = new IntDefautMeaning();
            break;
        case LONG:
            result = new LongDefautMeaning();
            break;
        case SHORT:
            result = new ShortDefautMeaning();
            break;
        case STRING:
            result = new StringDefautMeaning();
            break;

        default:
            throw new GranaryException("Cannot create DefaultMeaing. Incorrect EnumType");
        }
        result.setValue(value);
        result.setAdditionalValue(additional);
        if(!result.getType().isSimple()){
            result.setDataType(typeData);
        }
        return result;
    }
    
    private FactoryDefaultMeaning() {}
}
