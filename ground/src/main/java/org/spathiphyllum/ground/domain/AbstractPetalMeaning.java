/*
 * Copyright (C) 2018-2018 The Project Spathiphyllum Authors.
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
package org.spathiphyllum.ground.domain;

import java.math.BigDecimal;
import java.math.BigInteger;

import org.spathiphyllum.EnumTypes;
import org.spathiphyllum.ExchangeOfMeaning;
import org.spathiphyllum.InvokeELException;
import org.spathiphyllum.Petal;
import org.spathiphyllum.PetalMeaning;
import org.spathiphyllum.el.ParseELException;

/**
 * @author obelous
 *
 */
abstract class AbstractPetalMeaning implements PetalMeaning {
    private final Petal petal;
    private boolean dirty = false;
    
    AbstractPetalMeaning(Petal petal){
        this.petal = petal;
    }
    @Override
    public boolean isDirty(){
        return dirty;
    }
    @Override
    public void setDirty(){
        dirty = true;
    }
    @Override
    public void clearDirty(){
        dirty = false;
    }
    /* (non-Javadoc)
     * @see org.spathiphyllum.ground.PetalMeaning#getPetal()
     */
    @Override
    public Petal getPetal() {
        return petal;
    }
    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder("Meaning[");
        sb.append(petal.flower().getName()).append('.')
        .append(petal.getName()).append('(')
        .append(petal.getPetalType().toString());
        if(!petal.getPetalType().isSimple()){
            sb.append('.').append(this.getType());
        }
        sb.append(")]");
        return sb.toString();
    }
    @Override
    public void nourishPetal(Petal petal, int index) {
        if(getType() != EnumTypes.ARRAY){
            throw new InvokeELException("Only ARRAY supports index");
        }
    }
    @Override
    public void nourishBigDecimal(BigDecimal value) {
        throw new InvokeELException("Cannot be nourished with BigDecimal");
    }
    @Override
    public void nourishBoolean(boolean value) {
        throw new InvokeELException("Cannot be nourished with boolean");
    }
    @Override
    public void nourishBigInteger(BigInteger value) {
        throw new InvokeELException("Cannot be nourished with BigIntegerl");
    }
    @Override
    public void nourishByte(byte value) {
        throw new InvokeELException("Cannot be nourished with byte");
    }
    @Override
    public void nourishChar(char value) {
        throw new InvokeELException("Cannot be nourished with char");
    }
    @Override
    public void nourishDouble(double value) {
        throw new InvokeELException("Cannot be nourished with double");
    }
    @Override
    public void nourishFloat(float value) {
        throw new InvokeELException("Cannot be nourished with float");
    }
    @Override
    public void nourishInt(int value) {
        throw new InvokeELException("Cannot be nourished with int");
    }
    @Override
    public void nourishLong(long value) {
        throw new InvokeELException("Cannot be nourished with long");
    }
    @Override
    public void nourishShort(short value) {
        throw new InvokeELException("Cannot be nourished with short");
    }
    @Override
    public void nourishString(String value) throws ParseELException {
        throw new InvokeELException("Cannot be nourished with string");
    }
    @Override
    public void parseString(String value) throws ParseELException {
        nourishString(value);
    }
    @Override
    public void parseString(String value, String format) throws ParseELException {
        parseString(value);
    }
    @Override
    public void saveToExchage(ExchangeOfMeaning exchange, int index) {
        if(getType() != EnumTypes.ARRAY){
            throw new InvokeELException("Only ARRAY supports index");
        }
    }
    @Override
    public void initFormExcange(ExchangeOfMeaning exchange) throws ParseELException {
        if(getPetal().isConstant()){
            throw new InvokeELException("The constant Petal cannot be modified");
        }
        switch (exchange.getType()) {
        case BIG_DECIMAL:
            nourishBigDecimal(exchange.getDecimal());
            break;
        case BIG_INTEGER:
            nourishBigInteger(exchange.geBigInteger());
            break;
        case BYTE:
            nourishByte(exchange.getByte());
            break;
        case CHAR:
            nourishChar(exchange.getChar());
            break;
        case DOUBLE:
            nourishDouble(exchange.getDouble());
            break;
        case FLOAT:
            nourishFloat(exchange.getFloat());
            break;
        case INT:
            nourishInt(exchange.getInt());
            break;
        case LONG:
            nourishLong(exchange.getLong());
            break;
        case SHORT:
            nourishShort(exchange.getShort());
            break;
        case STRING:
            parseString(exchange.getString());
            break;
        case BOOLEAN:
            nourishBoolean(exchange.getBoolean());
            break;
        default:
            throw new InvokeELException(ERR_UNSUPPORT_TYPE);
        }
    }
    protected static final String WTF_IMPOSSIBLE = "WTF, it is impossible";
    protected static final String ERR_UNSUPPORT_TYPE = "Unsupported type";
    protected static final String ERR_PARSE = "Cannot nourish with string";
    protected static final String FMT_PARSE_ERR = "Invoke nourish from %s to %s with strig [%s]";
}
