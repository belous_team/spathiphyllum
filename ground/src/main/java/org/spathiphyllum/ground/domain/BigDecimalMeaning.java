/*
 * Copyright (C) 2018-2018 The Project Spathiphyllum Authors.
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
package org.spathiphyllum.ground.domain;

import java.math.BigDecimal;
import java.math.BigInteger;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.spathiphyllum.ExchangeOfMeaning;
import org.spathiphyllum.InvokeELException;
import org.spathiphyllum.Petal;
import org.spathiphyllum.PetalMeaning;
import org.spathiphyllum.el.ParseELException;
import org.spathiphyllum.el.tools.HelperStringParser;

/**
 * @author obelous
 *
 */
public class BigDecimalMeaning extends AbstractPetalMeaning {
    private static final Logger LOGGER = LoggerFactory.getLogger(BigDecimalMeaning.class);
    private BigDecimal decimal;
    /**
     * @param petal
     */
    public BigDecimalMeaning(Petal petal) {
        super(petal);
    }
    @Override
    public void nourishPetal(Petal petal) {
        PetalMeaning meaning = petal.getPatalMeaning();
        switch (petal.getPetalType()) {
        case BIG_DECIMAL:
            meaning.nourishBigDecimal(decimal);
            break;
        case BIG_INTEGER:
            meaning.nourishBigInteger(decimal.toBigInteger());
            break;
        case BYTE:
            meaning.nourishByte(decimal.byteValue());
            break;
        case CHAR:
            meaning.nourishChar((char) decimal.intValue());
            break;
        case DOUBLE:
            meaning.nourishDouble(decimal.doubleValue());
            break;
        case FLOAT:
            meaning.nourishFloat(decimal.floatValue());
            break;
        case INT:
            meaning.nourishInt(decimal.intValue());
            break;
        case LONG:
            meaning.nourishLong(decimal.longValue());
            break;
        case SHORT:
            meaning.nourishShort(decimal.shortValue());
            break;
        case STRING:
            String s = decimal.toString();
            try {
                meaning.nourishString(s);
            } catch (ParseELException e) {
                LOGGER.error(String.format(FMT_PARSE_ERR, this, meaning, s),e);
                throw new InvokeELException(ERR_PARSE);
            }
            break;
        default:
            throw new InvokeELException(ERR_UNSUPPORT_TYPE);
        }
    }
    @Override
    public void saveToExchage(ExchangeOfMeaning exchange) {
        exchange.setValue(decimal);
    }
    @Override
    public int length() {
        return decimal.toString().length();
    }
    @Override
    public void nourishBigDecimal(BigDecimal value) {
        decimal = value;
        setDirty();
    }
    @Override
    public void nourishBigInteger(BigInteger value) {
        decimal = new BigDecimal(value);
        setDirty();
    }
    @Override
    public void nourishByte(byte value) {
        decimal = BigDecimal.valueOf(value);
        setDirty();
    }
    @Override
    public void nourishChar(char value) {
        decimal = BigDecimal.valueOf(value);
        setDirty();
    }
    @Override
    public void nourishDouble(double value) {
        decimal = BigDecimal.valueOf(value);
        setDirty();
    }
    @Override
    public void nourishFloat(float value) {
        decimal = BigDecimal.valueOf(value);
        setDirty();
    }
    @Override
    public void nourishInt(int value) {
        decimal = BigDecimal.valueOf(value);
        setDirty();
    }
    @Override
    public void nourishLong(long value) {
        decimal = BigDecimal.valueOf(value);
        setDirty();
    }
    @Override
    public void nourishShort(short value) {
        decimal = BigDecimal.valueOf(value);
        setDirty();
    }
    @Override
    public void nourishString(String value) throws ParseELException {
        decimal = HelperStringParser.parseBigDecimal(value);
        setDirty();
    }
}
