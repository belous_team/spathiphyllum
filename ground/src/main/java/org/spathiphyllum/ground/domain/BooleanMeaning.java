/*
 * Copyright (C) 2018-2018 The Project Spathiphyllum Authors.
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
package org.spathiphyllum.ground.domain;

import org.spathiphyllum.ExchangeOfMeaning;
import org.spathiphyllum.InvokeELException;
import org.spathiphyllum.Petal;
import org.spathiphyllum.PetalMeaning;
import org.spathiphyllum.el.ParseELException;
import org.spathiphyllum.el.tools.HelperStringParser;

/**
 * @author obelous
 *
 */
public class BooleanMeaning extends AbstractPetalMeaning {
    private boolean value;
    /**
     * @param petal
     */
    public BooleanMeaning(Petal petal) {
        super(petal);
    }
    @Override
    public void nourishPetal(Petal petal) {
        PetalMeaning meaning = petal.getPatalMeaning();
        switch (petal.getPetalType()) {
        case BOOLEAN:
            meaning.nourishBoolean(value);
            break;
        case STRING:
            try {
                meaning.nourishString(HelperStringParser.toString(value));
            } catch (ParseELException e) {
                throw new InvokeELException(WTF_IMPOSSIBLE);
            }
            break;
        default:
            throw new InvokeELException(ERR_UNSUPPORT_TYPE);
        }
    }
    @Override
    public void saveToExchage(ExchangeOfMeaning exchange) {
        exchange.setValue(value);
    }
    @Override
    public void nourishBoolean(boolean value) {
        this.value = value;
        setDirty();
    }
    @Override
    public void nourishString(String s) throws ParseELException {
        value = HelperStringParser.parseBoolean(s);
    }
    @Override
    public int length() {
        return 1;
    }
}
