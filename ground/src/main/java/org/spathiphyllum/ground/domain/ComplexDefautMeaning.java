/*
 * Copyright (C) 2018-2018 The Project Spathiphyllum Authors.
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
package org.spathiphyllum.ground.domain;

import org.spathiphyllum.EnumTypes;
import org.spathiphyllum.Seed;
import org.spathiphyllum.el.EnumEL;
import org.spathiphyllum.el.ParseELException;
import org.spathiphyllum.el.tools.HelperStringParser;

/**
 * 
 * The main function of Complex type is provide access to children petals,
 * so a value of the Complex type can be of any type
 * @author obelous
 *
 */
public class ComplexDefautMeaning extends AbstractDefautMeaning {

    private EnumEL type;
    //TODO 
    private AbstractDefautMeaning value;
    public void setDataType(EnumEL type){
        this.type = type;
    }
    @Override
    public EnumTypes getType() {
        return EnumTypes.COMPLEX;
    }
    
    
    @Override
    public boolean validate(Seed seed) throws ParseELException {
        //TODO 
        if(type == null){
            throw new ParseELException(0, "Need to define type of Array");
        }
        HelperStringParser.parseArray(getValue(), getDataType());
        return true;
    }
    @Override
    public EnumEL getDataType() {
        return type;
    }

}
