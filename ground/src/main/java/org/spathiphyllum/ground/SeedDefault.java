/*
 * Copyright (C) 2018-2018 The Project Spathiphyllum Authors.
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
package org.spathiphyllum.ground;

import java.io.Serializable;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.HashMap;
import java.util.Map;

import org.spathiphyllum.DNA;
import org.spathiphyllum.EnumSeedStatus;
import org.spathiphyllum.EnumSeedType;
import org.spathiphyllum.Environment;
import org.spathiphyllum.SecurityContext;
import org.spathiphyllum.SeedDomain;
import org.spathiphyllum.el.EnumEL;
import org.spathiphyllum.el.PetalProvider;

/**
 * The class describes a Seed
 * 
 * @author obelous
 *
 */
public class SeedDefault implements Serializable, SeedDomain {
    private static final long serialVersionUID = -7626572821748660636L;
    
    private final String seedName;
    private HolderSeed domain;
    private HolderSeed parent;
    /**
     * The list of DNS without parent DNA
     */
    private Map<String, DNA> dna = new HashMap<>();
    private EnumSeedStatus status = EnumSeedStatus.UNSAVED;
    private EnumSeedType type = EnumSeedType.SEED;
    
    SeedDefault(String seedName){
        this.seedName = seedName;
    }
    //================== Implementation of Seed
    @Override
    public SeedDomain seedDomain() {
        return domain != null? domain.getSeed() : null;
    }
    /* (non-Javadoc)
     * @see org.spathiphyllum.ground.Seed#getSeedName()
     */
    @Override
    public String seedName() {
        return seedName;
    }
    /* (non-Javadoc)
     * @see org.spathiphyllum.ground.Seed#getDnaNames()
     */
    @Override
    public Map<String,String> namesDNA() {
        Map<String,String> result = new HashMap<>();
        String path = seedPath();
        for(String n : dna.keySet()){
            result.put(n, path);
        }
        if(getParent() != null){
            for(Map.Entry<String, String> n : getParent().namesDNA().entrySet()){
                result.putIfAbsent(n.getKey(), n.getValue());
            }
        }
        return result;
    }
    /* (non-Javadoc)
     * @see org.spathiphyllum.ground.Seed#getSeedPath()
     */
    @Override
    public String seedPath() {
        return calculateSeedPath(this);
    }
    /**
     * status of Seed: UNSAVED, PERSISTED, ACTIVE Only ACTIVE can interact
     * @return the status
     */
    public EnumSeedStatus seedStatus() {
        return status;
    }
    /**
     * type of Seed
     * @return
     */
    public EnumSeedType seedType() {
        return type;
    }
    Map<String,DNA> onlyDnaOfThisSeed(){
        return new HashMap<>(dna);
    }
    @Override
    public DNA dna(String dnaName) {
        //TODO maybe throw exception if not found?
        return dna.get(dnaName);
    }
    //================= Implementations SeedDomain
    @Override
    public Environment environment() {
        return null;
    }
    @Override
    public SecurityContext securityContext(){
        return null;
    }
    //================= Internal
    /**
     * @return the parent
     */
    SeedDefault getParent() {
        return parent != null? parent.getSeed() : null;
    }
    void setDomain(HolderSeed domain) {
        this.domain = domain;
    }
    void setParent(HolderSeed parent) {
        this.parent = parent;
    }
    void setDna(Map<String,DNA> dna) {
        this.dna.putAll(dna);
    }
    void setStatus(EnumSeedStatus status) {
        this.status = status;
    }
    void setType(EnumSeedType type) {
        this.type = type;
    }
    /**
     * calculate Seed's path
     * @param seedDefault
     * @return
     */
    public static String calculateSeedPath(SeedDefault seedDefault){
        StringBuilder sb = new StringBuilder();
        if(seedDefault.getParent() != null){
            sb.append(seedDefault.getParent().seedPath());
        }
        return sb.append('/').append(seedDefault.seedName()).toString();
    }
    @Override
    public boolean isAbstract() {
        // TODO Auto-generated method stub
        return false;
    }
    @Override
    public PetalProvider petalProvider() {
        return new PetalProvider() {
            /**
             * Only check if this petalName is presented
             * @param petalName
             * @return
             */
            @Override
            public boolean isValid(String petalName) {
                if(isAbstract())
                    return true;
                DNA d = dna(petalName);
                return d != null;
            }
            
            @Override
            public boolean isConstant(String petalName) {
                DNA d = dna(petalName);
                return d != null? d.isConstant() : false;
            }
            
            @Override
            public EnumEL getType(String petalName) {
                DNA d = dna(petalName);
                //TODO what return if this name is absent? throw exception?
                return d != null? d.defaultMeaning().getDataType(): EnumEL.STRING;
            }
            
            @Override
            public String asString(String petalName) {
                return null;
            }
            
            @Override
            public short asShort(String petalName) {
                return 0;
            }
            
            @Override
            public long asLong(String petalName) {
                return 0;
            }
            
            @Override
            public int asInt(String petalName) {
                return 0;
            }
            
            @Override
            public float asFloat(String petalName) {
                return 0;
            }
            
            @Override
            public double asDouble(String petalName) {
                return 0;
            }
            
            @Override
            public char asChar(String petalName) {
                return 0;
            }
            
            @Override
            public byte asByte(String petalName) {
                return 0;
            }
            
            @Override
            public boolean asBoolean(String petalName) {
                return false;
            }
            
            @Override
            public BigInteger asBigInteger(String petalName) {
                return null;
            }
            
            @Override
            public BigDecimal asBigDecimal(String petalName) {
                return null;
            }
        };
    }
}
