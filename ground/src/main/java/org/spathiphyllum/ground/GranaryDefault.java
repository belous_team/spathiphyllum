/*
 * Copyright (C) 2018-2018 The Project Spathiphyllum Authors.
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
package org.spathiphyllum.ground;

import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;
import java.util.TreeSet;
import java.util.UUID;
import java.util.concurrent.ConcurrentHashMap;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.spathiphyllum.DNA;
import org.spathiphyllum.EnumSeedType;
import org.spathiphyllum.EnumTypes;
import org.spathiphyllum.Flower;
import org.spathiphyllum.FlowerBusyException;
import org.spathiphyllum.FlowerNotFoundException;
import org.spathiphyllum.Granary;
import org.spathiphyllum.GranaryException;
import org.spathiphyllum.SecurityException;
import org.spathiphyllum.Seed;
import org.spathiphyllum.SeedDomain;
import org.spathiphyllum.SeedNotFoundException;

/**
 * @author obelous
 *
 */
public class GranaryDefault implements Granary, GranaryManagement {
    private static final Logger LOGGER = LoggerFactory.getLogger(GranaryDefault.class);
    //this is caches for store seeds and DNAs that is not through out validation process
    private static final Map<String, BuilderSeed> INVALIDATE_SEED = new ConcurrentHashMap<>();
    //this is caches for valid seeds and DNAs
    private static final Map<String, HolderSeed> CACHE_SEED = new ConcurrentHashMap<>();
    private static final Map<String, HolderSeed> CACHE_COMPOSITE_SEED = new ConcurrentHashMap<>();
    private static final Map<String, HolderFlower> CACHE_FLOWER = new ConcurrentHashMap<>();
    //this us cache for Flower
    private GranaryStorageProvider storageProvider;
    //====================== Implementation of Measurement 
    /* (non-Javadoc)
     * @see org.spathiphyllum.Measurement#measure(java.lang.String, java.util.Map)
     */
    @Override
    public void measure(String prefix, Map<String, Object> measure) {
        // TODO Auto-generated method stub
    }
    //====================== Implementation of Granary 
    /* (non-Javadoc)
     * @see org.spathiphyllum.Granary#getSeed(java.lang.String)
     */
    @Override
    public Seed getSeed(String seedPath) throws GranaryException {
        HolderSeed holder = CACHE_SEED.get(seedPath);
        Seed seed = holder != null? holder.getSeed() : null;
        if(seed == null){
            throw new SeedNotFoundException(String.format("Seed [%s] not found",seedPath));
        }
        return seed;
    }

    /* (non-Javadoc)
     * @see org.spathiphyllum.Granary#getCompositeTypes(org.spathiphyllum.Seed)
     */
    @Override
    public Set<Seed> getCompositeTypes(Seed seed) throws GranaryException {
        SeedDomain domain = seed.seedDomain();
        if(domain == null){
            LOGGER.error("The seed [{}] does not have domain", seed);
            throw new GranaryException("Repository has an inconsistent seed");
        }
        Set<Seed> compositeSeeds = new TreeSet<>();
        String domainPath = domain.seedPath();
        CACHE_COMPOSITE_SEED.forEach((k,v)->{
            if(k.startsWith(domainPath)){
                compositeSeeds.add(v.getSeed());
            }
        });
        if(domain instanceof SeedDefault){
            Seed parent = ((SeedDefault)domain).getParent();
            compositeSeeds.addAll(getCompositeTypes(parent));
        }
        return compositeSeeds;
    }

    /* (non-Javadoc)
     * @see org.spathiphyllum.Granary#blossom(java.lang.String)
     */
    @Override
    public Flower blossom(String seedPath) throws SeedNotFoundException {
        // TODO Auto-generated method stub
        return null;
    }

    /* (non-Javadoc)
     * @see org.spathiphyllum.Granary#findFlower(java.util.UUID)
     */
    @Override
    public Flower findFlower(UUID flowerId) throws FlowerNotFoundException, FlowerBusyException {
        // TODO Auto-generated method stub
        return null;
    }
    //====================== Implementation of GranaryManagement
    void cacheSeed(SeedDefault seed) throws GranaryException {
        HolderSeed newHolder = new HolderSeed(seed);
        if(seed.seedType() == EnumSeedType.COMPOSITE_TYPE){
            //composite types are provided by domain
        }else{
            HolderSeed holderSeed = CACHE_SEED.putIfAbsent(newHolder.seedPath, newHolder);
            if(holderSeed != null){
                holderSeed.setSeed(seed);
            }
        }
    }

    /* (non-Javadoc)
     * @see org.spathiphyllum.Granary#persistSeed(org.spathiphyllum.Seed)
     */
    @Override
    public void persistSeed(Seed seed) throws GranaryException {
        //TODO
    }
    /**
     * get Seed builder
     * @param seedPath
     * @return
     * @throws GranaryException 
     */
    BuilderSeed getOrInitSeedBuilder(String seedPath) throws GranaryException {
        BuilderSeed builder = INVALIDATE_SEED.get(seedPath);
        if(builder == null){
            builder = new BuilderSeed(this);
            HolderSeed holderSeed = CACHE_SEED.get(seedPath);
            if(holderSeed != null){
                return builder.modifySeed(holderSeed);
            }else{
                String[] names = NameHelper.parseSeedPath(seedPath);
                HolderSeed parent = null;
                if(names.length > 1){
                    parent = CACHE_SEED.get(NameHelper.calculateParentPath(names, 1));
                    if(parent != null){
                        return builder.inheritSeed(names[0], parent);
                    }else{
                        throw new GranaryException("Parent not fond");
                    }
                }else{
                    builder.seedName(names[0]);
                }
            }
        }
        return builder;
    }
    @Override
    public BuilderSeed builderSeed() {
        return new BuilderSeed(this);
    }
    
    @Override
    public void validateSeed(String seedPath) throws GranaryException {
        String[] paths = NameHelper.parseSeedPath(seedPath);
        BuilderSeed builderSeed = INVALIDATE_SEED.remove(seedPath);
        if(builderSeed != null){
            SeedDefault seed = new SeedDefault(paths[0]);
            String domain = builderSeed.getDomainPath();
            if(!seedPath.startsWith(domain)){
                throw new GranaryException("The Seed was corrupted");
            }
            validateSeed(domain);
            HolderSeed holder = CACHE_SEED.get(domain);
            if(holder == null){
                throw new GranaryException("The Seed was corrupted");
            }
            seed.setDomain(holder);
            String parent = builderSeed.getParentPath();
            if(parent != null){
                if(!seedPath.startsWith(parent)){
                    throw new GranaryException("The Seed was corrupted");
                }
                validateSeed(parent);
                holder = CACHE_SEED.get(domain);
                if(holder == null){
                    throw new GranaryException("The Seed was corrupted");
                }
                seed.setDomain(holder);
            }
            seed.setStatus(builderSeed.getStatus());
            seed.setType(builderSeed.getType());
            Map<String, BuilderDNA> modifiedDNA = builderSeed.getModifiedDNA();
            for(Iterator<Entry<String,BuilderDNA>> iter = modifiedDNA.entrySet().iterator(); iter.hasNext();){
                Entry<String,BuilderDNA> entry = iter.next();
                BuilderDNA buiderDNA = entry.getValue();
                if(!entry.getKey().equals(buiderDNA.getName())){
                    throw new GranaryException("The DNA was corrupted");
                }
                NameHelper.validateNameDNA(entry.getKey());
                if(buiderDNA.getType() == EnumTypes.COMPLEX){
                    String pathComlexType = entry.getValue().getAdditionalValue().get(EnumTypes.COMPLEX.getOptions()[0].getName());
                    NameHelper.parseSeedPath(pathComlexType);
                    validateSeed(pathComlexType);
                }
            }
            Map<String, DNA> dnas = new HashMap<>();
            holder = CACHE_SEED.get(seedPath);
            if(holder != null){
                dnas.putAll(holder.getSeed().onlyDnaOfThisSeed());
            }
            for(BuilderDNA b : modifiedDNA.values()){
                if(b.getModificator() == EnumModificator.DELETE){
                    dnas.remove(b.getName());
                }else{
                    dnas.put(b.getName(), b.build());
                }
            }
            seed.setDna(dnas);
            if(holder != null){
                holder.setSeed(seed);
            }else{
                holder = new HolderSeed(seed);
                CACHE_SEED.put(holder.seedPath, holder);
            }
        }
    }
    @Override
    public void validateAllSeeds() throws GranaryException {
        while(!INVALIDATE_SEED.isEmpty()){
            for(Iterator<String> iter = INVALIDATE_SEED.keySet().iterator(); iter.hasNext();){
                validateSeed(iter.next());
            }
        }
    }
    void deleteBuildeSeed(BuilderSeed builderSeed) {
        String seedPath = builderSeed.seedPath();
        INVALIDATE_SEED.remove(seedPath);
    }
    /**
     * return HolderSeed or null
     * @param seedPath
     * @return
     * @throws SecurityException
     */
    HolderSeed findHolderSeed(String seedPath) {
        //TODO add security check throw 
        return CACHE_SEED.get(seedPath);
    }
    /**
     * return BuilderSeed or null
     * @param seedPath
     * @return
     * @throws SecurityException
     */
    BuilderSeed getSeedBuilder(String seedPath) {
        //TODO add security check
        return INVALIDATE_SEED.get(seedPath);
    }

}
