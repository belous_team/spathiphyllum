/*
 * Copyright (C) 2018-2018 The Project Spathiphyllum Authors.
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
package org.spathiphyllum.ground;

import org.spathiphyllum.GranaryException;
import org.spathiphyllum.Seed;

/**
 * @author obelous
 *
 */
public interface GranaryManagement {
    /**
     * get DNA factory
     * @return
     */
    BuilderSeed builderSeed();
    /**
     * store this seed in DB layer<br>
     * @startuml
     * start
     * if (seed status UNSAVED?) then (yes)
     *    :insert simple field;
     * else (no)
     *    :update simple field;
     * endif
     * if (NOT modifiedDNA.isEmpty?) then (yes)
     *   :copy modifiedDNA;
     *   repeat
     *     :read first DNA from copy modifiedDNA;
     *     if (DNA present in cache DNA?) then (yes) 
     *        :update DNA in DB;
     *     else (no)
     *        :insert DNA in DB;
     *     endif
     *     :store in cache DNA;
     *     :remove DNA from copy modifiedDNA;
     *     
     *   repeat while (NOT copy modifiedDNA.isEmpty?)
     * endif
     * stop
     * @enduml
     * @param seedDefault
     */
    void persistSeed(Seed seed) throws GranaryException;
    void validateSeed(String seedName) throws GranaryException;
    void validateAllSeeds() throws GranaryException;
}
