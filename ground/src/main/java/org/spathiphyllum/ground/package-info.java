/*
 * Copyright (C) 2018-2018 The Project Spathiphyllum Authors.
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
/**
 */
/**
 * @startuml
 * main -> main : load environment\n initialize datasource\n(springframework)\n initialize shutdown thread
 * note right : The Equipment are\n like workhorse or DNA\n that define what the flower do 
 * main -> main : initialize Equipment and store in cache\n element cron is present
 * main -> granary : initialize
 * note right : granary is repository of all seeds, flowers, petals
 * granary -> granary : initialize cache for Seeds\n initialize cache for Flowers
 * granary -> granary : read all seeds
 * loop initialize seed with this petals (DNA) and store to cache
 *     granary -> seeds  : initialize Seed 
 * 	   note right: The Seed is model of flower
 *     seeds -> granary : Seed
 *     granary -> granary : store to cache
 * end
 * loop make flower and store to cache (only unsecured)
 *     granary -> flowers : initialize Flowery for seed
 * 	   note right: The Flower is model of flower
 *     flowers -> granary : The Flower
 *     granary -> granary : store to cache
 * end
 * granary -> main : result
 * main -> main : initialize cluster
 * 
 * @enduml
 * 
 * @author obelous
 *
 */
package org.spathiphyllum.ground;