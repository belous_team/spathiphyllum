/*
 * Copyright (C) 2018-2018 The Project Spathiphyllum Authors.
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
package org.spathiphyllum.ground;

import org.spathiphyllum.Environment;
import org.spathiphyllum.GranaryException;

/**
 * This is Transaction interface
 * @author obelous
 *
 */
public interface GranaryStorageProvider {
    /**
     * only ReadTransaction
     * Granary provides environment, because Granary can be served more than one storage
     * @param granary
     * @throws GranaryException
     */
    void initializeGranary(GranaryDefault granary, Environment environment) throws GranaryException;
    /**
     * write Transaction
     * @param seed
     * @throws GranaryException
     */
    void saveSeed(SeedDefault seed) throws GranaryException;
    /**
     * Read Transaction
     * @param granary
     * @throws GranaryException
     */
    void loadFlower(GranaryDefault granary) throws GranaryException;
    /**
     * Write Transaction
     * @param flower
     * @throws GranaryException
     */
    void saveFlower(FlowerDefault flower) throws GranaryException;
}
