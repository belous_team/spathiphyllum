/*
 * Copyright (C) 2018-2018 The Project Spathiphyllum Authors.
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
package org.spathiphyllum.ground;

import org.spathiphyllum.DNA;
import org.spathiphyllum.DefaultMeaning;
import org.spathiphyllum.EnumTypes;
import org.spathiphyllum.Seed;
import org.spathiphyllum.el.ParseELException;
import org.spathiphyllum.el.tools.HelperStringParser;

/**
 * The class describe one DNA (one field witch will be contained in Seed)
 * 
 * @author obelous
 *
 */
public class AbstractDNA implements DNA {
    HolderSeed seed;
    String name;
    boolean constant;
    boolean inheritance = true;
    EnumTypes type;
    boolean secured;
    DefaultMeaning defaultMeaning;
    
    @Override
    public boolean isValid() throws ParseELException {
        
        return seed != null && seed.getSeed() != null
                && HelperStringParser.isStringEmptySkipSpace(name)
                && defaultMeaning != null && defaultMeaning.validate(seed.getSeed());
    }

    @Override
    public DefaultMeaning defaultMeaning() {
        return defaultMeaning;
    }

    @Override
    public Seed seed() {
        return seed.getSeed();
    }

    @Override
    public String name() {
        return name;
    }

    @Override
    public boolean isConstant() {
        return constant;
    }

    @Override
    public boolean isInheritance() {
        return inheritance;
    }

    @Override
    public EnumTypes type() {
        return type;
    }

    @Override
    public boolean isSecured() {
        return secured;
    }

    @Override
    public String pathDNA() {
        return calculateDNAPath(seed(), name);
    }
    /**
     * calculate DNA's path
     * @param seed
     * @return
     */
    public static String calculateDNAPath(Seed seed, String dnaName){
        StringBuilder sb = new StringBuilder(seed.seedPath());
        return sb.append('/').append(dnaName).toString();
    }
    /**
     * calculate DNA's path
     * @param seedPath
     * @return
     */
    public static String calculateDNAPath(String seedPath, String dnaName){
        StringBuilder sb = new StringBuilder(seedPath);
        return sb.append('/').append(dnaName).toString();
    }


}
