/*
 * Copyright (C) 2018-2018 The Project Spathiphyllum Authors.
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
package org.spathiphyllum.ground.tools;

import static org.testng.Assert.assertEquals;
import static org.testng.Assert.assertNotEquals;

import java.io.IOException;
import java.net.URISyntaxException;
import java.nio.charset.Charset;
import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

import org.spathiphyllum.EnumEnviroment;
import org.spathiphyllum.LoadResourceException;
import org.testng.annotations.Test;

public class PropertiesLoaderTest {
    @Test
    public void loadYamlTest() throws LoadResourceException {
        Map<String,Object> map = PropertiesLoader.loadProperty("classpath:simple.yaml", Charset.forName(EnumEnviroment.DEFAULT_CHARSET.value()));
        Map<String, Object> helloworld = new HashMap<>();
        helloworld.put("integer-property", 0);
        helloworld.put("boolean-property", false);
        helloworld.put("string-property", "this is a string of text");
        assertEquals(PropertiesLoader.findProperty("helloworld", map), helloworld);
        Map<String, Object> HelloWordl = new HashMap<>();
        HelloWordl.put("integer-property", 1);
        HelloWordl.put("boolean-property", true);
        HelloWordl.put("string-property", "this is a another string of text");
        assertEquals(PropertiesLoader.findProperty("HelloWordl", map), HelloWordl);
        assertEquals(PropertiesLoader.findProperty("a-list-key", map), 
                Arrays.asList("value","test","another value","they can also contain explicit strings", true, 10));
        Set<String> expected = new HashSet<>();
        expected.addAll(Arrays.asList("helloworld.integer-property","helloworld.boolean-property","helloworld.string-property",
                "HelloWordl.integer-property","HelloWordl.boolean-property","HelloWordl.string-property",
                "hello-key","world-key","hello-world-key","a-list-key"));
        assertEquals(PropertiesLoader.allKeys("", map), expected);
    }
    @Test
    public void loadPropertiesTest() throws LoadResourceException {
        Map<String,Object> map = PropertiesLoader.loadProperty("classpath:simple.properties", Charset.forName(EnumEnviroment.DEFAULT_CHARSET.value()));
        Map<String, Object> helloworld = new HashMap<>();
        helloworld.put("integer-property", "0");
        helloworld.put("boolean-property", "false");
        helloworld.put("string-property", "'this is a string of text'");
        assertEquals(PropertiesLoader.findProperty("helloworld", map), helloworld);
        Map<String, Object> HelloWordl = new HashMap<>();
        HelloWordl.put("integer-property", "1");
        HelloWordl.put("boolean-property", "true");
        HelloWordl.put("string-property", "'this is a another string of text'");
        assertEquals(PropertiesLoader.findProperty("HelloWordl", map), HelloWordl);
        Set<String> expected = new HashSet<>();
        expected.addAll(Arrays.asList("helloworld.integer-property","helloworld.boolean-property","helloworld.string-property",
                "HelloWordl.integer-property","HelloWordl.boolean-property","HelloWordl.string-property",
                "hello-key","world-key","hello-world-key","hello-key-1"));
        assertEquals(PropertiesLoader.allKeys("", map), expected);
    }
    @Test
    public void isDirectoryTest() {
        boolean b = PropertiesLoader.isDirectory("classpath:/org/slf4j");
        assertEquals(b, true);
        b = PropertiesLoader.isDirectory("classpath:/org/slf4j/");
        assertEquals(b, true);
        b = PropertiesLoader.isDirectory("classpath:/org/slf4j/Logger.class");
        assertEquals(b, false);
        b = PropertiesLoader.isDirectory("classpath:/seeds");
        assertEquals(b, true);
        b = PropertiesLoader.isDirectory("classpath:/seeds/seed1.json");
        assertEquals(b, false);
        b = PropertiesLoader.isDirectory("classpath:/org/apache");
        assertEquals(b, true);
    }
    @Test
    public void readDirectoryTest() throws IOException, URISyntaxException {
        String[] str = PropertiesLoader.readDirectory("classpath:/org/slf4j");
        assertNotEquals(str.length, 0);
        str = PropertiesLoader.readDirectory("classpath:/org/slf4j/");
        assertNotEquals(str.length, 0);
        str = PropertiesLoader.readDirectory("classpath:/org/slf4j/Logger.class");
        assertEquals(str.length, 0);
        str = PropertiesLoader.readDirectory("classpath:/seeds");
        assertNotEquals(str.length, 0);
        str = PropertiesLoader.readDirectory("classpath:/seeds/seed1.json");
        assertEquals(str.length, 0);
        str = PropertiesLoader.readDirectory("classpath:/org/apache");
        assertEquals(str.length, 0);
    }
}
