/*
 * Copyright (C) 2018-2018 The Project Spathiphyllum Authors.
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
package org.spathiphyllum;

import java.math.BigDecimal;
import java.math.BigInteger;

/**
 * This class for providing exchange
 * 
 * @author obelous
 *
 */
public class ExchangeOfMeaning {
    private EnumTypes type;
    private Object obj;
    private int intv;
    private long longv;
    private double doublev;
    
    public EnumTypes getType(){
        return type;
    }
    public void setValue(byte b){
        type = EnumTypes.BYTE;
        intv = b;
    }
    public byte getByte(){
        return (byte) intv;
    }
    public void setValue(short v){
        type = EnumTypes.SHORT;
        intv = v;
    }
    public short getShort(){
        return (short) intv;
    }
    public void setValue(char v){
        type = EnumTypes.CHAR;
        intv = v;
    }
    public char getChar(){
        return (char) intv;
    }
    public void setValue(int v){
        type = EnumTypes.INT;
        intv = v;
    }
    public int getInt(){
        return intv;
    }
    public void setValue(BigDecimal decimal){
        type = EnumTypes.BIG_DECIMAL;
        obj = decimal;
    }
    public BigDecimal getDecimal(){
        return (BigDecimal) obj;
    }
    public void setValue(BigInteger integer){
        type = EnumTypes.BIG_INTEGER;
        obj = integer;
    }
    public BigInteger geBigInteger(){
        return (BigInteger) obj;
    }
    public void setValue(boolean value){
        type = EnumTypes.BOOLEAN;
        intv = (value)?1:0;
    }
    public boolean getBoolean(){
        return intv != 0;
    }
    public void setValue(long value){
        type = EnumTypes.LONG;
        longv = value;
    }
    public long getLong(){
        return longv;
    }
    public void setValue(float value){
        type = EnumTypes.FLOAT;
        doublev = value;
    }
    public float getFloat(){
        return (float) doublev;
    }
    public void setValue(double value){
        type = EnumTypes.DOUBLE;
        doublev = value;
    }
    public double getDouble(){
        return doublev;
    }
    public void setValue(String value){
        type = EnumTypes.STRING;
        obj = value;
    }
    public String getString(){
        return (String) obj;
    }
}
