/*
 * Copyright (C) 2018-2018 The Project Spathiphyllum Authors.
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
package org.spathiphyllum;

/**
 * 
 * @author obelous
 *
 */
public interface GardenShed extends Measurement {
    /**
     * initialize factory and equipments
     * @param baseEnviroment
     */
    void init(Environment baseEnviroment);
    /**
     * add equipment
     * @param name
     * @param clazz
     * @param environment
     * @throws InitializeException
     */
    void addEquipment(String name, Class<? extends Equipment> clazz, Environment environment)
            throws InitializeException;
    /**
     * add equipment
     * @param name
     * @param clazz
     * @throws InitializeException
     */
    void addEquipment(String name, Class<? extends Equipment> clazz) throws InitializeException;
    /**
     * get number of equipments
     * @return
     */
    int equipmentSize();
    /**
     * product equipment for SeedDomain
     * @param prototypeName
     * @param SeedDomain
     * @return
     * @throws InitializeException
     */
    Equipment product(String prototypeName, SeedDomain domain, DNA dna) throws InitializeException;
    /**
     * shutdown this factory
     */
    void shutdown();

}