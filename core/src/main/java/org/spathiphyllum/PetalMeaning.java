/*
 * Copyright (C) 2018-2018 The Project Spathiphyllum Authors.
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
package org.spathiphyllum;

import java.math.BigDecimal;
import java.math.BigInteger;

import org.spathiphyllum.el.ParseELException;

/**
 * @author obelous
 *
 */
public interface PetalMeaning {
    /**
     * Get Petal that possessed this PetalMeaning
     * @return
     */
    Petal getPetal();
    /**
     * Get Type of this meaning
     * @return
     */
    default EnumTypes getType(){
        return getPetal().getPetalType();
    }
    /**
     * Get Type of data this PatelMeaning<br>
     * for type.isSimple it is that same as getType()
     * @return
     */
    default EnumTypes getDataType(){
        return getType();
    }
    /**
     * indicate that this meaning was changed
     * @return
     */
    boolean isDirty();
    /**
     * clear dirty flag
     */
    void clearDirty();
    /**
     * set dirty flag
     */
    void setDirty();
    /**
     * Set current value to petal
     * @param petal
     * @throws InvokeELException
     */
    void nourishPetal(Petal petal);
    /**
     * Set current value to petal
     * for Array type
     * @param petal
     * @param index - index of array
     * @throws InvokeELException
     */
    void nourishPetal(Petal petal, int index);
    /**
     * nourish this Petal meaning with BigDecimal
     * @param value
     * @throws InvokeELException
     */
    void nourishBigDecimal(BigDecimal value);
    /**
     * nourish this Petal meaning with boolean
     * @param value
     * @throws InvokeELException
     */
    void nourishBoolean(boolean value);
    /**
     * nourish this Petal meaning with BigInteger
     * @param value
     * @throws InvokeELException
     */
    void nourishBigInteger(BigInteger value);
    /**
     * nourish this Petal meaning with byte
     * @param value
     * @throws InvokeELException
     */
    void nourishByte(byte value);
    /**
     * nourish this Petal meaning with char
     * @param value
     * @throws InvokeELException
     */
    void nourishChar(char value);
    /**
     * nourish this Petal meaning with double
     * @param value
     * @throws InvokeELException
     */
    void nourishDouble(double value);
    /**
     * nourish this Petal meaning with float
     * @param value
     * @throws InvokeELException
     */
    void nourishFloat(float value);
    /**
     * nourish this Petal meaning with integer
     * @param value
     */
    void nourishInt(int value);
    /**
     * nourish this Petal meaning with long
     * @param value
     * @throws InvokeELException
     */
    void nourishLong(long value);
    /**
     * nourish this Petal meaning with short
     * @param value
     * @throws InvokeELException
     */
    void nourishShort(short value);
    /**
     * nourish this Petal meaning with string
     * @param value
     * @throws InvokeELException
     * @throws ParseELException
     */
    void nourishString(String value) throws ParseELException;
    /**
     * nourish this Petal meaning with string
     * @param value
     * @param format
     * @throws InvokeELException
     * @throws ParseELException
     */
    void parseString(String value) throws ParseELException;
    /**
     * nourish this Petal meaning with string and uncommon format
     * @param value
     * @param format
     * @throws InvokeELException
     * @throws ParseELException
     */
    void parseString(String value, String format) throws ParseELException;
    /**
     * save current value to ExchangeOfMeaning
     * @param exange
     */
    void saveToExchage(ExchangeOfMeaning exchange);
    /**
     * save current value to ExchangeOfMeaning
     * for ARRAY only
     * @param exange
     */
    void saveToExchage(ExchangeOfMeaning exchange, int index);
    /**
     * initialize current value from ExchangeOfMeaning
     * @param exchange
     * @throws ParseELException
     */
    void initFormExcange(ExchangeOfMeaning exchange) throws ParseELException;
    /**
     * length of data
     * for ARRAY only
     * @param exange
     */
    int length();
}
