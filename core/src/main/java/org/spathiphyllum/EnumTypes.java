/*
 * Copyright (C) 2018-2018 The Project Spathiphyllum Authors.
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
package org.spathiphyllum;

import org.spathiphyllum.el.EnumEL;
import org.spathiphyllum.el.tools.HelperStringParser;

/**
 * Define types that the system can use
 * If the type isSimple = false, so to define type that used you need to ask type in AbstractValue
 * 
 * @author obelous
 *
 */
public enum EnumTypes {
    BOOLEAN(true, EnumEL.BOOLEAN,ExtraOption.EMPTY_OPTIONS), BYTE(true, EnumEL.BYTE,ExtraOption.EMPTY_OPTIONS), CHAR(true, EnumEL.CHAR,ExtraOption.EMPTY_OPTIONS), SHORT(true, EnumEL.SHORT,ExtraOption.EMPTY_OPTIONS), INT(true, EnumEL.INT,ExtraOption.EMPTY_OPTIONS), //all stored in int
    LONG(true, EnumEL.LONG,ExtraOption.EMPTY_OPTIONS), FLOAT(true, EnumEL.FLOAT,ExtraOption.EMPTY_OPTIONS), DOUBLE(true, EnumEL.DOUBLE,ExtraOption.EMPTY_OPTIONS), STRING(true, EnumEL.STRING,ExtraOption.EMPTY_OPTIONS), BIG_INTEGER(true, EnumEL.BIG_INTEGER,ExtraOption.EMPTY_OPTIONS), BIG_DECIMAL(true, EnumEL.BIG_DECIMAL,ExtraOption.EMPTY_OPTIONS),
    /**
     * this type contain ?[]. ? any petal simple type
     */
    ARRAY(false, EnumEL.SOP,ExtraOption.EMPTY_OPTIONS),
    /**
     * this type can have children petals like "petal_name$child1"
     */
    COMPLEX(false, EnumEL.SOP,new ExtraOption[]{new ExtraOption("complex_type_path","Path of Seed that present this Complext type",HelperStringParser.EMPTY_STRINGS)}),
    /**
     * this type contain EL script like petal["name1"]+petal["name2"]. 
     * The value of this script can be assigned to other petal in EL_MAP, EL_LIST petal or maybe a part of other EL petal
     */
    EL(false, EnumEL.SOP,ExtraOption.EMPTY_OPTIONS),
    /**
     * this type contain EL scripts as map. This type do not return value and can be run only.
     * most simple EL script is scripts that contain assigned logic like:
     * <pre>
     * key1:petal[name2]
     * key2:element[http://petal_name]
     * key3:petal[name3]
     * </pre>
     * key - is case sensitivity and can contain [A-Za-z0-9_] symbols only
     */
    EL_MAP(true, EnumEL.SOP,ExtraOption.EMPTY_OPTIONS),
    /**
     * this type contain EL scripts as list. This type do not return value and can be run only.
     * <pre>
     * petal[name1] = petal[name2]
     * petal[name2]
     * element[http://petal_name]
     * Name of petal is case sensitivity and can contain [A-Za-z0-9_] symbols only
     * </pre>
     */
    EL_LIST(true, EnumEL.SOP,ExtraOption.EMPTY_OPTIONS),
    /**
     * this type contain EL scripts as list. This type do not return value and can be run only.
     * <pre>
     * petal[name1] = petal[name2]
     * petal[name2]
     * element[http://petal_name]
     * Name of petal is case sensitivity and can contain [A-Za-z0-9_] symbols only
     * </pre>
     */
    EL_LIST_ASYNC(true, EnumEL.SOP, ExtraOption.EMPTY_OPTIONS),
    /**
     * this type contain document XML|JSON|YAML|PROPERTIES, and allow to access this document with XPath
     */
    DOCUMENT(true, EnumEL.STRING, new ExtraOption[]{new ExtraOption("document_type","Select type of document",
            new String[]{EnumSupportedDocument.XML.name(),
                    EnumSupportedDocument.JSON.name(),
                    EnumSupportedDocument.YAML.name(),
                    EnumSupportedDocument.PROPERTIES.name()})});
    
    private EnumTypes(boolean isSimple, EnumEL el, ExtraOption[] options){
        this.simple = isSimple;
        this.el = el;
        this.options = options;
    }
    private final boolean simple;
    private final EnumEL el;
    private final ExtraOption[] options;
    
    public boolean isSimple(){
        return simple;
    }
    public EnumEL typeEL(){
        return el;
    }
    public ExtraOption[] getOptions(){
        return options;
    }
    public static class ExtraOption {
        private static final ExtraOption[] EMPTY_OPTIONS = {};
        private String name;
        private String decription;
        private String[] choice;
        public ExtraOption(String name, String decription, String[]choice){
            this.name = name;
            this.decription = decription;
            this.choice = choice;
        }
        public String getName() {
            return name;
        }
        public String getDecription() {
            return decription;
        }
        public String[] getChoice() {
            return choice;
        }
    }
}
