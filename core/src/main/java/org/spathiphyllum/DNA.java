/*
 * Copyright (C) 2018-2018 The Project Spathiphyllum Authors.
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
package org.spathiphyllum;

import org.spathiphyllum.el.ParseELException;

public interface DNA {

    /**
     * check is the DNA (default value) is valid
     * @return
     * @throws ParseELException 
     */
    boolean isValid() throws ParseELException;

    /**
     * Get default value
     * @return
     */
    DefaultMeaning defaultMeaning();

    /**
     * It field defines which Seed possess this DNA
     * @return
     */
    Seed seed();

    /**
     * name of DNA
     * @return
     */
    String name();

    /**
     * if true this DNA will not be allowed to modify. This DNA will be as singleton in system 
     * @return
     */
    boolean isConstant();

    /**
     * if false this field is not inherited
     * @return
     */
    boolean isInheritance();

    /**
     * return petal's type
     * @return
     */
    EnumTypes type();

    /**
     * if true this field will be encrypted
     * if secured we will stored and encrypted value in the blob_dna table
     * @return
     */
    boolean isSecured();

    /**
     * get DNA path
     * @return
     */
    String pathDNA();

}