/*
 * Copyright (C) 2018-2018 The Project Spathiphyllum Authors.
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
package org.spathiphyllum;

/**
 * Design of Seeds is tree:<br>
 * <pre>
 * /&#x2510
 *  &#x251C common1&#x2510
 *  |        &#x251C domain1&#x2510
 *  |        |        &#x251C Seed1&#x2510
 *  |        |        |      &#x2514 Seed2
 *  |        |        &#x2514 Seed3  
 *  |        | 
 *  |        &#x2514 domain2&#x2510
 *  |                 &#x251C Seed1&#x2510
 *  |                 |      &#x2514 Seed2
 *  |                 &#x2514 Seed3
 *  &#x2514 common2
 *           &#x251C domain3 
 *           &#x251C domain4 
 * </pre>
 * so we allow access on a path<br>
 * If we say that somebody has access on domain1 this like as:<br>
 * approveExecutePath = /common/domain - allow execute seeds include domain1 and its children<br> 
 * approveReadPath = /common/domain - allow read all DNA that present in domain1 and its children<br>
 * approveModifyPath = /common/domain/ - do not allow modify DNA of domain1 but allow modify its children
 * 
 * @author obelous
 *
 */
public interface SecurityContext {
    String approveReadPath();
    String approveExecutePath();
    String approveModifyPath();
    default int approveNumberOfSeeds(){
        return Integer.MAX_VALUE;
    }
    default int approveNumberOfModifications(){
        return Integer.MAX_VALUE;
    }
    default boolean canRead(String path){
        try{
            return path != null? path.startsWith(approveReadPath()) : false;
        }catch(Throwable t){
            return false;
        }
    }
    default boolean canExecute(String path){
        try{
            return path != null? path.startsWith(approveExecutePath()) : false;
        }catch(Throwable t){
            return false;
        }
    }
    default boolean canModify(String path){
        try{
            return path != null? path.startsWith(approveModifyPath()) : false;
        }catch(Throwable t){
            return false;
        }
    }
}
