/*
 * Copyright (C) 2018-2018 The Project Spathiphyllum Authors.
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
package org.spathiphyllum;

/**
 * This is place when we stored default environment We do not use static final
 * variable
 * 
 * @author obelous
 *
 */
public enum EnumEnviroment {

    DEFAULT_CHARSET("default.charset", "UTF-8"),
    DEFAULT_CONFIGURATION("spathiphyllum.config", "spathiphyllum.yaml"),
    EQUIPMENT_CONFIGURATION("spathiphyllum.equipment.config","equipment.yaml"),
    DEFAULT_LIB_PATH("spathiphyllum.path.lib","./lib"),
    DEFAULT_CONF_PATH("spathiphyllum.path.config","./conf");
    
    private String value;
    private String propertyName;

    private EnumEnviroment(String propertyName, String defaultValue) {
        this.propertyName = propertyName;
        this.value = defaultValue;
    }
    public String propertyName(){
        return propertyName;
    }
    public String value() {
        return System.getProperty(propertyName, value);
    }
    public boolean isDefaultValue(String chkValue){
        return this.value.equals(chkValue);
    }
}
