/*
 * Copyright (C) 2018-2018 The Project Spathiphyllum Authors.
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
package org.spathiphyllum;

import org.spathiphyllum.el.PetalProvider;

/**
 * @author obelous
 *
 */
public interface Petal {
    /**
     * Return Flower what this Petal is belonged by
     * @return
     */
    Flower flower();
    /**
     * return implementation of PetalProvider
     * @return
     */
    PetalProvider petalProvider();
    /**
     * Return Petal's name
     * @return
     */
    String getName();
    /**
     * if true this Petal will not be allowed to modify. This Petal will be as singleton in system
     * @return
     */
    boolean isConstant();
    /**
     * return petal's type
     * @return
     */
    EnumTypes getPetalType();
    /**
     * return current value
     * @return
     */
    PetalMeaning getPatalMeaning();
    /**
     * Only for Complex type
     * Return a Petal that is belonged by this complex type
     * 
     * @param petalName
     * @return
     * @throws PetalNotFoundException 
     */
    default Petal child(String petalName) throws PetalNotFoundException {
        if(getPetalType() != EnumTypes.COMPLEX){
            throw new InvokeELException(getName()+": This is not Comlpex type");
        }
        return flower().find(getName()+"$"+petalName);
    }
}
