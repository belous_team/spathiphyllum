/*
 * Copyright (C) 2018-2018 The Project Spathiphyllum Authors.
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
package org.spathiphyllum;

import java.util.Set;

/**
 * The interface perform a particular work
 * 
 * @author obelous
 *
 */
public interface Equipment extends Measurement {
    /**
     * Initialize this instance
     * @param environment
     */
    void init(Environment environment) throws InitializeException;
    /**
     * return all DNA that can be used for initialize this tool
     * @return
     */
    Set<DescriptionDNA> initializeDNA();
    /**
     * return all DNA that can be used for put data to this tool
     * @return
     */
    Set<DescriptionDNA> inputDNA();
    /**
     * return all DNA that can be used for receive data from this tool
     * @return
     */
    Set<DescriptionDNA> outputDNA();
    /**
     * if this GeneticEngineering is thread safe
     * @return
     */
    boolean isThreadSafe();
    /**
     * shutdown this GeneticEngineering
     */
    void shutdown();
    /**
     * registration for a flower
     * This method is called only once for every Flower when Flower is initialized
     * @param petal
     */
    void registration(Flower flower);
    /**
     * Create Equipment of the same as this instance for SeedDomain
     * This method is called only once for every SeedDomain when SeedDomain is initialized
     * @param environment
     * @return
     */
    Equipment clone(SeedDomain domain) throws InitializeException;
}
