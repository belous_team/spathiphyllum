/*
 * Copyright (C) 2018-2018 The Project Spathiphyllum Authors.
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
package org.spathiphyllum;

import java.util.Map;

import org.spathiphyllum.EnumSeedStatus;
import org.spathiphyllum.EnumSeedType;
import org.spathiphyllum.el.PetalProvider;

public interface Seed {
    /**
     * Get SeedDomain
     * @return
     */
    SeedDomain seedDomain();
    /**
     * Get seed name
     * @return
     */
    String seedName();
    /**
     * A map of all DNA names for this Seed include parent
     * key - is name of DNA
     * value - is path of Seed that contain a last inherit DNA
     * @return
     */
    Map<String,String> namesDNA();
    /**
     * Return DNA of this Seed w/o parent<br>
     * null if this DNA absent
     * @param dnaName
     * @return
     */
    DNA dna(String dnaName);
    /**
     * get Seed path
     * This is unique ID 
     * @return
     */
    String seedPath();
    /**
     * Get Seed status
     * @return
     */
    EnumSeedStatus seedStatus();
    /**
     * Get Seed type
     * @return
     */
    EnumSeedType seedType();
    /**
     * return implementation of {@link PetalProvider}
     * @return
     */
    PetalProvider petalProvider();
    /**
     * if this Seed can produce Flower
     * @return
     */
    boolean isAbstract();
}