/*
 * Copyright (C) 2018-2018 The Project Spathiphyllum Authors.
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
package org.spathiphyllum;

import java.util.Set;
import java.util.UUID;

/**
 * Interface granary is repository of Seed, DNA, Flower and Petal
 * This interface provide an access only. Management is hidden in ground package
 * 
 * @author obelous
 *
 */
public interface Granary extends Measurement {
    /**
     * read Seed from cache
     * @param seedPath
     * @return
     */
    Seed getSeed(String seedPath) throws GranaryException;
    /**
     * returns list of composite types which can be provided for this seed
     * @param seed
     * @return
     */
    Set<Seed> getCompositeTypes(Seed seed) throws GranaryException;
    /**
     * Initializes Flower
     * @param seedPath
     * @return
     * @throws SeedNotFoundException
     */
    Flower blossom(String seedPath)  throws GranaryException;
    Flower findFlower(UUID flowerId) throws FlowerNotFoundException, FlowerBusyException;
}
