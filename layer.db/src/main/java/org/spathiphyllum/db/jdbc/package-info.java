/**
 * 
 */
/**
 * The package provides for access to DB
 * 
 * Define tables:
 * table seeds : contain information about seed
 *         secured : boolean : if true seed don't grow auto and for growing need the password ?change to security_role.isEmpty() 
 *         security_role : contains name of DNA that has a Security configuration
 *         elements : contains name of DNA that has a Elements configuration
 *         in : contains name of DNA that has a IN configuration
 *         out : contains name of DNA that has a OUT configuration
 *         start : contains name of DNA that has a configuration to start an interaction
 *         status : status of Seed: UNSAVED, SAVED, ACTIVE
 * table DNAs : contain common information\n about this DNA
 *         seedName : varchar
 *         name  : varchar
 *         constant : if true this field will not be allowed to modify. This field will be as singleton in system 
 *         inheritance : if false this field is not inherited
 *         mediaType: like text/html;encoding=UTF-8, application/xml. This field defines when default value is stored and what type of Petal will be created
 *         secured : boolean (if secured we will stored and encrypted value in the blob_dna table)
 * table plain_dna  : for store default values\n like as string, number, boolean, date
 *         seedName : varchar
 *         name  : varchar
 *         value : varchar
 * table blob_dba : for store values that is big, like as property, media files, documents
 *         seedName : varchar
 *         name  : varchar
 *         value : blob
 * 
 * @startuml
 * class seeds 
 * seeds : secured       : boolean 
 * seeds : security_role : varchar
 * seeds : elements      : varchar
 * seeds : in            : varchar
 * seeds : out           : varchar
 * class DNAs
 * DNAs : seedName    : varchar
 * DNAs : name        : varchar
 * DNAs : constant    : boolean
 * DNAs : inheritance : boolean
 * DNAs : mediaType   : varchar
 * DNAs : secured     : boolean 
 * class plain_dna
 * plain_dna : seedName : varchar
 * plain_dna : name     : varchar
 * plain_dna : value    : varchar
 * class blob_dba 
 * blob_dna : seedName : varchar
 * blob_dna : name     : varchar
 * blob_dna : value    : blob
 * @enduml
 * @author obelous
 *
 */
package org.spathiphyllum.db.jdbc;