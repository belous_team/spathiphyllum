/*
 * Copyright (C) 2018-2018 The Project Spathiphyllum Authors.
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
package org.spathiphyllum.el;

import static org.testng.Assert.assertEquals;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.HashMap;
import java.util.Map;

import org.spathiphyllum.el.HelperEL.Const;
import org.spathiphyllum.el.plugins.MathPlugin;
import org.testng.annotations.Test;

public class ELTest {
  @Test
  public void f() throws ParseELException {
      String arg = "${Int1} + ${Long1}";
      assertEquals(EL.build(arg, new PetalProviderImpl(), null).asInt(), 1+2);
      assertEquals(EL.build(arg, new PetalProviderImpl(), null).asString(), (1+2)+"");
  }
  @Test
  public void f1() throws ParseELException {
      String arg = "${Int1} + ${Str2}";
      assertEquals(EL.build(arg, new PetalProviderImpl(), null).asString(), "1string2");
  }
  @Test
  public void f2() throws ParseELException {
      String arg = "4 - - 5 + (-2) + 5";
      //(3+1) +  4 - - 5 + (-2) + 5 * 6 / 2 % 3
      //add add sub 4 (-5) (-2) 5 
      assertEquals(EL.build(arg, null, null).asInt(), 4 - - 5 + (-2) + 5);
  }
  @Test
  public void f3() throws ParseELException {
      String arg = "(3 == 2)?(3 == 2)?\"a\":\"b\":(2 == 2)?\"c\":\"d\";";
      assertEquals(EL.build(arg, null, null).asString(), "c");
  }
  @Test
  public void f4() throws ParseELException {
      String arg = "!false && true & (true ^ false) || !true | false || true";
      assertEquals(EL.build(arg, null, null).asBoolean(), !false && true & (true ^ false) || !true | false || true);
  }
  @Test
  public void f5() throws ParseELException {
      String arg = "4 * 7 / 9 % 5";
      assertEquals(EL.build(arg, null, null).asInt(), 4 * 7 / 9 % 5);
  }
  @Test
  public void f6() throws ParseELException {
      String arg = "false + \"asd\"";
      assertEquals(EL.build(arg, null, null).asString(), "falseasd");
  }
  @Test(expectedExceptions = {ParseELException.class})
  public void f7() throws ParseELException {
      String arg = "false * 4";
      assertEquals(EL.build(arg, null, null).asBoolean(), null);
  }
  @Test(expectedExceptions = {ParseELException.class})
  public void f8() throws ParseELException {
      String arg = "false \"aaaa\" 555";
      assertEquals(EL.build(arg, null, null).asString(), null);
  }
  @Test
  public void f9() throws ParseELException {
      String arg = "\"asd\" * 3";
      assertEquals(EL.build(arg, null, null).asString(), "asdasdasd");
  }
  @Test
  public void f10() throws ParseELException {
      String arg = "${Str1} + 3";
      assertEquals(EL.build(arg, new PetalProviderImpl(), null).asString(), "string13");
      arg = "${Str2} + 3";
      assertEquals(EL.build(arg, new PetalProviderImpl(), null).asString(), "string23");
  }
  @Test
  public void f11() throws ParseELException {
      String arg = "${Str1} + ${Str2}";
      assertEquals(EL.build(arg, new PetalProviderImpl(), null).asString(), "string1string2");
      arg = "${Str2} + ${Str1}";
      assertEquals(EL.build(arg, new PetalProviderImpl(), null).asString(), "string2string1");
  }
  @Test
  public void f12() throws ParseELException {
      String arg = "(${Boolean2})?(${Boolean1})?'a':'b':(2 == 2)?'c':'d';";
      assertEquals(EL.build(arg, new PetalProviderImpl(), null).asString(), "b");
  }
  @Test
  public void f13() throws ParseELException {
      String arg = "max(2,4);";
      assertEquals(EL.build(arg, new PetalProviderImpl(), new MathPlugin()).asInt(), Math.max(2, 4));
  }
  @Test
  public void f14() throws ParseELException {
      String arg = "'str1' + 'str2'";
      assertEquals(EL.build(arg, null, null).asString(), "str1" + "str2");
  }
  @Test
  public void f15() throws ParseELException {
      String arg = "'a'";
      assertEquals(EL.build(arg, null, null).asChar(), 'a');
  }
  static class PetalProviderImpl implements PetalProvider {
    Map<String, Const> map = new HashMap<>();
    public PetalProviderImpl() {
        map.put("Str1", new Const("string1", EnumEL.STRING));
        map.put("Str2", new Const("string2", EnumEL.STRING));
        map.put("Int1", new Const(1, EnumEL.INT));
        map.put("Long1", new Const(2L, EnumEL.LONG));
        map.put("Boolean1", new Const(false, EnumEL.BOOLEAN));
        map.put("Boolean2", new Const(true, EnumEL.BOOLEAN));
    }
    @Override
    public EnumEL getType(String petalName) {
        if(map.get(petalName) == null) {
            throw new RuntimeException();
        }
        return map.get(petalName).getType();
    }

    @Override
    public boolean isConstant(String petalName) {
        if(map.get(petalName) == null) {
            throw new RuntimeException();
        }
        return map.get(petalName).isConstant();
    }

    @Override
    public boolean isValid(String petalName) {
        return true;
    }

    @Override
    public BigDecimal asBigDecimal(String petalName){
        try {
            return map.get(petalName).asBigDecimal();
        } catch (ParseELException e) {
            throw new RuntimeException();
        }
    }

    @Override
    public BigInteger asBigInteger(String petalName){
        try {
            return map.get(petalName).asBigInteger();
        } catch (ParseELException e) {
            throw new RuntimeException();
        }
    }

    @Override
    public boolean asBoolean(String petalName){
        try {
            return map.get(petalName).asBoolean();
        } catch (ParseELException e) {
            throw new RuntimeException();
        }
    }

    @Override
    public String asString(String petalName){
        return map.get(petalName).asString();
    }

    @Override
    public byte asByte(String petalName){
        try {
            return map.get(petalName).asByte();
        } catch (ParseELException e) {
            throw new RuntimeException();
        }
    }

    @Override
    public short asShort(String petalName){
        try {
            return map.get(petalName).asShort();
        } catch (ParseELException e) {
            throw new RuntimeException();
        }
    }

    @Override
    public char asChar(String petalName){
        try {
            return map.get(petalName).asChar();
        } catch (ParseELException e) {
            throw new RuntimeException();
        }
    }

    @Override
    public int asInt(String petalName){
        try {
            return map.get(petalName).asInt();
        } catch (ParseELException e) {
            throw new RuntimeException();
        }
    }

    @Override
    public long asLong(String petalName){
        try {
            return map.get(petalName).asLong();
        } catch (ParseELException e) {
            throw new RuntimeException();
        }
    }

    @Override
    public float asFloat(String petalName){
        try {
            return map.get(petalName).asFloat();
        } catch (ParseELException e) {
            throw new RuntimeException();
        }
    }

    @Override
    public double asDouble(String petalName){
        try {
            return map.get(petalName).asDouble();
        } catch (ParseELException e) {
            throw new RuntimeException();
        }
    }
      
  }
}
