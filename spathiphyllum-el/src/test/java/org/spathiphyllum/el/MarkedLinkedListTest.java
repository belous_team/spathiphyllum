package org.spathiphyllum.el;

import static org.testng.Assert.assertEquals;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Iterator;

import org.spathiphyllum.el.HelperEL.Operator;
import org.spathiphyllum.el.tools.MarkedLinkedList;
import org.spathiphyllum.el.tools.MarkedLinkedList.Mark;
import org.spathiphyllum.el.tools.MarkedLinkedList.MarkQueueIterator;
import org.testng.annotations.Test;

public class MarkedLinkedListTest {
  @Test
  public void f() {
      MarkedLinkedList<String> list = new MarkedLinkedList();
      list.add("a");
      assertEquals(list.peek(), "a");
      list.push("b");
      assertEquals(list.peek(), "b");
      list.add("c");
      assertEquals(list.peek(), "b");
      list.push("d");
      assertEquals(list.peek(), "d");
      assertEquals(list.pop(), "d");
      assertEquals(list.pop(), "b");
      assertEquals(list.pop(), "a");
      assertEquals(list.pop(), "c");
  }
  @Test
  public void addAllTest() {
      MarkedLinkedList<String> list = new MarkedLinkedList();
      list.addAll(Arrays.asList("a", "b", "c", "d"));
      assertEquals(list.pop(), "a");
      assertEquals(list.pop(), "b");
      assertEquals(list.pop(), "c");
      assertEquals(list.pop(), "d");
  }
  @Test
  public void iteratorTest() {
      MarkedLinkedList<String> list = new MarkedLinkedList();
      list.addAll(Arrays.asList("a", "b", "c", "d"));
      Iterator<String> it = list.iterator();
      assertEquals(it.hasNext(), true);
      assertEquals(it.next(), "a");
      assertEquals(it.hasNext(), true);
      assertEquals(it.next(), "b");
      assertEquals(it.hasNext(), true);
      assertEquals(it.next(), "c");
      assertEquals(it.hasNext(), true);
      assertEquals(it.next(), "d");
      assertEquals(it.hasNext(), false);
  }
  @Test
  public void moveTest() {
      MarkedLinkedList<String> list = new MarkedLinkedList();
      list.addAll(Arrays.asList("a", "b", "c", "d", "e", "f"));
      MarkQueueIterator<String> it = (MarkQueueIterator<String>) list.iterator();
      while (it.hasNext()) {
          String comp = (String) it.next();
          if(comp.equals("b")) {
              break;
          }
      }
      Mark<String> newMark = it.mark();
      while (it.hasNext()) {
          String comp = (String) it.next();
          if(comp.equals("d")) {
              break;
          }
      }
      Mark<String> tempMark = it.mark();
      Mark<String> mark = list.getRoot();
      list.moveAfter(mark, newMark, tempMark);
      System.out.println(list.toString());
      Iterator<String> it1 = list.iterator();
      assertEquals(it1.hasNext(), true);
      assertEquals(it1.next(), "b");
      assertEquals(it1.hasNext(), true);
      assertEquals(it1.next(), "c");
      assertEquals(it1.hasNext(), true);
      assertEquals(it1.next(), "a");
      assertEquals(it1.hasNext(), true);
      assertEquals(it1.next(), "d");
      assertEquals(it1.hasNext(), true);
      assertEquals(it1.next(), "e");
      assertEquals(it1.hasNext(), true);
      assertEquals(it1.next(), "f");
      assertEquals(it1.hasNext(), false);
  }
  @Test
  public void arrayTest() {
      MarkedLinkedList<String> list = new MarkedLinkedList();
      list.addAll(Arrays.asList("a", "b", "c", "d", "e", "f"));
      Object[] a = list.toArray();
      assertEquals(a.length, 6);
      assertEquals(a, Arrays.asList("a", "b", "c", "d", "e", "f").toArray());
  }
}
