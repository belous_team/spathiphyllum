/*
 * Copyright (C) 2018-2018 The Project Spathiphyllum Authors.
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
package org.spathiphyllum.el;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.HashMap;
import java.util.Map;

import org.spathiphyllum.el.HelperEL.Const;
import org.springframework.expression.Expression;
import org.springframework.expression.spel.standard.SpelExpressionParser;

/**
 * @author dbilous
 *
 */
public class ComparePerformanceTest {

    /**
     * @param args
     * @throws ParseELException 
     */
    public static void main(String[] args) throws ParseELException {
        String arg = "(${a} < ${b})? ${a} * ${b} * ${c} * ${d} : ${a} + ${b} + ${c} +${d} + ${e};";
        String arg2 = "(1 < 2)? 1 * 2 * 3 * 4 : 1 + 2 + 3 + 4 + 5";
        SpelExpressionParser parser = new SpelExpressionParser();
        Expression exp = parser.parseExpression(arg2);
        int a = 1;
        int b = 2;
        int c = 3;
        int d = 4;
        int e = 5;
        ComparePerformanceTest test = new ComparePerformanceTest();
        long start = System.nanoTime();
        for(int i = 1; i < 1_000_000; i++) {
            test.javaTestConst(a,b,c,d,e);
        }
        long end = System.nanoTime();
        System.out.println("java const time took: " + (end - start) + " ns, 1 operation: " + (end - start)/1_000_000+" ns");
        System.gc();
        Map<String, Integer> map = new HashMap<>();
        map.put("a", 1);
        map.put("b", 2);
        map.put("c", 3);
        map.put("d", 4);
        map.put("e", 5);
        start = System.nanoTime();
        for(int i = 1; i < 1_000_000; i++) {
            test.javaTestMap(map);
        }
        end = System.nanoTime();
        System.out.println("java map time took: " + (end - start) + " ns, 1 operation: " + (end - start)/1_000_000+" ns");
        System.gc();
        EL el = EL.build(arg, new PetalProviderImpl(1,2,3,4,5), null);
        start = System.nanoTime();
        for(int i = 1; i < 1_000_000; i++) {
            el.asInt();
        }
        end = System.nanoTime();
        System.out.println("el map time took: " + (end - start) + " ns, 1 operation: " + (end - start)/1_000_000+" ns");
        System.gc();
        EL el2 = EL.build(arg2, null, null);
        start = System.nanoTime();
        for(int i = 1; i < 1_000_000; i++) {
            el2.asInt();
        }
        end = System.nanoTime();
        System.out.println("el const time took: " + (end - start) + " ns, 1 operation: " + (end - start)/1_000_000+" ns");
        System.gc();
        start = System.nanoTime();
        for(int i = 1; i < 1_000_000; i++) {
            exp.getValue();
        }
        end = System.nanoTime();
        System.out.println("spel const time took: " + (end - start) + " ns, 1 operation: " + (end - start)/1_000_000+" ns");
    }

    private int javaTestConst(int a, int b, int c, int d, int e) {
        return (a < b)? a * b * c * d : a + b + c + d + e;
    }
    private int javaTestMap(Map<String, Integer> map) {
        return (map.get("a") < map.get("b"))? map.get("a") * map.get("b") * map.get("c") * map.get("d") : map.get("a") + map.get("b") + map.get("c") + map.get("d") + map.get("e");
    }
    static class PetalProviderImpl implements PetalProvider {
        Map<String, Const> map = new HashMap<>();
        public PetalProviderImpl(int a, int b, int c, int d, int e) {
            map.put("a", new Const(a, EnumEL.INT));
            map.put("b", new Const(b, EnumEL.INT));
            map.put("c", new Const(c, EnumEL.INT));
            map.put("d", new Const(d, EnumEL.INT));
            map.put("e", new Const(e, EnumEL.INT));
        }
        @Override
        public EnumEL getType(String petalName) {
            if(map.get(petalName) == null) {
                throw new RuntimeException();
            }
            return map.get(petalName).getType();
        }

        @Override
        public boolean isConstant(String petalName) {
            if(map.get(petalName) == null) {
                throw new RuntimeException();
            }
            return map.get(petalName).isConstant();
        }

        @Override
        public boolean isValid(String petalName) {
            return true;
        }

        @Override
        public BigDecimal asBigDecimal(String petalName){
            try {
                return map.get(petalName).asBigDecimal();
            } catch (ParseELException e) {
                throw new RuntimeException();
            }
        }

        @Override
        public BigInteger asBigInteger(String petalName){
            try {
                return map.get(petalName).asBigInteger();
            } catch (ParseELException e) {
                throw new RuntimeException();
            }
        }

        @Override
        public boolean asBoolean(String petalName){
            try {
                return map.get(petalName).asBoolean();
            } catch (ParseELException e) {
                throw new RuntimeException();
            }
        }

        @Override
        public String asString(String petalName){
            return map.get(petalName).asString();
        }

        @Override
        public byte asByte(String petalName){
            try {
                return map.get(petalName).asByte();
            } catch (ParseELException e) {
                throw new RuntimeException();
            }
        }

        @Override
        public short asShort(String petalName){
            try {
                return map.get(petalName).asShort();
            } catch (ParseELException e) {
                throw new RuntimeException();
            }
        }

        @Override
        public char asChar(String petalName){
            try {
                return map.get(petalName).asChar();
            } catch (ParseELException e) {
                throw new RuntimeException();
            }
        }

        @Override
        public int asInt(String petalName){
            try {
                return map.get(petalName).asInt();
            } catch (ParseELException e) {
                throw new RuntimeException();
            }
        }

        @Override
        public long asLong(String petalName){
            try {
                return map.get(petalName).asLong();
            } catch (ParseELException e) {
                throw new RuntimeException();
            }
        }

        @Override
        public float asFloat(String petalName){
            try {
                return map.get(petalName).asFloat();
            } catch (ParseELException e) {
                throw new RuntimeException();
            }
        }

        @Override
        public double asDouble(String petalName){
            try {
                return map.get(petalName).asDouble();
            } catch (ParseELException e) {
                throw new RuntimeException();
            }
        }
    }
}
