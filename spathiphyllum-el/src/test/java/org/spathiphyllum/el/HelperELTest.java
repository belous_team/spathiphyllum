/*
 * Copyright (C) 2018-2018 The Project Spathiphyllum Authors.
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
package org.spathiphyllum.el;

import java.util.Deque;

import org.spathiphyllum.el.plugins.MathPlugin;
import org.testng.annotations.Test;

public class HelperELTest {
  @Test
  public void parseEL00() throws ParseELException {
	  String arg = "3+1";
	  Deque<AbstractComponent> result = HelperEL.parseEL(arg, null, null);
	  System.out.println("parseEL0: "+arg);
	  System.out.println(result);
	  System.out.println(EL.build(arg, null, null).asInt());
  }
  @Test
  public void parseEL01() throws ParseELException {
	  String arg = "(3+1) +  4 - - 5 + (-2)";
	  Deque<AbstractComponent> result = HelperEL.parseEL(arg, null, null);
	  System.out.println("parseEL1: "+arg);
	  System.out.println(result);
	  System.out.println(EL.build(arg, null, null).asInt());
  }
  @Test
  public void parseEL02() throws ParseELException {
	  String arg = "\"ASDqwe\" + \"GDFQWE\"";
	  Deque<AbstractComponent> result = HelperEL.parseEL(arg, null, null);
	  System.out.println("parseEL2: "+arg);
	  System.out.println(result);
	  System.out.println(EL.build(arg, null, null).asString());
  }
  @Test
  public void parseEL03() throws ParseELException {
	  String arg = "!false && true & (true ^ false)";
	  Deque<AbstractComponent> result = HelperEL.parseEL(arg, null, null);
	  System.out.println("parseEL3: "+arg);
	  System.out.println(result);
	  System.out.println(EL.build(arg, null, null).asBoolean());
  }
  @Test
  public void parseEL04() throws ParseELException {
	  String arg = "!true | false || true";
	  boolean b = !true | false || true;
	  Deque<AbstractComponent> result = HelperEL.parseEL(arg, null, null);
	  System.out.println("parseEL4: "+arg);
	  System.out.println(result);
	  System.out.println(EL.build(arg, null, null).asBoolean());
  }
  @Test
  public void parseEL05() throws ParseELException {
	  String arg = "5 * 6 / 2 % 3";
	  Deque<AbstractComponent> result = HelperEL.parseEL(arg, null, null);
	  System.out.println("parseEL5: "+arg);
	  System.out.println(result);
	  System.out.println(EL.build(arg, null, null).asDouble());
  }
  @Test
  public void parseEL06() throws ParseELException {
	  String arg = "(3 == 2)?(3 == 2)?\"c\":\"a\":(3 == 2)?\"c\":\"b\";";
	  //Ternary, EQ 3 2, ternTrue Ternary EQ 3 2 ternTru a ternFalse 
	  Deque<AbstractComponent> result = HelperEL.parseEL(arg, null, null);
	  System.out.println("parseEL6: "+arg);
	  System.out.println(result);
	  System.out.println(EL.build(arg, null, null).asString());
  }
  @Test
  public void parseEL07() throws ParseELException {
	  String arg = "${name}";
	  Deque<AbstractComponent> result = HelperEL.parseEL(arg, null, null);
	  System.out.println("parseEL7: "+arg);
	  System.out.println(result);
  }
  @Test
  public void parseEL08() throws ParseELException {
	  String arg = "2 + \"asd\"";
	  Deque<AbstractComponent> result = HelperEL.parseEL(arg, null, null);
  }
  @Test
  public void parseEL09() throws ParseELException {
	  String arg = "false + \"asd\"";
	  Deque<AbstractComponent> result = HelperEL.parseEL(arg, null, null);
  }
  @Test(expectedExceptions = {ParseELException.class})
  public void parseEL10() throws ParseELException {
	  String arg = "false * 4";
	  Deque<AbstractComponent> result = HelperEL.parseEL(arg, null, null);
  }
  @Test(expectedExceptions = {ParseELException.class})
  public void parseEL11() throws ParseELException {
      String arg = "false \"aaaa\" 555";
      Deque<AbstractComponent> result = HelperEL.parseEL(arg, null, null);
      System.out.println("parseEL11: "+arg);
      System.out.println(result);
  }
  @Test
  public void parseEL12() throws ParseELException {
      String arg = "max(-2, 4)";
      Deque<AbstractComponent> result = HelperEL.parseEL(arg, null, new MathPlugin());
      System.out.println("parseEL12: "+arg);
      System.out.println(result);
  }
  
}
