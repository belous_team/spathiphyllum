/*
 * Copyright (C) 2018-2018 The Project Spathiphyllum Authors.
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
package org.spathiphyllum.el;

import org.springframework.expression.Expression;
import org.springframework.expression.spel.standard.SpelExpressionParser;

public class PerformanceTest {

    public static void main(String[] args) throws ParseELException {
        System.out.println("Starting");
        String str = "'Hello World' + '!' + '1' + '2' + '3' + '4' + '5' + '6' + '6' + '6' + '6' + '6' + '6' + '6' + '6' + '6' + '6' + '6' + '6' + '6' + '6' + '6' + '6' + '6' + '6' + '6' + '6' + '6' + '6' + '6' + '6' + '6' + '6' + '6' + '6' + '6' + '6' + '6' + '6' + '6' + '6' + '6' + '6' + '6' + '6' + '6' + '6' + '6' + '6' + '6' + '6' + '6' + '6' + '6' + '6' + '6' + '6' + '6' + '6' + '6' + '6' + '6' + '6' + '6' + '6' + '6' + '6' + '6' + '6' + '6' + '6' + '6' + '6' + '6' + '6' + '6' + '6' + '6' + '6' + '6' + '6' + '6' + '6' + '6' + '6' + '6' + '6' + '6' + '6' + '6' + '6' + '6' + '6' + '6' + '6' + '6' + '6' + '6' + '6' + '6' + '6' + '6' + '6' + '6' + '6' + '6' + '6' + '6' + '6' + '6' + '6' + '6' + '6' + '6' + '6' + '6' + '6' + '6' + '6' + '6' + '6' + '6' + '6' + '6' + '6' + '6' + '6' + '6' + '6' + '6' + '6' + '6' + '6' + '6' + '6' + '6' + '6' + '6' + '6' + '6' + '6' + '6' + '6' + '6' + '6' + '6' + '6' + '6' + '6' + '6' + '6' + '6' + '6' + '6' + '6' + '6' + '6' + '6' + '6' + '6' + '6' + '6' + '6' + '6' + '6' + '6' + '6' + '6' + '6'";
        SpelExpressionParser parser = new SpelExpressionParser();
        Expression exp = parser.parseExpression(str);
        long start = System.nanoTime();
        for(int i = 1; i < 1_000_000; i++) {
            //exp = parser.parseExpression("'Hello World'.concat('!')");
            String message = (String) exp.getValue();
        }
        long end = System.nanoTime();
        System.out.println("spel time took: " + (end - start) + " ns, 1 operation: " + (end - start)/1_000_000+" ns");
        System.gc();
        start = System.nanoTime();
        for(int i = 1; i < 1_000_000; i++) {
            exp = parser.parseExpression(str);
        }
        end = System.nanoTime();
        System.out.println("spel 'compile' time took: " + (end - start) + " ns, 1 operation: " + (end - start)/1_000_000+" ns");
        System.gc();
        String s1 = "hello world";
        String s2 = "!";
        String s3 = "1";
        String s4 = "2";
        String s5 = "3";
        String s6 = "4";
        String s7 = "5";
        String s8 = "6";
        start = System.nanoTime();
        for(int i = 1; i < 1_000_000; i++) {
            String message = s1 + s2 + s3 + s4 + s5 + s6 + s7 + s8 + s8 + s8 + s8 + s8 + s8 + s8 + s8 + s8 + s8 + s8 + s8 + s8 + s8 + s8 + s8 + s8 + s8 + s8 + s8 + s8 + s8 + s8 + s8 + s8 + s8 + s8 + s8 + s8 + s8 + s8 + s8 + s8 + s8 + s8 + s8 + s8 + s8 + s8 + s8 + s8 + s8 + s8 + s8 + s8 + s8 + s8 + s8 + s8 + s8 + s8 + s8 + s8 + s8 + s8 + s8 + s8 + s8 + s8 + s8 + s8 + s8 + s8+ s8 + s8 + s8 + s8 + s8 + s8 + s8 + s8 + s8 + s8 + s8 + s8 + s8 + s8 + s8 + s8 + s8 + s8 + s8 + s8 + s8 + s8 + s8 + s8 + s8 + s8 + s8 + s8 + s8 + s8 + s8 + s8 + s8 + s8 + s8 + s8 + s8 + s8 + s8 + s8 + s8 + s8 + s8 + s8 + s8 + s8 + s8 + s8 + s8 + s8 + s8 + s8 + s8 + s8 + s8 + s8 + s8 + s8 + s8 + s8 + s8 + s8 + s8 + s8 + s8 + s8 + s8 + s8 + s8 + s8 + s8 + s8 + s8 + s8 + s8 + s8 + s8 + s8 + s8 + s8 + s8 + s8+ s8 + s8 + s8 + s8 + s8 + s8 + s8 + s8 + s8 + s8 + s8 + s8 + s8 + s8 + s8 + s8 + s8 + s8 + s8;
        }
        end = System.nanoTime();
        System.out.println("original time took: " + (end - start) + " ns, 1 operation: " + (end - start)/1_000_000+" ns");
        System.gc();
        EL ex = EL.build(str, null, null);
        start = System.nanoTime();
        for(int i = 1; i < 1_000_000; i++) {
            String message = ex.asString();
            
        }
        end = System.nanoTime();
        System.out.println("el time took: " + (end - start) + " ns, 1 operation: " + (end - start)/1_000_000+" ns");
        System.gc();
        start = System.nanoTime();
        for(int i = 1; i < 1_000_000; i++) {
            EL ex2 = EL.build(str, null, null);
        }
        end = System.nanoTime();
        System.out.println("el compile time took: " + (end - start) + " ns, 1 operation: " + (end - start)/1_000_000+" ns");
    }

}
