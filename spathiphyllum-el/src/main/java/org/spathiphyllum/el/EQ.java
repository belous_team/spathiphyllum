/*
 * Copyright (C) 2018-2018 The Project Spathiphyllum Authors.
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
package org.spathiphyllum.el;

import java.math.BigDecimal;
import java.math.BigInteger;

import org.spathiphyllum.el.HelperEL.Const;
import org.spathiphyllum.el.HelperEL.Operator;
/**
 * Equal
 * @author dbelous
 *
 */
class EQ extends Operator{
    static final Operator OPERATOR = new EQ();
    private EQ() {
        super("EQ", EnumEL.BOOLEAN);
    }
    @Override
    int priority() {
        return 7;
    }
    @Override
    EnumEL adjustType(EnumEL left, EnumEL right) throws ParseELException {
        if(left == EnumEL.SOP || right == EnumEL.SOP){
            throw new ParseELException(0, "Icorrect operator");
        }else if(left.isBoolean() && right.isBoolean()){
            return EnumEL.BOOLEAN;
        }else if(left.isNumber() && right.isNumber()){
            return HelperEL.approachNumerTypes(left, right);
        }else if(left.isString() && right.isString()){
            return EnumEL.STRING;
        }else{
            throw new ParseELException(0, "Cannot compare "+left+" and "+right);
        }
    }
    @Override
    Operator adjustOperator(int pos, AbstractComponent left, AbstractComponent right) throws ParseELException {
        if(left.getType() == EnumEL.SOP || right.getType() == EnumEL.SOP){
            throw new ParseELException(pos, "Incorrect operator");
        }else if((left.getType().isBoolean() ^ right.getType().isBoolean())
                ||(left.getType().isNumber() ^ right.getType().isNumber())
                ||(left.getType().isString() ^ right.getType().isString())){
            throw new ParseELException(pos, "Cannot compare "+left.getType()+" and "+right.getType());
        }
        return OPERATOR;
    }
    @Override
    Performer compile(EnumEL performerType, Performer leftP, Const leftC, Performer rightP, Const rightC)
            throws ParseELException {
        if(leftC != null && rightC != null){
            switch (performerType) {
            case BIG_DECIMAL:
                return performerBooleanBigDecimal(leftC.asBigDecimal(), rightC.asBigDecimal());
            case BIG_INTEGER:
                return performerBooleanBigInteger(leftC.asBigInteger(), rightC.asBigInteger());
            case BOOLEAN:
                return performerBooleanBoolean(leftC.asBoolean(), rightC.asBoolean());
            case DOUBLE:
                return performerBooleanDouble(leftC.asDouble(), rightC.asDouble());
            case FLOAT:
                return performerBooleanFloat(leftC.asFloat(), rightC.asFloat());
            case INT:
                return performerBooleanInt(leftC.asInt(), rightC.asInt());
            case LONG:
                return performerBooleanLong(leftC.asLong(), rightC.asLong());
            case STRING:
                return performerBooleanString(leftC.asString(), rightC.asString());
            default:
                break;
            }
        }else if(leftP != null && rightP != null){
            switch (performerType) {
            case BIG_DECIMAL:
                return performerBooleanBigDecimal(leftP, rightP);
            case BIG_INTEGER:
                return performerBooleanBigInteger(leftP, rightP);
            case BOOLEAN:
                return performerBooleanBoolean(leftP, rightP);
            case DOUBLE:
                return performerBooleanDouble(leftP, rightP);
            case FLOAT:
                return performerBooleanFloat(leftP, rightP);
            case INT:
                return performerBooleanInt(leftP, rightP);
            case LONG:
                return performerBooleanLong(leftP, rightP);
            case STRING:
                return performerBooleanString(leftP, rightP);
            default:
                break;
            }
        }else if(leftC != null && rightP != null){
            switch (performerType) {
            case BIG_DECIMAL:
                return performerBooleanBigDecimal(leftC.asBigDecimal(), rightP);
            case BIG_INTEGER:
                return performerBooleanBigInteger(leftC.asBigInteger(), rightP);
            case BOOLEAN:
                return performerBooleanBoolean(leftC.asBoolean(), rightP);
            case DOUBLE:
                return performerBooleanDouble(leftC.asDouble(), rightP);
            case FLOAT:
                return performerBooleanFloat(leftC.asFloat(), rightP);
            case INT:
                return performerBooleanInt(leftC.asInt(), rightP);
            case LONG:
                return performerBooleanLong(leftC.asLong(), rightP);
            case STRING:
                return performerBooleanString(leftC.asString(), rightP);
            default:
                break;
            }
        }else if(leftP != null && rightC != null){
            switch (performerType) {
            case BIG_DECIMAL:
                return performerBooleanBigDecimal(leftP, rightC.asBigDecimal());
            case BIG_INTEGER:
                return performerBooleanBigInteger(leftP, rightC.asBigInteger());
            case BOOLEAN:
                return performerBooleanBoolean(leftP, rightC.asBoolean());
            case DOUBLE:
                return performerBooleanDouble(leftP, rightC.asDouble());
            case FLOAT:
                return performerBooleanFloat(leftP, rightC.asFloat());
            case INT:
                return performerBooleanInt(leftP, rightC.asInt());
            case LONG:
                return performerBooleanLong(leftP, rightC.asLong());
            case STRING:
                return performerBooleanString(leftP, rightC.asString());
            default:
                break;
            }
        }
        throw new ParseELException(0, "Compile error");
    }
    private static Performer performerBooleanString(Performer leftP, String rightC) {
        return new PerformerBoolean(){
            Performer left = leftP;
            String right = rightC;
            @Override
            public boolean performBoolean(PetalProvider provider) {
                return left.performString(null, provider).toString().compareTo(right) == 0;
            }
        };
    }
    private static Performer performerBooleanLong(Performer leftP, long rightC) {
        return new PerformerBoolean(){
            Performer left = leftP;
            long right = rightC;
            @Override
            public boolean performBoolean(PetalProvider provider) {
                return left.performLong(provider) == right;
            }
        };
    }
    private static Performer performerBooleanInt(Performer leftP, int rightC) {
        return new PerformerBoolean(){
            Performer left = leftP;
            int right = rightC;
            @Override
            public boolean performBoolean(PetalProvider provider) {
                return left.performInt(provider) == right;
            }
        };
    }
    private static Performer performerBooleanFloat(Performer leftP, float rightC) {
        return new PerformerBoolean(){
            Performer left = leftP;
            float right = rightC;
            @Override
            public boolean performBoolean(PetalProvider provider) {
                return Float.compare(left.performFloat(provider), right) == 0;
            }
        };
    }
    private static Performer performerBooleanDouble(Performer leftP, double rightC) {
        return new PerformerBoolean(){
            Performer left = leftP;
            double right = rightC;
            @Override
            public boolean performBoolean(PetalProvider provider) {
                return Double.compare(left.performDouble(provider), right) == 0;
            }
        };
    }
    private static Performer performerBooleanBoolean(Performer leftP, boolean rightC) {
        return new PerformerBoolean(){
            Performer left = leftP;
            boolean right = rightC;
            @Override
            public boolean performBoolean(PetalProvider provider) {
                return left.performBoolean(provider) == right;
            }
        };
    }
    private static Performer performerBooleanBigInteger(Performer leftP, BigInteger rightC) {
        return new PerformerBoolean(){
            Performer left = leftP;
            BigInteger right = rightC;
            @Override
            public boolean performBoolean(PetalProvider provider) {
                return left.performBigInteger(provider).compareTo(right) == 0;
            }
        };
    }
    private static Performer performerBooleanBigDecimal(Performer leftP, BigDecimal rightC) {
        return new PerformerBoolean(){
            Performer left = leftP;
            BigDecimal right = rightC;
            @Override
            public boolean performBoolean(PetalProvider provider) {
                return left.performBigDecimal(provider).compareTo(right) == 0;
            }
        };
    }
    private static Performer performerBooleanString(Performer leftP, Performer rightP) {
        return new PerformerBoolean(){
            Performer left = leftP;
            Performer right = rightP;
            @Override
            public boolean performBoolean(PetalProvider provider) {
                return left.performString(null, provider).toString()
                        .compareTo(right.performString(null, provider).toString()) == 0;
            }
        };
    }
    private static Performer performerBooleanLong(Performer leftP, Performer rightP) {
        return new PerformerBoolean(){
            Performer left = leftP;
            Performer right = rightP;
            @Override
            public boolean performBoolean(PetalProvider provider) {
                return left.performLong(provider) == right.performLong(provider);
            }
        };
    }
    private static Performer performerBooleanInt(Performer leftP, Performer rightP) {
        return new PerformerBoolean(){
            Performer left = leftP;
            Performer right = rightP;
            @Override
            public boolean performBoolean(PetalProvider provider) {
                return left.performInt(provider) == right.performInt(provider);
            }
        };
    }
    private static Performer performerBooleanFloat(Performer leftP, Performer rightP) {
        return new PerformerBoolean(){
            Performer left = leftP;
            Performer right = rightP;
            @Override
            public boolean performBoolean(PetalProvider provider) {
                return Float.compare(left.performFloat(provider), right.performFloat(provider)) == 0;
            }
        };
    }
    private static Performer performerBooleanDouble(Performer leftP, Performer rightP) {
        return new PerformerBoolean(){
            Performer left = leftP;
            Performer right = rightP;
            @Override
            public boolean performBoolean(PetalProvider provider) {
                return Double.compare(left.performDouble(provider), right.performDouble(provider)) == 0;
            }
        };
    }
    private static Performer performerBooleanBoolean(Performer leftP, Performer rightP) {
        return new PerformerBoolean(){
            Performer left = leftP;
            Performer right = rightP;
            @Override
            public boolean performBoolean(PetalProvider provider) {
                return left.performBoolean(provider) == right.performBoolean(provider);
            }
        };
    }
    private static Performer performerBooleanBigInteger(Performer leftP, Performer rightP) {
        return new PerformerBoolean(){
            Performer left = leftP;
            Performer right = rightP;
            @Override
            public boolean performBoolean(PetalProvider provider) {
                return left.performBigInteger(provider).compareTo(right.performBigInteger(provider)) == 0;
            }
        };
    }
    private static Performer performerBooleanBigDecimal(Performer leftP, Performer rightP) {
        return new PerformerBoolean(){
            Performer left = leftP;
            Performer right = rightP;
            @Override
            public boolean performBoolean(PetalProvider provider) {
                return left.performBigDecimal(provider).compareTo(right.performBigDecimal(provider)) == 0;
            }
        };
    }
    private static Performer performerBooleanString(String leftC, String rightC) {
        return new PerformerBoolean(){
            String left = leftC;
            String right = rightC;
            @Override
            public boolean performBoolean(PetalProvider provider) {
                return left.compareTo(right) == 0;
            }
        };
    }
    private static Performer performerBooleanLong(long leftC, long rightC) {
        return new PerformerBoolean(){
            long left = leftC;
            long right = rightC;
            @Override
            public boolean performBoolean(PetalProvider provider) {
                return left == right;
            }
        };
    }
    private static Performer performerBooleanInt(int leftC, int rightC) {
        return new PerformerBoolean(){
            int left = leftC;
            int right = rightC;
            @Override
            public boolean performBoolean(PetalProvider provider) {
                return left == right;
            }
        };
    }
    private static Performer performerBooleanFloat(float leftC, float rightC) {
        return new PerformerBoolean(){
            float left = leftC;
            float right = rightC;
            @Override
            public boolean performBoolean(PetalProvider provider) {
                return Float.compare(left, right) == 0;
            }
        };
    }
    private static Performer performerBooleanDouble(double leftC, double rightC) {
        return new PerformerBoolean(){
            double left = leftC;
            double right = rightC;
            @Override
            public boolean performBoolean(PetalProvider provider) {
                return Double.compare(left, right) == 0;
            }
        };
    }
    private static Performer performerBooleanBoolean(boolean leftC, boolean rightC) {
        return new PerformerBoolean(){
            boolean left = leftC;
            boolean right = rightC;
            @Override
            public boolean performBoolean(PetalProvider provider) {
                return left == right;
            }
        };
    }
    private static Performer performerBooleanBigInteger(BigInteger leftC, BigInteger rightC) {
        return new PerformerBoolean(){
            BigInteger left = leftC;
            BigInteger right = rightC;
            @Override
            public boolean performBoolean(PetalProvider provider) {
                return left.compareTo(right) == 0;
            }
        };
    }
    private static Performer performerBooleanBigDecimal(BigDecimal leftC, BigDecimal rightC) {
        return new PerformerBoolean(){
            BigDecimal left = leftC;
            BigDecimal right = rightC;
            @Override
            public boolean performBoolean(PetalProvider provider) {
                return left.compareTo(right) == 0;
            }
        };
    }
    private static Performer performerBooleanString(String leftC, Performer rightP) {
        return new PerformerBoolean(){
            String left = leftC;
            Performer right = rightP;
            @Override
            public boolean performBoolean(PetalProvider provider) {
                return left.compareTo(right.performString(null, provider).toString()) == 0;
            }
        };
    }
    private static Performer performerBooleanLong(long leftC, Performer rightP) {
        return new PerformerBoolean(){
            long left = leftC;
            Performer right = rightP;
            @Override
            public boolean performBoolean(PetalProvider provider) {
                return left == right.performLong(provider);
            }
        };
    }
    private static Performer performerBooleanInt(int leftC, Performer rightP) {
        return new PerformerBoolean(){
            int left = leftC;
            Performer right = rightP;
            @Override
            public boolean performBoolean(PetalProvider provider) {
                return left == right.performInt(provider);
            }
        };
    }
    private static Performer performerBooleanFloat(float leftC, Performer rightP) {
        return new PerformerBoolean(){
            float left = leftC;
            Performer right = rightP;
            @Override
            public boolean performBoolean(PetalProvider provider) {
                return Float.compare(left, right.performFloat(provider)) == 0;
            }
        };
    }
    private static Performer performerBooleanDouble(double leftC, Performer rightP) {
        return new PerformerBoolean(){
            double left = leftC;
            Performer right = rightP;
            @Override
            public boolean performBoolean(PetalProvider provider) {
                return Double.compare(left, right.performDouble(provider)) == 0;
            }
        };
    }
    private static Performer performerBooleanBoolean(boolean leftC, Performer rightP) {
        return new PerformerBoolean(){
            boolean left = leftC;
            Performer right = rightP;
            @Override
            public boolean performBoolean(PetalProvider provider) {
                return left == right.performBoolean(provider);
            }
        };
    }
    private static Performer performerBooleanBigInteger(BigInteger leftC, Performer rightP) {
        return new PerformerBoolean(){
            BigInteger left = leftC;
            Performer right = rightP;
            @Override
            public boolean performBoolean(PetalProvider provider) {
                return left.compareTo(right.performBigInteger(provider)) == 0;
            }
        };
    }
    private static Performer performerBooleanBigDecimal(BigDecimal leftC, Performer rightP) {
        return new PerformerBoolean(){
            BigDecimal left = leftC;
            Performer right = rightP;
            @Override
            public boolean performBoolean(PetalProvider provider) {
                return left.compareTo(right.performBigDecimal(provider)) == 0;
            }
        };
    }
    @Override
    Const unitConst(Const left, Const right) throws ParseELException {
        switch(adjustType(left.getType(), right.getType())){
            case BIG_DECIMAL: return new Const(left.asBigDecimal().compareTo(right.asBigDecimal()) == 0, EnumEL.BOOLEAN);
            case BIG_INTEGER: return new Const(left.asBigInteger().compareTo(right.asBigInteger()) == 0, EnumEL.BOOLEAN);
            case DOUBLE: return new Const(Double.compare(left.asDouble(), right.asChar()) == 0, EnumEL.BOOLEAN);
            case FLOAT: return new Const(Float.compare(left.asFloat(), right.asFloat()) == 0, EnumEL.BOOLEAN);
            case INT: return new Const(left.asInt() == right.asInt(), EnumEL.BOOLEAN);
            case LONG: return new Const(left.asLong() == right.asLong(), EnumEL.BOOLEAN);
            case STRING: return new Const(left.asString().compareTo(right.asString()) == 0, EnumEL.BOOLEAN);
            case BOOLEAN: return new Const(Boolean.compare(left.asBoolean(), right.asBoolean()) == 0, EnumEL.BOOLEAN);
            default: throw new ParseELException(0, "Method is unsupported");
        }
    }
}
