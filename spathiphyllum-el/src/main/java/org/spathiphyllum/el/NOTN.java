/*
 * Copyright (C) 2018-2018 The Project Spathiphyllum Authors.
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
package org.spathiphyllum.el;

import java.math.BigInteger;

import org.spathiphyllum.el.HelperEL.Const;
import org.spathiphyllum.el.HelperEL.Operator;
import org.spathiphyllum.el.HelperEL.UnaryOperator;
/**
 * Inverts the value of a boolean (~)
 * @author dbelous
 *
 */
public class NOTN extends UnaryOperator{
    static final NOTN OPERATOR = new NOTN();
    private NOTN() {
        super("NOT", EnumEL.NUMBER);
    }
    @Override
    int priority() {
        return 0;
    }
    @Override
    EnumEL adjustType(EnumEL left, EnumEL right) throws ParseELException {
        if(left.isNumber()){
            if(left == EnumEL.BIG_DECIMAL){
                throw new ParseELException(0, "Operator '~' can be assigned to BigDecimal");
            }else if(left == EnumEL.DOUBLE){
                throw new ParseELException(0, "Operator '~' can be assigned to double");
            }else if(left == EnumEL.FLOAT){
                throw new ParseELException(0, "Operator '~' can be assigned to float");
            }
            return HelperEL.approachNumerTypes(left, left);
        }else{
            throw new ParseELException(0, "Operator '~' can be assigned number only");
        }
    }
    @Override
    Operator adjustOperator(int pos, AbstractComponent left, AbstractComponent right) throws ParseELException {
        if(left.getType().isNumber()){
            return OPERATOR;
        }else{
            throw new ParseELException(pos, "Operator '~' can be assigned number only");
        }
    }
    @Override
    Performer compile(EnumEL performerType, Performer leftP, Const leftC, Performer rightP, Const rightC)
            throws ParseELException {
        switch (performerType) {
        case BIG_INTEGER:
            if(leftP != null){
                return performerBigInteger(leftP);
            }else{
                return performerBigInteger(leftC.asBigInteger());
            }
        case INT:
            if(leftP != null){
                return performerInt(leftP);
            }else{
                return performerInt(leftC.asInt());
            }
        case LONG:
            if(leftP != null){
                return performerLong(leftP);
            }else{
                return performerLong(leftC.asLong());
            }
        case BYTE:
            if(leftP != null){
                return performerByte(leftP);
            }else{
                return performerByte(leftC.asByte());
            }
        case CHAR:
            if(leftP != null){
                return performerChar(leftP);
            }else{
                return performerChar(leftC.asChar());
            }
        case SHORT:
            if(leftP != null){
                return performerShort(leftP);
            }else{
                return performerShort(leftC.asShort());
            }
        default:
        }
        throw new ParseELException(0, "Compile error");
    }
    private static Performer performerBigInteger(Performer leftP) {
        return new PerformerBigInteger(){
            Performer left = leftP;
            @Override
            public BigInteger performBigInteger(PetalProvider provider) {
                return left.performBigInteger(provider).not();
            }
        };
    }
    private static Performer performerBigInteger(BigInteger leftC) {
        return new PerformerBigInteger(){
            BigInteger left = leftC;
            @Override
            public BigInteger performBigInteger(PetalProvider provider) {
                return left.not();
            }
        };
    }
    private static Performer performerInt(Performer leftP) {
        return new PerformerInt(){
            Performer left = leftP;
            @Override
            public int performInt(PetalProvider provider) {
                return ~left.performInt(provider);
            }
        };
    }
    private static Performer performerInt(int leftC) {
        return new PerformerInt(){
            int left = leftC;
            @Override
            public int performInt(PetalProvider provider) {
                return ~left;
            }
        };
    }
    private static Performer performerLong(Performer leftP) {
        return new PerformerLong(){
            Performer left = leftP;
            @Override
            public long performLong(PetalProvider provider) {
                return ~left.performLong(provider);
            }
        };
    }
    private static Performer performerLong(long leftC) {
        return new PerformerLong(){
            long left = leftC;
            @Override
            public long performLong(PetalProvider provider) {
                return ~left;
            }
        };
    }
    private static Performer performerByte(Performer leftP) {
        return new PerformerByte(){
            Performer left = leftP;
            @Override
            public byte performByte(PetalProvider provider) {
                return (byte) ~left.performByte(provider);
            }
        };
    }
    private static Performer performerByte(byte leftC) {
        return new PerformerByte(){
            byte left = leftC;
            @Override
            public byte performByte(PetalProvider provider) {
                return (byte) ~left;
            }
        };
    }
    private static Performer performerChar(Performer leftP) {
        return new PerformerChar(){
            Performer left = leftP;
            @Override
            public char performChar(PetalProvider provider) {
                return (char) ~left.performChar(provider);
            }
        };
    }
    private static Performer performerChar(char leftC) {
        return new PerformerChar(){
            char left = leftC;
            @Override
            public char performChar(PetalProvider provider) {
                return (char) ~left;
            }
        };
    }
    private static Performer performerShort(Performer leftP) {
        return new PerformerShort(){
            Performer left = leftP;
            @Override
            public short performShort(PetalProvider provider) {
                return (short) ~left.performShort(provider);
            }
        };
    }
    private static Performer performerShort(short leftC) {
        return new PerformerShort(){
            short left = leftC;
            @Override
            public short performShort(PetalProvider provider) {
                return (short) ~left;
            }
        };
    }
    @Override
    Const unitConst(Const left) throws ParseELException {
        switch(left.getType()){
        case BIG_INTEGER: return new Const(left.asBigInteger().not(), EnumEL.BIG_INTEGER);
        case BYTE: return new Const(~left.asByte(), EnumEL.BYTE);
        case CHAR: return new Const(~left.asChar(), EnumEL.CHAR);
        case INT: return new Const(~left.asInt(), EnumEL.INT);
        case LONG: return new Const(~left.asLong(), EnumEL.LONG);
        case SHORT: return new Const(~left.asShort(), EnumEL.SHORT);
        default: throw new ParseELException(0, "Method is unsupported");
    }
    }
}
