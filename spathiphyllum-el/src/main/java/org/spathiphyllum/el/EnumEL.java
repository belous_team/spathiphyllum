/*
 * Copyright (C) 2018-2018 The Project Spathiphyllum Authors.
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
package org.spathiphyllum.el;

/**
 * 
 * @author dbelous
 *
 */
public enum EnumEL {

    BOOLEAN(true, false, false), STRING(false, false, true),
    /**
     * Special number type
     */
    NUMBER(false, true, false), 
    /**
     * 
     */
    BYTE(false, true, false), SHORT(false, true, false), CHAR(false, true, false), INT(false, true, false), LONG(false, true, false), FLOAT(false, true, false), BIG_INTEGER(false, true, false), DOUBLE(false, true, false), BIG_DECIMAL(false, true, false),
    /**
     * Special Operator in stack for utility purposes
     */
    SOP();
    private boolean isBoolean = false;
    private boolean isNumber = false;
    private boolean isString = false;

    private EnumEL() {
    };

    private EnumEL(boolean isBoolean, boolean isNumber, boolean isString) {
        this.isBoolean = isBoolean;
        this.isNumber = isNumber;
        this.isString = isString;
    }

    public boolean isBoolean() {
        return isBoolean;
    };

    public boolean isNumber() {
        return isNumber;
    }

    public boolean isString() {
        return isString;
    };
}
