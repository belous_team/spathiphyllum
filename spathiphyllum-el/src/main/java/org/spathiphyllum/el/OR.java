/*
 * Copyright (C) 2018-2018 The Project Spathiphyllum Authors.
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
package org.spathiphyllum.el;

import java.math.BigInteger;

import org.spathiphyllum.el.HelperEL.Const;
import org.spathiphyllum.el.HelperEL.Operator;
/**
 * Boolean OR
 * @author obelous
 *
 */
public class OR extends Operator {
    static final Operator NUMBER = new OR(EnumEL.NUMBER);
    static final Operator BOOLEAN = new OR(EnumEL.BOOLEAN);
    private OR(EnumEL type) {
        super("OR", type);
    }
    @Override
    int priority() {
        return 4;
    }
    @Override
    EnumEL adjustType(EnumEL left, EnumEL right) throws ParseELException {
        if(left.isBoolean() && right.isBoolean()){
            return EnumEL.BOOLEAN;
        }else if(left.isNumber() && right.isNumber()){
            if(left == EnumEL.BIG_DECIMAL || right == EnumEL.BIG_DECIMAL){
                throw new ParseELException(0, "Operator '|' can be assigned to BigDecimal");
            }else if(left == EnumEL.DOUBLE || right == EnumEL.DOUBLE){
                throw new ParseELException(0, "Operator '|' can be assigned to double");
            }else if(left == EnumEL.FLOAT || right == EnumEL.FLOAT){
                throw new ParseELException(0, "Operator '|' can be assigned to float");
            }
            return HelperEL.approachNumerTypes(left, right);
        }else{
            throw new ParseELException(0, "Operator '|' can be assigned only to couple of boolean ar couple of number");
        }
    }
    @Override
    Operator adjustOperator(int pos, AbstractComponent left, AbstractComponent right) throws ParseELException {
        if(left.getType().isBoolean() && right.getType().isBoolean()){
            return BOOLEAN;
        }else if(left.getType().isNumber() && right.getType().isNumber()){
            return NUMBER;
        }else{
            throw new ParseELException(pos, "Operator '|' can be assigned only to couple of boolean ar couple of number");
        }
    }
    @Override
    Performer compile(EnumEL performerType, Performer leftP, Const leftC, Performer rightP, Const rightC)
            throws ParseELException {
        if(leftC != null && rightC != null){
            switch (performerType) {
            case BIG_INTEGER:
                return performerBigInteger(leftC.asBigInteger(), rightC.asBigInteger());
            case INT:
                return performerInt(leftC.asInt(), rightC.asInt());
            case LONG:
                return performerLong(leftC.asLong(), rightC.asLong());
            case BYTE:
                return performerByte(leftC.asByte(), rightC.asByte());
            case CHAR:
                return performerChar(leftC.asChar(), rightC.asChar());
            case SHORT:
                return performerShort(leftC.asShort(), rightC.asShort());
            case BOOLEAN:
                return performerBoolean(leftC.asBoolean(), rightC.asBoolean());
            default:
            }
        }else if(leftP != null && rightP != null){
            switch (performerType) {
            case BIG_INTEGER:
                return performerBigInteger(leftP, rightP);
            case INT:
                return performerInt(leftP, rightP);
            case LONG:
                return performerLong(leftP, rightP);
            case BOOLEAN:
                return performerBoolean(leftP, rightP);
            case BYTE:
                return performerByte(leftP, rightP);
            case CHAR:
                return performerChar(leftP, rightP);
            case SHORT:
                return performerShort(leftP, rightP);
            default:
            }
        }else if(leftC != null && rightP != null){
            switch (performerType) {
            case BIG_INTEGER:
                return performerBigInteger(leftC.asBigInteger(), rightP);
            case INT:
                return performerInt(leftC.asInt(), rightP);
            case LONG:
                return performerLong(leftC.asLong(), rightP);
            case BOOLEAN:
                return performerBoolean(leftC.asBoolean(), rightP);
            case BYTE:
                return performerByte(leftC.asByte(), rightP);
            case CHAR:
                return performerChar(leftC.asChar(), rightP);
            case SHORT:
                return performerShort(leftC.asShort(), rightP);
            default:
            }
        }else if(leftP != null && rightC != null){
            switch (performerType) {
            case BIG_INTEGER:
                return performerBigInteger(leftP, rightC.asBigInteger());
            case INT:
                return performerInt(leftP, rightC.asInt());
            case LONG:
                return performerLong(leftP, rightC.asLong());
            case BOOLEAN:
                return performerBoolean(leftP, rightC.asBoolean());
            case BYTE:
                return performerByte(leftP, rightC.asByte());
            case CHAR:
                return performerChar(leftP, rightC.asChar());
            case SHORT:
                return performerShort(leftP, rightC.asShort());
            default:
            }
        }
        throw new ParseELException(0, "Compile error");
    }
    private static Performer performerShort(Performer leftP, short rightC) {
        return new PerformerShort(){
            Performer left = leftP;
            short right = rightC;
            @Override
            public short performShort(PetalProvider provider) {
                return (short) (left.performShort(provider) | right);
            }
        };
    }
    private static Performer performerChar(Performer leftP, char rightC) {
        return new PerformerChar(){
            Performer left = leftP;
            char right = rightC;
            @Override
            public char performChar(PetalProvider provider) {
                return (char) (left.performChar(provider) | right);
            }
        };
    }
    private static Performer performerByte(Performer leftP, byte rightC) {
        return new PerformerByte(){
            Performer left = leftP;
            byte right = rightC;
            @Override
            public byte performByte(PetalProvider provider) {
                return (byte) (left.performByte(provider) | right);
            }
        };
    }
    private static Performer performerBoolean(Performer leftP, boolean rightC) {
        return new PerformerBoolean(){
            Performer left = leftP;
            boolean right = rightC;
            @Override
            public boolean performBoolean(PetalProvider provider) {
                return left.performBoolean(provider) | right;
            }
        };
    }
    private static Performer performerLong(Performer leftP, long rightC) {
        return new PerformerLong(){
            Performer left = leftP;
            long right = rightC;
            @Override
            public long performLong(PetalProvider provider) {
                return left.performLong(provider) | right;
            }
        };
    }
    private static Performer performerInt(Performer leftP, int rightC) {
        return new PerformerInt(){
            Performer left = leftP;
            int right = rightC;
            @Override
            public int performInt(PetalProvider provider) {
                return left.performInt(provider) | right;
            }
        };
    }
    private static Performer performerBigInteger(Performer leftP, BigInteger rightC) {
        return new PerformerBigInteger(){
            Performer left = leftP;
            BigInteger right = rightC;
            @Override
            public BigInteger performBigInteger(PetalProvider provider) {
                return left.performBigInteger(provider).or(right);
            }
        };
    }
    private static Performer performerShort(short leftC, Performer rightP) {
        return new PerformerShort(){
            short left = leftC;
            Performer right = rightP;
            @Override
            public short performShort(PetalProvider provider) {
                return (short) (left | right.performShort(provider));
            }
        };
    }
    private static Performer performerChar(char leftC, Performer rightP) {
        return new PerformerChar(){
            char left = leftC;
            Performer right = rightP;
            @Override
            public char performChar(PetalProvider provider) {
                return (char) (left | right.performChar(provider));
            }
        };
    }
    private static Performer performerByte(byte leftC, Performer rightP) {
        return new PerformerByte(){
            byte left = leftC;
            Performer right = rightP;
            @Override
            public byte performByte(PetalProvider provider) {
                return (byte) (left | right.performByte(provider));
            }
        };
    }
    private static Performer performerBoolean(boolean leftC, Performer rightP) {
        return new PerformerBoolean(){
            boolean left = leftC;
            Performer right = rightP;
            @Override
            public boolean performBoolean(PetalProvider provider) {
                return left | right.performBoolean(provider);
            }
        };
    }
    private static Performer performerLong(long leftC, Performer rightP) {
        return new PerformerLong(){
            long left = leftC;
            Performer right = rightP;
            @Override
            public long performLong(PetalProvider provider) {
                return left | right.performLong(provider);
            }
        };
    }
    private static Performer performerInt(int leftC, Performer rightP) {
        return new PerformerInt(){
            int left = leftC;
            Performer right = rightP;
            @Override
            public int performInt(PetalProvider provider) {
                return left | right.performInt(provider);
            }
        };
    }
    private static Performer performerBigInteger(BigInteger leftC, Performer rightP) {
        return new PerformerBigInteger(){
            BigInteger left = leftC;
            Performer right = rightP;
            @Override
            public BigInteger performBigInteger(PetalProvider provider) {
                return left.or(right.performBigInteger(provider));
            }
        };
    }
    private static Performer performerShort(Performer leftP, Performer rightP) {
        return new PerformerShort(){
            Performer left = leftP;
            Performer right = rightP;
            @Override
            public short performShort(PetalProvider provider) {
                return (short) (left.performShort(provider) | right.performShort(provider));
            }
        };
    }
    private static Performer performerChar(Performer leftP, Performer rightP) {
        return new PerformerChar(){
            Performer left = leftP;
            Performer right = rightP;
            @Override
            public char performChar(PetalProvider provider) {
                return (char) (left.performChar(provider) | right.performChar(provider));
            }
        };
    }
    private static Performer performerByte(Performer leftP, Performer rightP) {
        return new PerformerByte(){
            Performer left = leftP;
            Performer right = rightP;
            @Override
            public byte performByte(PetalProvider provider) {
                return (byte) (left.performByte(provider) | right.performByte(provider));
            }
        };
    }
    private static Performer performerBoolean(Performer leftP, Performer rightP) {
        return new PerformerBoolean(){
            Performer left = leftP;
            Performer right = rightP;
            @Override
            public boolean performBoolean(PetalProvider provider) {
                return left.performBoolean(provider) | right.performBoolean(provider);
            }
        };
    }
    private static Performer performerLong(Performer leftP, Performer rightP) {
        return new PerformerLong(){
            Performer left = leftP;
            Performer right = rightP;
            @Override
            public long performLong(PetalProvider provider) {
                return left.performLong(provider) | right.performLong(provider);
            }
        };
    }
    private static Performer performerInt(Performer leftP, Performer rightP) {
        return new PerformerInt(){
            Performer left = leftP;
            Performer right = rightP;
            @Override
            public int performInt(PetalProvider provider) {
                return left.performInt(provider) | right.performInt(provider);
            }
        };
    }
    private static Performer performerBigInteger(Performer leftP, Performer rightP) {
        return new PerformerBigInteger(){
            Performer left = leftP;
            Performer right = rightP;
            @Override
            public BigInteger performBigInteger(PetalProvider provider) {
                return left.performBigInteger(provider).or(right.performBigInteger(provider));
            }
        };
    }
    private static Performer performerBoolean(boolean leftC, boolean rightC) {
        return new PerformerBoolean(){
            boolean left = leftC;
            boolean right = rightC;
            @Override
            public boolean performBoolean(PetalProvider provider) {
                return left | right;
            }
        };
    }
    private static Performer performerShort(short leftC, short rightC) {
        return new PerformerShort(){
            short left = leftC;
            short right = rightC;
            @Override
            public short performShort(PetalProvider provider) {
                return (short) (left | right);
            }
        };
    }
    private static Performer performerChar(char leftC, char rightC) {
        return new PerformerChar(){
            char left = leftC;
            char right = rightC;
            @Override
            public char performChar(PetalProvider provider) {
                return (char) (left | right);
            }
        };
    }
    private static Performer performerByte(byte leftC, byte rightC) {
        return new PerformerByte(){
            byte left = leftC;
            byte right = rightC;
            @Override
            public byte performByte(PetalProvider provider) {
                return (byte) (left | right);
            }
        };
    }
    private static Performer performerLong(long leftC, long rightC) {
        return new PerformerLong(){
            long left = leftC;
            long right = rightC;
            @Override
            public long performLong(PetalProvider provider) {
                return left | right;
            }
        };
    }
    private static Performer performerInt(int leftC, int rightC) {
        return new PerformerInt(){
            int left = leftC;
            int right = rightC;
            @Override
            public int performInt(PetalProvider provider) {
                return left | right;
            }
        };
    }
    private static Performer performerBigInteger(BigInteger leftC, BigInteger rightC) {
        return new PerformerBigInteger(){
            BigInteger left = leftC; 
            BigInteger right = rightC;
            @Override
            public BigInteger performBigInteger(PetalProvider provider) {
                return left.or(right);
            }
        };
    }
    @Override
    Const unitConst(Const left, Const right) throws ParseELException {
        switch(adjustType(left.getType(), right.getType())){
            case BIG_INTEGER: return new Const(left.asBigInteger().or(right.asBigInteger()), EnumEL.BIG_INTEGER);
            case BYTE: return new Const(left.asByte() | right.asByte(), EnumEL.BYTE);
            case BOOLEAN: return new Const(left.asBoolean() | right.asBoolean(), EnumEL.BOOLEAN);
            case CHAR: return new Const(left.asChar() | right.asChar(), EnumEL.CHAR);
            case INT: return new Const(left.asInt() | right.asInt(), EnumEL.INT);
            case LONG: return new Const(left.asLong() | right.asLong(), EnumEL.LONG);
            case SHORT: return new Const(left.asShort() | right.asShort(), EnumEL.SHORT);
            default: throw new ParseELException(0, "Method is unsupported");
        }
    }
}
