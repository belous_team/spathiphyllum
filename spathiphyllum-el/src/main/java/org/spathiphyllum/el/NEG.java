/*
 * Copyright (C) 2018-2018 The Project Spathiphyllum Authors.
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
package org.spathiphyllum.el;

import java.math.BigDecimal;
import java.math.BigInteger;

import org.spathiphyllum.el.HelperEL.Const;
import org.spathiphyllum.el.HelperEL.Operator;
import org.spathiphyllum.el.HelperEL.UnaryOperator;
/**
 * Unary minus operator;
 * @author dbelous
 *
 */
public class NEG extends UnaryOperator{
    static final Operator OPERATOR = new NEG();
    private NEG() {
        super("NEG", EnumEL.NUMBER);
    }
    @Override
    int priority() {
        return 12;
    }
    @Override
    Operator adjustOperator(int pos, AbstractComponent left, AbstractComponent right) throws ParseELException {
        if(left.getType().isNumber()){
            return OPERATOR;
        }else{
            throw new ParseELException(pos, "Unary operator '-' can be assigned number only");
        }
    }
    @Override
    EnumEL adjustType(EnumEL left, EnumEL right) throws ParseELException {
        if(left.isNumber()){
            return left == EnumEL.NUMBER? EnumEL.INT : left;
        }else{
            throw new ParseELException(0, "Unary operator '-' can be assigned number only");
        }
    }
    @Override
    Performer compile(EnumEL performerType, Performer leftP, Const leftC, Performer rightP, Const rightC)
            throws ParseELException {
        switch (performerType) {
        case BIG_DECIMAL:
            if(leftC != null){
                return performerBigDecimal(leftC.asBigDecimal());
            }else{
                return performerBigDecimal(leftP);
            }
        case BIG_INTEGER:
            if(leftC != null){
                return performerBigInteger(leftC.asBigInteger());
            }else{
                return performerBigInteger(leftP);
            }
        case BYTE:
            if(leftC != null){
                return performerByte(leftC.asByte());
            }else{
                return performerByte(leftP);
            }
        case CHAR:
            if(leftC != null){
                return performerChar(leftC.asChar());
            }else{
                return performerChar(leftP);
            }
        case DOUBLE:
            if(leftC != null){
                return performerDouble(leftC.asDouble());
            }else{
                return performerDouble(leftP);
            }
        case FLOAT:
            if(leftC != null){
                return performerFloat(leftC.asFloat());
            }else{
                return performerFloat(leftP);
            }
        case INT:
            if(leftC != null){
                return performerInt(leftC.asInt());
            }else{
                return performerInt(leftP);
            }
        case LONG:
            if(leftC != null){
                return performerLong(leftC.asLong());
            }else{
                return performerLong(leftP);
            }
        case SHORT:
            if(leftC != null){
                return performerShort(leftC.asShort());
            }else{
                return performerShort(leftP);
            }
        default:
            throw new ParseELException(0, "Compile error");
        }
    }
    private static Performer performerShort(Performer leftP) {
        return new PerformerShort(){
            Performer left = leftP;
            @Override
            public short performShort(PetalProvider provider) {
                return (short) -left.performShort(provider);
            }
        };
    }
    private static Performer performerShort(short leftC) {
        return new PerformerShort(){
            short left = leftC;
            @Override
            public short performShort(PetalProvider provider) {
                return (short) -left;
            }
        };
    }
    private static Performer performerLong(Performer leftP) {
        return new PerformerLong(){
            Performer left = leftP;
            @Override
            public long performLong(PetalProvider provider) {
                return -left.performLong(provider);
            }
            
        };
    }
    private static Performer performerLong(long leftC) {
        return new PerformerLong(){
            long left = leftC;
            @Override
            public long performLong(PetalProvider provider) {
                return -left;
            }
            
        };
    }
    private static Performer performerInt(Performer leftP) {
        return new PerformerInt(){
            Performer left = leftP;
            @Override
            public int performInt(PetalProvider provider) {
                return -left.performInt(provider);
            }
        };
    }
    private static Performer performerInt(int leftC) {
        return new PerformerInt(){
            int left = leftC;
            @Override
            public int performInt(PetalProvider provider) {
                return -left;
            }
            
        };
    }
    private static Performer performerFloat(Performer leftP) {
        return new PerformerFloat(){
            Performer left = leftP;
            @Override
            public float performFloat(PetalProvider provider) {
                return -left.performFloat(provider);
            }
        };
    }
    private static Performer performerFloat(float leftC) {
        return new PerformerFloat(){
            float left = leftC;
            @Override
            public float performFloat(PetalProvider provider) {
                return -left;
            }
        };
    }
    private static Performer performerDouble(Performer leftP) {
        return new PerformerDouble(){
            Performer left = leftP;
            @Override
            public double performDouble(PetalProvider provider) {
                return -left.performDouble(provider);
            }
        };
    }
    private static Performer performerDouble(double leftC) {
        return new PerformerDouble(){
            double left = leftC;
            @Override
            public double performDouble(PetalProvider provider) {
                return -left;
            }
        };
    }
    private static Performer performerChar(Performer leftP) {
        return new PerformerChar(){
            Performer left = leftP;
            @Override
            public char performChar(PetalProvider provider) {
                return (char) -left.performChar(provider);
            }
        };
    }
    private static Performer performerChar(char leftC) {
        return new PerformerChar(){
            char left = leftC;
            @Override
            public char performChar(PetalProvider provider) {
                return (char) -left;
            }
        };
    }
    private static Performer performerByte(Performer leftP) {
        return new PerformerByte(){
            Performer left = leftP;
            @Override
            public byte performByte(PetalProvider provider) {
                return (byte) -left.performByte(provider);
            }
        };
    }
    private static Performer performerByte(byte leftC) {
        return new PerformerByte(){
            byte left = leftC;
            @Override
            public byte performByte(PetalProvider provider) {
                return (byte) -left;
            }
        };
    }
    private static Performer performerBigInteger(Performer leftP) {
        return new PerformerBigInteger(){
            Performer left = leftP;
            @Override
            public BigInteger performBigInteger(PetalProvider provider) {
                return left.performBigInteger(provider).negate();
            }
        };
    }
    private static Performer performerBigInteger(BigInteger leftC) {
        return new PerformerBigInteger(){
            BigInteger left = leftC;
            @Override
            public BigInteger performBigInteger(PetalProvider provider) {
                return left.negate();
            }
        };
    }
    private static Performer performerBigDecimal(Performer leftP) {
        return new PerformerBigDecimal(){
            Performer left = leftP;
            @Override
            public BigDecimal performBigDecimal(PetalProvider provider) {
                return left.performBigDecimal(provider).negate();
            }
        };
    }
    private static Performer performerBigDecimal(BigDecimal leftC) {
        return new PerformerBigDecimal(){
            BigDecimal left = leftC;
            @Override
            public BigDecimal performBigDecimal(PetalProvider provider) {
                return left.negate();
            }
        };
    }
    @Override
    Const unitConst(Const left) throws ParseELException {
        switch(left.getType()){
            case BIG_DECIMAL: return new Const(left.asBigDecimal().negate(), EnumEL.BIG_DECIMAL);
            case BIG_INTEGER: return new Const(left.asBigInteger().negate(), EnumEL.BIG_INTEGER);
            case BYTE: return new Const(-left.asByte(), EnumEL.BYTE);
            case CHAR: return new Const(-left.asChar(), EnumEL.CHAR);
            case DOUBLE: return new Const(-left.asDouble(), EnumEL.DOUBLE);
            case FLOAT: return new Const(-left.asFloat(), EnumEL.FLOAT);
            case INT: return new Const(-left.asInt(), EnumEL.INT);
            case LONG: return new Const(-left.asLong(), EnumEL.LONG);
            case SHORT: return new Const(-left.asShort(), EnumEL.SHORT);
            default: throw new ParseELException(0, "Method is unsupported");
        }
    }
    
}
