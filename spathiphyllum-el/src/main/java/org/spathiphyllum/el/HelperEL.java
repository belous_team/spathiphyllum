/*
 * Copyright (C) 2018-2018 The Project Spathiphyllum Authors.
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
package org.spathiphyllum.el;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.Deque;

import org.spathiphyllum.el.HelperEL.Const;
import org.spathiphyllum.el.HelperEL.Operator;
import org.spathiphyllum.el.tools.HelperStringParser;
import org.spathiphyllum.el.tools.MarkedLinkedList;
import org.spathiphyllum.el.tools.MarkedLinkedList.Mark;
import org.spathiphyllum.el.tools.MarkedLinkedList.MarkQueueIterator;

/**
 * @author dbelous
 *
 */
public class HelperEL {
    private HelperEL(){}

    private static final char[] TRUE = "true".toCharArray();
    private static final char[] FALSE = "false".toCharArray();
    private static final char[] PETAL = "${".toCharArray();
    private static final char[] MATH = "Math.".toCharArray();
    
    private static boolean optimisationDisabled = Boolean.getBoolean("org.spathiphyllum.el.disableOptimisation");
    
    public static Deque<AbstractComponent> parseEL(String expression, PetalProvider petalProvider, BaseExtendPlugin plugin) throws ParseELException {
        if(expression == null)
            throw new ParseELException(0, "Cannot be null");
        char[] chars = expression.toCharArray();
        MarkedLinkedList<AbstractComponent> stack = new MarkedLinkedList<>();
        readExpression(chars, 0, stack, stack.getRoot(), petalProvider, plugin);
        return stack;
    }
    /**
     * parse expression.<br>
     * expression can begin with
     * <ul>
     * <li> constant
     * <li> unary operator 
     * <li> bracket
     * </ul>
     * and can contain
     * <ul>
     * <li> operand
     * <li> unary operator and operand
     * <li> operand binary operator operand
     * <li> operand ternary operator operand (true) operand (false)
     * <li> bracket that contain expression/operand
     * </ul>
     * @param el
     * @param pos
     * @param stack
     * @param mark 
     * @param petalProvider
     * @param plugin 
     * @return
     * @throws ParseELException
     */
    private static int readExpression(char[] el, int pos, MarkedLinkedList<AbstractComponent> stack, Mark<AbstractComponent> mark, PetalProvider petalProvider, BaseExtendPlugin plugin) throws ParseELException {
        int i = HelperStringParser.skipSpace(el, pos, false, null);
        i = readOperand(el, i, stack, mark, petalProvider, plugin);
        while(i < el.length){
            if(el[i] == ';'){
                i++;
                break;
            }else if(el[i] == ')' || el[i] == ':' || el[i] == ','){
                break;
            }
            i = readBinaryOperator(null, el, i, stack, mark, petalProvider, plugin);
        }
        return i;
    }
    private static int readOperand(char[] el, int pos, MarkedLinkedList<AbstractComponent> stack, Mark<AbstractComponent> mark, PetalProvider petalProvider, BaseExtendPlugin plugin) throws ParseELException {
        int i = HelperStringParser.skipSpace(el, pos, true, "Operand must be present");
        char ch = el[i];
        if(ch >= '0' && ch <= '9'){//constants
            i = readNumber(el, i, stack, mark);
        }else if(ch == '"'){
            i = readString(el, i + 1, stack, mark);
        }else if(ch == '\''){
            i = readString(el, i + 1, stack, mark, true);
        }else if(ch == 't' && HelperStringParser.isNextSymbol(el, i, TRUE)){
            i += TRUE.length;
            stack.push(new Const(true, EnumEL.BOOLEAN), mark);
        }else if(ch == 'f'&& HelperStringParser.isNextSymbol(el, i, FALSE)){
            i += FALSE.length;
            stack.push(new Const(false, EnumEL.BOOLEAN), mark);
        }else if(ch == '$' && HelperStringParser.isNextTheSame(el, i, PETAL)){
            i = readPetal(el, i+PETAL.length, stack, mark, petalProvider);
        }else if(ch == '(') {//brackets
            i = readExpression(el, i + 1, stack, mark, petalProvider, plugin);
            if(el[i] == ')'){
                ++i;
            }else{
                throw new ParseELException(i, "No closing parenthesis");
            }
        }else if(ch == '+' || ch == '-' || ch == '!' || ch == '~'){//unary operators
            i = readOperand(el, i + 1, stack, mark, petalProvider, plugin);
            Operator operator = null;
            if(ch == '+'){
                operator = POS.OPERATOR.adjustOperator(pos, stack.peek(mark), null);
            }else if(ch == '-'){ 
                operator = NEG.OPERATOR.adjustOperator(pos, stack.peek(mark), null);
            }else if(ch == '!') {
                operator = NOT.OPERATOR.adjustOperator(pos, stack.peek(mark), null);
            }else if(ch == '~') {
                operator = NOTN.OPERATOR.adjustOperator(pos, stack.peek(mark), null);
            }else{
                throw new ParseELException(pos, "Cannot recognize operator");
            }
            AbstractComponent comp = stack.peek(mark);
            Const newConst = null;
            if(!optimisationDisabled && comp instanceof Const && ((Const)comp).isConstant() 
                    && (newConst = operator.unitConst((Const) comp)) != null){
                stack.pop(mark);
                stack.push(newConst, mark);
            }else{
                stack.push(operator, mark);
            }
        }else if(plugin != null){
            i = plugin.readOperand(el, i, stack, mark, petalProvider);
        }
        return HelperStringParser.skipSpace(el, i, false, null);
    }
    private static int readBinaryOperator(Operator operator, char[] el, int pos, MarkedLinkedList<AbstractComponent> stack, Mark<AbstractComponent> mark, PetalProvider petalProvider, BaseExtendPlugin plugin) throws ParseELException {
        int i = pos;
        if(i + 1 >= el.length){
            if(operator == null){
                throw new ParseELException(i+1, "Expected operator");
            }else{
                return i;
            }
        }
        Operator current = null;
        int operandPosition = -1;
        if(el[i] == '+' && (operator == null || operator.priority() <= ADD.NUMBER.priority())){
            current = ADD.NUMBER;
            operandPosition = i + 1;
        }else if(el[i] == '-' && (operator == null || operator.priority() <= SUB.OPERATOR.priority())){ 
            current = SUB.OPERATOR;
            operandPosition = i + 1;
        }else if(el[i] == '*' && (operator == null || operator.priority() <= MUL.NUMBER.priority())) {
            current = MUL.NUMBER;
            operandPosition = i + 1;
        }else if(el[i] == '/' && (operator == null || operator.priority() <= DIV.OPERATOR.priority())) {
            current = DIV.OPERATOR;
            operandPosition = i + 1;
        }else if(el[i] == '%' && (operator == null || operator.priority() <= MOD.OPERATOR.priority())) {
            current = MOD.OPERATOR;
            operandPosition = i + 1;
        }else if(el[i] == '^' && (operator == null || operator.priority() <= XOR.BOOLEAN.priority())) {
            current = XOR.BOOLEAN;
            operandPosition = i + 1;
        }else if(el[i] == '&') {
            if(el[i+1] == '&' && (operator == null || operator.priority() <= CAND.OPERATOR.priority())){
                current = CAND.OPERATOR;
                operandPosition = i + 2;
            }else if(el[i+1] != '&' && (operator == null || operator.priority() <= AND.NUMBER.priority())){
                current = AND.NUMBER;
                operandPosition = i + 1;
            }
        }else if(el[i] == '|') {
            if(el[i+1] == '|' && (operator == null || operator.priority() <= COR.OPERATOR.priority())){
                current = COR.OPERATOR;
                operandPosition = i + 2;
            }else if(el[i+1] != '|' && (operator == null || operator.priority() <= OR.BOOLEAN.priority())){
                current = OR.BOOLEAN;
                operandPosition = i + 1;
            }
        }else if(el[i] == '?' && (operator == null)) {
            current = Ternary.OPERATOR;
            operandPosition = i + 1;
        }else if(el[i] == '!' && el[i+1] == '='  && (operator == null || operator.priority() <= NEQ.OPERATOR.priority())) {
            current = NEQ.OPERATOR;
            operandPosition = i + 2;
        }else if(el[i] == '=' && el[i+1] == '='  && (operator == null || operator.priority() <= EQ.OPERATOR.priority())) {
            current = EQ.OPERATOR;
            operandPosition = i + 2;
        }else if(el[i] == '>') {
            if(el[i+1] == '=' && (operator == null || operator.priority() <= GEQ.OPERATOR.priority())){
                current = GEQ.OPERATOR;
                operandPosition = i + 2;
            }else if(el[i+1] == '>' && (operator == null || operator.priority() <= SHR.OPERATOR.priority())){
                if(i + 3 < el.length && el[i+2] == '>'){
                    current = SHRU.OPERATOR;
                    operandPosition = i + 3;
                }else{
                    current = SHR.OPERATOR;
                    operandPosition = i + 2;
                }
            }else if(el[i+1] != '=' && (operator == null || operator.priority() <= GREATER.OPERATOR.priority())){
                current = GREATER.OPERATOR;
                operandPosition = i + 1;
            }
        }else if(el[i] == '<') {
            if(el[i+1] == '=' && (operator == null || operator.priority() <= LEQ.OPERATOR.priority())){
                current = LEQ.OPERATOR;
                operandPosition = i + 2;
            }else if(el[i+1] == '<' && (operator == null || operator.priority() <= SHL.OPERATOR.priority())){
                current = SHL.OPERATOR;
                operandPosition = i + 2;
            }else if(el[i+1] != '<' && (operator == null || operator.priority() <= LESS.OPERATOR.priority())){
                current = LESS.OPERATOR;
                operandPosition = i + 1;
            }
        }
        if(operator == null && current == null){
            throw new ParseELException(pos, "Expected operator:'*' '/' '%' '+' '-' '<<' '>>' '>>>' '<' '>' '<=' '>=' '==' '!=' '&' '^' '|' '&&' '||' '?'");
        }
        if(current != null){
            if(current == Ternary.OPERATOR){
                Mark<AbstractComponent> newMark = stack.addMark(mark);
                i = readExpression(el, operandPosition, stack, newMark, petalProvider, plugin);
                if(el[i] == ':'){
                    ++i;
                }else{
                    throw new ParseELException(i, "No closing parenthesis");
                }
                stack.push(TerTrue.OPERATOR, newMark);
                stack.removeMark(newMark);
                newMark = stack.addMark(mark);
                i = readExpression(el, i, stack, newMark, petalProvider, plugin);
                stack.push(TerFalse.OPERATOR, newMark);
                stack.push(Ternary.OPERATOR, mark);
                stack.removeMark(newMark);
            }else{
                AbstractComponent leftO = stack.peek(mark);
                Mark<AbstractComponent> newMark = stack.addMark(mark);
                i = readOperand(el, operandPosition, stack, newMark, petalProvider, plugin);
                i = readBinaryOperator(current, el, i, stack, newMark, petalProvider, plugin);
                stack.push(current.adjustOperator(pos, leftO, stack.peek(newMark)), mark);
                MarkQueueIterator<AbstractComponent> it = (MarkQueueIterator<AbstractComponent>) stack.iterator(newMark, mark);
                int isMarked = 0;
                while (it.hasNext()) {
                    AbstractComponent comp = it.next();
                    if(!(comp instanceof Operator && comp.priority() == current.priority())) {
                        break;
                    }
                    isMarked++;
                }
                if(isMarked > 0) {
                    Mark<AbstractComponent> tempMark = it.mark();
                    stack.moveAfter(mark, newMark, tempMark);
                    stack.removeMark(tempMark);
                }
                stack.removeMark(newMark);
            }
        }
        return HelperStringParser.skipSpace(el, i, false, null);
    }
    //============== read Constant
    /**
     * allowed only 0-9A-Za-z_
     * on start 'i' in position of first symbol
     * on exit 'i' in position after ']'
     * @param stack 
     * @param mark 
     * @param petalProvider 
     * @return
     * @throws ParseELException 
     */
    private static int readPetal(char[] el, int i, MarkedLinkedList<AbstractComponent> stack, Mark<AbstractComponent> mark, PetalProvider petalProvider) throws ParseELException {
        i = HelperStringParser.skipSpace(el, i, true, "expecting ${some_petal}");
        StringBuilder sb = new StringBuilder();
        for(; i < el.length; i++){
            if((el[i] >= '0' && el[i] <= '9')
                    || el[i] == '_'
                    ||(el[i] >= 'A' && el[i] <= 'Z')
                    ||(el[i] >= 'a' && el[i] <= 'z')){
                //valid symbol
                sb.append(el[i]);
            }else if(el[i] == '}'){
                break;
            }else {
                throw new ParseELException(i, "Petal name must contain only 0-9A-Za-z_");
            }
        }
        if(sb.length() == 0){
            throw new ParseELException(i, "Petal name must not be empty");
        }
        stack.push(new PetalConst(sb.toString(), petalProvider), mark);
        return i + 1;
    }
    /**
     * read string as constant.
     * on start 'i' on position after '"'
     * on exit 'i' on position next after '"'
     * @param el
     * @param i
     * @param stack 
     * @param mark 
     * @return
     * @throws ParseELException 
     */
    private static int readString(char[] el, int i, MarkedLinkedList<AbstractComponent> stack, Mark<AbstractComponent> mark) throws ParseELException {
        return readString(el, i, stack, mark, false);
    }
    /**
     * read string as constant.
     * if isSingleQuotes is true, string reading exits on position after '\''
     * @param el
     * @param i
     * @param stack
     * @param mark
     * @param isSingleQuotes
     * @return
     * @throws ParseELException
     */
    private static int readString(char[] el, int i, MarkedLinkedList<AbstractComponent> stack, Mark<AbstractComponent> mark, boolean isSingleQuotes) throws ParseELException {
        StringBuilder sb = new StringBuilder();
        i = HelperStringParser.readNextString(el, i, sb, true, isSingleQuotes);
        if(isSingleQuotes && sb.length() == 1) {
            stack.push(new Const(sb.charAt(0), EnumEL.CHAR), mark);
        }else {
            stack.push(new Const(sb.toString(), EnumEL.STRING), mark);
        }
        return i;
    }
    /**
     * read number as integer as constant
     * on start 'i' in position first number 0-9
     * on exit 'i' in position after number  
     * @param el
     * @param i
     * @param stack 
     * @param mark 
     * @return
     * @throws ParseELException 
     */
    private static int readNumber(char[] el, int i, MarkedLinkedList<AbstractComponent> stack, Mark<AbstractComponent> mark) throws ParseELException {
        StringBuilder sb = new StringBuilder();
        i = HelperStringParser.readNextNumber(el, i, sb);
        Number number = HelperStringParser.parseNumber(sb.toString());
        stack.push(new Const(number, HelperStringParser.defineNumberType(number)), mark);
        return i;
    }
    
    static EnumEL approachNumerTypes(EnumEL left, EnumEL right) throws ParseELException{
        if(!(left.isNumber() && right.isNumber())){
            throw new ParseELException(0, "Only number can approach");
        }
        int max = Math.max(Math.max(left.ordinal(), right.ordinal()), EnumEL.INT.ordinal());
        return EnumEL.values()[max]; 
    }
    
    static class Const extends AbstractComponent {
        @Override
        public String toString() {
            return "Const [value=" + getValue() + "]";
        }
        
        Const(Object value, EnumEL type){
            super(value, type);
        }
        boolean isConstant(){
            return true;
        }

        @Override
        int priority() {
            return 100;
        }
        /**
         * get petal's value
         */
        BigDecimal asBigDecimal() throws ParseELException{
            if(getType().isNumber()){
                return HelperStringParser.numberAsBigDecimal((Number) getValue());
            }
            throw new ParseELException(0, "Expected BigDecimal but: "+this);
        }
        BigInteger asBigInteger() throws ParseELException{
            if(getType().isNumber()){
                return HelperStringParser.numberAsBigInteger((Number) getValue());
            }
            throw new ParseELException(0, "Expected BigInteger but: "+this);
        }
        boolean asBoolean() throws ParseELException{
            if(getType().isBoolean()){
                return (boolean) getValue();
            }
            throw new ParseELException(0, "Expected boolean but: "+this);
        }
        String asString(){
            if(getType().isBoolean()){
                return Boolean.toString((boolean) getValue());
            }else if(getType().isNumber()){
                return HelperStringParser.numberAsString((Number) getValue());
            }else{
                return (String) getValue();
            }
        }
        byte asByte() throws ParseELException{
            if(getType().isNumber()){
                return HelperStringParser.numberAsByte((Number) getValue());
            }
            throw new ParseELException(0, "Expected byte but: "+this);
        }
        short asShort() throws ParseELException{
            if(getType().isNumber()){
                return HelperStringParser.numberAsShort((Number) getValue());
            }
            throw new ParseELException(0, "Expected short but: "+this);
        }
        char asChar() throws ParseELException{
            if(getType().isNumber()){
                //TODO check, Character cannot be cast to Number
                //return (char) HelperStringParser.numberAsInt((Number) getValue());
                return (char) getValue();
            }
            throw new ParseELException(0, "Expected char but: "+this);
        }
        int asInt() throws ParseELException{
            if(getType().isNumber()){
                return HelperStringParser.numberAsInt((Number) getValue());
            }
            throw new ParseELException(0, "Expected int but: "+this);
        }
        long asLong() throws ParseELException{
            if(getType().isNumber()){
                return HelperStringParser.numberAsLong((Number) getValue());
            }
            throw new ParseELException(0, "Expected long but: "+this);
        }
        float asFloat() throws ParseELException{
            if(getType().isNumber()){
                return HelperStringParser.numberAsFloat((Number) getValue());
            }
            throw new ParseELException(0, "Expected float but: "+this);
        }
        double asDouble() throws ParseELException{
            if(getType().isNumber()){
                return HelperStringParser.numberAsDouble((Number) getValue());
            }
            throw new ParseELException(0, "Expected double but "+this);
        }
        Performer asPerformer() throws ParseELException{
            EnumEL thisType = getType();
            switch (thisType) {
            case BIG_DECIMAL:
                return new PerformerConstBigDecimal(asBigDecimal());
            case BIG_INTEGER:
                return new PerformerConstBigInteger(asBigInteger());
            case BOOLEAN:
                return new PerformerConstBoolean(asBoolean());
            case BYTE:
                return new PerformerConstByte(asByte());
            case CHAR:
                return new PerformerConstChar(asChar());
            case DOUBLE:
                return new PerformerConstDouble(asDouble());
            case FLOAT:
                return new PerformerConstFloat(asFloat());
            case NUMBER:
                //TODO logging this occurrence
            case INT:
                return new PerformerConstInt(asInt());
            case LONG:
                return new PerformerConstLong(asLong());
            case SHORT:
                return new PerformerConstShort(asShort());
            case STRING:
                return new PerformerConstString(asString());
            default:
                throw new ParseELException(0, "Incorrect type");
            }
        }
    }
    static class PetalConst extends Const {
        PetalProvider petalProvider;
        PetalConst(String petalName, PetalProvider petalProvider){
            super(petalName, EnumEL.SOP);
            this.petalProvider = petalProvider;
        }
        @Override
        boolean isConstant(){
            return petalProvider.isConstant((String) getValue());
        }
        @Override
        public EnumEL getType() {
            return petalProvider.getType((String) getValue());
        }
        @Override
        int priority() {
            return 0;
        }
        @Override
        Performer asPerformer() throws ParseELException{
            EnumEL thisType = getType();
            switch (thisType) {
            case BIG_DECIMAL:
                return new PerformerPetalBigDecimal((String) getValue());
            case BIG_INTEGER:
                return new PerformerPetalBigInteger((String) getValue());
            case BOOLEAN:
                return new PerformerPetalBoolean((String) getValue());
            case BYTE:
                return new PerformerPetalByte((String) getValue());
            case CHAR:
                return new PerformerPetalChar((String) getValue());
            case DOUBLE:
                return new PerformerPetalDouble((String) getValue());
            case FLOAT:
                return new PerformerPetalFloat((String) getValue());
            case NUMBER:
                //TODO logging this occurrence
            case INT:
                return new PerformerPetalInt((String) getValue());
            case LONG:
                return new PerformerPetalLong((String) getValue());
            case SHORT:
                return new PerformerPetalShort((String) getValue());
            case STRING:
                return new PerformerPetalString((String) getValue());
            default:
                throw new ParseELException(0, "Incorrect type");
            }
        }
        @Override
        BigDecimal asBigDecimal() throws ParseELException {
            return petalProvider.asBigDecimal((String) getValue());
        }
        @Override
        BigInteger asBigInteger() throws ParseELException {
            return petalProvider.asBigInteger((String) getValue());
        }
        @Override
        boolean asBoolean() throws ParseELException {
            return petalProvider.asBoolean((String) getValue());
        }
        @Override
        String asString() {
            return petalProvider.asString((String) getValue());
        }
        @Override
        byte asByte() throws ParseELException {
            return petalProvider.asByte((String) getValue());
        }
        @Override
        short asShort() throws ParseELException {
            return petalProvider.asShort((String) getValue());
        }
        @Override
        char asChar() throws ParseELException {
            return petalProvider.asChar((String) getValue());
        }
        @Override
        int asInt() throws ParseELException {
            return petalProvider.asInt((String) getValue());
        }
        @Override
        long asLong() throws ParseELException {
            return petalProvider.asLong((String) getValue());
        }
        @Override
        float asFloat() throws ParseELException {
            return petalProvider.asFloat((String) getValue());
        }
        @Override
        double asDouble() throws ParseELException {
            return petalProvider.asDouble((String) getValue());
        }
    }
    abstract static class Operator extends AbstractComponent {
        @Override
        public String toString() {
            return "Operator [value=" + getValue() + "]";
        }

        Operator(String value, EnumEL type){
            super(value, type);
        }
        boolean isOptimizable() {
            return true;
        }
        abstract EnumEL adjustType(EnumEL left, EnumEL right) throws ParseELException;
        abstract Operator adjustOperator(int pos, AbstractComponent left, AbstractComponent right) throws ParseELException;
        
        Const unitConst(Const left, Const right) throws ParseELException{
            return null;
        }
        Const unitConst(Const left) throws ParseELException{
            return null;
        }
        Performer compile(Deque<AbstractComponent> list) throws ParseELException{
            Performer leftP = null;
            Performer rightP = null;
            Const leftC = null;
            Const rightC = null;
            AbstractComponent leftA = list.pop();
            if(leftA instanceof Operator){
                leftP = ((Operator) leftA).compile(list);
            }else if(leftA instanceof PetalConst){
                leftP = ((PetalConst) leftA).asPerformer();
            }else{
                leftC = (Const) leftA;
            }
            AbstractComponent rightA = list.pop();
            if(rightA instanceof Operator){
                rightP = ((Operator) rightA).compile(list);
            }else if(rightA instanceof PetalConst){
                rightP = ((PetalConst) rightA).asPerformer();
            }else{
                rightC = (Const) rightA;
            }
            EnumEL performerType = adjustType(leftP != null?leftP.type():leftC.getType(), 
                    rightP != null? rightP.type(): rightA.getType());
            return compile(performerType, leftP, leftC, rightP, rightC);
        }
        abstract Performer compile(EnumEL performerType, Performer leftP, Const leftC, Performer rightP, Const rightC) throws ParseELException;
    }
    abstract static class UnaryOperator extends Operator {
        UnaryOperator(String value, EnumEL type){
            super(value, type);
        }
        Performer compile(MarkedLinkedList<AbstractComponent> list) throws ParseELException{
            Performer leftP = null;
            Const leftC = null;
            AbstractComponent leftA = list.pop();
            if(leftA instanceof Operator){
                leftP = ((Operator) leftA).compile(list);
            }else if(leftA instanceof PetalConst){
                leftP = ((PetalConst) leftA).asPerformer();
            }else{
                leftC = (Const) leftA;
            }
            if(leftP == null && leftC == null) {
                throw new InvokeELException("Error in extracting unary operand");
            }
            EnumEL performerType = adjustType(leftP != null?leftP.type():leftC.getType(), null);
            return compile(performerType, leftP, leftC, null, null);
        }
    }
    public abstract static class BaseExtendPlugin {
        public abstract int readOperand(char[] el, int pos, MarkedLinkedList<AbstractComponent> stack, Mark<AbstractComponent> mark, PetalProvider petalProvider) throws ParseELException;
        protected static int readExpression(char[] el, int pos, MarkedLinkedList<AbstractComponent> stack, Mark<AbstractComponent> mark, PetalProvider petalProvider, BaseExtendPlugin plugin) throws ParseELException{
            return HelperEL.readExpression(el, pos, stack, mark, petalProvider, plugin);
        }
    }
    public abstract static class BaseOperatorForPlugin extends Operator {
        
        protected abstract int getParameterCount();
        
        @Override
        Performer compile(Deque<AbstractComponent> list) throws ParseELException {
            Performer[] parameters = new Performer[getParameterCount()];
            for(int i = 0; i < parameters.length; i++){
                AbstractComponent leftA = list.pop();
                if(leftA instanceof Operator){
                    parameters[i] = ((Operator) leftA).compile(list);
                }else{
                    parameters[i] = ((Const) leftA).asPerformer();
                }
            }
            return compile(parameters);
        }
        
        /**
         * @param parameters
         * @return
         * @throws ParseELException 
         */
        protected abstract Performer compile(Performer[] parameters) throws ParseELException;

        boolean isOptimizable() {
            return false;
        }
        
        @Override
        EnumEL adjustType(EnumEL left, EnumEL right) throws ParseELException {
            return getType();
        }

        @Override
        Operator adjustOperator(int pos, AbstractComponent left, AbstractComponent right) throws ParseELException {
            return this;
        }
        
        
        @Override
        Performer compile(EnumEL performerType, Performer leftP, Const leftC, Performer rightP, Const rightC)
                throws ParseELException {
            return null;
        }


        @Override
        public int priority() {
            return 100;
        }

        /**
         * @param value
         * @param type
         */
        public BaseOperatorForPlugin(String value, EnumEL type) {
            super(value, type);
        }
        
    }
}
