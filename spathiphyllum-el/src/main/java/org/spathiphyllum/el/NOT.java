/*
 * Copyright (C) 2018-2018 The Project Spathiphyllum Authors.
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
package org.spathiphyllum.el;

import org.spathiphyllum.el.HelperEL.Const;
import org.spathiphyllum.el.HelperEL.Operator;
import org.spathiphyllum.el.HelperEL.UnaryOperator;
/**
 * Inverts the value of a boolean (!)
 * @author dbelous
 *
 */
public class NOT extends UnaryOperator{
    static final NOT OPERATOR = new NOT();
    private NOT() {
        super("NOT", EnumEL.BOOLEAN);
    }
    @Override
    int priority() {
        return 0;
    }
    @Override
    EnumEL adjustType(EnumEL left, EnumEL right) throws ParseELException {
        if(left.isBoolean()){
            return EnumEL.BOOLEAN;
        }else{
            throw new ParseELException(0, "Operator '!' can be assigned boolean only");
        }
    }
    @Override
    Operator adjustOperator(int pos, AbstractComponent left, AbstractComponent right) throws ParseELException {
        if(left.getType().isBoolean()){
            return OPERATOR;
        }else{
            throw new ParseELException(pos, "Operator '!' can be assigned boolean only");
        }
    }
    @Override
    Performer compile(EnumEL performerType, Performer leftP, Const leftC, Performer rightP, Const rightC)
            throws ParseELException {
        if(performerType.isBoolean()){
            if(leftP != null){
                return performerBoolean(leftP);
            }else{
                return performerBoolean(leftC.asBoolean());
            }
        }
        throw new ParseELException(0, "Compile error");
    }
    private static Performer performerBoolean(boolean asBoolean) {
        return new PerformerBoolean(){
            boolean left = asBoolean;
            @Override
            public boolean performBoolean(PetalProvider provider) {
                return left;
            }
        };
    }
    private static Performer performerBoolean(Performer leftP) {
        return new PerformerBoolean(){
            Performer left = leftP;
            @Override
            public boolean performBoolean(PetalProvider provider) {
                return left.performBoolean(provider);
            }
        };
    }
    @Override
    Const unitConst(Const left) throws ParseELException {
        if(left.getType() == EnumEL.BOOLEAN){
            return new Const(!left.asBoolean(), EnumEL.BOOLEAN);
        }else{
            throw new ParseELException(0, "Method is unsupported");
        }
    }
}
