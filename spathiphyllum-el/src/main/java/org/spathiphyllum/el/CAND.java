/*
 * Copyright (C) 2018-2018 The Project Spathiphyllum Authors.
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
package org.spathiphyllum.el;

import org.spathiphyllum.el.HelperEL.Const;
import org.spathiphyllum.el.HelperEL.Operator;
/**
 * Conditional-AND
 * @author dbelous
 *
 */
class CAND extends Operator{
    static final Operator OPERATOR = new CAND();
    private CAND() {
        super("CAND", EnumEL.BOOLEAN);
    }
    @Override
    int priority() {
        return 3;
    }
    @Override
    EnumEL adjustType(EnumEL left, EnumEL right) throws ParseELException {
        if(!left.isBoolean() || !right.isBoolean()){
            throw new ParseELException(0, "Operator '&&' can be assigned couple boolean only");
        }
        return EnumEL.BOOLEAN;
    }
    @Override
    Operator adjustOperator(int pos, AbstractComponent left, AbstractComponent right) throws ParseELException {
        if(!left.getType().isBoolean() || !right.getType().isBoolean()){
            throw new ParseELException(pos, "Operator '&&' can be assigned couple boolean only");
        }
        return OPERATOR;
    }
    @Override
    Performer compile(EnumEL performerType, Performer leftP, Const leftC, Performer rightP, Const rightC)
            throws ParseELException {
        if(leftC != null && rightC != null){
            return performerBoolean(leftC.asBoolean(), rightC.asBoolean());
        }else if(leftP != null && rightP != null){
            return performerBoolean(leftP, rightP);
        }else if(leftC != null && rightP != null){
            return performerBoolean(leftC.asBoolean(), rightP);
        }else if(leftP != null && rightC != null){
            return performerBoolean(leftP, rightC.asBoolean());
        }
        throw new ParseELException(0, "Compile error");
    }
    private static Performer performerBoolean(Performer leftP, boolean rightC) {
        return new PerformerBoolean(){
            Performer left = leftP;
            boolean right = rightC;
            @Override
            public boolean performBoolean(PetalProvider provider) {
                return left.performBoolean(provider) && right;
            }
        };
    }
    private static Performer performerBoolean(boolean leftC, Performer rightP) {
        return new PerformerBoolean(){
            boolean left = leftC;
            Performer right = rightP;
            @Override
            public boolean performBoolean(PetalProvider provider) {
                return left && right.performBoolean(provider);
            }
        };
    }
    private static Performer performerBoolean(Performer leftP, Performer rightP) {
        return new PerformerBoolean(){
            Performer left = leftP;
            Performer right = rightP;
            @Override
            public boolean performBoolean(PetalProvider provider) {
                return left.performBoolean(provider) && right.performBoolean(provider);
            }
        };
    }
    private static Performer performerBoolean(boolean leftC, boolean rightC) {
        return new PerformerBoolean(){
            boolean left = leftC;
            boolean right = rightC;
            @Override
            public boolean performBoolean(PetalProvider provider) {
                return left && right;
            }
        };
    }
    @Override
    Const unitConst(Const left, Const right) throws ParseELException {
        if(adjustType(left.getType(), right.getType()) == EnumEL.BOOLEAN){
            return new Const(left.asBoolean() && right.asBoolean(), EnumEL.BOOLEAN);
        }else{
            throw new ParseELException(0, "Method is unsupported");
        }
    }
}
