/*
 * Copyright (C) 2018-2018 The Project Spathiphyllum Authors.
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
package org.spathiphyllum.el;

import java.math.BigDecimal;
import java.math.BigInteger;

import org.spathiphyllum.el.HelperEL.Const;
import org.spathiphyllum.el.HelperEL.Operator;
import org.spathiphyllum.el.HelperEL.PetalConst;
import org.spathiphyllum.el.HelperEL.UnaryOperator;
import org.spathiphyllum.el.tools.MarkedLinkedList;
/**
 * Ternary false's part
 * @author obelous
 *
 */
public class Ternary extends UnaryOperator {
    static final Operator OPERATOR = new Ternary();
    private Ternary(){
        super("TERNARY", EnumEL.SOP);
    }
    @Override
    int priority() {
        return 1;
    }
    @Override
    EnumEL adjustType(EnumEL left, EnumEL right) throws ParseELException {
        throw new ParseELException(0, "Incorrect algoritm");
    }
    @Override
    Operator adjustOperator(int pos, AbstractComponent left, AbstractComponent right) throws ParseELException {
        throw new ParseELException(0, "Incorrect algoritm");
    }
    @Override
    Performer compile(EnumEL performerType, Performer leftP, Const leftC, Performer rightP, Const rightC)
            throws ParseELException {
        throw new ParseELException(0, "Incorrect algoritm");
    }
    @Override
    Performer compile(MarkedLinkedList<AbstractComponent> list) throws ParseELException {
        Performer conditionP = null;
        Const conditionC = null;
        Performer trueP = null;
        Const trueC = null;
        Performer falseP = null;
        Const falseC = null;
        AbstractComponent leftA = list.pop();
        if(leftA instanceof Operator){
            conditionP = ((Operator) leftA).compile(list);
        }else if(leftA instanceof PetalConst){
            conditionP = ((PetalConst) leftA).asPerformer();
        }else{
            conditionC = (Const) leftA;
        }
        if(conditionP != null && !conditionP.type().isBoolean()){
            throw new ParseELException(0, "Ternary operator excepcted boolean condition but found: "+conditionP.type());
        }
        if(conditionC != null && !conditionC.getType().isBoolean()){
            throw new ParseELException(0, "Ternary operator excepcted boolean condition but found: "+conditionC.getType());
        }
        leftA = list.pop();
        if(leftA instanceof Operator){
            trueP = ((Operator) leftA).compile(list);
        }else if(leftA instanceof PetalConst){
            trueP = ((PetalConst) leftA).asPerformer();
        }else{
            trueC = (Const) leftA;
        }
        if(trueP == null && trueC == null) {
            throw new ParseELException(0, "Error in extracting true expression");
        }
        leftA = list.pop();
        if(leftA instanceof Operator){
            falseP = ((Operator) leftA).compile(list);
        }else if(leftA instanceof PetalConst){
            falseP = ((PetalConst) leftA).asPerformer();
        }else{
            falseC = (Const) leftA;
        }
        if(falseP == null && falseC == null) {
            throw new ParseELException(0, "Error in extracting false expression");
        }
        EnumEL performerType = trueP != null?trueP.type():trueC.getType();
        return compileTernary(performerType, conditionP, conditionC, trueP, trueC, falseP, falseC);
    }
    private static Performer compileTernary(EnumEL performerType, Performer condP, Const condC, Performer trueP,
            Const trueC, Performer falseP, Const falseC) throws ParseELException {
        switch (performerType) {
        case BIG_DECIMAL:
            if(condC != null && trueC != null && falseC != null){
                return new PerformerBigDecimal() {
                    boolean cond = condC.asBoolean();
                    BigDecimal t = trueC.asBigDecimal();
                    BigDecimal f = falseC.asBigDecimal();
                    @Override
                    public BigDecimal performBigDecimal(PetalProvider provider) {
                        return cond? t : f;
                    }
                };
            }else if(condC != null && trueC != null && falseP != null){
                return new PerformerBigDecimal() {
                    boolean cond = condC.asBoolean();
                    BigDecimal t = trueC.asBigDecimal();
                    Performer f = falseP;
                    @Override
                    public BigDecimal performBigDecimal(PetalProvider provider) {
                        return cond? t : f.performBigDecimal(provider);
                    }
                };
            }else if(condC != null && trueP != null && falseC != null){
                return new PerformerBigDecimal() {
                    boolean cond = condC.asBoolean();
                    Performer t = trueP;
                    BigDecimal f = falseC.asBigDecimal();
                    @Override
                    public BigDecimal performBigDecimal(PetalProvider provider) {
                        return cond? t.performBigDecimal(provider) : f;
                    }
                };
            }else if(condC != null && trueP != null && falseP != null){
                return new PerformerBigDecimal() {
                    boolean cond = condC.asBoolean();
                    Performer t = trueP;
                    Performer f = falseP;
                    @Override
                    public BigDecimal performBigDecimal(PetalProvider provider) {
                        return cond? t.performBigDecimal(provider) : f.performBigDecimal(provider);
                    }
                };
            }else if(condP != null && trueC != null && falseC != null){
                return new PerformerBigDecimal() {
                    Performer cond = condP;
                    BigDecimal t = trueC.asBigDecimal();
                    BigDecimal f = falseC.asBigDecimal();
                    @Override
                    public BigDecimal performBigDecimal(PetalProvider provider) {
                        return cond.performBoolean(provider)? t : f;
                    }
                };
            }else if(condP != null && trueC != null && falseP != null){
                return new PerformerBigDecimal() {
                    Performer cond = condP;
                    BigDecimal t = trueC.asBigDecimal();
                    Performer f = falseP;
                    @Override
                    public BigDecimal performBigDecimal(PetalProvider provider) {
                        return cond.performBoolean(provider)? t : f.performBigDecimal(provider);
                    }
                };
            }else if(condP != null && trueP != null && falseC != null){
                return new PerformerBigDecimal() {
                    Performer cond = condP;
                    Performer t = trueP;
                    BigDecimal f = falseC.asBigDecimal();
                    @Override
                    public BigDecimal performBigDecimal(PetalProvider provider) {
                        return cond.performBoolean(provider)? t.performBigDecimal(provider) : f;
                    }
                };
            }else{ //condP != null && trueP != null && falseP != null
                return new PerformerBigDecimal() {
                    Performer cond = condP;
                    Performer t = trueP;
                    Performer f = falseP;
                    @Override
                    public BigDecimal performBigDecimal(PetalProvider provider) {
                        return cond.performBoolean(provider)? t.performBigDecimal(provider) : f.performBigDecimal(provider);
                    }
                };
            }
        case BIG_INTEGER:
            if(condC != null && trueC != null && falseC != null){
                return new PerformerBigInteger() {
                    boolean cond = condC.asBoolean();
                    BigInteger t = trueC.asBigInteger();
                    BigInteger f = falseC.asBigInteger();
                    @Override
                    public BigInteger performBigInteger(PetalProvider provider) {
                        return cond? t : f;
                    }
                };
            }else if(condC != null && trueC != null && falseP != null){
                return new PerformerBigInteger() {
                    boolean cond = condC.asBoolean();
                    BigInteger t = trueC.asBigInteger();
                    Performer f = falseP;
                    @Override
                    public BigInteger performBigInteger(PetalProvider provider) {
                        return cond? t : f.performBigInteger(provider);
                    }
                };
            }else if(condC != null && trueP != null && falseC != null){
                return new PerformerBigInteger() {
                    boolean cond = condC.asBoolean();
                    Performer t = trueP;
                    BigInteger f = falseC.asBigInteger();
                    @Override
                    public BigInteger performBigInteger(PetalProvider provider) {
                        return cond? t.performBigInteger(provider) : f;
                    }
                };
            }else if(condC != null && trueP != null && falseP != null){
                return new PerformerBigInteger() {
                    boolean cond = condC.asBoolean();
                    Performer t = trueP;
                    Performer f = falseP;
                    @Override
                    public BigInteger performBigInteger(PetalProvider provider) {
                        return cond? t.performBigInteger(provider) : f.performBigInteger(provider);
                    }
                };
            }else if(condP != null && trueC != null && falseC != null){
                return new PerformerBigInteger() {
                    Performer cond = condP;
                    BigInteger t = trueC.asBigInteger();
                    BigInteger f = falseC.asBigInteger();
                    @Override
                    public BigInteger performBigInteger(PetalProvider provider) {
                        return cond.performBoolean(provider)? t : f;
                    }
                };
            }else if(condP != null && trueC != null && falseP != null){
                return new PerformerBigInteger() {
                    Performer cond = condP;
                    BigInteger t = trueC.asBigInteger();
                    Performer f = falseP;
                    @Override
                    public BigInteger performBigInteger(PetalProvider provider) {
                        return cond.performBoolean(provider)? t : f.performBigInteger(provider);
                    }
                };
            }else if(condP != null && trueP != null && falseC != null){
                return new PerformerBigInteger() {
                    Performer cond = condP;
                    Performer t = trueP;
                    BigInteger f = falseC.asBigInteger();
                    @Override
                    public BigInteger performBigInteger(PetalProvider provider) {
                        return cond.performBoolean(provider)? t.performBigInteger(provider) : f;
                    }
                };
            }else{ //condP != null && trueP != null && falseP != null
                return new PerformerBigInteger() {
                    Performer cond = condP;
                    Performer t = trueP;
                    Performer f = falseP;
                    @Override
                    public BigInteger performBigInteger(PetalProvider provider) {
                        return cond.performBoolean(provider)? t.performBigInteger(provider) : f.performBigInteger(provider);
                    }
                };
            }
        case BOOLEAN:
            if(condC != null && trueC != null && falseC != null){
                return new PerformerBoolean() {
                    boolean cond = condC.asBoolean();
                    boolean t = trueC.asBoolean();
                    boolean f = falseC.asBoolean();
                    @Override
                    public boolean performBoolean(PetalProvider provider) {
                        return cond? t : f;
                    }
                };
            }else if(condC != null && trueC != null && falseP != null){
                return new PerformerBoolean() {
                    boolean cond = condC.asBoolean();
                    boolean t = trueC.asBoolean();
                    Performer f = falseP;
                    @Override
                    public boolean performBoolean(PetalProvider provider) {
                        return cond? t : f.performBoolean(provider);
                    }
                };
            }else if(condC != null && trueP != null && falseC != null){
                return new PerformerBoolean() {
                    boolean cond = condC.asBoolean();
                    Performer t = trueP;
                    boolean f = falseC.asBoolean();
                    @Override
                    public boolean performBoolean(PetalProvider provider) {
                        return cond? t.performBoolean(provider) : f;
                    }
                };
            }else if(condC != null && trueP != null && falseP != null){
                return new PerformerBoolean() {
                    boolean cond = condC.asBoolean();
                    Performer t = trueP;
                    Performer f = falseP;
                    @Override
                    public boolean performBoolean(PetalProvider provider) {
                        return cond? t.performBoolean(provider) : f.performBoolean(provider);
                    }
                };
            }else if(condP != null && trueC != null && falseC != null){
                return new PerformerBoolean() {
                    Performer cond = condP;
                    boolean t = trueC.asBoolean();
                    boolean f = falseC.asBoolean();
                    @Override
                    public boolean performBoolean(PetalProvider provider) {
                        return cond.performBoolean(provider)? t : f;
                    }
                };
            }else if(condP != null && trueC != null && falseP != null){
                return new PerformerBoolean() {
                    Performer cond = condP;
                    boolean t = trueC.asBoolean();
                    Performer f = falseP;
                    @Override
                    public boolean performBoolean(PetalProvider provider) {
                        return cond.performBoolean(provider)? t : f.performBoolean(provider);
                    }
                };
            }else if(condP != null && trueP != null && falseC != null){
                return new PerformerBoolean() {
                    Performer cond = condP;
                    Performer t = trueP;
                    boolean f = falseC.asBoolean();
                    @Override
                    public boolean performBoolean(PetalProvider provider) {
                        return cond.performBoolean(provider)? t.performBoolean(provider) : f;
                    }
                };
            }else{ //condP != null && trueP != null && falseP != null
                return new PerformerBoolean() {
                    Performer cond = condP;
                    Performer t = trueP;
                    Performer f = falseP;
                    @Override
                    public boolean performBoolean(PetalProvider provider) {
                        return cond.performBoolean(provider)? t.performBoolean(provider) : f.performBoolean(provider);
                    }
                };
            }
        case BYTE:
            if(condC != null && trueC != null && falseC != null){
                return new PerformerByte() {
                    boolean cond = condC.asBoolean();
                    byte t = trueC.asByte();
                    byte f = falseC.asByte();
                    @Override
                    public byte performByte(PetalProvider provider) {
                        return cond? t : f;
                    }
                };
            }else if(condC != null && trueC != null && falseP != null){
                return new PerformerByte() {
                    boolean cond = condC.asBoolean();
                    byte t = trueC.asByte();
                    Performer f = falseP;
                    @Override
                    public byte performByte(PetalProvider provider) {
                        return cond? t : f.performByte(provider);
                    }
                };
            }else if(condC != null && trueP != null && falseC != null){
                return new PerformerByte() {
                    boolean cond = condC.asBoolean();
                    Performer t = trueP;
                    byte f = falseC.asByte();
                    @Override
                    public byte performByte(PetalProvider provider) {
                        return cond? t.performByte(provider) : f;
                    }
                };
            }else if(condC != null && trueP != null && falseP != null){
                return new PerformerByte() {
                    boolean cond = condC.asBoolean();
                    Performer t = trueP;
                    Performer f = falseP;
                    @Override
                    public byte performByte(PetalProvider provider) {
                        return cond? t.performByte(provider) : f.performByte(provider);
                    }
                };
            }else if(condP != null && trueC != null && falseC != null){
                return new PerformerByte() {
                    Performer cond = condP;
                    byte t = trueC.asByte();
                    byte f = falseC.asByte();
                    @Override
                    public byte performByte(PetalProvider provider) {
                        return cond.performBoolean(provider)? t : f;
                    }
                };
            }else if(condP != null && trueC != null && falseP != null){
                return new PerformerByte() {
                    Performer cond = condP;
                    byte t = trueC.asByte();
                    Performer f = falseP;
                    @Override
                    public byte performByte(PetalProvider provider) {
                        return cond.performBoolean(provider)? t : f.performByte(provider);
                    }
                };
            }else if(condP != null && trueP != null && falseC != null){
                return new PerformerByte() {
                    Performer cond = condP;
                    Performer t = trueP;
                    byte f = falseC.asByte();
                    @Override
                    public byte performByte(PetalProvider provider) {
                        return cond.performBoolean(provider)? t.performByte(provider) : f;
                    }
                };
            }else{ //condP != null && trueP != null && falseP != null
                return new PerformerByte() {
                    Performer cond = condP;
                    Performer t = trueP;
                    Performer f = falseP;
                    @Override
                    public byte performByte(PetalProvider provider) {
                        return cond.performBoolean(provider)? t.performByte(provider) : f.performByte(provider);
                    }
                };
            }
        case CHAR:
            if(condC != null && trueC != null && falseC != null){
                return new PerformerChar() {
                    boolean cond = condC.asBoolean();
                    char t = trueC.asChar();
                    char f = falseC.asChar();
                    @Override
                    public char performChar(PetalProvider provider) {
                        return cond? t : f;
                    }
                };
            }else if(condC != null && trueC != null && falseP != null){
                return new PerformerChar() {
                    boolean cond = condC.asBoolean();
                    char t = trueC.asChar();
                    Performer f = falseP;
                    @Override
                    public char performChar(PetalProvider provider) {
                        return cond? t : f.performChar(provider);
                    }
                };
            }else if(condC != null && trueP != null && falseC != null){
                return new PerformerChar() {
                    boolean cond = condC.asBoolean();
                    Performer t = trueP;
                    char f = falseC.asChar();
                    @Override
                    public char performChar(PetalProvider provider) {
                        return cond? t.performChar(provider) : f;
                    }
                };
            }else if(condC != null && trueP != null && falseP != null){
                return new PerformerChar() {
                    boolean cond = condC.asBoolean();
                    Performer t = trueP;
                    Performer f = falseP;
                    @Override
                    public char performChar(PetalProvider provider) {
                        return cond? t.performChar(provider) : f.performChar(provider);
                    }
                };
            }else if(condP != null && trueC != null && falseC != null){
                return new PerformerChar() {
                    Performer cond = condP;
                    char t = trueC.asChar();
                    char f = falseC.asChar();
                    @Override
                    public char performChar(PetalProvider provider) {
                        return cond.performBoolean(provider)? t : f;
                    }
                };
            }else if(condP != null && trueC != null && falseP != null){
                return new PerformerChar() {
                    Performer cond = condP;
                    char t = trueC.asChar();
                    Performer f = falseP;
                    @Override
                    public char performChar(PetalProvider provider) {
                        return cond.performBoolean(provider)? t : f.performChar(provider);
                    }
                };
            }else if(condP != null && trueP != null && falseC != null){
                return new PerformerChar() {
                    Performer cond = condP;
                    Performer t = trueP;
                    char f = falseC.asChar();
                    @Override
                    public char performChar(PetalProvider provider) {
                        return cond.performBoolean(provider)? t.performChar(provider) : f;
                    }
                };
            }else{ //condP != null && trueP != null && falseP != null
                return new PerformerChar() {
                    Performer cond = condP;
                    Performer t = trueP;
                    Performer f = falseP;
                    @Override
                    public char performChar(PetalProvider provider) {
                        return cond.performBoolean(provider)? t.performChar(provider) : f.performChar(provider);
                    }
                };
            }
        case DOUBLE:
            if(condC != null && trueC != null && falseC != null){
            return new PerformerDouble() {
                    boolean cond = condC.asBoolean();
                    double t = trueC.asDouble();
                    double f = falseC.asDouble();
                    @Override
                    public double performDouble(PetalProvider provider) {
                        return cond? t : f;
                    }
                };
            }else if(condC != null && trueC != null && falseP != null){
                return new PerformerDouble() {
                    boolean cond = condC.asBoolean();
                    double t = trueC.asDouble();
                    Performer f = falseP;
                    @Override
                    public double performDouble(PetalProvider provider) {
                        return cond? t : f.performDouble(provider);
                    }
                };
            }else if(condC != null && trueP != null && falseC != null){
                return new PerformerDouble() {
                    boolean cond = condC.asBoolean();
                    Performer t = trueP;
                    double f = falseC.asDouble();
                    @Override
                    public double performDouble(PetalProvider provider) {
                        return cond? t.performDouble(provider) : f;
                    }
                };
            }else if(condC != null && trueP != null && falseP != null){
                return new PerformerDouble() {
                    boolean cond = condC.asBoolean();
                    Performer t = trueP;
                    Performer f = falseP;
                    @Override
                    public double performDouble(PetalProvider provider) {
                        return cond? t.performDouble(provider) : f.performDouble(provider);
                    }
                };
            }else if(condP != null && trueC != null && falseC != null){
                return new PerformerDouble() {
                    Performer cond = condP;
                    double t = trueC.asDouble();
                    double f = falseC.asDouble();
                    @Override
                    public double performDouble(PetalProvider provider) {
                        return cond.performBoolean(provider)? t : f;
                    }
                };
            }else if(condP != null && trueC != null && falseP != null){
                return new PerformerDouble() {
                    Performer cond = condP;
                    double t = trueC.asDouble();
                    Performer f = falseP;
                    @Override
                    public double performDouble(PetalProvider provider) {
                        return cond.performBoolean(provider)? t : f.performDouble(provider);
                    }
                };
            }else if(condP != null && trueP != null && falseC != null){
                return new PerformerDouble() {
                    Performer cond = condP;
                    Performer t = trueP;
                    double f = falseC.asDouble();
                    @Override
                    public double performDouble(PetalProvider provider) {
                        return cond.performBoolean(provider)? t.performDouble(provider) : f;
                    }
                };
            }else{ //condP != null && trueP != null && falseP != null
                return new PerformerDouble() {
                    Performer cond = condP;
                    Performer t = trueP;
                    Performer f = falseP;
                    @Override
                    public double performDouble(PetalProvider provider) {
                        return cond.performBoolean(provider)? t.performDouble(provider) : f.performDouble(provider);
                    }
                };
            }
        case FLOAT:
            if(condC != null && trueC != null && falseC != null){
                return new PerformerFloat() {
                    boolean cond = condC.asBoolean();
                    float t = trueC.asFloat();
                    float f = falseC.asFloat();
                    @Override
                    public float performFloat(PetalProvider provider) {
                        return cond? t : f;
                    }
                };
            }else if(condC != null && trueC != null && falseP != null){
                return new PerformerFloat() {
                    boolean cond = condC.asBoolean();
                    float t = trueC.asFloat();
                    Performer f = falseP;
                    @Override
                    public float performFloat(PetalProvider provider) {
                        return cond? t : f.performFloat(provider);
                    }
                };
            }else if(condC != null && trueP != null && falseC != null){
                return new PerformerFloat() {
                    boolean cond = condC.asBoolean();
                    Performer t = trueP;
                    float f = falseC.asFloat();
                    @Override
                    public float performFloat(PetalProvider provider) {
                        return cond? t.performFloat(provider) : f;
                    }
                };
            }else if(condC != null && trueP != null && falseP != null){
                return new PerformerFloat() {
                    boolean cond = condC.asBoolean();
                    Performer t = trueP;
                    Performer f = falseP;
                    @Override
                    public float performFloat(PetalProvider provider) {
                        return cond? t.performFloat(provider) : f.performFloat(provider);
                    }
                };
            }else if(condP != null && trueC != null && falseC != null){
                return new PerformerFloat() {
                    Performer cond = condP;
                    float t = trueC.asFloat();
                    float f = falseC.asFloat();
                    @Override
                    public float performFloat(PetalProvider provider) {
                        return cond.performBoolean(provider)? t : f;
                    }
                };
            }else if(condP != null && trueC != null && falseP != null){
                return new PerformerFloat() {
                    Performer cond = condP;
                    float t = trueC.asFloat();
                    Performer f = falseP;
                    @Override
                    public float performFloat(PetalProvider provider) {
                        return cond.performBoolean(provider)? t : f.performFloat(provider);
                    }
                };
            }else if(condP != null && trueP != null && falseC != null){
                return new PerformerFloat() {
                    Performer cond = condP;
                    Performer t = trueP;
                    float f = falseC.asFloat();
                    @Override
                    public float performFloat(PetalProvider provider) {
                        return cond.performBoolean(provider)? t.performFloat(provider) : f;
                    }
                };
            }else{ //condP != null && trueP != null && falseP != null
                return new PerformerFloat() {
                    Performer cond = condP;
                    Performer t = trueP;
                    Performer f = falseP;
                    @Override
                    public float performFloat(PetalProvider provider) {
                        return cond.performBoolean(provider)? t.performFloat(provider) : f.performFloat(provider);
                    }
                };
            }
        case INT:
            if(condC != null && trueC != null && falseC != null){
                return new PerformerInt() {
                    boolean cond = condC.asBoolean();
                    int t = trueC.asInt();
                    int f = falseC.asInt();
                    @Override
                    public int performInt(PetalProvider provider) {
                        return cond? t : f;
                    }
                };
            }else if(condC != null && trueC != null && falseP != null){
                return new PerformerInt() {
                    boolean cond = condC.asBoolean();
                    int t = trueC.asInt();
                    Performer f = falseP;
                    @Override
                    public int performInt(PetalProvider provider) {
                        return cond? t : f.performInt(provider);
                    }
                };
            }else if(condC != null && trueP != null && falseC != null){
                return new PerformerInt() {
                    boolean cond = condC.asBoolean();
                    Performer t = trueP;
                    int f = falseC.asInt();
                    @Override
                    public int performInt(PetalProvider provider) {
                        return cond? t.performInt(provider) : f;
                    }
                };
            }else if(condC != null && trueP != null && falseP != null){
                return new PerformerInt() {
                    boolean cond = condC.asBoolean();
                    Performer t = trueP;
                    Performer f = falseP;
                    @Override
                    public int performInt(PetalProvider provider) {
                        return cond? t.performInt(provider) : f.performInt(provider);
                    }
                };
            }else if(condP != null && trueC != null && falseC != null){
                return new PerformerInt() {
                    Performer cond = condP;
                    int t = trueC.asInt();
                    int f = falseC.asInt();
                    @Override
                    public int performInt(PetalProvider provider) {
                        return cond.performBoolean(provider)? t : f;
                    }
                };
            }else if(condP != null && trueC != null && falseP != null){
                return new PerformerInt() {
                    Performer cond = condP;
                    int t = trueC.asInt();
                    Performer f = falseP;
                    @Override
                    public int performInt(PetalProvider provider) {
                        return cond.performBoolean(provider)? t : f.performInt(provider);
                    }
                };
            }else if(condP != null && trueP != null && falseC != null){
                return new PerformerInt() {
                    Performer cond = condP;
                    Performer t = trueP;
                    int f = falseC.asInt();
                    @Override
                    public int performInt(PetalProvider provider) {
                        return cond.performBoolean(provider)? t.performInt(provider) : f;
                    }
                };
            }else{ //condP != null && trueP != null && falseP != null
                return new PerformerInt() {
                    Performer cond = condP;
                    Performer t = trueP;
                    Performer f = falseP;
                    @Override
                    public int performInt(PetalProvider provider) {
                        return cond.performBoolean(provider)? t.performInt(provider) : f.performInt(provider);
                    }
                };
            }
        case LONG:
            if(condC != null && trueC != null && falseC != null){
                return new PerformerLong() {
                    boolean cond = condC.asBoolean();
                    long t = trueC.asLong();
                    long f = falseC.asLong();
                    @Override
                    public long performLong(PetalProvider provider) {
                        return cond? t : f;
                    }
                };
            }else if(condC != null && trueC != null && falseP != null){
                return new PerformerLong() {
                    boolean cond = condC.asBoolean();
                    long t = trueC.asLong();
                    Performer f = falseP;
                    @Override
                    public long performLong(PetalProvider provider) {
                        return cond? t : f.performLong(provider);
                    }
                };
            }else if(condC != null && trueP != null && falseC != null){
                return new PerformerLong() {
                    boolean cond = condC.asBoolean();
                    Performer t = trueP;
                    long f = falseC.asLong();
                    @Override
                    public long performLong(PetalProvider provider) {
                        return cond? t.performLong(provider) : f;
                    }
                };
            }else if(condC != null && trueP != null && falseP != null){
                return new PerformerLong() {
                    boolean cond = condC.asBoolean();
                    Performer t = trueP;
                    Performer f = falseP;
                    @Override
                    public long performLong(PetalProvider provider) {
                        return cond? t.performLong(provider) : f.performLong(provider);
                    }
                };
            }else if(condP != null && trueC != null && falseC != null){
                return new PerformerLong() {
                    Performer cond = condP;
                    long t = trueC.asLong();
                    long f = falseC.asLong();
                    @Override
                    public long performLong(PetalProvider provider) {
                        return cond.performBoolean(provider)? t : f;
                    }
                };
            }else if(condP != null && trueC != null && falseP != null){
                return new PerformerLong() {
                    Performer cond = condP;
                    long t = trueC.asLong();
                    Performer f = falseP;
                    @Override
                    public long performLong(PetalProvider provider) {
                        return cond.performBoolean(provider)? t : f.performLong(provider);
                    }
                };
            }else if(condP != null && trueP != null && falseC != null){
                return new PerformerLong() {
                    Performer cond = condP;
                    Performer t = trueP;
                    long f = falseC.asLong();
                    @Override
                    public long performLong(PetalProvider provider) {
                        return cond.performBoolean(provider)? t.performLong(provider) : f;
                    }
                };
            }else{ //condP != null && trueP != null && falseP != null
                return new PerformerLong() {
                    Performer cond = condP;
                    Performer t = trueP;
                    Performer f = falseP;
                    @Override
                    public long performLong(PetalProvider provider) {
                        return cond.performBoolean(provider)? t.performLong(provider) : f.performLong(provider);
                    }
                };
            }
        case SHORT:
            if(condC != null && trueC != null && falseC != null){
                return new PerformerShort() {
                    boolean cond = condC.asBoolean();
                    short t = trueC.asShort();
                    short f = falseC.asShort();
                    @Override
                    public short performShort(PetalProvider provider) {
                        return cond? t : f;
                    }
                };
            }else if(condC != null && trueC != null && falseP != null){
                return new PerformerShort() {
                    boolean cond = condC.asBoolean();
                    short t = trueC.asShort();
                    Performer f = falseP;
                    @Override
                    public short performShort(PetalProvider provider) {
                        return cond? t : f.performShort(provider);
                    }
                };
            }else if(condC != null && trueP != null && falseC != null){
                return new PerformerShort() {
                    boolean cond = condC.asBoolean();
                    Performer t = trueP;
                    short f = falseC.asShort();
                    @Override
                    public short performShort(PetalProvider provider) {
                        return cond? t.performShort(provider) : f;
                    }
                };
            }else if(condC != null && trueP != null && falseP != null){
                return new PerformerShort() {
                    boolean cond = condC.asBoolean();
                    Performer t = trueP;
                    Performer f = falseP;
                    @Override
                    public short performShort(PetalProvider provider) {
                        return cond? t.performShort(provider) : f.performShort(provider);
                    }
                };
            }else if(condP != null && trueC != null && falseC != null){
                return new PerformerShort() {
                    Performer cond = condP;
                    short t = trueC.asShort();
                    short f = falseC.asShort();
                    @Override
                    public short performShort(PetalProvider provider) {
                        return cond.performBoolean(provider)? t : f;
                    }
                };
            }else if(condP != null && trueC != null && falseP != null){
                return new PerformerShort() {
                    Performer cond = condP;
                    short t = trueC.asShort();
                    Performer f = falseP;
                    @Override
                    public short performShort(PetalProvider provider) {
                        return cond.performBoolean(provider)? t : f.performShort(provider);
                    }
                };
            }else if(condP != null && trueP != null && falseC != null){
                return new PerformerShort() {
                    Performer cond = condP;
                    Performer t = trueP;
                    short f = falseC.asShort();
                    @Override
                    public short performShort(PetalProvider provider) {
                        return cond.performBoolean(provider)? t.performShort(provider) : f;
                    }
                };
            }else{ //condP != null && trueP != null && falseP != null
                return new PerformerShort() {
                    Performer cond = condP;
                    Performer t = trueP;
                    Performer f = falseP;
                    @Override
                    public short performShort(PetalProvider provider) {
                        return cond.performBoolean(provider)? t.performShort(provider) : f.performShort(provider);
                    }
                };
            }
        case STRING:
            if(condC != null && trueC != null && falseC != null){
                return new PerformerString() {
                    boolean cond = condC.asBoolean();
                    String t = trueC.asString();
                    String f = falseC.asString();
                    @Override
                    public StringBuilder performString(StringBuilder sb, PetalProvider provider) {
                        if(sb == null)
                            sb = new StringBuilder();
                        return cond? sb.append(t) : sb.append(f);
                    }
                };
            }else if(condC != null && trueC != null && falseP != null){
                return new PerformerString() {
                    boolean cond = condC.asBoolean();
                    String t = trueC.asString();
                    Performer f = falseP;
                    @Override
                    public StringBuilder performString(StringBuilder sb, PetalProvider provider) {
                        if(cond){
                            if(sb == null){
                                return new StringBuilder(t); 
                            }else{
                                return sb.append(t);
                            }
                        }else
                            return f.performString(sb, provider);
                    }
                };
            }else if(condC != null && trueP != null && falseC != null){
                return new PerformerString() {
                    boolean cond = condC.asBoolean();
                    Performer t = trueP;
                    String f = falseC.asString();
                    @Override
                    public StringBuilder performString(StringBuilder sb, PetalProvider provider) {
                        if(cond){
                            return t.performString(sb, provider);
                        }else{
                            if(sb == null){
                                return new StringBuilder(f); 
                            }else{
                                return sb.append(f);
                            }
                        }
                    }
                };
            }else if(condC != null && trueP != null && falseP != null){
                return new PerformerString() {
                    boolean cond = condC.asBoolean();
                    Performer t = trueP;
                    Performer f = falseP;
                    @Override
                    public StringBuilder performString(StringBuilder sb, PetalProvider provider) {
                        return cond? t.performString(sb, provider) : f.performString(sb, provider);
                    }
                };
            }else if(condP != null && trueC != null && falseC != null){
                return new PerformerString() {
                    Performer cond = condP;
                    String t = trueC.asString();
                    String f = falseC.asString();
                    @Override
                    public StringBuilder performString(StringBuilder sb, PetalProvider provider) {
                        if(sb == null)
                            sb = new StringBuilder();
                        return cond.performBoolean(provider)? sb.append(t) : sb.append(f);
                    }
                };
            }else if(condP != null && trueC != null && falseP != null){
                return new PerformerString() {
                    Performer cond = condP;
                    String t = trueC.asString();
                    Performer f = falseP;
                    @Override
                    public StringBuilder performString(StringBuilder sb, PetalProvider provider) {
                        if(cond.performBoolean(provider)){
                            if(sb == null){
                                return new StringBuilder(t);
                            }else{
                                return sb.append(t);
                            }
                        }else{
                            return f.performString(sb, provider);
                        }
                    }
                };
            }else if(condP != null && trueP != null && falseC != null){
                return new PerformerString() {
                    Performer cond = condP;
                    Performer t = trueP;
                    String f = falseC.asString();
                    @Override
                    public StringBuilder performString(StringBuilder sb, PetalProvider provider) {
                        if(cond.performBoolean(provider)){
                            return t.performString(sb, provider);
                        }else{
                            if(sb == null){
                                return new StringBuilder(f);
                            }else{
                                return sb.append(f);
                            }
                        }
                    }
                };
            }else{ //condP != null && trueP != null && falseP != null
                return new PerformerString() {
                    Performer cond = condP;
                    Performer t = trueP;
                    Performer f = falseP;
                    @Override
                    public StringBuilder performString(StringBuilder sb, PetalProvider provider) {
                        return cond.performBoolean(provider)? t.performString(sb, provider) : f.performString(sb, provider);
                    }
                };
            }
        default:
        }
        throw new ParseELException(0, "Unknown type: "+performerType);
    }
}
