/*
 * Copyright (C) 2018-2018 The Project Spathiphyllum Authors.
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
package org.spathiphyllum.el;

import java.math.BigDecimal;
import java.math.BigInteger;

import org.spathiphyllum.el.HelperEL.Const;
import org.spathiphyllum.el.HelperEL.Operator;
/**
 * Substract
 * @author obelous
 *
 */
public class SUB extends Operator {
    static final Operator OPERATOR = new SUB();
    private SUB() {
        super("SUB", EnumEL.NUMBER);
    }
    @Override
    int priority() {
        return 10;
    }
    @Override
    EnumEL adjustType(EnumEL left, EnumEL right) throws ParseELException {
        if(left.isNumber() && right.isNumber()){
            return HelperEL.approachNumerTypes(left, right);
        }else{
            throw new ParseELException(0, "Operator '-' can be assigned numbers only");
        }
    }
    @Override
    Operator adjustOperator(int pos, AbstractComponent left, AbstractComponent right) throws ParseELException {
        if(left.getType().isNumber() && right.getType().isNumber()){
            return OPERATOR;
        }else{
            throw new ParseELException(pos, "Operator '-' can be assigned only to couple of number");
        }
    }
    @Override
    Performer compile(EnumEL performerType, Performer leftP, Const leftC, Performer rightP, Const rightC)
            throws ParseELException {
        if(leftC != null && rightC != null){
            switch (performerType) {
            case BIG_DECIMAL:
                return performerBigDecimal(leftC.asBigDecimal(), rightC.asBigDecimal());
            case BIG_INTEGER:
                return performerBigInteger(leftC.asBigInteger(), rightC.asBigInteger());
            case DOUBLE:
                return performerDouble(leftC.asDouble(), rightC.asDouble());
            case FLOAT:
                return performerFloat(leftC.asFloat(), rightC.asFloat());
            case INT:
                return performerInt(leftC.asInt(), rightC.asInt());
            case LONG:
                return performerLong(leftC.asLong(), rightC.asLong());
            default:
            }
        }else if(leftC != null && rightP != null){
            switch (performerType) {
            case BIG_DECIMAL:
                return performerBigDecimal(leftC.asBigDecimal(), rightP);
            case BIG_INTEGER:
                return performerBigInteger(leftC.asBigInteger(), rightP);
            case DOUBLE:
                return performerDouble(leftC.asDouble(), rightP);
            case FLOAT:
                return performerFloat(leftC.asFloat(), rightP);
            case INT:
                return performerInt(leftC.asInt(), rightP);
            case LONG:
                return performerLong(leftC.asLong(), rightP);
            default:
            }
        }else if(leftP != null && rightP != null){
            switch (performerType) {
            case BIG_DECIMAL:
                return performerBigDecimal(leftP, rightP);
            case BIG_INTEGER:
                return performerBigInteger(leftP, rightP);
            case DOUBLE:
                return performerDouble(leftP, rightP);
            case FLOAT:
                return performerFloat(leftP, rightP);
            case INT:
                return performerInt(leftP, rightP);
            case LONG:
                return performerLong(leftP, rightP);
            default:
            }
        }else if(leftP != null && rightC != null){
            switch (performerType) {
            case BIG_DECIMAL:
                return performerBigDecimal(leftP, rightC.asBigDecimal());
            case BIG_INTEGER:
                return performerBigInteger(leftP, rightC.asBigInteger());
            case DOUBLE:
                return performerDouble(leftP, rightC.asDouble());
            case FLOAT:
                return performerFloat(leftP, rightC.asFloat());
            case INT:
                return performerInt(leftP, rightC.asInt());
            case LONG:
                return performerLong(leftP, rightC.asLong());
            default:
            }
        }
        throw new ParseELException(0, "Compile error");
    }
    private static Performer performerBigDecimal(Performer leftP, BigDecimal rightC){
        return new PerformerBigDecimal() {
            Performer left = leftP;
            BigDecimal right = rightC;
            @Override
            public BigDecimal performBigDecimal(PetalProvider provider) {
                return left.performBigDecimal(provider).subtract(right);
            }
        };
    }
    private static Performer performerBigDecimal(BigDecimal leftC, Performer rightP){
        return new PerformerBigDecimal() {
            BigDecimal left = leftC;
            Performer right = rightP;
            @Override
            public BigDecimal performBigDecimal(PetalProvider provider) {
                return left.subtract(right.performBigDecimal(provider));
            }
        };
    }
    private static Performer performerBigDecimal(Performer leftP, Performer rightP){
        return new PerformerBigDecimal() {
            Performer left = leftP;
            Performer right = rightP;
            @Override
            public BigDecimal performBigDecimal(PetalProvider provider) {
                return left.performBigDecimal(provider).subtract(right.performBigDecimal(provider));
            }
        };
    }
    private static Performer performerBigDecimal(BigDecimal leftC, BigDecimal rightC){
        return new PerformerBigDecimal() {
            BigDecimal left = leftC;
            BigDecimal right = rightC;
            @Override
            public BigDecimal performBigDecimal(PetalProvider provider) {
                return left.subtract(right);
            }
        };
    }
    private static Performer performerBigInteger(Performer leftP, BigInteger rightC){
        return new PerformerBigInteger() {
            Performer left = leftP;
            BigInteger right = rightC;
            @Override
            public BigInteger performBigInteger(PetalProvider provider) {
                return left.performBigInteger(provider).subtract(right);
            }
        };
    }
    private static Performer performerBigInteger(BigInteger leftC, Performer rightP){
        return new PerformerBigInteger() {
            BigInteger left = leftC;
            Performer right = rightP;
            @Override
            public BigInteger performBigInteger(PetalProvider provider) {
                return left.subtract(right.performBigInteger(provider));
            }
        };
    }
    private static Performer performerBigInteger(Performer leftP, Performer rightP){
        return new PerformerBigInteger() {
            Performer left = leftP;
            Performer right = rightP;
            @Override
            public BigInteger performBigInteger(PetalProvider provider){
                return left.performBigInteger(provider).subtract(right.performBigInteger(provider));
            }
        };
    }
    private static Performer performerBigInteger(BigInteger leftC, BigInteger rightC){
        return new PerformerBigInteger() {
            BigInteger left = leftC;
            BigInteger right = rightC;
            @Override
            public BigInteger performBigInteger(PetalProvider provider) {
                return left.subtract(right);
            }
        };
    }
    private static Performer performerLong(Performer leftP, long rightC){
        return new PerformerLong() {
            Performer left = leftP;
            long right = rightC;
            @Override
            public long performLong(PetalProvider provider) {
                return left.performLong(provider) - right;
            }
        };
    }
    private static Performer performerLong(long leftC, Performer rightP){
        return new PerformerLong() {
            long left = leftC;
            Performer right = rightP;
            @Override
            public long performLong(PetalProvider provider) {
                return left - right.performLong(provider);
            }
        };
    }
    private static Performer performerLong(Performer leftP, Performer rightP){
        return new PerformerLong() {
            Performer left = leftP;
            Performer right = rightP;
            @Override
            public long performLong(PetalProvider provider) {
                return left.performLong(provider) - right.performLong(provider);
            }
        };
    }
    private static Performer performerLong(long leftC, long rightC){
        return new PerformerLong() {
            long left = leftC;
            long right = rightC;
            @Override
            public long performLong(PetalProvider provider) {
                return left - right;
            }
        };
    }
    private static Performer performerInt(Performer leftP, int rightC){
        return new PerformerInt() {
            Performer left = leftP;
            int right = rightC;
            @Override
            public int performInt(PetalProvider provider) {
                return left.performInt(provider) - right;
            }
        };
    }
    private static Performer performerInt(int leftC, Performer rightP){
        return new PerformerInt() {
            int left = leftC;
            Performer right = rightP;
            @Override
            public int performInt(PetalProvider provider) {
                return left - right.performInt(provider);
            }
        };
    }
    private static Performer performerInt(Performer leftP, Performer rightP){
        return new PerformerInt() {
            Performer left = leftP;
            Performer right = rightP;
            @Override
            public int performInt(PetalProvider provider) {
                return left.performInt(provider) - right.performInt(provider);
            }
        };
    }
    private static Performer performerInt(int leftC, int rightC){
        return new PerformerInt() {
            int left = leftC;
            int right = rightC;
            @Override
            public int performInt(PetalProvider provider) {
                return left - right;
            }
        };
    }
    private static Performer performerDouble(Performer leftP, double rightC){
        return new PerformerDouble() {
            Performer left = leftP;
            double right = rightC;
            @Override
            public double performDouble(PetalProvider provider) {
                return left.performDouble(provider) - right;
            }
        };
    }
    private static Performer performerDouble(double leftC, Performer rightP){
        return new PerformerDouble() {
            double left = leftC;
            Performer right = rightP;
            @Override
            public double performDouble(PetalProvider provider) {
                return left - right.performDouble(provider);
            }
        };
    }
    private static Performer performerDouble(Performer leftP, Performer rightP){
        return new PerformerDouble() {
            Performer left = leftP;
            Performer right = rightP;
            @Override
            public double performDouble(PetalProvider provider) {
                return left.performDouble(provider) - right.performDouble(provider);
            }
        };
    }
    private static Performer performerDouble(double leftC, double rightC){
        return new PerformerDouble() {
            double left = leftC;
            double right = rightC;
            @Override
            public double performDouble(PetalProvider provider) {
                return left - right;
            }
        };
    }
    private static Performer performerFloat(Performer leftP, float rightC){
        return new PerformerFloat() {
            Performer left = leftP;
            float right = rightC;
            @Override
            public float performFloat(PetalProvider provider) {
                return left.performFloat(provider) - right;
            }
        };
    }
    private static Performer performerFloat(float leftC, Performer rightP){
        return new PerformerFloat() {
            float left = leftC;
            Performer right = rightP;
            @Override
            public float performFloat(PetalProvider provider) {
                return left - right.performFloat(provider);
            }
        };
    }
    private static Performer performerFloat(Performer leftP, Performer rightP){
        return new PerformerFloat() {
            Performer left = leftP;
            Performer right = rightP;
            @Override
            public float performFloat(PetalProvider provider) {
                return left.performFloat(provider) - right.performFloat(provider);
            }
        };
    }
    private static Performer performerFloat(float leftC, float rightC){
        return new PerformerFloat() {
            float left = leftC;
            float right = rightC;
            @Override
            public float performFloat(PetalProvider provider) {
                return left - right;
            }
        };
    }
    @Override
    Const unitConst(Const left, Const right) throws ParseELException {
        switch(adjustType(left.getType(), right.getType())){
            case BIG_DECIMAL: return new Const(left.asBigDecimal().subtract(right.asBigDecimal()), EnumEL.BIG_DECIMAL);
            case BIG_INTEGER: return new Const(left.asBigInteger().subtract(right.asBigInteger()), EnumEL.BIG_INTEGER);
            case DOUBLE: return new Const(left.asDouble() - right.asDouble(), EnumEL.DOUBLE);
            case FLOAT: return new Const(left.asFloat() - right.asFloat(), EnumEL.FLOAT);
            case INT: return new Const(left.asInt() - right.asInt(), EnumEL.INT);
            case LONG: return new Const(left.asLong() - right.asLong(), EnumEL.LONG);
            default: throw new ParseELException(0, "Method is unsupported");
        }
    }
}
