/*
 * Copyright (C) 2018-2018 The Project Spathiphyllum Authors.
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
package org.spathiphyllum.el;

import java.math.BigDecimal;
import java.math.BigInteger;

import org.spathiphyllum.el.HelperEL.Const;
import org.spathiphyllum.el.HelperEL.Operator;
/**
 * Multiplication operator (NUMBER)
 * @author dbelous
 *
 */
public class MUL extends Operator{
    static final Operator STRING = new MUL(EnumEL.STRING);
    static final Operator NUMBER = new MUL(EnumEL.NUMBER);
    private MUL(EnumEL type) {
        super("Mul", type);
    }
    @Override
    int priority() {
        return 11;
    }
    @Override
    EnumEL adjustType(EnumEL left, EnumEL right) throws ParseELException {
        if((left.isNumber() && right.isString())
                || (left.isString() && right.isNumber())){
            return EnumEL.STRING;
        }else if(left.isNumber() && right.isNumber()){
            return HelperEL.approachNumerTypes(left, right);
        }else{
            throw new ParseELException(0, "Operator '*' cannot be assign to "+ left + " and " +right);
        }
    }
    @Override
    Operator adjustOperator(int pos, AbstractComponent left, AbstractComponent right) throws ParseELException {
        if((left.getType().isNumber() && right.getType().isString())
                || (left.getType().isString() && right.getType().isNumber())){
            return STRING;
        }else if(left.getType().isNumber() && right.getType().isNumber()){
            return NUMBER;
        }else{
            throw new ParseELException(pos, "Operator '*' cannot be assign to "+ left.getType() + " and " +right.getType());
        }
    }
    @Override
    Performer compile(EnumEL performerType, Performer leftP, Const leftC, Performer rightP, Const rightC) throws ParseELException {
        if(leftC != null && rightC != null){
            switch (performerType) {
            case BIG_DECIMAL:
                return performerBigDecimal(leftC.asBigDecimal(), rightC.asBigDecimal());
            case BIG_INTEGER:
                return performerBigInteger(leftC.asBigInteger(), rightC.asBigInteger());
            case DOUBLE:
                return performerDouble(leftC.asDouble(), rightC.asDouble());
            case FLOAT:
                return performerFloat(leftC.asFloat(), rightC.asFloat());
            case INT:
                return performerInt(leftC.asInt(), rightC.asInt());
            case LONG:
                return performerLong(leftC.asLong(), rightC.asLong());
            case STRING:
                if(leftC.getType().isString() && rightC.getType().isNumber())
                    return performerStringInt(leftC.asString(), rightC.asInt());
                else if(leftC.getType().isNumber() && rightC.getType().isString())
                    return performerIntString(leftC.asInt(), rightC.asString());
                else
                    throw new ParseELException(0, "Operator '*' can be used with string but when other is int");
            default:
            }
        }else if(leftP != null && rightP != null){
            switch (performerType) {
            case BIG_DECIMAL:
                return performerBigDecimal(leftP, rightP);
            case BIG_INTEGER:
                return performerBigInteger(leftP, rightP);
            case DOUBLE:
                return performerDouble(leftP, rightP);
            case FLOAT:
                return performerFloat(leftP, rightP);
            case INT:
                return performerInt(leftP, rightP);
            case LONG:
                return performerLong(leftP, rightP);
            case STRING:
                if(leftP.type().isString() && rightP.type().isNumber())
                    return performerStringInt(leftP, rightP);
                else if(leftP.type().isNumber() && rightP.type().isString())
                    return performerIntString(leftP, rightP);
                else
                    throw new ParseELException(0, "Operator '*' can be used with string but when other is int");
            default:
            }
        }else if(leftC != null && rightP != null){
            switch (performerType) {
            case BIG_DECIMAL:
                return performerBigDecimal(leftC.asBigDecimal(), rightP);
            case BIG_INTEGER:
                return performerBigInteger(leftC.asBigInteger(), rightP);
            case DOUBLE:
                return performerDouble(leftC.asDouble(), rightP);
            case FLOAT:
                return performerFloat(leftC.asFloat(), rightP);
            case INT:
                return performerInt(leftC.asInt(), rightP);
            case LONG:
                return performerLong(leftC.asLong(), rightP);
            case STRING:
                if(leftC.getType().isString() && rightP.type().isNumber())
                    return performerStringInt(leftC.asString(), rightP);
                else if(leftC.getType().isNumber() && rightP.type().isString())
                    return performerIntString(leftC.asInt(), rightP);
                else
                    throw new ParseELException(0, "Operator '*' can be used with string but when other is int");
            default:
            }
        }else if(leftP != null && rightC != null){
            switch (performerType) {
            case BIG_DECIMAL:
                return performerBigDecimal(leftP, rightC.asBigDecimal());
            case BIG_INTEGER:
                return performerBigInteger(leftP, rightC.asBigInteger());
            case DOUBLE:
                return performerDouble(leftP, rightC.asDouble());
            case FLOAT:
                return performerFloat(leftP, rightC.asFloat());
            case INT:
                return performerInt(leftP, rightC.asInt());
            case LONG:
                return performerLong(leftP, rightC.asLong());
            case STRING:
                if(leftP.type().isString() && rightC.getType().isNumber())
                    return performerStringInt(leftP, rightC.asInt());
                else if(leftP.type().isNumber() && rightC.getType().isString())
                    return performerIntString(leftP, rightC.asString());
                else
                    throw new ParseELException(0, "Operator '*' can be used with string but when other is int");
            default:
            }
        }
        throw new ParseELException(0, "Compile error");
    }
    private static Performer performerIntString(Performer leftP, String rightC) {
        return new PerformerString(){
            Performer left = leftP;
            String right = rightC;
            @Override
            public StringBuilder performString(StringBuilder sb, PetalProvider provider) {
                int count = left.performInt(provider);
                if(sb == null)
                    sb = new StringBuilder();
                for(int i = 0; i < count; i++){
                    sb.append(right);
                }
                return sb;
            }
        };
    }
    private static Performer performerStringInt(Performer leftP, int rightC) {
        return new PerformerString(){
            Performer left = leftP;
            int count = rightC;
            @Override
            public StringBuilder performString(StringBuilder sb, PetalProvider provider) {
                String base = left.performString(null, provider).toString();
                if(sb == null)
                    sb = new StringBuilder();
                for(int i = 0; i < count; i++){
                    sb.append(base);
                }
                return sb;
            }
        };
    }
    private static Performer performerIntString(int leftC, Performer rightP) {
        return new PerformerString(){
            int left = leftC;
            Performer right = rightP;
            @Override
            public StringBuilder performString(StringBuilder sb, PetalProvider provider) {
                String base = right.performString(null, provider).toString();
                if(sb == null)
                    sb = new StringBuilder();
                for(int i = 0; i < left; i++){
                    sb.append(base);
                }
                return sb;
            }
        };
    }
    private static Performer performerStringInt(String leftC, Performer rightP) {
        return new PerformerString(){
            String left = leftC;
            Performer right = rightP;
            @Override
            public StringBuilder performString(StringBuilder sb, PetalProvider provider) {
                int count = right.performInt(provider);
                if(sb == null)
                    sb = new StringBuilder();
                for(int i = 0; i < count; i++){
                    sb.append(left);
                }
                return sb;
            }
        };
    }
    private static Performer performerIntString(Performer leftP, Performer rightP) {
        return new PerformerString(){
            Performer left = leftP;
            Performer right = rightP;
            @Override
            public StringBuilder performString(StringBuilder sb, PetalProvider provider) {
                String base = right.performString(null, provider).toString();
                int count = left.performInt(provider);
                if(sb == null)
                    sb = new StringBuilder();
                for(int i = 0; i < count; i++){
                    sb.append(base);
                }
                return sb;
            }
        };
    }
    private static Performer performerStringInt(Performer leftP, Performer rightP) {
        return new PerformerString(){
            Performer left = leftP;
            Performer right = rightP;
            @Override
            public StringBuilder performString(StringBuilder sb, PetalProvider provider) {
                String base = left.performString(null, provider).toString();
                int count = right.performInt(provider);
                if(sb == null)
                    sb = new StringBuilder();
                for(int i = 0; i < count; i++){
                    sb.append(base);
                }
                return sb;
            }
        };
    }
    private static Performer performerIntString(int leftC, String rightC) {
        return new PerformerString(){
            int left = leftC;
            String right = rightC;
            @Override
            public StringBuilder performString(StringBuilder sb, PetalProvider provider) {
                String base = right;
                int count = left;
                if(sb == null)
                    sb = new StringBuilder();
                for(int i = 0; i < count; i++){
                    sb.append(base);
                }
                return sb;
            }
        };
    }
    private static Performer performerStringInt(String leftC, int rightC) {
        return new PerformerString(){
            String left = leftC;
            int right = rightC;
            @Override
            public StringBuilder performString(StringBuilder sb, PetalProvider provider) {
                String base = left;
                int count = right;
                if(sb == null)
                    sb = new StringBuilder();
                for(int i = 0; i < count; i++){
                    sb.append(base);
                }
                return sb;
            }
        };
    }
    private static Performer performerBigInteger(Performer leftP, BigInteger rightC){
        return new PerformerBigInteger() {
            Performer left = leftP;
            BigInteger right = rightC;
            @Override
            public BigInteger performBigInteger(PetalProvider provider) {
                return left.performBigInteger(provider).multiply(right);
            }
        };
    }
    private static Performer performerBigInteger(BigInteger leftC, Performer rightP){
        return new PerformerBigInteger() {
            BigInteger left = leftC;
            Performer right = rightP;
            @Override
            public BigInteger performBigInteger(PetalProvider provider) {
                return left.multiply(right.performBigInteger(provider));
            }
        };
    }
    private static Performer performerBigInteger(Performer leftP, Performer rightP){
        return new PerformerBigInteger() {
            Performer left = leftP;
            Performer right = rightP;
            @Override
            public BigInteger performBigInteger(PetalProvider provider){
                return left.performBigInteger(provider).multiply(right.performBigInteger(provider));
            }
        };
    }
    private static Performer performerBigInteger(BigInteger leftC, BigInteger rightC){
        return new PerformerBigInteger() {
            BigInteger left = leftC;
            BigInteger right = rightC;
            @Override
            public BigInteger performBigInteger(PetalProvider provider) {
                return left.multiply(right);
            }
        };
    }
    private static Performer performerLong(Performer leftP, long rightC){
        return new PerformerLong() {
            Performer left = leftP;
            long right = rightC;
            @Override
            public long performLong(PetalProvider provider) {
                return left.performLong(provider) * right;
            }
        };
    }
    private static Performer performerLong(long leftC, Performer rightP){
        return new PerformerLong() {
            long left = leftC;
            Performer right = rightP;
            @Override
            public long performLong(PetalProvider provider) {
                return left * right.performLong(provider);
            }
        };
    }
    private static Performer performerLong(Performer leftP, Performer rightP){
        return new PerformerLong() {
            Performer left = leftP;
            Performer right = rightP;
            @Override
            public long performLong(PetalProvider provider) {
                return left.performLong(provider) * right.performLong(provider);
            }
        };
    }
    private static Performer performerLong(long leftC, long rightC){
        return new PerformerLong() {
            long left = leftC;
            long right = rightC;
            @Override
            public long performLong(PetalProvider provider) {
                return left * right;
            }
        };
    }
    private static Performer performerInt(Performer leftP, int rightC){
        return new PerformerInt() {
            Performer left = leftP;
            int right = rightC;
            @Override
            public int performInt(PetalProvider provider) {
                return left.performInt(provider) * right;
            }
        };
    }
    private static Performer performerInt(int leftC, Performer rightP){
        return new PerformerInt() {
            int left = leftC;
            Performer right = rightP;
            @Override
            public int performInt(PetalProvider provider) {
                return left * right.performInt(provider);
            }
        };
    }
    private static Performer performerInt(Performer leftP, Performer rightP){
        return new PerformerInt() {
            Performer left = leftP;
            Performer right = rightP;
            @Override
            public int performInt(PetalProvider provider) {
                return left.performInt(provider) * right.performInt(provider);
            }
        };
    }
    private static Performer performerInt(int leftC, int rightC){
        return new PerformerInt() {
            int left = leftC;
            int right = rightC;
            @Override
            public int performInt(PetalProvider provider) {
                return left * right;
            }
        };
    }
    private static Performer performerDouble(Performer leftP, double rightC){
        return new PerformerDouble() {
            Performer left = leftP;
            double right = rightC;
            @Override
            public double performDouble(PetalProvider provider) {
                return left.performDouble(provider) * right;
            }
        };
    }
    private static Performer performerDouble(double leftC, Performer rightP){
        return new PerformerDouble() {
            double left = leftC;
            Performer right = rightP;
            @Override
            public double performDouble(PetalProvider provider) {
                return left * right.performDouble(provider);
            }
        };
    }
    private static Performer performerDouble(Performer leftP, Performer rightP){
        return new PerformerDouble() {
            Performer left = leftP;
            Performer right = rightP;
            @Override
            public double performDouble(PetalProvider provider) {
                return left.performDouble(provider) * right.performDouble(provider);
            }
        };
    }
    private static Performer performerDouble(double leftC, double rightC){
        return new PerformerDouble() {
            double left = leftC;
            double right = rightC;
            @Override
            public double performDouble(PetalProvider provider) {
                return left * right;
            }
        };
    }
    private static Performer performerFloat(Performer leftP, float rightC){
        return new PerformerFloat() {
            Performer left = leftP;
            float right = rightC;
            @Override
            public float performFloat(PetalProvider provider) {
                return left.performFloat(provider) * right;
            }
        };
    }
    private static Performer performerFloat(float leftC, Performer rightP){
        return new PerformerFloat() {
            float left = leftC;
            Performer right = rightP;
            @Override
            public float performFloat(PetalProvider provider) {
                return left * right.performFloat(provider);
            }
        };
    }
    private static Performer performerFloat(Performer leftP, Performer rightP){
        return new PerformerFloat() {
            Performer left = leftP;
            Performer right = rightP;
            @Override
            public float performFloat(PetalProvider provider) {
                return left.performFloat(provider) * right.performFloat(provider);
            }
        };
    }
    private static Performer performerFloat(float leftC, float rightC){
        return new PerformerFloat() {
            float left = leftC;
            float right = rightC;
            @Override
            public float performFloat(PetalProvider provider) {
                return left * right;
            }
        };
    }
    private static Performer performerBigDecimal(Performer leftP, BigDecimal rightC){
        return new PerformerBigDecimal() {
            Performer left = leftP;
            BigDecimal right = rightC;
            @Override
            public BigDecimal performBigDecimal(PetalProvider provider) {
                return left.performBigDecimal(provider).multiply(right);
            }
        };
    }
    private static Performer performerBigDecimal(BigDecimal leftC, Performer rightP){
        return new PerformerBigDecimal() {
            BigDecimal left = leftC;
            Performer right = rightP;
            @Override
            public BigDecimal performBigDecimal(PetalProvider provider) {
                return left.multiply(right.performBigDecimal(provider));
            }
        };
    }
    private static Performer performerBigDecimal(Performer leftP, Performer rightP){
        return new PerformerBigDecimal() {
            Performer left = leftP;
            Performer right = rightP;
            @Override
            public BigDecimal performBigDecimal(PetalProvider provider) {
                return left.performBigDecimal(provider).multiply(right.performBigDecimal(provider));
            }
        };
    }
    private static Performer performerBigDecimal(BigDecimal leftC, BigDecimal rightC){
        return new PerformerBigDecimal() {
            BigDecimal left = leftC;
            BigDecimal right = rightC;
            @Override
            public BigDecimal performBigDecimal(PetalProvider provider) {
                return left.multiply(right);
            }
        };
    }
    @Override
    Const unitConst(Const left, Const right) throws ParseELException {
        switch(adjustType(left.getType(), right.getType())){
            case BIG_DECIMAL: return new Const(left.asBigDecimal().multiply(right.asBigDecimal()), EnumEL.BIG_DECIMAL);
            case BIG_INTEGER: return new Const(left.asBigInteger().multiply(right.asBigInteger()), EnumEL.BIG_INTEGER);
            case DOUBLE: return new Const(left.asDouble() * right.asDouble(), EnumEL.DOUBLE);
            case FLOAT: return new Const(left.asFloat() * right.asFloat(), EnumEL.FLOAT);
            case INT: return new Const(left.asInt() * right.asInt(), EnumEL.INT);
            case LONG: return new Const(left.asLong() * right.asLong(), EnumEL.LONG);
            case STRING: StringBuilder sb = new StringBuilder();
                        if(left.getType() == EnumEL.STRING) {
                            for(int i = 0; i < right.asInt(); i++){                        
                                sb.append(left.asString());
                            }
                        }else{
                            for(int i = 0; i < left.asInt(); i++){                        
                                sb.append(right.asString());
                            }
                        }
                        return new Const(sb.toString(), EnumEL.STRING);
            default: throw new ParseELException(0, "Method is unsupported");
        }
    }
}
