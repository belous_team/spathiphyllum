/*
 * Copyright (C) 2018-2018 The Project Spathiphyllum Authors.
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
package org.spathiphyllum.el.plugins;

import java.lang.reflect.Method;
import java.lang.reflect.Modifier;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;

import org.spathiphyllum.el.AbstractComponent;
import org.spathiphyllum.el.EnumEL;
import org.spathiphyllum.el.HelperEL.BaseExtendPlugin;
import org.spathiphyllum.el.HelperEL.BaseOperatorForPlugin;
import org.spathiphyllum.el.InvokeELException;
import org.spathiphyllum.el.ParseELException;
import org.spathiphyllum.el.Performer;
import org.spathiphyllum.el.PetalProvider;
import org.spathiphyllum.el.tools.HelperStringParser;
import org.spathiphyllum.el.tools.MarkedLinkedList;
import org.spathiphyllum.el.tools.MarkedLinkedList.Mark;

/**
 * @author dbilous
 *
 */
public class MathPlugin extends BaseExtendPlugin {
    
    private Map<String, Function> methods;
    /**
     * 
     */
    public MathPlugin() {
        methods = new HashMap<>();
        Arrays.stream(Math.class.getDeclaredMethods()).filter(m -> isMethodAviable(m))
                                                .forEach(m -> methods.put(m.getName(), Function.makeFunction(m)));
    }
    private static boolean isMethodAviable(Method m) {
        if(Modifier.isStatic(m.getModifiers()) && Modifier.isPublic(m.getModifiers()) && !m.getReturnType().equals(Void.TYPE)){
            try {
                elType(m.getReturnType());
                for(Class<?> p:m.getParameterTypes()) {
                    elType(p);
                }
            } catch (ParseELException e) {
                return false;
            }
            return true;
        }
        return false;
    }
    @Override
    public int readOperand(char[] el, int pos, MarkedLinkedList<AbstractComponent> stack, Mark<AbstractComponent> mark,
            PetalProvider petalProvider) throws ParseELException {
        int i = HelperStringParser.skipSpace(el, pos, true, "Operand must be present");
        while(i < el.length && (el[i] >= 'A' && el[i] <= 'Z' || el[i] >= 'a' && el[i] <= 'z' || el[i] >= '0' && el[i] <= '9')) {
            i++;
        }
        if(i < el.length && el[i] == '('){
            String name = new String(el, pos, i-pos);
            Function method = methods.get(name);
            if(method == null){
                throw new ParseELException(pos, "Unknown function: " + name);
            }
            i++;
            for(int f = 0; f < method.parameterCount; f++){
                Mark<AbstractComponent> newMark = stack.addMark(mark);
                i = readExpression(el, i, stack, newMark, petalProvider, this);
                stack.removeMark(newMark);
                if(f < method.parameterCount - 1) {
                    if(el[i] == ','){
                        ++i;
                    }else{
                        throw new ParseELException(i, "No closing parenthesis");
                    }
                }
            }
            if(el[i] == ')'){
                ++i;
            }else{
                throw new ParseELException(i, "No closing parenthesis");
            }
            stack.push(method);
            return HelperStringParser.skipSpace(el, i, false, null);
        }
        return pos;
    }
    static EnumEL elType(Class<?> clazz) throws ParseELException {
        if(clazz == int.class || clazz == Integer.class) {
            return EnumEL.INT;
        }else if(clazz == BigInteger.class) {
            return EnumEL.BIG_INTEGER;
        }else if(clazz == BigDecimal.class) {
            return EnumEL.BIG_DECIMAL;
        }else if(clazz == boolean.class || clazz == Boolean.class) {
            return EnumEL.BOOLEAN;
        }else if(clazz == byte.class || clazz == Byte.class) {
            return EnumEL.BYTE;
        }else if(clazz == char.class || clazz == Character.class) {
            return EnumEL.CHAR;
        }else if(clazz == double.class || clazz == Double.class) {
            return EnumEL.DOUBLE;
        }else if(clazz == float.class || clazz == Float.class) {
            return EnumEL.FLOAT;
        }else if(clazz == long.class || clazz == Long.class) {
            return EnumEL.LONG;
        }else if(clazz == short.class || clazz == Short.class) {
            return EnumEL.SHORT;
        }else if(clazz == String.class) {
            return EnumEL.STRING;
        }else{
            throw new ParseELException(0, "");
        }
    }
    private static Object[] getArguments(PetalProvider provider, Method method, Performer[] parameters) throws ParseELException {
        Class<?>[] a = method.getParameterTypes();
        Object[] args = new Object[parameters.length];
        for(int i = 0; i < parameters.length; i++){
            switch(elType(a[i])) {
            case BIG_INTEGER: args[i] = parameters[i].performBigInteger(provider);
                break;
            case BIG_DECIMAL: args[i] = parameters[i].performBigDecimal(provider);
                break;
            case BOOLEAN: args[i] = parameters[i].performBoolean(provider);
                break;
            case BYTE: args[i] = parameters[i].performByte(provider);
                break;
            case CHAR: args[i] = parameters[i].performChar(provider);
                break;
            case DOUBLE: args[i] = parameters[i].performDouble(provider);
                break;
            case FLOAT: args[i] = parameters[i].performFloat(provider);
                break;
            case INT: args[i] = parameters[i].performInt(provider);
                break;
            case LONG: args[i] = parameters[i].performLong(provider);
                break;
            case SHORT: args[i] = parameters[i].performShort(provider);
                break;
            case STRING: args[i] = parameters[i].performString(new StringBuilder(), provider);
                break;
            default: throw new InvokeELException("Method is unsupported");
            }
            
        }
        return args;
    }
    static class Function extends BaseOperatorForPlugin{
        Method method;
        int parameterCount;
        private Function(String value, EnumEL type){
            super(value, type);
        }
        static Function makeFunction(Method method) {
            EnumEL el;
            try {
                el = elType(method.getReturnType());
            } catch (ParseELException e) {
                return null;
            }
            Function f = new Function(method.getName(), el);
            f.method = method;
            f.parameterCount = method.getParameterCount();
            return f;
        }
        @Override
        protected int getParameterCount() {
            return parameterCount;
        }
        @Override
        protected Performer compile(Performer[] parameters) throws ParseELException {
            switch (getType()) {
            case BIG_DECIMAL:
                return performerBigDecimal(parameters);
            case BIG_INTEGER:
                return performerBigInteger(parameters);
            case DOUBLE:
                return performerDouble(parameters);
            case FLOAT:
                return performerFloat(parameters);
            case INT:
                return performerInt(parameters);
            case LONG:
                return performerLong(parameters);
            case STRING:
                return performerString(parameters);
            default: throw new ParseELException(0, "Compile error");
            }
        }
        /**
         * @param parameters
         * @return
         */
        private Performer performerString(Performer[] parameters) {
            return new Performer() {
                @Override
                public StringBuilder performString(StringBuilder sb, PetalProvider provider) {
                    try {
                        return sb.append(method.invoke(null, getArguments(provider, method, parameters)));
                    } catch (Exception e) {
                        throw new InvokeELException(e);
                    }
                }
                @Override
                public EnumEL type() {
                    return getType();
                }
            };
        }

        /**
         * @param parameters
         * @return
         */
        private Performer performerLong(Performer[] parameters) {
            return new Performer() {
                @Override
                public long performLong(PetalProvider provider) {
                    try {
                        return (long) method.invoke(null, getArguments(provider, method, parameters));
                    } catch (Exception e) {
                        throw new InvokeELException(e);
                    }
                }
                @Override
                public EnumEL type() {
                    return getType();
                }
            };
        }

        /**
         * @param parameters
         * @return
         */
        private Performer performerInt(Performer[] parameters) {
            return new Performer() {
                @Override
                public int performInt(PetalProvider provider) {
                    try {
                        return (int) method.invoke(null, getArguments(provider, method, parameters));
                    } catch (Exception e) {
                        throw new InvokeELException(e);
                    }
                }
                @Override
                public EnumEL type() {
                    return getType();
                }
            };
        }

        /**
         * @param parameters
         * @return
         */
        private Performer performerFloat(Performer[] parameters) {
            return new Performer() {
                @Override
                public float performFloat(PetalProvider provider) {
                    try {
                        return (float) method.invoke(null, getArguments(provider, method, parameters));
                    } catch (Exception e) {
                        throw new InvokeELException(e);
                    }
                }
                @Override
                public EnumEL type() {
                    return getType();
                }
            };
        }

        /**
         * @param parameters
         * @return
         */
        private Performer performerDouble(Performer[] parameters) {
            return new Performer() {
                @Override
                public double performDouble(PetalProvider provider) {
                    try {
                        return (double) method.invoke(null, getArguments(provider, method, parameters));
                    } catch (Exception e) {
                        throw new InvokeELException(e);
                    }
                }
                @Override
                public EnumEL type() {
                    return getType();
                }
            };
        }

        /**
         * @param parameters
         * @return
         */
        private Performer performerBigInteger(Performer[] parameters) {
            return new Performer() {
                @Override
                public BigInteger performBigInteger(PetalProvider provider) {
                    try {
                        return (BigInteger) method.invoke(null, getArguments(provider, method, parameters));
                    } catch (Exception e) {
                        throw new InvokeELException(e);
                    }
                }
                @Override
                public EnumEL type() {
                    return getType();
                }
            };
        }

        /**
         * @param parameters
         * @return
         */
        private Performer performerBigDecimal(Performer[] parameters) {
            return new Performer() {
                @Override
                public BigDecimal performBigDecimal(PetalProvider provider) {
                    try {
                        return (BigDecimal) method.invoke(null, getArguments(provider, method, parameters));
                    } catch (Exception e) {
                        throw new InvokeELException(e);
                    }
                }
                @Override
                public EnumEL type() {
                    return getType();
                }
            };
        }
    }
}
