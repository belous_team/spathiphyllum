/*
 * Copyright (C) 2018-2018 The Project Spathiphyllum Authors.
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
package org.spathiphyllum.el;

import java.math.BigInteger;

import org.spathiphyllum.el.HelperEL.Const;
import org.spathiphyllum.el.HelperEL.Operator;
/**
 * Shift right
 * @author obelous
 *
 */
public class SHR extends Operator {
    static final Operator OPERATOR = new SHR();
    private SHR() {
        super("OR", EnumEL.NUMBER);
    }
    @Override
    int priority() {
        return 9;
    }
    @Override
    EnumEL adjustType(EnumEL left, EnumEL right) throws ParseELException {
        if(left.isNumber() && right.isNumber()){
            if(left == EnumEL.BIG_DECIMAL || right == EnumEL.BIG_DECIMAL){
                throw new ParseELException(0, "Operator '>>' can not be assigned to BigDecimal");
            }else if(left == EnumEL.DOUBLE || right == EnumEL.DOUBLE){
                throw new ParseELException(0, "Operator '>>' can not be assigned to double");
            }else if(left == EnumEL.FLOAT || right == EnumEL.FLOAT){
                throw new ParseELException(0, "Operator '>>' can not be assigned to float");
            }
            return HelperEL.approachNumerTypes(left, left);//only left. right will be taken as integer
        }else{
            throw new ParseELException(0, "Operator '>>' can not be assigned numbers only");
        }
    }
    @Override
    Operator adjustOperator(int pos, AbstractComponent left, AbstractComponent right) throws ParseELException {
        if(left.getType().isNumber() && right.getType().isNumber()){
            return OPERATOR;
        }else{
            throw new ParseELException(pos, "Operator '>>' can not be assigned numbers only");
        }
    }
    @Override
    Performer compile(EnumEL performerType, Performer leftP, Const leftC, Performer rightP, Const rightC)
            throws ParseELException {
        if(leftC != null && rightC != null){
            switch (performerType) {
            case BIG_INTEGER:
                return performerBigInteger(leftC.asBigInteger(), rightC.asInt());
            case INT:
                return performerInt(leftC.asInt(), rightC.asInt());
            case LONG:
                return performerLong(leftC.asLong(), rightC.asInt());
            case BYTE:
                return performerByte(leftC.asByte(), rightC.asByte());
            case CHAR:
                return performerChar(leftC.asChar(), rightC.asChar());
            case SHORT:
                return performerShort(leftC.asShort(), rightC.asShort());
            default:
            }
        }else if(leftP != null && rightP != null){
            switch (performerType) {
            case BIG_INTEGER:
                return performerBigInteger(leftP, rightP);
            case INT:
                return performerInt(leftP, rightP);
            case LONG:
                return performerLong(leftP, rightP);
            case BYTE:
                return performerByte(leftP, rightP);
            case CHAR:
                return performerChar(leftP, rightP);
            case SHORT:
                return performerShort(leftP, rightP);
            default:
            }
        }else if(leftC != null && rightP != null){
            switch (performerType) {
            case BIG_INTEGER:
                return performerBigInteger(leftC.asBigInteger(), rightP);
            case INT:
                return performerInt(leftC.asInt(), rightP);
            case LONG:
                return performerLong(leftC.asLong(), rightP);
            case BYTE:
                return performerByte(leftC.asByte(), rightP);
            case CHAR:
                return performerChar(leftC.asChar(), rightP);
            case SHORT:
                return performerShort(leftC.asShort(), rightP);
            default:
            }
        }else if(leftP != null && rightC != null){
            switch (performerType) {
            case BIG_INTEGER:
                return performerBigInteger(leftP, rightC.asInt());
            case INT:
                return performerInt(leftP, rightC.asInt());
            case LONG:
                return performerLong(leftP, rightC.asInt());
            case BYTE:
                return performerByte(leftP, rightC.asByte());
            case CHAR:
                return performerChar(leftP, rightC.asChar());
            case SHORT:
                return performerShort(leftP, rightC.asShort());
            default:
            }
        }
        throw new ParseELException(0, "Compile error");    
    }
    private static Performer performerShort(Performer leftP, short rightC) {
        return new PerformerShort(){
            Performer left = leftP;
            short right = rightC;
            @Override
            public short performShort(PetalProvider provider) {
                return (short) (left.performShort(provider) >> right);
            }
        };
    }
    private static Performer performerChar(Performer leftP, char rightC) {
        return new PerformerChar(){
            Performer left = leftP;
            char right = rightC;
            @Override
            public char performChar(PetalProvider provider) {
                return (char) (left.performChar(provider) >> right);
            }
        };
    }
    private static Performer performerByte(Performer leftP, byte rightC) {
        return new PerformerByte(){
            Performer left = leftP;
            byte right = rightC;
            @Override
            public byte performByte(PetalProvider provider) {
                return (byte) (left.performByte(provider) >> right);
            }
        };
    }
    private static Performer performerLong(Performer leftP, int rightC) {
        return new PerformerLong(){
            Performer left = leftP;
            int right = rightC;
            @Override
            public long performLong(PetalProvider provider) {
                return left.performLong(provider) >> right;
            }
        };
    }
    private static Performer performerInt(Performer leftP, int rightC) {
        return new PerformerInt(){
            Performer left = leftP;
            int right = rightC;
            @Override
            public int performInt(PetalProvider provider) {
                return left.performInt(provider) >> right;
            }
        };
    }
    private static Performer performerBigInteger(Performer leftP, int rightC) {
        return new PerformerBigInteger(){
            Performer left = leftP;
            int right = rightC;
            @Override
            public BigInteger performBigInteger(PetalProvider provider) {
                return left.performBigInteger(provider).shiftRight(right);
            }
        };
    }
    private static Performer performerShort(short leftC, Performer rightP) {
        return new PerformerShort(){
            short left = leftC;
            Performer right = rightP;
            @Override
            public short performShort(PetalProvider provider) {
                return (short) (left >> right.performShort(provider));
            }
        };
    }
    private static Performer performerChar(char leftC, Performer rightP) {
        return new PerformerChar(){
            char left = leftC;
            Performer right = rightP;
            @Override
            public char performChar(PetalProvider provider) {
                return (char) (left >> right.performChar(provider));
            }
        };
    }
    private static Performer performerByte(byte leftC, Performer rightP) {
        return new PerformerByte(){
            byte left = leftC;
            Performer right = rightP;
            @Override
            public byte performByte(PetalProvider provider) {
                return (byte) (left >> right.performByte(provider));
            }
        };
    }
    private static Performer performerLong(long leftC, Performer rightP) {
        return new PerformerLong(){
            long left = leftC;
            Performer right = rightP;
            @Override
            public long performLong(PetalProvider provider) {
                return left >> right.performInt(provider);
            }
        };
    }
    private static Performer performerInt(int leftC, Performer rightP) {
        return new PerformerInt(){
            int left = leftC;
            Performer right = rightP;
            @Override
            public int performInt(PetalProvider provider) {
                return left >> right.performInt(provider);
            }
        };
    }
    private static Performer performerBigInteger(BigInteger leftC, Performer rightP) {
        return new PerformerBigInteger(){
            BigInteger left = leftC;
            Performer right = rightP;
            @Override
            public BigInteger performBigInteger(PetalProvider provider) {
                return left.shiftRight(right.performInt(provider));
            }
        };
    }
    private static Performer performerShort(Performer leftP, Performer rightP) {
        return new PerformerShort(){
            Performer left = leftP;
            Performer right = rightP;
            @Override
            public short performShort(PetalProvider provider) {
                return (short) (left.performShort(provider) >> right.performShort(provider));
            }
        };
    }
    private static Performer performerChar(Performer leftP, Performer rightP) {
        return new PerformerChar(){
            Performer left = leftP;
            Performer right = rightP;
            @Override
            public char performChar(PetalProvider provider) {
                return (char) (left.performChar(provider) >> right.performChar(provider));
            }
        };
    }
    private static Performer performerByte(Performer leftP, Performer rightP) {
        return new PerformerByte(){
            Performer left = leftP;
            Performer right = rightP;
            @Override
            public byte performByte(PetalProvider provider) {
                return (byte) (left.performByte(provider) >> right.performByte(provider));
            }
        };
    }
    private static Performer performerLong(Performer leftP, Performer rightP) {
        return new PerformerLong(){
            Performer left = leftP;
            Performer right = rightP;
            @Override
            public long performLong(PetalProvider provider) {
                return left.performLong(provider) >> right.performInt(provider);
            }
        };
    }
    private static Performer performerInt(Performer leftP, Performer rightP) {
        return new PerformerInt(){
            Performer left = leftP;
            Performer right = rightP;
            @Override
            public int performInt(PetalProvider provider) {
                return left.performInt(provider) >> right.performInt(provider);
            }
        };
    }
    private static Performer performerBigInteger(Performer leftP, Performer rightP) {
        return new PerformerBigInteger(){
            Performer left = leftP;
            Performer right = rightP;
            @Override
            public BigInteger performBigInteger(PetalProvider provider) {
                return left.performBigInteger(provider).shiftRight(right.performInt(provider));
            }
        };
    }
    private static Performer performerShort(short leftC, short rightC) {
        return new PerformerShort(){
            short left = leftC;
            short right = rightC;
            @Override
            public short performShort(PetalProvider provider) {
                return (short) (left >> right);
            }
        };
    }
    private static Performer performerChar(char leftC, char rightC) {
        return new PerformerChar(){
            char left = leftC;
            char right = rightC;
            @Override
            public char performChar(PetalProvider provider) {
                return (char) (left >> right);
            }
        };
    }
    private static Performer performerByte(byte leftC, byte rightC) {
        return new PerformerByte(){
            byte left = leftC;
            byte right = rightC;
            @Override
            public byte performByte(PetalProvider provider) {
                return (byte) (left >> right);
            }
        };
    }
    private static Performer performerLong(long leftC, int rightC) {
        return new PerformerLong(){
            long left = leftC;
            int right = rightC;
            @Override
            public long performLong(PetalProvider provider) {
                return left >> right;
            }
        };
    }
    private static Performer performerInt(int leftC, int rightC) {
        return new PerformerInt(){
            int left = leftC;
            int right = rightC;
            @Override
            public int performInt(PetalProvider provider) {
                return left >> right;
            }
        };
    }
    private static Performer performerBigInteger(BigInteger leftC, int rightC) {
        return new PerformerBigInteger(){
            BigInteger left = leftC; 
            int right = rightC;
            @Override
            public BigInteger performBigInteger(PetalProvider provider) {
                return left.shiftRight(right);
            }
        };
    }
    @Override
    Const unitConst(Const left, Const right) throws ParseELException {
        switch(adjustType(left.getType(), right.getType())){
            case BIG_INTEGER: return new Const(left.asBigInteger().shiftRight(right.asInt()), EnumEL.BIG_INTEGER);
            case BYTE: return new Const(left.asByte() >> right.asByte(), EnumEL.BYTE);
            case CHAR: return new Const(left.asChar() >> right.asChar(), EnumEL.CHAR);
            case INT: return new Const(left.asInt() >> right.asInt(), EnumEL.INT);
            case LONG: return new Const(left.asLong() >> right.asInt(), EnumEL.LONG);
            case SHORT: return new Const(left.asShort() >> right.asShort(), EnumEL.SHORT);
            default: throw new ParseELException(0, "Method is unsupported");
        }
    }
}
