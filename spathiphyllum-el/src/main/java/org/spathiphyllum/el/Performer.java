/*
 * Copyright (C) 2018-2018 The Project Spathiphyllum Authors.
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
package org.spathiphyllum.el;

import java.math.BigDecimal;
import java.math.BigInteger;

/**
 * The interface implements telescoping inquiry<br>
 * How does it work?<br>
 * Expression: (3 + 2)*2 + " is " + 2*(2 + 3)
 * <ul>
 * <li> will be parsed to: [add [add [mul [add 3,2] 2] "is"] [mul 2 [add 2, 3]]]
 * <li> compiled to perfomer:<br>
 * <pre>
 * add[String].performString(
 *      add[String].performString(
 *              mul[byte].performString() --> mul[byte].performByte(add[byte](3,2),2),
 *              " is "), 
 *      mul[byte].perfomByte(2, add[byte].performByte(2,3))) 
 * </pre>
 * </ul>
 * @author obelous
 *
 */
public interface Performer {
    EnumEL type();
    /**
     * Performs EL expression
     * This method will be called by PerformerInt
     * @param provider
     * @return
     * @throws ParseELException 
     * @throws InvokeELException
     */
    default int performInt(PetalProvider provider){
        switch (type()) {
        case BYTE:
            return performByte(provider);
        case SHORT:
            return performShort(provider);
        case CHAR:
            return performChar(provider);
        case LONG:
            return (int) performLong(provider);
        case BIG_DECIMAL:
            return performBigDecimal(provider).intValue();
        case BIG_INTEGER:
            return performBigInteger(provider).intValue();
        case DOUBLE:
            return (int) performDouble(provider);
        case FLOAT:
            return (int) performFloat(provider);
        default:
            throw new InvokeELException("Method is unsupported");
        }
    }
    /**
     * Performs EL expression
     * This method will be called by PerformerByte
     * @param provider
     * @return
     * @throws ParseELException 
     * @throws InvokeELException
     */
    default byte performByte(PetalProvider provider){
        switch (type()) {
        case INT:
            return (byte) performInt(provider);
        case SHORT:
            return (byte) performShort(provider);
        case CHAR:
            return (byte) performChar(provider);
        case LONG:
            return (byte) performLong(provider);
        case BIG_DECIMAL:
            return performBigDecimal(provider).byteValue();
        case BIG_INTEGER:
            return performBigInteger(provider).byteValue();
        case DOUBLE:
            return (byte) performDouble(provider);
        case FLOAT:
            return (byte) performFloat(provider);
        default:
            throw new InvokeELException("Method is unsupported");
        }
    }
    /**
     * Performs EL expression
     * This method will be called by PerformerShort
     * @param provider
     * @return
     * @throws InvokeELException
     */
    default short performShort(PetalProvider provider){
        switch (type()) {
        case INT:
            return (byte) performInt(provider);
        case CHAR:
            return (byte) performChar(provider);
        case LONG:
            return (byte) performLong(provider);
        case BIG_DECIMAL:
            return performBigDecimal(provider).shortValue();
        case BIG_INTEGER:
            return performBigInteger(provider).shortValue();
        case DOUBLE:
            return (byte) performDouble(provider);
        case FLOAT:
            return (byte) performFloat(provider);
        case BYTE:
            return performByte(provider);
        default:
            throw new InvokeELException("Method is unsupported");
        }
    }
    /**
     * Performs EL expression
     * This method will be called by PerformerChar
     * @param provider
     * @return
     * @throws InvokeELException
     */
    default char performChar(PetalProvider provider){
        switch (type()) {
        case INT:
            return (char) performInt(provider);
        case SHORT:
            return (char) performShort(provider);
        case BYTE:
            return (char) performByte(provider);
        case LONG:
            return (char) performLong(provider);
        case BIG_DECIMAL:
            return (char) performBigDecimal(provider).intValue();
        case BIG_INTEGER:
            return (char) performBigInteger(provider).intValue();
        case DOUBLE:
            return (char) performDouble(provider);
        case FLOAT:
            return (char) performFloat(provider);
        default:
            throw new InvokeELException("Method is unsupported");
        }
    }
    /**
     * Performs EL expression
     * This method will be called by PerformerFloat
     * @param provider
     * @return
     * @throws InvokeELException
     */
    default float performFloat(PetalProvider provider){
        switch (type()) {
        case INT:
            return performInt(provider);
        case SHORT:
            return performShort(provider);
        case CHAR:
            return performChar(provider);
        case LONG:
            return performLong(provider);
        case BIG_DECIMAL:
            return performBigDecimal(provider).floatValue();
        case BIG_INTEGER:
            return performBigInteger(provider).floatValue();
        case DOUBLE:
            return (float) performDouble(provider);
        case BYTE:
            return performFloat(provider);
        default:
            throw new InvokeELException("Method is unsupported");
        }
    }
    /**
     * Performs EL expression
     * This method will be called by PerformerLong
     * @param provider
     * @return
     * @throws InvokeELException
     */
    default long performLong(PetalProvider provider){
        switch (type()) {
        case INT:
            return performInt(provider);
        case SHORT:
            return performShort(provider);
        case CHAR:
            return performChar(provider);
        case FLOAT:
            return (long) performFloat(provider);
        case BIG_DECIMAL:
            return performBigDecimal(provider).longValue();
        case BIG_INTEGER:
            return performBigInteger(provider).longValue();
        case DOUBLE:
            return (long) performDouble(provider);
        case BYTE:
            return performByte(provider);
        default:
            throw new InvokeELException("Method is unsupported");
        }
    }
    /**
     * Performs EL expression
     * This method will be called by PerformerDouble
     * @param provider
     * @return
     * @throws InvokeELException
     */
    default double performDouble(PetalProvider provider){
        switch (type()) {
        case INT:
            return performInt(provider);
        case SHORT:
            return performShort(provider);
        case CHAR:
            return performChar(provider);
        case LONG:
            return performLong(provider);
        case BIG_DECIMAL:
            return performBigDecimal(provider).doubleValue();
        case BIG_INTEGER:
            return performBigInteger(provider).doubleValue();
        case FLOAT:
            return performFloat(provider);
        case BYTE:
            return performByte(provider);
        default:
            throw new InvokeELException("Method is unsupported");
        }
    }
    /**
     * Performs EL expression
     * This method will be called by PerformerBiDecimal
     * @param provider
     * @return
     * @throws InvokeELException
     */
    default BigDecimal performBigDecimal(PetalProvider provider){
        switch (type()) {
        case INT:
            return BigDecimal.valueOf(performInt(provider));
        case SHORT:
            return BigDecimal.valueOf(performShort(provider));
        case CHAR:
            return BigDecimal.valueOf(performChar(provider));
        case LONG:
            return BigDecimal.valueOf(performLong(provider));
        case DOUBLE:
            return BigDecimal.valueOf(performDouble(provider));
        case BIG_INTEGER:
            return new BigDecimal(performBigInteger(provider));
        case FLOAT:
            return BigDecimal.valueOf(performFloat(provider));
        case BYTE:
            return BigDecimal.valueOf(performByte(provider));
        default:
            throw new InvokeELException("Method is unsupported");
        }
    }
    /**
     * Performs EL expression
     * This method will be called by PerformerBigInteger
     * @param provider
     * @return
     * @throws InvokeELException
     */
    default BigInteger performBigInteger(PetalProvider provider){
        switch (type()) {
        case INT:
            return BigInteger.valueOf(performInt(provider));
        case SHORT:
            return BigInteger.valueOf(performShort(provider));
        case CHAR:
            return BigInteger.valueOf(performChar(provider));
        case LONG:
            return BigInteger.valueOf(performLong(provider));
        case DOUBLE:
            return BigDecimal.valueOf(performDouble(provider)).toBigInteger();
        case BIG_DECIMAL:
            return performBigDecimal(provider).toBigInteger();
        case FLOAT:
            return BigDecimal.valueOf(performFloat(provider)).toBigInteger();
        case BYTE:
            return BigInteger.valueOf(performByte(provider));
        default:
            throw new InvokeELException("Method is unsupported");
        }
    }
    /**
     * Performs EL expression
     * This method will be called by PerformerBoolean
     * @param provider
     * @return
     * @throws InvokeELException
     */
    default boolean performBoolean(PetalProvider provider){
        throw new InvokeELException("Method is unsupported");
    }
    /**
     * Performs EL expression
     * This method will be called by PerformerString
     * @param sb
     * @param provider
     * @return
     * @throws InvokeELException
     */
    default StringBuilder performString(StringBuilder sb, PetalProvider provider){
        if(sb == null){
            sb = new StringBuilder();
        }
        switch (type()) {
        case INT:
            sb.append(performInt(provider));
            break;
        case SHORT:
            sb.append(performShort(provider));
            break;
        case CHAR:
            sb.append(performChar(provider));
            break;
        case LONG:
            sb.append(performLong(provider));
            break;
        case BIG_DECIMAL:
            sb.append(performBigDecimal(provider).toPlainString());
            break;
        case BIG_INTEGER:
            sb.append(performBigInteger(provider).toString());
            break;
        case DOUBLE:
            sb.append(performDouble(provider));
            break;
        case FLOAT:
            sb.append(performFloat(provider));
            break;
        case BOOLEAN:
            sb.append(performBoolean(provider));
            break;
        case BYTE:
            sb.append(performByte(provider));
            break;
        default:
            throw new InvokeELException("Method is unsupported");
        }
        return sb;
    }
}
