/*
 * Copyright (C) 2018-2018 The Project Spathiphyllum Authors.
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
package org.spathiphyllum.el;

import java.math.BigDecimal;
import java.math.BigInteger;

/**
 * @author obelous
 *
 */
public interface PetalProvider {
    /**
     * Get type of this petal
     * @param petalName
     * @return
     */
    EnumEL getType(String petalName);
    /**
     * If this petal is constant. Can be cached
     * @param petalName
     * @return
     */
    boolean isConstant(String petalName);
    /**
     * Is this name is valid
     * @param petalName
     * @return
     */
    boolean isValid(String petalName);
    /**
     * get petal's value
     */
    BigDecimal asBigDecimal(String petalName);
    BigInteger asBigInteger(String petalName);
    boolean asBoolean(String petalName);
    String asString(String petalName);
    byte asByte(String petalName);
    short asShort(String petalName);
    char asChar(String petalName);
    int asInt(String petalName);
    long asLong(String petalName);
    float asFloat(String petalName);
    double asDouble(String petalName);
}
