/*
 * Copyright (C) 2018-2018 The Project Spathiphyllum Authors.
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
package org.spathiphyllum.el;

import org.spathiphyllum.el.HelperEL.Const;
import org.spathiphyllum.el.HelperEL.Operator;
import org.spathiphyllum.el.HelperEL.UnaryOperator;
import org.spathiphyllum.el.tools.MarkedLinkedList;
/**
 * Ternary true's part
 * @author obelous
 *
 */
public class TerTrue extends UnaryOperator {
    static final Operator OPERATOR = new TerTrue();
    private TerTrue(){
        super("TERTRUE", EnumEL.SOP);
    }
    @Override
    int priority() {
        return 1;
    }
    @Override
    EnumEL adjustType(EnumEL left, EnumEL right) throws ParseELException {
        throw new ParseELException(0, "Incorrect algoritm");
    }
    @Override
    Operator adjustOperator(int pos, AbstractComponent left, AbstractComponent right) throws ParseELException {
        throw new ParseELException(0, "Incorrect algoritm");
    }
    @Override
    Performer compile(EnumEL performerType, Performer leftP, Const leftC, Performer rightP, Const rightC)
            throws ParseELException {
        throw new ParseELException(0, "Incorrect algoritm");
    }
    @Override
    Performer compile(MarkedLinkedList<AbstractComponent> list) throws ParseELException {
        AbstractComponent leftA = list.pop();
        if(leftA instanceof Operator){
            return ((Operator) leftA).compile(list);
        }else{
            return ((Const) leftA).asPerformer();
        }
    }
}
