/*
 * Copyright (C) 2018-2018 The Project Spathiphyllum Authors.
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
package org.spathiphyllum.el;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.Deque;

import org.spathiphyllum.el.HelperEL.BaseExtendPlugin;
import org.spathiphyllum.el.HelperEL.Const;
import org.spathiphyllum.el.HelperEL.Operator;
import org.spathiphyllum.el.tools.MarkedLinkedList;

/**
 * @author obelous
 *
 */
public class EL {
    private static boolean optimisationDisabled = Boolean.getBoolean("org.spathiphyllum.el.disableOptimisation");
    public static EL build(String expression, PetalProvider provider, BaseExtendPlugin plugin) throws ParseELException {
        Deque<AbstractComponent> compile = HelperEL.parseEL(expression, provider, plugin);
        EL el = new EL();
        el.provider = provider;
        el.data.compileInfo = compile.toString();
        if(!optimisationDisabled){
            compile = optimisation(compile);
        }
        AbstractComponent component = compile.pop();
        if(component instanceof Operator){
            el.data.performer = ((Operator) component).compile(compile);
        }else{
            el.data.compileInfo = component.toString();
            el.data.performer = ((Const)component).asPerformer();
        }
        return el;
    }
    private Data data;
    private PetalProvider provider;
    private EL(){
        data = new Data();
    }
    private EL(EL el){
        data = el.data;
    }
    public EL clone(PetalProvider provider){
        EL el = new EL(this);
        el.provider = provider;
        return el;
    }
    private static class Data {
        Performer performer;
        String compileInfo;
    }
    public String printDebug() {
        return data.compileInfo;
    }
    /**
     * Check if type is compatible
     * @param type
     * @return
     */
    boolean ifCompatibleType(EnumEL type) {
        if(type == null){
            return false;
        }else{
            return type.isString() || (type.isBoolean() && data.performer.type().isBoolean())
                    ||(type.isNumber() && data.performer.type().isNumber());
        }
    }
    /**
     * get petal's value
     */
    public BigDecimal asBigDecimal(){
        return data.performer.performBigDecimal(provider);
    }
    public BigInteger asBigInteger(){
        return data.performer.performBigInteger(provider);
    }
    public boolean asBoolean() {
        return data.performer.performBoolean(provider);
    }
    public String asString(){
        return data.performer.performString(null, provider).toString();
    }
    public StringBuilder asString(StringBuilder sb){
        return data.performer.performString(sb, provider);
    }
    public byte asByte() {
        return data.performer.performByte(provider);
    }
    public short asShort() {
        return data.performer.performShort(provider);
    }
    public char asChar() {
        return data.performer.performChar(provider);
    }
    public int asInt() {
        return data.performer.performInt(provider);
    }
    public long asLong() {
        return data.performer.performLong(provider);
    }
    public float asFloat() {
        return data.performer.performFloat(provider);
    }
    public double asDouble() {
        return data.performer.performDouble(provider);
    }
    private static Deque<AbstractComponent> optimisation(Deque<AbstractComponent> list) throws ParseELException{
        if(list.peek() instanceof Operator && ((Operator) list.peek()).isOptimizable()){
            Operator op = (Operator) list.pop();
            Deque<AbstractComponent> leftList = null;
            Deque<AbstractComponent> rightList = null;
            Const leftC = null;
            Const rightC = null;
            Deque<AbstractComponent> listResult = new MarkedLinkedList<>();
            if(list.peek() instanceof Operator){
                leftList = optimisation(list);
            }else{
                leftC = (Const) list.pop();
            }
            if(leftList == null && leftC == null) {
                throw new InvokeELException("Error in extracting left operand in optimization");
            }
            if(op == Ternary.OPERATOR){
                list.pop();
                boolean isFalse;
                if(leftList == null){
                    if(leftC.isConstant()){
                        isFalse = !leftC.asBoolean();
                    }else{
                        listResult.push(op);
                        listResult.add(leftC);
                        listResult.add(TerTrue.OPERATOR);
                        listResult.addAll(list);
                        return listResult;
                    }
                }else{
                    if(leftList.peek() instanceof Const && ((Const) leftList.peek()).isConstant()) {
                        isFalse = !((Const) leftList.pop()).asBoolean();
                    }else{
                        listResult.push(op);
                        listResult.addAll(leftList);
                        listResult.add(TerTrue.OPERATOR);
                        listResult.addAll(list);
                        return listResult;
                    }
                }
                if(isFalse){
                  //skipping ternary true
                    int i = 1;
                    while(i != 0){
                        if(list.peek() == Ternary.OPERATOR){
                            i++;
                        }else if(list.peek() == TerFalse.OPERATOR){
                            i--;
                        }
                        list.pop();
                    }
                }
                if(list.peek() instanceof Operator){
                    rightList = optimisation(list);
                    if(rightList.peek() instanceof Const && ((Const) rightList.peek()).isConstant()){
                        listResult.push((Const) rightList.pop());
                    }else{
                        listResult.addAll(rightList);
                    }
                }else{
                    listResult.push((Const) list.pop());
                }
                return listResult;
            }
            if(list.peek() instanceof Operator){
                rightList = optimisation(list);
            }else{
                rightC = (Const) list.pop();
            }
            if(rightList == null && rightC == null) {
                throw new InvokeELException("Error in extracting right operand in optimization");
            }
            if(leftList == null && rightList == null){
                if(leftC.isConstant() && rightC.isConstant()) {
                    listResult.push(op.unitConst(leftC, rightC));
                }else {
                    listResult.push(rightC);
                    listResult.push(leftC);
                    listResult.push(op);
                }
            }else if(leftList == null){
                if(rightList.peek() instanceof Const && ((Const) rightList.peek()).isConstant()){
                    listResult.push(op.unitConst(leftC, (Const) rightList.pop()));
                }else{
                    listResult.push(leftC);
                    listResult.addAll(rightList);
                    listResult.push(op);
                }
            }else if(rightList == null){
                if(leftList.peek() instanceof Const && ((Const) leftList.peek()).isConstant()){
                    listResult.push(op.unitConst((Const) leftList.pop(), rightC));
                }else{
                    listResult.push(op);
                    listResult.addAll(leftList);
                    listResult.add(rightC);
                }
            }else{
                if(rightList.peek() instanceof Const && ((Const) rightList.peek()).isConstant()
                        && leftList.peek() instanceof Const && ((Const) leftList.peek()).isConstant()){
                    listResult.push(op.unitConst((Const) leftList.pop(), (Const) rightList.pop()));
                }else{
                    listResult.push(op);
                    listResult.addAll(leftList);
                    listResult.addAll(rightList);
                }
            }
            return listResult;
        }
        return list;
    }
}
