/*
 * Copyright (C) 2018-2018 The Project Spathiphyllum Authors.
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
package org.spathiphyllum.el.tools;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.Arrays;
import java.util.LinkedList;
import java.util.stream.Collectors;

import org.apache.commons.text.StringEscapeUtils;
import org.spathiphyllum.el.EnumEL;
import org.spathiphyllum.el.InvokeELException;
import org.spathiphyllum.el.ParseELException;

/**
 * @author obelous
 *
 */
public class HelperStringParser {
    public static final boolean[] EMPTY_BOOLEANS = {};
    public static final byte[] EMPTY_BYTES = {};
    public static final char[] EMPTY_CHARS = {};
    public static final short[] EMPTY_SHORTS = {};
    public static final int[] EMPTY_INTS = {};
    public static final long[] EMPTY_LONGS = {};
    public static final float[] EMPTY_FLOATS = {};
    public static final double[] EMPTY_DOUBLES = {};
    public static final String[] EMPTY_STRINGS = {};
    public static final BigInteger[] EMPTY_BIG_INTEGERS = {};
    public static final BigDecimal[] EMPTY_BIG_DECIMALS = {};
    public static final String BOOLEAN_TRUE = "true";
    public static final String BOOLEAN_FALSE = "false";
    private static final char[] DECIMAL = { 0, 1, 2, 3, 4, 5, 6, 7, 8, 9 };
    private static final int MAX_DIGITS_INT_BINARY = 32;
    private static final int MAX_DIGITS_INT_HEX = 8;
    private static final int MAX_DIGITS_INT_DECIMAL = 10;
    private static final int MAX_DIGITS_LONG_BINARY = 64;
    private static final int MAX_DIGITS_LONG_HEX = 16;
    private static final int MAX_DIGITS_LONG_DECIMAL = 19;

    /**
     * Check if String is empty
     * 
     * @param s
     * @return true if s is null or s.lenght() = 0
     */
    public static boolean isStringEmpty(String s) {
        return s == null || s.length() == 0;
    }

    /**
     * Check if String is empty (or contain only whitespace symbols)
     * 
     * @param s
     * @return
     */
    public static boolean isStringEmptySkipSpace(String s) {
        return s == null || s.trim().length() == 0;
    }

    /**
     * check next symbol return true if next symbol present in symbols, else
     * false
     * 
     * @param exp
     * @param pos
     * @param symbols
     * @return
     */
    public static boolean isNextSymbol(String exp, int pos, char... symbols) {
        return isNextSymbol(exp.toCharArray(), pos, symbols);
    }
    public static boolean isNextSymbol(char[] chars, int pos, char... symbols) {
        int p = pos +1;
        if (p < chars.length) {
            for (int i = 0; i < symbols.length; i++) {
                if (symbols[i] == chars[p]) {
                    return true;
                }
            }
        }
        return false;
    }
    /**
     * check if the sample present in this position
     * @param exp
     * @param pos
     * @param sample
     * @return
     */
    public static boolean isNextTheSame(String exp, int pos, String sample){
        return isNextTheSame(exp.toCharArray(), pos, sample.toCharArray());
    }
    public static boolean isNextTheSame(char[] chars, int pos, char[] sample){
        if(pos + sample.length < chars.length){
            for(int i = 0; i < sample.length; i++){
                if(chars[pos+i] != sample[i]){
                    return false;
                }
            }
            return true;
        }
        return false;
    }
    /**
     * we skip all space, on exit 'i' on position 'not space'
     * 
     * @param el
     * @param pos
     * @param checkEndOf
     *            - if true - check if 'i' is not end of string, else throw
     *            exception with 'suggestion'
     * @param suggestion
     * @return
     * @throws ParseELException
     */
    public static int skipSpace(String el, int pos, boolean checkEndOf, String suggestion) throws ParseELException {
        return skipSpace(el.toCharArray(), pos, checkEndOf, suggestion);
    }
    public static int skipSpace(char[] chars, int pos, boolean checkEndOf, String suggestion) throws ParseELException {
        for (; pos < chars.length; pos++) {
            if (!Character.isWhitespace(chars[pos])) {
                break;
            }
        }
        if (checkEndOf && pos >= chars.length) {
            throw new ParseELException("Unexpected end of expression", pos, suggestion);
        }
        return pos;
    }

    /**
     * read string<br> This symbols are allowed:<br>
     * '\t','\b','\n','\r','\f','\'','\"','\\','\uFFFF' on start 'i' on position
     * after '"' on exit 'i' on position next after '"'
     * 
     * @param el
     * @param i
     * @param sb
     * @return
     * @throws ParseELException
     */
    public static int readNextString(String el, int i, StringBuilder sb, boolean checkLastQuetes) throws ParseELException {
        return readNextString(el.toCharArray(), i, sb, checkLastQuetes, false);
    }
    public static int readNextString(char[] el, int i, StringBuilder sb, boolean checkLastQuetes) throws ParseELException {
        return readNextString(el, i, sb, checkLastQuetes, false);
    }
    public static int readNextString(char[] el, int i, StringBuilder sb, boolean checkLastQuetes, boolean isSingleQuotes) throws ParseELException {
        int offset = 0;
        while (i < el.length) {
            char ch = el[i];
            // "'''" or '"""'
            if((ch == '"' && isSingleQuotes) || 
               (ch == '\'' && !isSingleQuotes) ||
               (ch != '"' && ch != '\\' && ch != '\'')) {
                if(offset == 0){
                    offset = i;
                }
                i++;
            } else if (ch == '\\') {
                if(offset != 0){
                    sb.append(el, offset, i-offset);
                    offset = 0;
                }
                if (isNextSymbol(el, i, 't', 'b', 'n', 'r', 'f', '\'', '"', '\\')) {
                    switch (el[++i]) {
                    case 't':
                        sb.append('\t');
                        break;
                    case 'b':
                        sb.append('\b');
                        break;
                    case 'n':
                        sb.append('\n');
                        break;
                    case 'r':
                        sb.append('\r');
                        break;
                    case 'f':
                        sb.append('\f');
                        break;
                    case '\'':
                        sb.append('\'');
                        break;
                    case '"':
                        sb.append('"');
                        break;
                    case '\\':
                        sb.append('\\');
                        break;
                    case 'u':
                    case 'U':
                        // Unicode, next must be 4 Hex-numbers
                        StringBuilder u = new StringBuilder();
                        for (++i; i < el.length && u.length() < 4; i++) {
                            char c = el[i];
                            if ((c >= '0' && c <= '9') || (c >= 'a' && c <= 'f') || (c >= 'A' && c <= 'F')) {
                                u.append(c);
                            } else {
                                break;
                            }
                        }
                        if (u.length() != 4) {
                            throw new ParseELException(i, "Must be unicode \"\\uFFFF\"");
                        }
                        sb.append((char) Integer.parseInt(u.toString(), 16));
                        break;
                    default:
                        throw new ParseELException(i, "Incorrect symbol");
                    }
                }
            } else {
                if(offset != 0){
                    sb.append(el, offset, i-offset);
                    offset = 0;
                }
                break;
            }
        }
        if ((checkLastQuetes && el[i] != '"' && !isSingleQuotes) || (checkLastQuetes && el[i] != '\'' && isSingleQuotes)) {
            throw new ParseELException(i, "string must end with double quote");
        }
        return i + 1;
    }

    /**
     * read number to StringBuilder on start 'i' in position first number 0-9 or
     * '#' on exit 'i' in position after number
     * 
     * @param el
     * @param pos
     * @param sb
     * @return
     * @throws ParseELException
     */
    public static int readNextNumber(String el, int pos, StringBuilder sb) throws ParseELException {
        return readNextNumber(el.toCharArray(), pos, sb);
    }
    public static int readNextNumber(char[] el, int pos, StringBuilder sb) throws ParseELException {
        char ch = el[pos];
        if (ch == '#' && !((ch >= '0' && ch <= '9') || (ch >= 'a' && ch <= 'f') || (ch >= 'A' && ch <= 'F'))) {
            throw new ParseELException(pos, "Invalid number");
        }
        sb.append(ch);
        pos++;
        while (pos < el.length) {
            ch = el[pos];
            if ((ch >= '0' && ch <= '9') || (ch >= 'a' && ch <= 'f') || (ch >= 'A' && ch <= 'F') || (ch == '.')
                    || (ch == 'e' || ch == 'E')
                    || ((ch == '+' || ch == '-') && (sb.charAt(sb.length()-1) == 'e' || sb.charAt(sb.length()-1) == 'E'))) {
                sb.append(ch);
                pos++;
            } else {
                break;
            }
        }
        return pos;
    }

    /**
     * This method define the best EnumTypes on result of parseNumber
     * 
     * @param number
     * @return
     */
    public static EnumEL defineNumberType(Number number) {
        if (number instanceof Integer) {
            int i = number.intValue();
            if((Byte.MIN_VALUE >= i) && (i <= Byte.MAX_VALUE) ){
                return EnumEL.BYTE;
            }else if((Short.MIN_VALUE >= i) && (i <= Short.MAX_VALUE) ){
                return EnumEL.SHORT;
            }
            return EnumEL.INT;
        } else if (number instanceof Long) {
            long i = number.intValue();
            if((Integer.MIN_VALUE >= i) && (i <= Integer.MAX_VALUE) ){
                return EnumEL.INT;
            }
            return EnumEL.LONG;
        } else if (number instanceof Float) {
            return EnumEL.FLOAT;
        } else if (number instanceof Double) {
            return EnumEL.DOUBLE;
        } else if (number instanceof BigInteger) {
            return EnumEL.BIG_INTEGER;
        } else {
            return EnumEL.BIG_DECIMAL;
        }
    }

    /**
     * Helper to convert result of parseNumber to EnumTypes
     * 
     * @param number
     * @param types
     * @return
     * @throws ParseELException
     */
    public static Object numberToEnumType(Number number, EnumEL types) throws ParseELException {
        switch (types) {
        case BYTE:
            return numberAsByte(number);
        case SHORT:
            return numberAsShort(number);
        case INT:
            return numberAsInt(number);
        case LONG:
            return numberAsLong(number);
        case FLOAT:
            return numberAsFloat(number);
        case DOUBLE:
            return numberAsDouble(number);
        case BIG_INTEGER:
            return numberAsBigInteger(number);
        case BIG_DECIMAL:
            return numberAsBigDecimal(number);
        default:
            throw new ParseELException(0, "The type is not numberic");
        }
    }

    /**
     * parse boolean is available only true and false
     * 
     * @param s
     * @return
     * @throws ParseELException
     */
    public static final boolean parseBoolean(String s) throws ParseELException {
        if (isStringEmptySkipSpace(s) || BOOLEAN_FALSE.equals(s)) {
            return false;
        } else if (BOOLEAN_TRUE.equals(s)) {
            return true;
        }
        throw new ParseELException(0, "Expected boolean 'true' or 'false'");
    }

    public static String toString(boolean value) {
        return value ? BOOLEAN_TRUE : BOOLEAN_FALSE;
    }

    /**
     * The first symbol must be 0-9 or '-' or '+'<br>
     * Last symbol - can be suffix 'l','L' - for long, and if this is not Hex
     * 'f','F' for float, 'd','D' for double.<br>
     * if number start with 0x, 0X, # - this is hex presentation (not float)<br>
     * if number start with 0b, 0B - this is binary presentation (not float)
     * other is decimal presentation
     * 
     * @param presentation
     * @return
     * @throws ParseELException
     */
    public static Number parseNumber(String presentation) throws ParseELException {
        String el = presentation.trim();
        if (isStringEmpty(el)) {
            return 0;
        }
        int pos = 0;
        StringBuilder sb = new StringBuilder();
        boolean neg = false;
        boolean isPoint = false;
        boolean isExp = false;
        int digits = 0;
        int radix = 10;
        int lenght = el.length();
        char charEnd = el.charAt(el.length() - 1);
        EnumEL typeOfResult = null;
        if (charEnd == 'l' || charEnd == 'L') {
            typeOfResult = EnumEL.LONG;
            lenght--;
        } else if (charEnd == 'f' || charEnd == 'F') {
            typeOfResult = EnumEL.FLOAT;
            lenght--;
        } else if (charEnd == 'd' || charEnd == 'D') {
            typeOfResult = EnumEL.DOUBLE;
            lenght--;
        }
        for (; pos < lenght; pos++) {
            char currentChar = el.charAt(pos);
            if (digits == 0) {
                if (currentChar == '+') {
                    //continue
                } else if (currentChar == '-') {
                    if (neg) {
                        throw new ParseELException(pos, "Symbol '-' can be once");
                    }
                    neg = true;
                    sb.append('-');
                    pos = skipSpace(el, pos, false, null);
                } else if (currentChar == 0 && isNextSymbol(el, pos, 'b', 'B')) {
                    // radix format
                    if (radix != 10) {
                        throw new ParseELException(pos, ERR_RADIX);
                    }
                    pos++;
                    radix = 2;
                } else if ((currentChar == 0 && isNextSymbol(el, pos, 'x', 'X')) || currentChar == '#') {
                    // radix format
                    if (radix != 10) {
                        throw new ParseELException(pos, ERR_RADIX);
                    }
                    pos++;
                    radix = 16;
                } else if (currentChar >= '0' && currentChar <= '9') {
                    sb.append(currentChar);
                    digits++;
                } else {
                    throw new ParseELException(pos, ERR_PARSE_NUMBER);
                }
            } else {
                if (currentChar == '_') {
                    if (!isNextSymbol(el, pos, DECIMAL)) {
                        throw new ParseELException(pos, "Symbol '_' can be placed between number only");
                    }
                } else if ((radix == 10 && (currentChar >= '0' && currentChar <= '9'))
                        || (radix == 2 && (currentChar >= '0' && currentChar <= '1'))
                        || (radix == 16 && ((currentChar >= '0' && currentChar <= '9')
                                || (currentChar >= 'A' && currentChar <= 'F')
                                || (currentChar >= 'a' && currentChar <= 'f')))) {
                    sb.append(currentChar);
                    digits++;
                } else if (radix == 10 && currentChar == '.') {
                    if (isPoint) {
                        throw new ParseELException(pos, "Symbol '.' can be once");
                    }
                    isPoint = true;
                    sb.append(currentChar);
                } else if (radix == 10 && (currentChar == 'e' || currentChar == 'E')) {
                    if (isExp) {
                        throw new ParseELException(pos, "Symbol 'e' can be once");
                    }
                    isExp = true;
                    sb.append(currentChar);
                } else {
                    throw new ParseELException(pos, ERR_PARSE_NUMBER);
                }
            }
        }
        try {
            if (typeOfResult != null) {
                switch (typeOfResult) {
                case LONG:
                    return Long.parseLong(sb.toString(), radix);
                case FLOAT:
                    return Float.valueOf(sb.toString());
                case DOUBLE:
                    return Double.valueOf(sb.toString());
                default:
                    throw new ParseELException(pos, "Unknown type. Available 'L','F','D'");
                }
            } else {
                if (radix == 2) {
                    if (digits < MAX_DIGITS_INT_BINARY) {
                        return Integer.valueOf(sb.toString(), 2);
                    } else if (digits < MAX_DIGITS_LONG_BINARY) {
                        return Long.valueOf(sb.toString(), 2);
                    } else {
                        return new BigInteger(sb.toString(), 2);
                    }
                } else if (radix == 16) {
                    if (digits < MAX_DIGITS_INT_HEX) {
                        return Integer.valueOf(sb.toString(), 16);
                    } else if (digits < MAX_DIGITS_LONG_HEX) {
                        return Long.valueOf(sb.toString(), 16);
                    } else {
                        return new BigInteger(sb.toString(), 16);
                    }
                } else {
                    if (isPoint || isExp) {
                        return new BigDecimal(sb.toString());
                    } else {
                        if (digits < MAX_DIGITS_INT_DECIMAL) {
                            return Integer.valueOf(sb.toString(), 10);
                        } else if (digits < MAX_DIGITS_LONG_DECIMAL) {
                            return Long.valueOf(sb.toString(), 10);
                        } else {
                            return new BigInteger(sb.toString(), 10);
                        }
                    }
                }
            }
        } catch (NumberFormatException e) {
            throw new ParseELException(pos, ERR_PARSE_NUMBER);
        } catch (ArithmeticException e) {
            throw new ParseELException(pos, "Out of long range, try other type");
        }
    }

    /**
     * parse byte
     * 
     * @param value
     * @return
     * @throws ParseELException
     */
    public static byte parseByte(String value) throws ParseELException {
        return numberAsByte(HelperStringParser.parseNumber(value));
    }

    public static String toString(byte value) {
        return Byte.toString(value);
    }

    /**
     * parse short
     * 
     * @param value
     * @return
     * @throws ParseELException
     */
    public static short parseShort(String value) throws ParseELException {
        return numberAsShort(HelperStringParser.parseNumber(value));
    }
    public static String toString(short value) {
        return Short.toString(value);
    }
    public static char parseChar(String value) throws ParseELException {
        StringBuilder sb = new StringBuilder();
        readNextString(value, 0, sb, false);
        if(sb.length() > 1){
            throw new ParseELException(0, "Invalid char");
        }else if(sb.length() == 0){
            return 0;
        }
        return sb.charAt(0);
    }
    public static String toString(char value){
        return StringEscapeUtils.escapeJava(String.valueOf(value));
    }
    /**
     * parse integer
     * 
     * @param value
     * @return
     * @throws ParseELException
     */
    public static int parseInt(String value) throws ParseELException {
        return numberAsInt(HelperStringParser.parseNumber(value));
    }

    public static String toString(int value) {
        return Integer.toString(value);
    }

    /**
     * parse long
     * 
     * @param value
     * @return
     * @throws ParseELException
     */
    public static long parseLong(String value) throws ParseELException {
        return numberAsLong(HelperStringParser.parseNumber(value));
    }

    public static String toString(long value) {
        return Long.toString(value);
    }

    /**
     * parse float
     * 
     * @param value
     * @return
     * @throws ParseELException
     */
    public static float parseFloat(String value) throws ParseELException {
        return numberAsFloat(HelperStringParser.parseNumber(value));
    }

    public static String toString(float value) {
        return Float.toString(value);
    }

    /**
     * parse double
     * 
     * @param value
     * @return
     * @throws ParseELException
     */
    public static double parseDouble(String value) throws ParseELException {
        return numberAsDouble(HelperStringParser.parseNumber(value));
    }

    public static String toString(double value) {
        return Double.toString(value);
    }

    /**
     * parse BigInteger
     * 
     * @param value
     * @return
     * @throws ParseELException
     */
    public static BigInteger parseBigInteger(String value) throws ParseELException {
        return numberAsBigInteger(parseNumber(value));
    }

    public static String toString(BigInteger value) {
        return value.toString();
    }

    /**
     * parse BigDecimal
     * 
     * @param value
     * @return
     * @throws ParseELException
     */
    public static BigDecimal parseBigDecimal(String value) throws ParseELException {
        return numberAsBigDecimal(parseNumber(value));
    }
    public static String toString(BigDecimal value) {
        return value.toString();
    }
    /**
     * parse String<br>
     * apply escaped characters
     * @param value
     * @return
     * @throws ParseELException
     */
    public static String parseString(String value) throws ParseELException {
        StringBuilder sb = new StringBuilder();
        readNextString(value, 0, sb, false);
        if(sb.length() == 0){
            return "";
        }else{
            return sb.toString();
        }
    }
    public static String toString(String value){
        return StringEscapeUtils.escapeJava(String.valueOf(value));
    }
    /**
     * convert Number to byte with check
     * 
     * @param number
     * @return
     * @throws ParseELException
     */
    public static byte numberAsByte(Number number) throws ParseELException {
        int i = number.intValue();
        if (i < Byte.MIN_VALUE || i > Byte.MAX_VALUE) {
            throw new ParseELException(0, new StringBuilder(ERR_EXP_NUMBER).append(Byte.MIN_VALUE).append(ERR_AND)
                    .append(Byte.MAX_VALUE).toString());
        }
        return number.byteValue();
    }

    /**
     * convert Number to short with check
     * 
     * @param number
     * @return
     * @throws ParseELException
     */
    public static short numberAsShort(Number number) throws ParseELException {
        int i = number.intValue();
        if (i < Short.MIN_VALUE || i > Short.MAX_VALUE) {
            throw new ParseELException(0, new StringBuilder(ERR_EXP_NUMBER).append(Short.MIN_VALUE).append(ERR_AND)
                    .append(Short.MAX_VALUE).toString());
        }
        return number.shortValue();
    }

    /**
     * convert Number to int with check
     * 
     * @param number
     * @return
     * @throws ParseELException
     */
    public static int numberAsInt(Number number) throws ParseELException {
        long i = number.longValue();
        if (i < Integer.MIN_VALUE || i > Integer.MAX_VALUE) {
            throw new ParseELException(0, new StringBuilder(ERR_EXP_NUMBER).append(Integer.MIN_VALUE).append(ERR_AND)
                    .append(Integer.MAX_VALUE).toString());
        }
        return number.intValue();
    }

    /**
     * convert Number to long with check
     * 
     * @param number
     * @return
     * @throws ParseELException
     */
    public static long numberAsLong(Number number) throws ParseELException {
        if (number instanceof BigInteger || number instanceof BigDecimal) {
            try {
                if (number instanceof BigInteger) {
                    return ((BigInteger) number).longValueExact();
                } else {
                    return ((BigDecimal) number).longValueExact();
                }
            } catch (ArithmeticException e) {
                throw new ParseELException(0, new StringBuilder(ERR_EXP_NUMBER).append(Long.MIN_VALUE).append(ERR_AND)
                        .append(Long.MAX_VALUE).toString());
            }
        } else {
            return number.longValue();
        }
    }

    /**
     * convert Number to Float with check
     * 
     * @param number
     * @return
     * @throws ParseELException
     */
    public static float numberAsFloat(Number number) throws ParseELException {
        double d = number.doubleValue();
        if (d < Float.MIN_VALUE || d > Float.MAX_VALUE) {
            throw new ParseELException(0, new StringBuilder(ERR_EXP_NUMBER).append(Float.MIN_VALUE).append(ERR_AND)
                    .append(Float.MAX_VALUE).toString());
        }
        return number.floatValue();
    }

    /**
     * convert Number to double with check
     * 
     * @param number
     * @return
     * @throws ParseELException
     */
    public static double numberAsDouble(Number number) throws ParseELException {
        if (number instanceof BigDecimal && !bigDecimalBetween((BigDecimal) number,
                BigDecimal.valueOf(Double.MIN_VALUE), BigDecimal.valueOf(Double.MAX_VALUE))) {
            throw new ParseELException(0, new StringBuilder(ERR_EXP_NUMBER).append(Double.MIN_VALUE).append(ERR_AND)
                    .append(Double.MAX_VALUE).toString());
        }
        return number.doubleValue();
    }

    /**
     * convert Number to BigInteger with check
     * 
     * @param number
     * @return
     */
    public static BigInteger numberAsBigInteger(Number number) {
        if (number instanceof BigDecimal) {
            return ((BigDecimal) number).toBigInteger();
        } else if (number instanceof BigInteger) {
            return (BigInteger) number;
        } else if((number instanceof Float)||(number instanceof Double)){
            return BigDecimal.valueOf(number.doubleValue()).toBigInteger();
        } else {
            return BigInteger.valueOf((long)number);
        }
    }

    /**
     * convert Number to BigDecimal with check
     * 
     * @param number
     * @return
     */
    public static BigDecimal numberAsBigDecimal(Number number) {
        if (number instanceof BigDecimal) {
            return (BigDecimal) number;
        } else if (number instanceof BigInteger) {
            return new BigDecimal((BigInteger) number);
        } else {
            return BigDecimal.valueOf(number.doubleValue());
        }
    }
    public static String numberAsString(Number number){
        if (number instanceof BigDecimal) {
            return ((BigDecimal) number).toPlainString();
        } else {
            return number.toString();
        }
    }
    /**
     * Check if BigInteger between min an max
     * 
     * @param i
     * @param min
     * @param max
     * @return
     */
    public static boolean bigIntegerBetween(BigInteger i, BigInteger min, BigInteger max) {
        return (i.compareTo(min) >= 0) && (i.compareTo(max) <= 0);
    }

    /**
     * Check is BigDecimal between min and max
     * 
     * @param i
     * @param min
     * @param max
     * @return
     */
    public static boolean bigDecimalBetween(BigDecimal i, BigDecimal min, BigDecimal max) {
        return (i.compareTo(min) >= 0) && (i.compareTo(max) <= 0);
    }

    /**
     * parse array Format of array:
     * 
     * <pre>
     * Strings: "s1","\u0048\u0065\u006c\u006c\u006f","\n", "\"", ""...
     * char : "s", "\u0048", "\n", "\"", "'", ...
     * Boolean: true,false,true...
     * Numbers: 
     *    1,2,3,4,5,6 ...
     *    0x0A,0xFE,0,12...
     * </pre>
     * 
     * @param arrayPresentation
     * @param type
     * @return
     */
    public static Object parseArray(String arrayPresentation, EnumEL type) throws ParseELException {
        if (type == null) {
            throw new ParseELException(0, "Need to define type of Array");
        }
        switch (type) {
        case BOOLEAN:
            return parseBooleanArray(arrayPresentation);
        case BYTE:
            return parseByteArray(arrayPresentation);
        case CHAR:
            return parseCharArray(arrayPresentation);
        case SHORT:
            return parseShortArray(arrayPresentation);
        case INT:
            return parseIntArray(arrayPresentation);
        case LONG:
            return parseLongArray(arrayPresentation);
        case FLOAT:
            return parseFloatArray(arrayPresentation);
        case DOUBLE:
            return parseDoubleArray(arrayPresentation);
        case STRING:
            return parseStringArray(arrayPresentation);
        case BIG_INTEGER:
            return parseBigIntegerArray(arrayPresentation);
        case BIG_DECIMAL:
            return parseBigDecimalArray(arrayPresentation);
        default:
            throw new ParseELException(0, "Only simple types is allowed");
        }
    }

    /**
     * parse BigDecimal[]
     * 
     * @param arrayPresentation
     * @return
     * @throws ParseELException
     */
    public static BigDecimal[] parseBigDecimalArray(String arrayPresentation) throws ParseELException {
        if (isStringEmptySkipSpace(arrayPresentation)) {
            return EMPTY_BIG_DECIMALS;
        } else {
            String[] parts = arrayPresentation.split(",");
            BigDecimal[] array = new BigDecimal[parts.length];
            int pos = 0;
            for (int i = 0; i < parts.length; i++) {
                try {
                    array[i] = parseBigDecimal(parts[i]);
                    pos += parts[i].length() + 1;
                } catch (ParseELException e) {
                    throw new ParseELException(pos, e.getMessage());
                }
            }
            return array;
        }
    }

    /**
     * Convert BigDecimal[] to string
     * 
     * @param array
     * @return
     */
    public static String arrayToString(BigDecimal[] array) {
        if (array == null) {
            throw new InvokeELException(ERR_ARRAY_NULL);
        }
        if (array.length == 0)
            return "";
        StringBuilder sb = new StringBuilder();
        for (int i = 0;; i++) {
            sb.append(array[i]);
            if (i == array.length) {
                return sb.toString();
            }
            sb.append(",");
        }
    }

    /**
     * parse BigInteger[]
     * 
     * @param arrayPresentation
     * @return
     * @throws ParseELException
     */
    public static BigInteger[] parseBigIntegerArray(String arrayPresentation) throws ParseELException {
        if (isStringEmptySkipSpace(arrayPresentation)) {
            return EMPTY_BIG_INTEGERS;
        } else {
            String[] parts = arrayPresentation.split(",");
            BigInteger[] array = new BigInteger[parts.length];
            int pos = 0;
            for (int i = 0; i < parts.length; i++) {
                try {
                    array[i] = parseBigInteger(parts[i]);
                    pos += parts[i].length() + 1;
                } catch (ParseELException e) {
                    throw new ParseELException(pos, e.getMessage());
                }
            }
            return array;
        }
    }

    /**
     * Convert BigInteger[] to string
     * 
     * @param array
     * @return
     */
    public static String arrayToString(BigInteger[] array) {
        if (array == null) {
            throw new InvokeELException(ERR_ARRAY_NULL);
        }
        if (array.length == 0)
            return "";
        StringBuilder sb = new StringBuilder();
        for (int i = 0;; i++) {
            sb.append(array[i]);
            if (i == array.length) {
                return sb.toString();
            }
            sb.append(",");
        }
    }

    /**
     * parse String[]
     * 
     * @param arrayPresentation
     * @return
     * @throws ParseELException
     */
    public static String[] parseStringArray(String arrayPresentation) throws ParseELException {
        if (isStringEmptySkipSpace(arrayPresentation)) {
            return EMPTY_STRINGS;
        } else {
            LinkedList<String> list = new LinkedList<>();
            int i = skipSpace(arrayPresentation, 0, false, null);
            while (i < arrayPresentation.length()) {
                StringBuilder sb = new StringBuilder();
                if (arrayPresentation.charAt(i) == '"') {
                    i = readNextString(arrayPresentation, i, sb, true);
                    list.add(sb.toString());
                    i = skipSpace(arrayPresentation, i, false, null);
                } else if (arrayPresentation.charAt(i) == ',') {
                    i = skipSpace(arrayPresentation, i + 1, true, "Cannot ',' be last symbol");
                } else {
                    throw new ParseELException(i, "Cannot parse this string's array");
                }
            }
            return list.toArray(new String[list.size()]);
        }
    }

    /**
     * Convert string[] to string
     * 
     * @param array
     * @return
     */
    public static String arrayToString(String[] array) {
        if (array == null) {
            throw new InvokeELException(ERR_ARRAY_NULL);
        }
        return Arrays.stream(array).map(StringEscapeUtils::escapeJava).collect(Collectors.joining("\",\""));
    }

    /**
     * parse double[]
     * 
     * @param arrayPresentation
     * @return
     * @throws ParseELException
     */
    public static double[] parseDoubleArray(String arrayPresentation) throws ParseELException {
        if (isStringEmptySkipSpace(arrayPresentation)) {
            return EMPTY_DOUBLES;
        } else {
            String[] parts = arrayPresentation.split(",");
            double[] array = new double[parts.length];
            int pos = 0;
            for (int i = 0; i < parts.length; i++) {
                try {
                    array[i] = parseDouble(parts[i]);
                    pos += parts[i].length() + 1;
                } catch (ParseELException e) {
                    throw new ParseELException(pos, e.getMessage());
                }
            }
            return array;
        }
    }

    /**
     * Convert double[] to string
     * 
     * @param array
     * @return
     */
    public static String arrayToString(double[] array) {
        if (array == null) {
            throw new InvokeELException(ERR_ARRAY_NULL);
        }
        return Arrays.stream(array).mapToObj(Double::toString).collect(Collectors.joining(","));
    }

    /**
     * parse float[]
     * 
     * @param arrayPresentation
     * @return
     * @throws ParseELException
     */
    public static float[] parseFloatArray(String arrayPresentation) throws ParseELException {
        if (isStringEmptySkipSpace(arrayPresentation)) {
            return EMPTY_FLOATS;
        } else {
            String[] parts = arrayPresentation.split(",");
            float[] array = new float[parts.length];
            int pos = 0;
            for (int i = 0; i < parts.length; i++) {
                try {
                    array[i] = parseFloat(parts[i]);
                    pos += parts[i].length() + 1;
                } catch (ParseELException e) {
                    throw new ParseELException(pos, e.getMessage());
                }
            }
            return array;
        }
    }

    /**
     * Convert float[] to string
     * 
     * @param array
     * @return
     */
    public static String arrayToString(float[] array) {
        if (array == null) {
            throw new InvokeELException(ERR_ARRAY_NULL);
        }
        if (array.length == 0)
            return "";
        StringBuilder sb = new StringBuilder();
        for (int i = 0;; i++) {
            sb.append(array[i]);
            if (i == array.length) {
                return sb.toString();
            }
            sb.append(",");
        }
    }

    /**
     * parse long[]
     * 
     * @param arrayPresentation
     * @return
     * @throws ParseELException
     */
    public static long[] parseLongArray(String arrayPresentation) throws ParseELException {
        if (isStringEmptySkipSpace(arrayPresentation)) {
            return EMPTY_LONGS;
        } else {
            String[] parts = arrayPresentation.split(",");
            long[] array = new long[parts.length];
            int pos = 0;
            for (int i = 0; i < parts.length; i++) {
                try {
                    array[i] = parseLong(parts[i]);
                    pos += parts[i].length() + 1;
                } catch (ParseELException e) {
                    throw new ParseELException(pos, e.getMessage());
                }
            }
            return array;
        }
    }

    /**
     * Convert long[] to string
     * 
     * @param array
     * @return
     */
    public static String arrayToString(long[] array) {
        if (array == null) {
            throw new InvokeELException(ERR_ARRAY_NULL);
        }
        return Arrays.stream(array).mapToObj(Long::toString).collect(Collectors.joining(","));
    }

    /**
     * parse int[]
     * 
     * @param arrayPresentation
     * @return
     * @throws ParseELException
     */
    public static int[] parseIntArray(String arrayPresentation) throws ParseELException {
        if (isStringEmptySkipSpace(arrayPresentation)) {
            return EMPTY_INTS;
        } else {
            String[] parts = arrayPresentation.split(",");
            int[] array = new int[parts.length];
            int pos = 0;
            for (int i = 0; i < parts.length; i++) {
                try {
                    array[i] = parseInt(parts[i]);
                    pos += parts[i].length() + 1;
                } catch (ParseELException e) {
                    throw new ParseELException(pos, e.getMessage());
                }
            }
            return array;
        }
    }

    /**
     * Convert long[] to string
     * 
     * @param array
     * @return
     */
    public static String arrayToString(int[] array) {
        if (array == null) {
            throw new InvokeELException(ERR_ARRAY_NULL);
        }
        return Arrays.stream(array).mapToObj(Integer::toString).collect(Collectors.joining(","));
    }

    /**
     * parse short[]
     * 
     * @param arrayPresentation
     * @return
     * @throws ParseELException
     */
    public static short[] parseShortArray(String arrayPresentation) throws ParseELException {
        if (isStringEmptySkipSpace(arrayPresentation)) {
            return EMPTY_SHORTS;
        } else {
            String[] parts = arrayPresentation.split(",");
            short[] array = new short[parts.length];
            int pos = 0;
            for (int i = 0; i < parts.length; i++) {
                try {
                    array[i] = parseShort(parts[i]);
                    pos += parts[i].length() + 1;
                } catch (ParseELException e) {
                    throw new ParseELException(pos, e.getMessage());
                }
            }
            return array;
        }
    }

    /**
     * Convert short[] to string
     * 
     * @param array
     * @return
     */
    public static String arrayToString(short[] array) {
        if (array == null) {
            throw new InvokeELException(ERR_ARRAY_NULL);
        }
        if (array.length == 0)
            return "";
        StringBuilder sb = new StringBuilder();
        for (int i = 0;; i++) {
            sb.append(array[i]);
            if (i == array.length) {
                return sb.toString();
            }
            sb.append(",");
        }
    }

    /**
     * parse char[]
     * 
     * @param arrayPresentation
     * @return
     * @throws ParseELException
     */
    public static char[] parseCharArray(String arrayPresentation) throws ParseELException {
        if (isStringEmptySkipSpace(arrayPresentation)) {
            return EMPTY_CHARS;
        } else {
            int i = skipSpace(arrayPresentation, 0, false, null);
            StringBuilder sb = new StringBuilder();
            int counter = 0;
            while (i < arrayPresentation.length()) {
                if (arrayPresentation.charAt(i) == '"') {
                    counter = sb.length();
                    i = readNextString(arrayPresentation, i, sb, true);
                    if (counter + 1 != sb.length()) {
                        throw new ParseELException(i, "Must be one symbol");
                    }
                    i = skipSpace(arrayPresentation, i, false, null);
                } else if (arrayPresentation.charAt(i) == ',') {
                    i = skipSpace(arrayPresentation, i + 1, true, "Cannot ',' be last symbol");
                } else {
                    throw new ParseELException(i, "Cannot parse this char's array");
                }
            }
            return sb.toString().toCharArray();
        }
    }

    /**
     * Convert char[] to string
     * 
     * @param array
     * @return
     */
    public static String arrayToString(char[] array) {
        if (array == null) {
            throw new InvokeELException(ERR_ARRAY_NULL);
        }
        if (array.length == 0)
            return "";
        StringBuilder sb = new StringBuilder();
        for (int i = 0;; i++) {
            sb.append(array[i]);
            if (i == array.length) {
                return sb.toString();
            }
            sb.append(",");
        }
    }

    /**
     * parse byte[]
     * 
     * @param arrayPresentation
     * @return
     * @throws ParseELException
     */
    public static byte[] parseByteArray(String arrayPresentation) throws ParseELException {
        if (isStringEmptySkipSpace(arrayPresentation)) {
            return EMPTY_BYTES;
        } else {
            String[] parts = arrayPresentation.split(",");
            byte[] array = new byte[parts.length];
            int pos = 0;
            for (int i = 0; i < parts.length; i++) {
                try {
                    array[i] = parseByte(parts[i]);
                    pos += parts[i].length() + 1;
                } catch (ParseELException e) {
                    throw new ParseELException(pos, e.getMessage());
                }
            }
            return array;
        }
    }

    /**
     * Convert byte[] to string
     * 
     * @param array
     * @return
     */
    public static String arrayToString(byte[] array) {
        if (array == null) {
            throw new InvokeELException(ERR_ARRAY_NULL);
        }
        if (array.length == 0)
            return "";
        StringBuilder sb = new StringBuilder();
        for (int i = 0;; i++) {
            sb.append(array[i]);
            if (i == array.length) {
                return sb.toString();
            }
            sb.append(",");
        }
    }

    /**
     * parse boolean[]
     * 
     * @param arrayPresentation
     * @return
     * @throws ParseELException
     */
    public static boolean[] parseBooleanArray(String arrayPresentation) throws ParseELException {
        if (isStringEmptySkipSpace(arrayPresentation)) {
            return EMPTY_BOOLEANS;
        } else {
            String[] parts = arrayPresentation.split(",");
            boolean[] array = new boolean[parts.length];
            int pos = 0;
            for (int i = 0; i < parts.length; i++) {
                try {
                    array[i] = parseBoolean(parts[i]);
                    pos += parts[i].length() + 1;
                } catch (ParseELException e) {
                    throw new ParseELException(pos, e.getMessage());
                }
            }
            return array;
        }
    }

    /**
     * Convert boolean[] to string
     * 
     * @param array
     * @return
     */
    public static String arrayToString(boolean[] array) {
        if (array == null) {
            throw new InvokeELException(ERR_ARRAY_NULL);
        }
        if (array.length == 0)
            return "";
        StringBuilder sb = new StringBuilder();
        for (int i = 0;; i++) {
            sb.append(array[i]);
            if (i == array.length) {
                return sb.toString();
            }
            sb.append(",");
        }
    }

    private HelperStringParser() {
    }

    private static final String ERR_EXP_NUMBER = "Expected number between ";
    private static final String ERR_AND = " and ";
    private static final String ERR_RADIX = "Radix can be once";
    private static final String ERR_PARSE_NUMBER = "Cannot parse a number";
    private static final String ERR_ARRAY_NULL = "ARRAY cannot by null";
}
