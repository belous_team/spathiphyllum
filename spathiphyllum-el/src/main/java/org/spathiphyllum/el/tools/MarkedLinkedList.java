/**
 * 
 */
package org.spathiphyllum.el.tools;

import java.lang.reflect.Array;
import java.util.Collection;
import java.util.Deque;
import java.util.Iterator;
import java.util.NoSuchElementException;
import java.util.Objects;
import java.util.concurrent.atomic.AtomicInteger;

/**
 * @author obelous
 * @author dbilous
 *
 */
public class MarkedLinkedList<E> implements Deque<E>{
    
    private static class Node<E> {
        E obj;
        Node<E> prevNode;
        Node<E> nextNode;
        Node(){
            prevNode = this;
            nextNode = this;
        }
        void remove(){
            //this isn't threadsafe operation
            prevNode.nextNode = nextNode;
            nextNode.prevNode = prevNode;
        }
        @Override
        public String toString() {
            return Objects.toString(obj);
        }
    }
    public static class Mark<E> extends Node<E> {
        int size; //Zero is marker of EMPTY
        Mark<E> prevMark;
        Mark<E> nextMark;
        int number;
        Mark(){
            super();
            prevMark = this;
            nextMark = this;
        }
        @Override
        public String toString() {
            return new StringBuilder("[M").append(number).append("]").toString();
        }
    }
    
    private final Mark<E> ROOT = new Mark<>();
    private AtomicInteger markCounter = new AtomicInteger();
    
    public boolean isRoot(Mark<E> mark){
        return ROOT == mark;
    }
    public Mark<E> getRoot(){
        return ROOT;
    }
    public Mark<E> addMark(Mark<E> mark){
        Mark<E> newMark = new Mark<>();
        newMark.number = markCounter.incrementAndGet();
        //Put in Node circle
        newMark.prevNode = mark.nextMark.prevNode; 
        newMark.nextNode = mark.nextMark;
        mark.nextMark.prevNode.nextNode = newMark;
        mark.nextMark.prevNode = newMark;
        //Put in Mark circle
        newMark.prevMark = mark;
        newMark.nextMark = mark.nextMark;
        mark.nextMark.prevMark = newMark;
        mark.nextMark = newMark;
        return newMark;
    }
    public Mark<E> pushMark(Mark<E> mark){
        Mark<E> newMark = new Mark<>();
        newMark.number = markCounter.incrementAndGet();
        //Put in Node circle
        newMark.prevNode = mark;
        newMark.nextNode = mark.nextNode;
        mark.nextNode.prevNode = newMark;
        mark.nextNode = newMark;
        //Put in Mark circle
        newMark.prevMark = mark;
        newMark.nextMark = mark.nextMark;
        mark.nextMark.prevMark = newMark;
        mark.nextMark = newMark;
        return newMark;
    }
    public void removeMark(Mark<E> mark) {
        //delete from Node circle
        mark.remove();
        //delete from Mark circle
        mark.nextMark.prevMark = mark.prevMark;
        mark.prevMark.nextMark = mark.nextMark;
    }
    /**
     * Moves elements to position after {@link #mark} from {@link #beginMark} until {@link #endMark}
     * @param mark
     * @param beginMark
     * @param endMark
     */
    public void moveAfter(Mark<E> mark, Mark<E> beginMark, Mark<E> endMark) {
        mark.nextNode.prevNode = endMark.prevNode;
        endMark.prevNode.nextNode = mark.nextNode;
        mark.nextNode = beginMark.nextNode;
        beginMark.nextNode.prevNode = mark;
        beginMark.nextNode = endMark;
        endMark.prevNode = beginMark;
    }
    
    public boolean isEmpty(Mark<E> mark) {
        return mark!=null? mark.size == 0 : true;
    }
    @Override
    public boolean isEmpty() {
        return isEmpty(ROOT);
    }
    
    public Object[] toArray(Mark<E> mark) {
        int size = mark != null? mark.size: 0;
        Object[] result = new Object[size];
        if(size == 0)
            return result;
        Iterator<E> iter = iterator(mark);
        for(int i = 0; iter.hasNext() && i < size; i++){
            result[i] = iter.next();
        }
        return result;
    }
    @Override
    public Object[] toArray() {
        return toArray(ROOT);
    }

    @SuppressWarnings("unchecked")
    public <T> T[] toArray(T[] a, Mark<E> mark) {
        int size = mark != null? mark.size: 0;
        T[] result = (T[]) Array.newInstance(a.getClass(), size);
        if(size == 0){
            return result;
        }
        Iterator<E> iter = iterator(mark);
        for(int i = 0; iter.hasNext() && i < size; i++){
            result[i] = (T) iter.next();
        }
        return result;
    }
    @Override
    public <T> T[] toArray(T[] a) {
        return toArray(a, ROOT);
    }

    public boolean containsAll(Collection<?> c, Mark<E> mark) {
        boolean result = true;
        for(Object o : c){
            result &= contains(o, mark);
            if(!result)
                break;
        }
        return result;
    }
    @Override
    public boolean containsAll(Collection<?> c) {
        return containsAll(c, ROOT);
    }

    public boolean addAll(Collection<? extends E> c, Mark<E> mark) {
        boolean modified = false;
        for (E e : c)
            if (add(e, mark))
                modified = true;
        return modified;
    }
    @Override
    public boolean addAll(Collection<? extends E> c) {
        return addAll(c, ROOT);
    }

    @Override
    public boolean removeAll(Collection<?> c) {
        Objects.requireNonNull(c);
        boolean modified = false;
        Iterator<?> it = iterator();
        while (it.hasNext()) {
            if (c.contains(it.next())) {
                it.remove();
                modified = true;
            }
        }
        return modified;
    }

    @Override
    public boolean retainAll(Collection<?> c) {
        Objects.requireNonNull(c);
        boolean modified = false;
        Iterator<E> it = iterator();
        while (it.hasNext()) {
            if (!c.contains(it.next())) {
                it.remove();
                modified = true;
            }
        }
        return modified;
    }

    @Override
    public void clear() {
        Iterator<E> it = iterator();
        while (it.hasNext()) {
            it.next();
            it.remove();
        }
    }

    @Override
    public void addFirst(E e) {
        push(e);
    }

    @Override
    public void addLast(E e) {
        add(e);
    }

    @Override
    public boolean offerFirst(E e) {
        push(e);
        return true;
    }
    
    public boolean offerLast(E e, Mark<E> mark) {
        return add(e, mark);
    }
    @Override
    public boolean offerLast(E e) {
        return add(e);
    }
    
    public E removeFirst(Mark<E> mark) {
        return pop(mark);
    }
    @Override
    public E removeFirst() {
        return pop();
    }
    
    public E removeLast(Mark<E> mark) {
        if(mark.size == 0) {
            throw new NoSuchElementException();
        }
        return pollLast(mark);
    }
    @Override
    public E removeLast() {
        return removeLast(ROOT);
    }
    
    public E pollFirst(Mark<E> mark) {
        if(mark.size == 0) {
            return null;
        }
        return pop(mark);
    }
    @Override
    public E pollFirst() {
        return pollFirst(ROOT);
    }
    
    public E pollLast(Mark<E> mark) {
        if(mark.size == 0){
            return null;
        }
        E obj = mark.prevNode.obj;
        Node<E> t = mark.prevNode;
        mark.prevNode = mark.prevNode.prevNode;
        t.remove();
        if(mark != ROOT){
            ROOT.size--;
        }
        mark.size--;
        return obj;
    }
    @Override
    public E pollLast() {
        return pollLast(ROOT);
    }
    
    public E getFirst(Mark<E> mark) {
        return element(mark);
    }
    @Override
    public E getFirst() {
        return element(ROOT);
    }
    
    public E getLast(Mark<E> mark) {
        if(mark.size == 0){
            throw new NoSuchElementException();
        }
        return peekLast(mark);
    }
    @Override
    public E getLast() {
        return getLast(ROOT);
    }
    
    public E peekFirst(Mark<E> mark) {
        return peek(mark);
    }
    @Override
    public E peekFirst() {
        return peekFirst(ROOT);
    }
    
    public E peekLast(Mark<E> mark) {
        if(mark.size == 0) {
            return null;
        }
        return mark.prevNode.obj;
    }
    @Override
    public E peekLast() {
        return peekLast(ROOT);
    }
    
    public boolean removeFirstOccurrence(Object o, Mark<E> mark) {
        return remove(o, mark);
    }
    @Override
    public boolean removeFirstOccurrence(Object o) {
        return remove(o);
    }
    
    public boolean removeLastOccurrence(Object o, Mark<E> mark) {
        Iterator<E> it = descendingIterator(mark);
        if (o==null) {
            while (it.hasNext()) {
                if (it.next()==null) {
                    it.remove();
                    return true;
                }
            }
        } else {
            while (it.hasNext()) {
                if (o.equals(it.next())) {
                    it.remove();
                    return true;
                }
            }
        }
        return false;
    }
    @Override
    public boolean removeLastOccurrence(Object o) {
        return removeLastOccurrence(o, ROOT);
    }
    
    public boolean add(E e, Mark<E> mark) {
        Node<E> node = new Node<>();
        node.obj = e;
        node.prevNode = mark.prevNode;
        node.nextNode = mark;
        mark.prevNode.nextNode = node;
        mark.prevNode = node;
        if(mark != ROOT){
            ROOT.size++;
        }
        mark.size++;
        return true;
    }
    @Override
    public boolean add(E e) {
        return add(e, ROOT);
    }
    
    public boolean offer(E e, Mark<E> mark) {
        return add(e, mark);
    }
    @Override
    public boolean offer(E e) {
        return offer(e, ROOT);
    }
    
    public E remove(Mark<E> mark) {
        if(mark.size != 0) {
            return pop(mark);
        }else{
            throw new NoSuchElementException();
        }
    }
    @Override
    public E remove() {
        return remove(ROOT);
    }
    
    public E poll(Mark<E> mark) {
        if(mark.size != 0) {
            return pop(mark);
        }else{
            return null;
        }
    }
    @Override
    public E poll() {
        return poll(ROOT);
    }
    
    public E element(Mark<E> mark) {
        if(mark.size == 0){
            throw new NoSuchElementException();
        }
        return peek(mark);
    }
    @Override
    public E element() {
        return element(ROOT);
    }

    public E peek(Mark<E> mark) {
        if(mark.size == 0){
            return null;
        }
        return mark.nextNode.obj;
    }
    @Override
    public E peek() {
        return peek(ROOT);
    }
    
    public void push(E e, Mark<E> mark) {
        Node<E> node = new Node<>();
        node.obj = e;
        node.prevNode = mark;
        node.nextNode = mark.nextNode;
        mark.nextNode.prevNode = node;
        mark.nextNode = node;
        if(mark != ROOT){
            ROOT.size++;
        }
        mark.size++;
    }
    @Override
    public void push(E e) {
        push(e, ROOT);
    }
    
    public E pop(Mark<E> mark) {
        if(mark.size == 0){
            throw new NoSuchElementException();
        }
        E obj = mark.nextNode.obj;
        mark.nextNode.remove();
        if(mark != ROOT){
            ROOT.size--;
        }
        mark.size--;
        return obj;
    }
    @Override
    public E pop() {
        return pop(ROOT);
    }
    
    public boolean remove(Object o, Mark<E> mark) {
        Iterator<E> it = iterator(mark);
        if (o==null) {
            while (it.hasNext()) {
                if (it.next()==null) {
                    it.remove();
                    return true;
                }
            }
        } else {
            while (it.hasNext()) {
                if (o.equals(it.next())) {
                    it.remove();
                    return true;
                }
            }
        }
        return false;
    }
    @Override
    public boolean remove(Object o) {
        return remove(o, ROOT);
    }

    public boolean contains(Object o, Mark<E> mark) {
        Iterator<E> iter = iterator(mark);
        boolean result = false;
        if(o == null){
            while(iter.hasNext()) {
                if(iter.next() == null){
                    result = true;
                    break;
                }
            }
        }else{
            while(iter.hasNext()) {
                if(o.equals(iter.next())){
                    result = true;
                    break;
                }
            }
        }
        return result;
    }
    @Override
    public boolean contains(Object o) {
        return contains(o, ROOT);
    }

    public int size(Mark<E> mark) {
        return mark != null? mark.size : 0;
    }
    @Override
    public int size() {
        return ROOT.size;
    }
    
    public Iterator<E> iterator(Mark<E> mark, Mark<E> stopMark) {
        return new MarkQueueIterator<>(mark, stopMark, true, markCounter);
    }
    public Iterator<E> iterator(Mark<E> mark) {
        return new MarkQueueIterator<>(mark, mark, true, markCounter);
    }
    @Override
    public Iterator<E> iterator() {
        return iterator(ROOT);
    }
    
    public Iterator<E> descendingIterator(Mark<E> mark, Mark<E> stopMark) {
        return new MarkQueueIterator<>(mark, stopMark, false, markCounter);
    }
    public Iterator<E> descendingIterator(Mark<E> mark) {
        return new MarkQueueIterator<>(mark, mark, false, markCounter);
    }
    @Override
    public Iterator<E> descendingIterator() {
        return descendingIterator(ROOT);
    }
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append('[');
        Node<E> n = ROOT;
        do{
            sb.append(n);
            sb.append(", ");
            n = n.nextNode;
        }while(n != ROOT);
        return sb.append(']').toString();
    }
    public static class MarkQueueIterator<E> implements Iterator<E> {
        final Mark<E> mark;
        final Mark<E> stopMark;
        final boolean clockwise;
        final AtomicInteger markCounter;
        Node<E> next;
        MarkQueueIterator(Mark<E> mark, Mark<E> stopMark, boolean clockwise, AtomicInteger markCounter){
            this.mark = mark;
            this.stopMark = stopMark;
            this.clockwise = clockwise;
            this.markCounter = markCounter;
        }
        @Override
        public boolean hasNext() {
            if(mark == null || mark.size == 0)
                return false;
            return next == null || (clockwise? next.nextNode : next.prevNode) != stopMark;
        }
        @Override
        public E next() {
            if(!hasNext()){
                throw new NoSuchElementException();
            }
            if(next == null){
                next = clockwise? mark.nextNode : mark.prevNode;
            }else{
                next = clockwise? next.nextNode : next.prevNode;
            }
            while(next instanceof Mark && next != stopMark){
                next = clockwise? next.nextNode : next.prevNode;
            }
            if(next == stopMark){
                throw new NoSuchElementException();
            }
            return next.obj;
        }
        @Override
        public void remove() {
            if(next == null){
                throw new NoSuchElementException("Need to call next before");
            }
            while(next instanceof Mark && next != stopMark){
                next = clockwise? next.nextNode : next.prevNode;
            }
            if(next == stopMark){
                throw new NoSuchElementException();
            }
            Node<E> t = next;
            t.remove();
        }
        public Mark<E> mark() {
            if(next == null){
                throw new NoSuchElementException("Need to call next before");
            }
            Mark<E> newMark = new Mark<>();
            newMark.number = markCounter.incrementAndGet();
            //Put in node circle
            newMark.nextNode = next;
            newMark.prevNode = next.prevNode;
            next.prevNode.nextNode = newMark;
            next.prevNode = newMark;
            //Search nearest left mark
            Node<E> tempNext = newMark.prevNode;
            while(!(tempNext instanceof Mark)){
                tempNext = tempNext.prevNode;
            }
            //Put in mark circle
            newMark.prevMark = (Mark<E>) tempNext;
            newMark.nextMark = ((Mark<E>) tempNext).nextMark;
            ((Mark<E>) tempNext).nextMark.prevMark = newMark;
            ((Mark<E>) tempNext).nextMark = newMark;
            return newMark;
        }
        
    }
    

}
